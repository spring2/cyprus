//css_pre soapsuds(http://localhost:8086//MyRemotingApp/CompanyLists?WSDL, CompanyLists, -new); 
//css_ref CompanyLists.dll;
using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

class Script
{
	static public void Main(string[] args)
	{
		CompanyLists cLst = (CompanyLists)Activator.GetObject(typeof(CompanyLists), 
															  "http://localhost:8086/CompanyLists",
															  WellKnownObjectMode.Singleton);
		cLst.addCountryList("Australia");
	}
}
