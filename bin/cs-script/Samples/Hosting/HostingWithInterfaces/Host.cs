using System;
using System.IO;
using System.Reflection;
using CSScriptLibrary;

public interface IHost
{
	string Name { get; }
	void CreateDocument();
	void CloseDocument();
	void OpenDocument(string file);
	void SaveDocument(string file);
}

public interface IScript
{
	IHost Parent { set; }
	void Execute();
}

class Host : IHost
{
	string name = "Host";
	void IHost.CreateDocument() { Console.WriteLine("Host: creating document..."); }
	void IHost.CloseDocument() { Console.WriteLine("Host: closing document..."); }
	void IHost.OpenDocument(string file) { Console.WriteLine("Host: opening documant (" + file + ")..."); }
	void IHost.SaveDocument(string file) { Console.WriteLine("Host: saving documant (" + file + ")..."); }
	string IHost.Name { get { return name; } }

	static void Main()
	{
		//The following line is required only for executing C#3.0 (.NET 3.5) scripts.
        CSScript.GlobalSettings.UseAlternativeCompiler = "CSSCodeProvider.v3.5.dll";
		
        Host host = new Host();
		host.Start();
	}
        
	void Start()
	{
		IScript script = Load("script.cs");
		script.Parent = this;
		script.Execute();
	}
	IScript Load(string script)
	{
        CSScript.ShareHostRefAssemblies = true;

        AsmHelper helper = new AsmHelper(CSScript.Load(Path.GetFullPath(script), null, true));
		return (IScript)helper.CreateObject("Script");
	}
}
