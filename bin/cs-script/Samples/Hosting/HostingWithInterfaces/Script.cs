using System;
using System.Windows.Forms;

public class Script : IScript
{
    IHost parent;
    IHost IScript.Parent { set { parent = value; } }
    void IScript.Execute()
    {
		Console.WriteLine("Script: working with "+parent.Name);
        parent.CreateDocument();
        parent.CloseDocument();
        parent.OpenDocument("document.txt");
        parent.SaveDocument("document1.txt");
    }
}

