The Host.cs script is an example of hosting the script which requires an external assembly at run-time. The assembly which is not part of the host application assembly set.

It represents the same code base as the Host.cs from cs-script\Samples\Hosting\HostingSimplified folder except it utilises C# 3.0 (.NET 3.5) version of the CSScriptLibrary assembly (CSScriptLibrary.v3.5.dll).

CSScriptLibrary assembly is typically compiled with C# 1.1 compiler and named CSScriptLibrary.dll. because of this CSScriptLibrary can be loaded in any CLR transparently. The only limitation is the if you want to target C# 3.0 compiler you need to use CSScriptLibrary in conjunction with CSSCodeProvider.v3.5.dll. This technique is demonstrated in the cs-script\Samples\Hosting\HostingSimplified\Host.cs sample. 

However in some cases you may prefer to use single file CSScriptLibrary compiled just with the compiler used for your host application. In such cases you may want to recompile CSScriptLibrary with the compiler of your choice. CSScriptLibrary.v3.5.dll is the CSScriptLibrary assembly compiled with C# 3.0 (.NET 3.5) and Host.cs in the current folder is the example of how to use this compiler specific version of CSScriptLibrary.

Note: both compiler neutral (CSScriptLibrary.dll) and compiler specific (CSScriptLibrary.v3.5.dll) versions of the CSScriptLibrary assembly can be found in cs-script\Lib directory.

Host directory contains the same code sample packed to the Visual Studio 2008 project.