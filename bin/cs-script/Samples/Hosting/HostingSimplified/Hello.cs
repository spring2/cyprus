using System;
using System.Reflection;
using CSScriptLibrary;

public class Host
{
    static void Main()
    {
		Assembly assembly = CSScript.LoadCode(
			@"using System;
			  public class Script
			  {
				  public static void SayHello(string gritting)
				  {
					  Console.WriteLine(gritting);
				  }
			  }");

		AsmHelper script = new AsmHelper(assembly);
		script.Invoke("Script.SayHello", "Hello World!");
    }
}

