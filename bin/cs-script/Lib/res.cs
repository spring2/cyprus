using System;
using System.IO;
using System.Text;
using Microsoft.Win32;
using System.Reflection;
using System.Windows.Forms;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Diagnostics;
using CSScriptLibrary;
using csscript;

class Scripting
{
	class Script
	{
		[STAThread]
		static public void Main(string[] args)
		{
			const string usage = "Usage: cscscript res <fileIn> <fileOut>...\n" +
								"Generates .resources file.\n" +
								"fileIn - input file (etc .resx). Specify 'dummy' to prepare enpty .resources file (etc. css res dummy Scripting.Form1.resources).\n" +
								"fileOut - output file name (etc .resx).\n";

			try
			{
				Debug.Assert(false);
				if (args.Length < 2)
					Console.WriteLine(usage);
				else
				{
					if (Environment.GetEnvironmentVariable(@"CSSCRIPT_DIR") == null)
						return; //CS-Script is not installed

					//currently no .resx compilation with ResGen.exe is implemented, only dummy ressource copying
					if (string.Compare(args[0] , "dummy") == 0) 
					{
						string srcFile = Environment.ExpandEnvironmentVariables(@"%CSSCRIPT_DIR%\lib\resources.template");
						string destFile = ResolveAsmLocation(args[1]);
						
						if (!File.Exists(destFile))
							File.Copy(srcFile, destFile, true);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				throw ex;
			}
		}
		static string ResolveAsmLocation(string file)
		{
            if (CSScript.GlobalSettings.HideAutoGeneratedFiles == Settings.HideOptions.HideAll)
				return Path.Combine(CSSEnvironment.GetCacheDirectory(CSSEnvironment.PrimaryScriptFile), Path.GetFileName(file));
			else
				return Path.Combine(Path.GetDirectoryName(CSSEnvironment.PrimaryScriptFile), file);
		}
	}
}