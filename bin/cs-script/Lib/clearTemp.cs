using System;
using System.Threading;
using System.Collections;
using System.IO;

class Script
{
	const string usage = "Usage: cscscript clearTemp\n" +
						 "Deletes all temporary files created by the script engine.\n";


	static public void Main(string[] args)
	{
		if (args.Length == 1 && (args[0] == "?" || args[0] == "/?" || args[0] == "-?" || args[0].ToLower() == "help"))
		{
			Console.WriteLine(usage);
		}
		else
		{
			//delete temporary Visusl Studio projects
			foreach (string projectDir in Directory.GetDirectories(Path.Combine(Path.GetTempPath(), "CSSCRIPT")))
			{
				bool projectOpened = false;

				foreach (string projFile in Directory.GetFiles(projectDir, "*.csproj")) //it will alway be only one project file
				{
					projectOpened = true;

					try
					{
						string projName = Path.GetFileNameWithoutExtension(projFile);
						if (File.Exists(Path.Combine(projectDir, @"bin\Debug\" + projName + ".vshost.exe"))) //VS2005
						{
							File.Delete(Path.Combine(projectDir, @"bin\Debug\" + projName + ".vshost.exe"));
							projectOpened = false;
						}
						else if (File.Exists(Path.Combine(projectDir, @"obj\Debug\" + projName + ".projdata")))//VS2003
						{
							File.Delete(Path.Combine(projectDir, @"obj\Debug\" + projName + ".projdata"));
							projectOpened = false;
						}
						else if (!Directory.Exists(Path.Combine(projectDir, @"bin")))
							projectOpened = false; //project has never been opened
						else
							continue; //unknown; do not delete
					}
					catch { }
				}
				if (!projectOpened)
				{
					DeleteDir(projectDir);
				}
			}

			//delete all DLLs; any dll that is currently in use will not be deleted (try...catch); 
			ArrayList assemblies = new ArrayList();
			assemblies.AddRange(Directory.GetFiles(Path.Combine(Path.GetTempPath(), "CSSCRIPT"), "*.dll"));

			Thread.Sleep(5000); //allow just created DLLs to be loaded

			foreach (string file in assemblies)
			{
				try
				{
					File.Delete(file);
				}
				catch { }
			}
		}
	}

	static private void DeleteDir(string dir)
	{
		//deletes folder recursively
		try
		{
			foreach (string file in Directory.GetFiles(dir))
				try
				{
					File.Delete(file);
				}
				catch
				{
				}

			foreach (string subDir in Directory.GetDirectories(dir))
				DeleteDir(subDir);

			Directory.Delete(dir);
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message);
		}
	}
}