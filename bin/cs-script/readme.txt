C# Script execution engine;
Copyright (C) 2004-2007 Oleg Shilo.

-----------------------------------------------------------------------------------------
Licence:
Written by Oleg Shilo (oshilo@gmail.com)

 Copyright (c) 2004-2007 Oleg Shilo

 All rights reserved. 

 (The author reserve rights to change the licence for future releases.)

Redistribution and use of this software in source and binary forms, without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 

2. Neither the name of an author nor the names of the contributors may be used to endorse or promote products bundled from this software without specific prior written permission.

Redistribution and use of this software in source and binary forms, with modification, are permitted provided that all above conditions are met and software is not used or sold for profit. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR USINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR SORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 


-----------------------------------------------------------------------------------------
Contact: 
 csscript.support@gmail.com, galos.co@gmail.com 
-----------------------------------------------------------------------------------------
Installation:

 Precondition: .NET runtime must be installed. You can download it from here or from other well known locations:
	http://www.microsoft.com/downloads/details.aspx?FamilyID=0856eacb-4362-4b0d-8edd-aab15c5e04f5&displaylang=en

 To install:
   extract content of the cs-script.zip on your HD
   run css_config.exe (it will bring the configuration console)
   adjust the CS-Script settings in the configuration console according your needs (eg. enabled debuggers, shell extensions...)
   
 To uininstall:
   run css_config.exe
   press 'Deactivate' button on the 'General' tab in the configuration console

 To upgrade:
   No special steps are required. Just do as for normal installation according instructions above.
   
NOTE: 
   - After running css_config.exe from some third-party file navigation utilities (eg. Total Commander)
   it might be required to restart this utility in order for changes to take effect.
   
   - css_config.exe is the only component, which is build against .NET 2.0 thus it will not work on PC that has only 
   lower version of CLR. In such cases you would need to create and use css_config.bat file instead. This file should 
   contain the following command "start csws /c lib\config.cs".

-----------------------------------------------------------------------------------------
Running:
 Script engine can be run in two different modes:
 as a console application (cscscript.exe) and as a WinExe application (cswscript.exe).
 
C# Script execution engine. Version 1.8.0.0.
Copyright (C) 2004-2007 Oleg Shilo.

Usage: cscs.exe <switch 1> <switch 2> <file> [params] [//x]

<switch 1>
 /?    - Display help info.
 /e    - Compile script into console application executable.
 /ew   - Compile script into Windows application executable.
 /c    - Use compiled file (cache file .csc) if found (to improve performance).
 /ca   - Compile script file into cache file (.csc) without execution.
 /cd   - Compile script file into assembly (.dll) without execution.

 /co:<options>
       - Pass compiler options directy to the language compiler
       (eg. /co:/d:TRACE pass /d:TRACE option to C# compiler).

 /s    - Print content of sample script file (eg. cscs.exe /s > sample.cs).

<switch 2>
 /nl   - No logo mode: No banner will be shown at execution time.
 /dbg | /d
       - Force compiler to include debug information.
 /l    - 'local'(makes the script directory a 'current directory')
 /noconfig[:<file>]
       - Do not use default config file or use alternative one.
         (eg. cscs.exe /noconfig sample.cs
              cscs.exe /noconfig:c:\cs-script\css_VB.dat sample.vb)
 /sconfig
       - Use script config file (eg. script.cs.config) as an application configuration file for the script engine process.
       This option might be usefull fo running scripts, which usually cannot be executed without configuration file (eg. WCF, remoting).

 /r:<assembly 1>:<assembly N>
       - Use explicitly referenced assembly. It is required only for
         rare cases when namespace cannot be resolved into assembly.
         (eg. cscs.exe /r:myLib.dll myScript.cs).

file   - Specifies name of a script file to be run.
params - Specifies optional parameters for a script file to be run.
 //x   - Launch debugger just before starting the script.


**************************************
Script specific syntax
**************************************

Engine directives:
------------------------------------
//css_import <file>[, preserve_main][, rename_namespace(<oldName>, <newName>)];

Aliase - //css_imp
There are also another two aliases //css_include and //css_inc. They are equivalents of //css_import <file>, preserve_main

file            - name of a script file to be imported at compile-time.
<preserve_main> - do not rename 'static Main'
oldName         - name of a namespace to be renamed during importing
newName         - new name of a namespace to be renamed during importing

This directive is used to inject one script into another at compile time. Thus code from one script can be exercised in another one.
'Rename' clause can appear in the directive multiple times.
------------------------------------
//css_args arg0[,arg1]..[,argN];

Embedded script arguments. The both script and engine arguments are allowed except "/noconfig" engine command switch.
 Example: //css_args /dbg;
 This directive will always force script engine to execute the script in debug mode.
------------------------------------
//css_reference <file>;

Aliase - //css_ref

file    - name of the assembly file to be loaded at run-time.

This directive is used to reference assemblies required at run time.
The assembly must be in GAC, the same folder with the script file or in the 'Script Library' folders (see 'CS-Script settings').
------------------------------------
//css_resource <file>;

Aliase - //css_res

file    - name of the resource file to be used with the script.

This directive is used to reference resourrce file for script.
 Example: //css_res Scripting.Form1.resources;
------------------------------------
//css_prescript file([arg0][,arg1]..[,argN])[ignore];
//css_postscript file([arg0][,arg1]..[,argN])[ignore];

Aliases - //css_pre and //css_post

file    - script file (extension is optional)
arg0..N - script string arguments
ignore  - continue execution of the main script in case of error

These directives are used to execute secondary pre- and post-action scripts.
If $this is specified as arg0..N it will be replaced at execution time with the
main script full name.
------------------------------------

Any directive has to be written as a single line in order to have no impact on compiling by CLI compliant compiler.
It also must be placed before any namespace or class declaration.

------------------------------------
Example:

 using System;
 //css_prescript com(WScript.Shell, swshell.dll);
 //css_import tick, rename_namespace(CSScript, TickScript);
 //css_reference teechart.lite.dll;

 namespace CSScript
 {
   class TickImporter
   {
      static public void Main(string[] args)
      {
         TickScript.Ticker.i_Main(args);
      }
   }
 }
