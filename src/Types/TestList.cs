using Spring2.DataTierGenerator.Attribute;
using System;


namespace Spring2.Types {
    /// <summary>
    /// Test generic collection
    /// </summary>
    public class TestList : System.Collections.CollectionBase {
	[Generate]
	public static readonly TestList UNSET = new TestList(true);
	[Generate]
	public static readonly TestList DEFAULT = new TestList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private TestList (Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public TestList() {}

	// Indexer implementation.
	[Generate]
	public Test this[int index] {
	    get { return (Test)List[index]; }
	    set {
		if (!immutable) {
		    List[index] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(Test value) {
	    if (!immutable) {
		List.Add(value);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public Boolean Contains(Test value) {
	    return List.Contains(value);
	}

	[Generate]
	public Int32 IndexOf(Test value) {
	    return List.IndexOf(value);
	}

	[Generate]
	public void Insert(Int32 index, Test value) {
	    if (!immutable) {
		List.Insert(index, value);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(Test value) {
	    if (!immutable) {
		List.Remove(value);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(System.Collections.IList list) {
	    foreach (Object o in list) {
		if (o is Test) {
		    Add((Test)o);
		} else {
		    throw new System.InvalidCastException("object in list could not be cast to Test");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get { return Object.ReferenceEquals(this, DEFAULT); }
	}

	[Generate]
	public Boolean IsUnset {
	    get { return Object.ReferenceEquals(this, UNSET); }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}
    }
}
