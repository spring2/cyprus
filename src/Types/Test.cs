using Spring2.Core.Types;
using System.Collections;
using System;

namespace Spring2.Types {
    public class Test : Spring2.Core.Types.EnumDataType {
	private static readonly EnumDataTypeList OPTIONS = new EnumDataTypeList();

	public static readonly new Test DEFAULT = new Test();
	public static readonly new Test UNSET = new Test();

	public static readonly Test TEST = new Test("Test", "Test");

	public static Test GetInstance(Object value) {
	    if (value is String) {
		foreach (Test t in OPTIONS) {
		    if (t.Value.Equals(value)) {
			return t;
		    }
		}
	    }

	    return UNSET;
	}

	private Test() {}

	private Test(String code, String name) {
	    this.code = code;
	    this.name = name;
	    OPTIONS.Add(this);
	}

	public override Boolean IsDefault {
	    get { return Object.ReferenceEquals(this, DEFAULT); }
	}

	public override Boolean IsUnset {
	    get { return Object.ReferenceEquals(this, UNSET); }
	}

	public static EnumDataTypeList Options {
	    get { return OPTIONS; }
	}
    }
}
