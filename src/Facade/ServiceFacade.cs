﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;
using Spring2.Exceptions;
using Spring2.Util;

using Spring2.Core;
using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.Core.Message;
using Spring2.Core.Configuration;

using log4net;

namespace Spring2.Facade {
    public class ServiceFacade {
	static SimpleFormatter formatter = new SimpleFormatter();
	private static readonly ILog log = LogManager.GetLogger("ServiceFacade");

	Random r = new Random();
	public ServiceFacade() {
	    // use MDC to set the hostname for log messages -- MDC is thread specific
	    MDC.Set("hostname", Environment.MachineName);
	}

	#region Load Row Values
	public void LoadAccountValues(LoanPortfolioData data, DataRow accountRow) {
	    data.AccountID = accountRow["ACCT_NUM"].ToString();
	    data.Member = accountRow["MEM_NUM"].ToString();
	    data.Ltype = GetIntegerValueFromDataRow(accountRow, "L_ACCT_LEVEL");
	    data.LtypeSub = GetDecimalValueFromDataRow(accountRow, "ACCT_SUBLEV");
	    data.LoanDate = GetDateTypeFromDataRow(accountRow, "ACCOUNT_OPEN_DATE");
	    data.RptCode = GetIntegerValueFromDataRow(accountRow, "SPEC_REPORT_CODE");
	    data.OrigPmt = GetCurrencyValueFromDataRow(accountRow, "ORIG_PAY");
	    data.RelPriceGrp = accountRow["PG_CODE"].ToString();
	    data.RiskOffset = GetDecimalValueFromDataRow(accountRow, "RISK_OFFSET_VAL");
	    data.FirstPmtDate = GetDateTypeFromDataRow(accountRow, "FIRST_PAYDATE");
	    data.OrigTerm = GetIntegerValueFromDataRow(accountRow, "ORIG_LOAN_TERM");
	    data.MatDate = GetDateTypeFromDataRow(accountRow, "L_MATURITY_DATE");
	    data.InsCode = GetIntegerValueFromDataRow(accountRow, "L_INSCODE");
	    data.SecType = GetIntegerValueFromDataRow(accountRow, "L_SECCODE");
	    data.SecDesc1 = accountRow["L_SECDESC1"].ToString();
	    data.SecDesc2 = accountRow["L_SECDESC2"].ToString();
	    data.DeliDays = GetIntegerValueFromDataRow(accountRow, "ARREAR_DAYS");
	    data.ApprOff = GetIntegerValueFromDataRow(accountRow, "APPR_OFFICER");
	    data.OrigAmount = GetCurrencyValueFromDataRow(accountRow, "CONS_LOANAMT");
	    data.CurCrLim = GetCurrencyValueFromDataRow(accountRow, "CURR_CREDIT_LIM");

	    data.LoanDate = GetDateTypeFromDataRow(accountRow, "US_ACCT_CLOSED");
	    data.CurCrLim = GetCurrencyValueFromDataRow(accountRow, "FASB_DEF_COST");
	    data.CurCrLim = GetCurrencyValueFromDataRow(accountRow, "LTD_AMORT_FEES");
	    data.DeliDays = GetIntegerValueFromDataRow(accountRow, "L_INSCODE");
	}

	public void LoadApplicValues(LoanPortfolioData data, DataSet ds, DataRow accountRow) {
	    DataRow applicRow = null;
	    try {
		applicRow = ds.Tables["Applic"].Rows.Find(accountRow["US_APPLIC_ID"]);
	    } catch {
		log.Error("Error Finding Applic Row: US_APPLIC_ID = " + accountRow["US_APPLIC_ID"].ToString());
	    }
	    if (applicRow != null) {
		data.FundedBr = GetIntegerValueFromDataRow(applicRow, "FINAL_STAMP_BR");
		data.LoadedBr = GetIntegerValueFromDataRow(applicRow, "LOAD_STAMP_BR");
		data.FICO = GetIntegerValueFromDataRow(applicRow, "AP_RISK_SCORE");
		data.FastStart = GetIntegerValueFromDataRow(applicRow, "STAGE2_CRSCORE");
		data.Bankruptcy = GetIntegerValueFromDataRow(applicRow, "BANKRUPT_SCORE");
		data.RiskLev = applicRow["AP_RISK_LEVEL"].ToString();
		data.Traffic = GetIntegerValueFromDataRow(applicRow, "ALPS_COLOR");
		data.PmtFreq = applicRow["PAYMENT_FREQ"].ToString();
		data.BalloonDate = GetDateTypeFromDataRow(applicRow, "BALLOON_DATE");
		data.Dealer = GetIntegerValueFromDataRow(applicRow, "DEALER_NUMBER");
		data.RelPriceDisc = GetIntegerValueFromDataRow(applicRow, "AP_RP_DISCOUNT");
		data.CreditType = applicRow["CRSCORE_TYPE"].ToString();
		data.PropType = applicRow["HELOC_TYPE"].ToString();
		data.Occupancy = applicRow["HELOC_OCCUPANCY"].ToString();
		data.OrigApprVal = GetCurrencyValueFromDataRow(applicRow, "HELOC_VALUE");
		data.OrigApprDate = GetDateTypeFromDataRow(applicRow, "HELOC_ORIG_DATE");
		data.FirstMortBal = GetCurrencyValueFromDataRow(applicRow, "HELOC_1STMORT_BAL");
		data.FirstMortMoPyt = GetCurrencyValueFromDataRow(applicRow, "HELOC_1STMORT_PAY");
		data.SecondMortBal = GetCurrencyValueFromDataRow(applicRow, "HELOC_2NDMORT_BAL");
		data.SecondMortPymt = GetCurrencyValueFromDataRow(applicRow, "HELOC_2NDMORT_PAY");
		data.InsCode = GetIntegerValueFromDataRow(applicRow, "INSURE_CODE");
		data.OrigRate = GetDecimalValueFromDataRow(applicRow, "AP_INTEREST_RATE");
		data.LoadOff = GetIntegerValueFromDataRow(applicRow, "LOAD_STAMP_OFF");
		data.FundingOpp = GetIntegerValueFromDataRow(applicRow, "FINAL_STAMP_OPER");
		data.CurTerm = GetIntegerValueFromDataRow(applicRow, "NUM_PAYMENTS");
		data.Product = GetIntegerValueFromDataRow(applicRow, "AP_PURPOSE_CODE");
		data.TaxInsNotInc = GetCurrencyValueFromDataRow(applicRow, "HELOC_MTH_TAX");
		data.DiscReason = applicRow["DISCOUNT_REASON"].ToString();
		data.LoadOperator = GetIntegerValueFromDataRow(applicRow, "LOAD_STAMP_OPER");

		//  Loan Applic Calc Info
		DataRelation dr = applicRow.Table.ChildRelations["ApplicApplicCalcInfo"];
		DataRow[] childRows3 = applicRow.GetChildRows(dr);
		if (childRows3.Length > 0) {
		    DataRow childRow2 = childRows3[0];
		    data.DebtRatio = GetDecimalValueFromDataRow(childRow2, "DEBT_AFTER");
		}
	    }
	}

	public void LoadCoborrowValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountCoborrow"];
	    DataRow[] coborrowRows = accountRow.GetChildRows(dataRelation);
	    if (coborrowRows.Length > 0) {
		DataRow childRow = coborrowRows[0];
		data.CoborComak = !String.IsNullOrEmpty(childRow["COBORROW_MEMBERS"].ToString()) ? "Y" : "N";
	    }
	}

	public void LoadCollatValueValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountCollatValue"];
	    DataRow[] collatValueRows = accountRow.GetChildRows(dataRelation);
	    if (collatValueRows.Length > 0) {
		DataRow childRow = collatValueRows[0];
		data.CollateralValue = GetCurrencyValueFromDataRow(childRow, "US_COLLATERAL_VALUE");
	    }
	}

	public void LoadAccountUsValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountAccountUs"];
	    DataRow[] accountUsRows = accountRow.GetChildRows(dataRelation);
	    if (accountUsRows.Length > 0) {
		DataRow childRow = accountUsRows[0];
		data.Status = childRow["US_LOANSTATUS"].ToString();
	    }
	}

	public void LoadDelinquencyDataValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountDelinquencyData"];
	    DataRow[] delinquencyDataRows = accountRow.GetChildRows(dataRelation);
	    if (delinquencyDataRows.Length > 0) {
		DataRow childRow = delinquencyDataRows[0];
		data.DeliSect = GetIntegerValueFromDataRow(childRow, "SECTION_NUM");
		data.DeliHist = childRow["DLQ_HIST"].ToString();
		data.DeliAmt = GetCurrencyValueFromDataRow(childRow, "PAST_DUE_AHEAD_AMT");
	    }
	}

	public void LoadAccountUdUsValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountAccountUdUs"];
	    DataRow[] accountUdUsRows = accountRow.GetChildRows(dataRelation);
	    if (accountUdUsRows.Length > 0) {
		DataRow childRow = accountUdUsRows[0];
		data.StatedIncome = childRow["US_INCOME_STATED"].ToString();
		data.Extensions = childRow["US_NUM_OF_EXTENSIONS"].ToString();
		data.FirstExtDate = childRow["US_1ST_EXT_DTE"].ToString();
		data.LastExtDate = childRow["US_MOSTRECENT_EXT_DTE"].ToString();
		data.RiskLevel = childRow["US_L_RISKLEV"].ToString();
		data.RiskDate = childRow["US_RISKLEVEL_DTE"].ToString();
		data.Watch = childRow["US_WATCHLIST"].ToString();
		data.WatchDate = childRow["US_DATE_WATCHLIST"].ToString();
		data.WorkOut = childRow["US_WORKOUT_LOANS"].ToString();
		data.WorkOutDate = childRow["US_WORKOUT_DTE"].ToString();
		data.BusPerReg = childRow["US_UD_BUS_LOAN_PER_REG"].ToString();
		data.BusGroup = childRow["US_UD_BUS_LOAN_COL_GRP"].ToString();
		data.BusDesc = childRow["US_UD_BUS_LOAN_COL_DESC"].ToString();
		data.BusPart = childRow["US_UD_BUS_LOAN_PART"].ToString();
		data.SBAguar = childRow["US_UD_SBA_GUARANTY"].ToString();
		data.FirstMortType = childRow["US_UD_1ST_MORT_TYPE"].ToString();
		data.BusPartPerc = childRow["US_UD_BUS_LOAN_PERCNT_PART"].ToString();
		data.Gap = childRow["US_GAP"].ToString();

		data.UsSecDesc = childRow["US_SECDESC"].ToString();
		data.TroubledDebt = childRow["US_UD_TROUBLED_DEBT"].ToString();
		data.TdrDate = childRow["US_UD_TDR_DATE"].ToString();

		data.JntFicoUsed = childRow["US_COBORROWER_FICO_USED"].ToString();
		data.ApprovingOfficerUD = childRow["US_APPR_OFFICER"].ToString();
		data.InsuranceCodeDesc = childRow["US_INS_CODE_DESC"].ToString();
		data.GapIns = childRow["US_GAP_INS"].ToString();
		data.MmpIns = childRow["US_MMP_INS"].ToString();
		data.MbrAgeAtApproval = IntegerType.Parse(childRow["US_MBR_YAGE_LOAN_APPR"].ToString());
		data.SingleOrJointLoan = childRow["US_JNT_ACCT"].ToString();
		data.ReportCodeDesc = childRow["US_RPT_CODE_DESC"].ToString();
		data.FixedOrVariableRateLoan = childRow["US_RATE_FIX_VAR"].ToString();
		data.CUDLAppNumber = childRow["US_CUDL_APP_NUM"].ToString();
		data.LTV = DecimalType.Parse(childRow["US_LTV"].ToString());
	    }
	}

	public void LoadTranDataValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountTranData"];
	    DataRow[] tranDataRows = accountRow.GetChildRows(dataRelation);
	    if (tranDataRows.Length > 0) {
		DataRow childRow = tranDataRows[0];
		data.CurBal = GetCurrencyValueFromDataRow(childRow, "CURRENT_BALANCE").ToDecimal() * -1;
		data.CurRate = GetDecimalValueFromDataRow(childRow, "INT_RATE");
		data.InterestPriorYtd = GetDecimalValueFromDataRow(childRow, "LAST_YEAR_INT");
		data.LastTranDate = GetDateTypeFromDataRow(childRow, "LAST_TRANSACT_DATE");
	    }
	}

	public void LoadTranLoanValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountTranLoan"];
	    DataRow[] tranLoanRows = accountRow.GetChildRows(dataRelation);
	    if (tranLoanRows.Length > 0) {
		DataRow childRow = tranLoanRows[0];
		data.OrigCrLim = GetCurrencyValueFromDataRow(childRow, "L_PREVIOUS_CREDIT_LIMIT");
		data.CurPmt = GetCurrencyValueFromDataRow(childRow, "L_REPAY_AMT");

		data.AccruedInterest = GetCurrencyValueFromDataRow(childRow, "L_ACCR_INT");
		data.InterestLTD = GetCurrencyValueFromDataRow(childRow, "L_TOT_INT");
		data.InterestYtd = GetCurrencyValueFromDataRow(childRow, "L_INT_YTD");
		data.PartialPayAmt = GetCurrencyValueFromDataRow(childRow, "L_PARTIAL_PAY_ACCUM");
		data.InterestUncollected = GetCurrencyValueFromDataRow(childRow, "L_UNCOLL_INT_AMT");
		data.NextDueDate = GetDateTypeFromDataRow(childRow, "L_NEXT_DUE_DATE");
	    }
	}

	public void LoadAccountChgOffValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountChgOff"];
	    DataRow[] chgOffRows = accountRow.GetChildRows(dataRelation);
	    if (chgOffRows.Length > 0) {
		DataRow childRow = chgOffRows[0];
		data.ChrgOffDate = GetDateTypeFromDataRow(childRow, "CHARGEOFF_APPR");
		data.ChgOffAmt = GetCurrencyValueFromDataRow(childRow, "CHARGEOFF_AMT");
	    }
	}

	public void LoadProfileValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountProfile"];
	    DataRow[] profileRows = accountRow.GetChildRows(dataRelation);
	    if (profileRows.Length > 0) {
		DataRow childRow = profileRows[0];
		//data.Housing = childRow["CURR_LIVING_STATUS"].ToString();
		//data.YrsResidence = GetIntegerValueFromDataRow(childRow, "NO_YRS_ADDR");
		//data.YrsPrevResidence = GetIntegerValueFromDataRow(childRow, "PREV_YRS_ADDR");
	    }
	}

	public void LoadProfileEmployerInfoValues(LoanPortfolioData data, DataRow accountRow) {
	    DataRelation dataRelation = accountRow.Table.ChildRelations["AccountProfileEmployerInfo"];
	    DataRow[] ProfileEmployerInfoRows = accountRow.GetChildRows(dataRelation);
	    if (ProfileEmployerInfoRows.Length > 0) {
		DataRow childRow = ProfileEmployerInfoRows[0];
		//data.GrossInc = GetCurrencyValueFromDataRow(childRow, "EMP_GROSS_INCOME");
		//data.YrsProf = GetIntegerValueFromDataRow(childRow, "EMP_YRS_PROF");
	    }
	}

	public void LoadDealerDataValues(LoanPortfolioData data, DataSet ds, DataRow accountRow) {
	    DataRow dealerRow = null;
	    try {
		dealerRow = ds.Tables["DealerData"].Rows.Find(accountRow["DEALER"]);
	    } catch {
		log.Error("Error Finding Dealder Row: DEALER = " + accountRow["DEALER"].ToString());
	    }
	    if (dealerRow != null) {
		data.DealerName = dealerRow["DEALER_NAME"].ToString();
	    }
	}
	#endregion

	public MessageList ImportLoanPortfolios(DataSet ds, DateType eomDate) {
	    MessageList errors = new MessageList();

	    DateTimeType processDate = DateTimeType.Now;
	    if(!eomDate.IsValid)
		eomDate = DateType.Now.FirstOfMonth.AddDays(-1);

	    //	Get lookup lists for derived fields
	    LoanTypeList loanTypes = LoanTypeDAO.DAO.GetList();
	    FICORangeList ficoRanges = FICORangeDAO.DAO.GetList();
	    FastStartRangeList fastStartRanges = FastStartRangeDAO.DAO.GetList();
	    BankruptcyRangeList bankruptcyRanges = BankruptcyRangeDAO.DAO.GetList();

	    RiskTypeList riskTypes = RiskTypeDAO.DAO.GetList();
	    ChanelList chanels = ChanelDAO.DAO.GetList();

	    DataTable clientTable = ds.Tables["Client"];
	    log.Info("Processing Loan information for " + clientTable.Rows.Count + " clients");
	    //	For each client record
	    foreach (DataRow clientRow in clientTable.Rows) {
		try {
		    ImportLoanPortfolio(clientRow, ds, processDate, eomDate, loanTypes, ficoRanges, fastStartRanges, bankruptcyRanges, riskTypes, chanels);
		} catch (Exception ex) {
		    errors.Add(new LoanPortfolioImportError(ex.Message, clientRow["MEM_NUM"].ToString(), String.Empty));
		}
	    }

	    return errors;
	}

	public void ImportLoanPortfolio(DataRow clientRow, DataSet ds, DateTimeType processDate, DateType eomDate, LoanTypeList loanTypes, FICORangeList ficoRanges, FastStartRangeList fastStartRanges, BankruptcyRangeList bankruptcyRanges, RiskTypeList riskTypes, ChanelList chanels) {
	    //	Set Client Data
	    IdType mNum = GetIdValueFromDataRow(clientRow, "MEM_NUM");
	    IntegerType mClass = GetIntegerValueFromDataRow(clientRow, "CLIENT_CLASS");
	    DateType mDate = GetDateTypeFromDataRow(clientRow, "JOIN_DATE");
	    StringType sex = clientRow["SEX"].ToString();
	    DateTimeType birthDate = GetDateTimeTypeFromDataRow(clientRow, "BIRTH_DATE");
	    IntegerType age = IntegerType.UNSET;
	    if (birthDate.IsValid)
		age = DateType.Now.Year - birthDate.Year;

	    DataRelation dataRelation = clientRow.Table.ChildRelations["ClientAccount"];
	    DataRow[] accountRows = clientRow.GetChildRows(dataRelation);
	    foreach (DataRow accountRow in accountRows) {
		LoanPortfolioData data = new LoanPortfolioData();
		data.UpDated = processDate;

		//  Set Client Information used for each account
		data.MClass = mClass;
		data.MembershipDate = mDate;
		data.MemSex = sex;
		data.MemAge = age;
		data.EOMDate = eomDate;

		ProcessAccountRow(ds, data, accountRow);

		//  Create the new Loan Portfolio
		LoanPortfolio loanPortfolio = LoanPortfolio.NewInstance();
		loanPortfolio.Update(data, false);
		SetLoanPortfolioDerivedValues(loanPortfolio, loanTypes, ficoRanges, fastStartRanges, bankruptcyRanges, riskTypes, chanels);
		loanPortfolio.Store();
	    }
	}

	public void ProcessAccountRow(DataSet ds, LoanPortfolioData data, DataRow accountRow) {
	    LoadAccountValues(data, accountRow);
	    LoadApplicValues(data, ds, accountRow);
	    LoadCoborrowValues(data, accountRow);
	    LoadCollatValueValues(data, accountRow);
	    LoadAccountUsValues(data, accountRow);
	    LoadDelinquencyDataValues(data, accountRow);
	    LoadAccountUdUsValues(data, accountRow);
	    LoadTranDataValues(data, accountRow);
	    LoadTranLoanValues(data, accountRow);
	    LoadAccountChgOffValues(data, accountRow);
	    LoadDealerDataValues(data, ds, accountRow);
	    //LoadProfileValues(data, accountRow);
	    //LoadProfileEmployerInfoValues(data, accountRow);
	}

	private void SetLoanPortfolioDerivedValues(LoanPortfolio loanPortfolio, LoanTypeList loanTypes, FICORangeList ficoRanges, FastStartRangeList fastStartRanges, BankruptcyRangeList bankruptcyRanges, RiskTypeList riskTypes, ChanelList chanels) {
	    int lType = IntegerType.Parse(loanPortfolio.Ltype.ToString("00")).ToInt32();

	    ILoanType loanType = loanTypes.FindByLType(lType);
	    loanPortfolio.LoanDesc = loanType != null ? loanType.LoanDesc : StringType.UNSET;

	    IRiskType riskType = riskTypes.FindByLType(lType);
	    loanPortfolio.RiskType = riskType != null ? riskType.RiskTypeDesc : StringType.UNSET;

	    IChanel chanel = chanels.FindByReportCode(loanPortfolio.RptCode);
	    loanPortfolio.Chanel = chanel != null ? chanel.ChanelDesc : StringType.UNSET;

	    loanPortfolio.LoanDay = loanPortfolio.LoanDate.DayOfWeek.ToString().Substring(0, 3);
	    loanPortfolio.LoanMonth = loanPortfolio.LoanDate.Month;
	    loanPortfolio.LoanYear = loanPortfolio.LoanDate.Year;

	    if (loanPortfolio.ChrgOffDate.IsValid) {
		loanPortfolio.ChrgOffMnthsBF = ServiceUtil.MonthDifference(loanPortfolio.LoanDate, loanPortfolio.ChrgOffDate);
		loanPortfolio.ChrgOffMonth = loanPortfolio.ChrgOffDate.Month;
		loanPortfolio.ChrgOffYear = loanPortfolio.ChrgOffDate.Year;
	    }

	    loanPortfolio.FICORange = ficoRanges.GetRangeValue(loanPortfolio.FICO) != null ? ficoRanges.GetRangeValue(loanPortfolio.FICO).Description : StringType.UNSET;
	    loanPortfolio.FastStartRange = fastStartRanges.GetRangeValue(loanPortfolio.FastStart) != null ? fastStartRanges.GetRangeValue(loanPortfolio.FastStart).Description : StringType.UNSET;
	    loanPortfolio.BankruptcyRange = bankruptcyRanges.GetRangeValue(loanPortfolio.Bankruptcy) != null ? bankruptcyRanges.GetRangeValue(loanPortfolio.Bankruptcy).Description : StringType.UNSET;

	    StringType newMemberDays = ConfigurationProvider.Instance.Settings["LoanPortfolio.DerivedValues.NewMemberDays"];
	    int days = 0;
	    if (newMemberDays.IsValid && int.TryParse(newMemberDays, out days)) {
		loanPortfolio.NewMember = loanPortfolio.MembershipDate > DateType.Now.AddDays(-days) ? "Y" : "N";
	    }

	    if (loanPortfolio.Traffic.IsValid && loanPortfolio.Traffic > 0) {
		int traffic = loanPortfolio.Traffic.ToInt32();
		if (traffic.Equals(1)) {
		    loanPortfolio.TrafficColor = "R";
		} else if (traffic.Equals(2)) {
		    loanPortfolio.TrafficColor = "Y";
		} else if (traffic.Equals(3)) {
		    loanPortfolio.TrafficColor = "G";
		}
	    }

	    if (loanPortfolio.OrigAmount.IsValid && loanPortfolio.CollateralValue.IsValid && loanPortfolio.CollateralValue != 0) {
		loanPortfolio.LTV = (decimal)(loanPortfolio.OrigAmount / loanPortfolio.CollateralValue);  //	Loan Amount / Value of Collateral
	    }

	    loanPortfolio.DeliHistQ1 = loanPortfolio.DelinquentInQuarter(1) ? "Y" : "N";  //	Based on Delinquency History
	    loanPortfolio.DeliHistQ2 = loanPortfolio.DelinquentInQuarter(2) ? "Y" : "N";
	    loanPortfolio.DeliHistQ3 = loanPortfolio.DelinquentInQuarter(3) ? "Y" : "N";
	    loanPortfolio.DeliHistQ4 = loanPortfolio.DelinquentInQuarter(4) ? "Y" : "N";
	    loanPortfolio.DeliHistQ5 = loanPortfolio.DelinquentInQuarter(5) ? "Y" : "N";
	    loanPortfolio.DeliHistQ6 = loanPortfolio.DelinquentInQuarter(6) ? "Y" : "N";
	    loanPortfolio.DeliHistQ7 = loanPortfolio.DelinquentInQuarter(7) ? "Y" : "N";
	    loanPortfolio.DeliHistQ8 = loanPortfolio.DelinquentInQuarter(8) ? "Y" : "N";
	}

	public void CreateLoanPortfolio() {
	    LoanPortfolio loanPortfolio = LoanPortfolio.NewInstance();
	    //loanPortfolio.Update();
	}

	public void CreateLoanPortfolio(LoanPortfolioData data) {
	    LoanPortfolio loanPortfolio = LoanPortfolio.NewInstance();
	    loanPortfolio.Update(data);
	}

	public DecimalType GetDecimalValueFromDataRow(DataRow dr, String columnName) {
	    DecimalType result = DecimalType.UNSET;
	    String columnValue = dr[columnName].ToString();
	    if (!String.IsNullOrEmpty(columnValue)) {
		decimal value = 0;
		if (decimal.TryParse(columnValue, out value)) {
		    result = value;
		} else {
		    log.Error("Invalid Decimal Parse Attempt. Column: " + columnName + " Value: " + columnValue);
		}
	    }
	    return result;
	}

	public CurrencyType GetCurrencyValueFromDataRow(DataRow dr, String columnName) {
	    CurrencyType result = CurrencyType.UNSET;
	    String columnValue = dr[columnName].ToString();
	    if (!String.IsNullOrEmpty(columnValue)) {
		decimal value = 0;
		if (decimal.TryParse(columnValue, out value)) {
		    result = value;
		} else {
		    log.Error("Invalid Currency Parse Attempt. Column: " + columnName + " Value: " + columnValue);
		}
	    }
	    return result;
	}

	public IntegerType GetIntegerValueFromDataRow(DataRow dr, String columnName) {
	    IntegerType result = IntegerType.UNSET;
	    String columnValue = dr[columnName].ToString();
	    if (!String.IsNullOrEmpty(columnValue)) {
		int value = 0;
		if (int.TryParse(columnValue, out value)) {
		    result = value;
		} else {
		    log.Error("Invalid Int Parse Attempt. Column: " + columnName + " Value: " + columnValue);
		}
	    }
	    return result;
	}

	public IdType GetIdValueFromDataRow(DataRow dr, String columnName) {
	    IntegerType value = GetIntegerValueFromDataRow(dr, columnName).ToInt32();
	    return value.IsValid ? value.ToInt32() : IdType.UNSET;
	}

	public DateTimeType GetDateTimeTypeFromDataRow(DataRow dr, String columnName) {
	    DateTimeType value = DateTimeType.UNSET;
	    String columnValue = dr[columnName].ToString();
	    if (!String.IsNullOrEmpty(columnValue)) {
		try {
		    value = DateTimeType.Parse(dr[columnName].ToString());
		} catch (Exception) {
		    log.Error("Invalid DateTime Parse Attempt. Column: " + columnName + " Value: " + columnValue);
		    value = new DateTimeType(1753, 1, 1);
		}

		if(value.Year < 1753 || value.Year > 9999){
		    log.Error("SqlDateTime Overflow Detected. Column: " + columnName + " Value: " + columnValue);
		    value = new DateTimeType(1753, 1, 1);
		}
	    }
	    
	    return value;
	}

	public DateType GetDateTypeFromDataRow(DataRow dr, String columnName) {
	    DateType value = DateType.UNSET;
	    String columnValue = dr[columnName].ToString();
	    if (!String.IsNullOrEmpty(columnValue)) {
		try {
		    value = DateType.Parse(dr[columnName].ToString());
		} catch (Exception) {
		    log.Error("Invalid Date Parse Attempt. Column: " + columnName + " Value: " + columnValue);
		}
	    }

	    return value;
	}

	public void ReDeriveAllLoanPortfolios() {
	    LoanPortfolioList loanPortfolios = LoanPortfolioDAO.DAO.GetList();

	    log.Info("Loan Portfolio Count: " + loanPortfolios.Count);

	    //	Get lookup lists for derived fields
	    LoanTypeList loanTypes = LoanTypeDAO.DAO.GetList();
	    FICORangeList ficoRanges = FICORangeDAO.DAO.GetList();
	    FastStartRangeList fastStartRanges = FastStartRangeDAO.DAO.GetList();
	    BankruptcyRangeList bankruptcyRanges = BankruptcyRangeDAO.DAO.GetList();

	    RiskTypeList riskTypes = RiskTypeDAO.DAO.GetList();
	    ChanelList chanels = ChanelDAO.DAO.GetList();

	    foreach (LoanPortfolio lp in loanPortfolios) {
		try {
		    SetLoanPortfolioDerivedValues(lp, loanTypes, ficoRanges, fastStartRanges, bankruptcyRanges, riskTypes, chanels);
		    lp.Store();
		}catch(MessageListException mle){
		    StringBuilder messageText = new StringBuilder();
		    foreach (Message msg in mle.Messages)
			messageText.Append(formatter.Format(msg) + Environment.NewLine);
		    log.Error(messageText, mle);
		}catch (Exception ex) {
		    log.Error("Error Updating Loan Portfolio Derived Fields (" + lp.LoanPortfolioId.Display() + ")", ex);
		}
	    }
	}

	public void LogTest() {
	    log.Info("This is a test from the Service Facade!");
	}

	public void SendMailMessage(string subject, string body) {
	    string fromAddress = "support@cypruscu.com";
	    string toAddress = ConfigurationProvider.Instance.Settings["Notification.ToAddress"];
	    string smtpHost = ConfigurationProvider.Instance.Settings["SMTP.Host"];
	    string smtpUserName = ConfigurationProvider.Instance.Settings["SMTP.UserName"];
	    string smtpPassword = ConfigurationProvider.Instance.Settings["SMTP.Password"];
	    int smtpPort = 25;
	    int.TryParse(ConfigurationProvider.Instance.Settings["SMTP.Port"], out smtpPort);

	    if (!string.IsNullOrEmpty(toAddress) &&
		!string.IsNullOrEmpty(smtpHost)) {
		MailMessage msg = new MailMessage(
		    new MailAddress(fromAddress), 
		    new MailAddress(toAddress));
		msg.Subject = subject;
		msg.SubjectEncoding = System.Text.Encoding.UTF8;
		msg.Body = body;
		msg.BodyEncoding = System.Text.Encoding.UTF8;
		msg.IsBodyHtml = false;

		SmtpClient client = new SmtpClient(smtpHost, smtpPort);

		if (!string.IsNullOrEmpty(smtpUserName) || !string.IsNullOrEmpty(smtpPassword)) {
		    client.Credentials = new NetworkCredential(smtpUserName, smtpPassword);
		} else {
		    client.Credentials = null;
		}

		client.EnableSsl = false; 

		try {
		    client.Send(msg);
		    log.Info("Your Email was sent successfully.");
		} catch (SmtpException ex) {
		    log.Error(string.Format("Email Error: {0}", ex.Message));
		}
	    } else {
		log.Error("Unable to send Email: Invalid Configuration.");
	    }
	}

	public string GetUniDataODBCConnectionString() {
	    return ConfigurationProvider.Instance.Settings["UniData.ODBC.ConnectionString"];
	}

	public string GetRestoreODBCConnectionString() {
	    return ConfigurationProvider.Instance.Settings["Test.ODBC.ConnectionString"];
	}
    }
}
