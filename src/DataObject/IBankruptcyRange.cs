using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class BankruptcyRangeFields {
	private BankruptcyRangeFields() {}
	public static readonly String ENTITY_NAME = "BankruptcyRange";

	public static readonly ColumnMetaData BANKRUPTCYRANGEID = new ColumnMetaData("BankruptcyRangeId", "BankruptcyRangeId", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData VALUE = new ColumnMetaData("Value", "Value", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DESCRIPTION = new ColumnMetaData("Description", "Description", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface IBankruptcyRange : IBusinessEntity {
	IdType BankruptcyRangeId {
	    get;
	}
	IntegerType Value {
	    get;
	}
	StringType Description {
	    get;
	}
    }
}
