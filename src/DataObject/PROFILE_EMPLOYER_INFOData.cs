using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class PROFILE_EMPLOYER_INFOData : Spring2.Core.DataObject.DataObject {
	public static readonly PROFILE_EMPLOYER_INFOData DEFAULT = new PROFILE_EMPLOYER_INFOData();

	private IdType rECORD_KEY = IdType.DEFAULT;
	private CurrencyType eMP_GROSS_INCOME = CurrencyType.DEFAULT;
	private IntegerType eMP_YRS_PROF = IntegerType.DEFAULT;

	public IdType RECORD_KEY {
	    get { return this.rECORD_KEY; }
	    set { this.rECORD_KEY = value; }
	}

	public CurrencyType EMP_GROSS_INCOME {
	    get { return this.eMP_GROSS_INCOME; }
	    set { this.eMP_GROSS_INCOME = value; }
	}

	public IntegerType EMP_YRS_PROF {
	    get { return this.eMP_YRS_PROF; }
	    set { this.eMP_YRS_PROF = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
