using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ITRAN_LOAN_STAT generic collection
    /// </summary>
    public class TRAN_LOAN_STATList : CollectionBase {
	[Generate]
	public static readonly TRAN_LOAN_STATList UNSET = new TRAN_LOAN_STATList(true);
	[Generate]
	public static readonly TRAN_LOAN_STATList DEFAULT = new TRAN_LOAN_STATList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private TRAN_LOAN_STATList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public TRAN_LOAN_STATList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ITRAN_LOAN_STAT this[int index] {
	    get {
		return (ITRAN_LOAN_STAT)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ITRAN_LOAN_STAT value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ITRAN_LOAN_STAT value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ITRAN_LOAN_STAT value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ITRAN_LOAN_STAT) {
		    Add((ITRAN_LOAN_STAT)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ITRAN_LOAN_STAT");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ITRAN_LOAN_STAT this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as ITRAN_LOAN_STAT;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public TRAN_LOAN_STATList RetainAll(TRAN_LOAN_STATList list) {
	    TRAN_LOAN_STATList result = new TRAN_LOAN_STATList();

	    foreach (ITRAN_LOAN_STAT data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public TRAN_LOAN_STATList RemoveAll(TRAN_LOAN_STATList list) {
	    TRAN_LOAN_STATList result = new TRAN_LOAN_STATList();

	    foreach (ITRAN_LOAN_STAT data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		ITRAN_LOAN_STAT objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_LOAN_STAT o1 = (ITRAN_LOAN_STAT)a;
		ITRAN_LOAN_STAT o2 = (ITRAN_LOAN_STAT)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class L_STAT_CHANGE_ACTUAL_DATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_LOAN_STAT o1 = (ITRAN_LOAN_STAT)a;
		ITRAN_LOAN_STAT o2 = (ITRAN_LOAN_STAT)b;

		if (o1 == null || o2 == null || !o1.L_STAT_CHANGE_ACTUAL_DATE.IsValid || !o2.L_STAT_CHANGE_ACTUAL_DATE.IsValid) {
		    return 0;
		}
		return o1.L_STAT_CHANGE_ACTUAL_DATE.CompareTo(o2.L_STAT_CHANGE_ACTUAL_DATE);
	    }
	}
    }
}
