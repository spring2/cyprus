using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class LoanTypeFields {
	private LoanTypeFields() {}
	public static readonly String ENTITY_NAME = "LoanType";

	public static readonly ColumnMetaData LOANTYPEID = new ColumnMetaData("LoanTypeId", "LoanTypeId", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LTYPE = new ColumnMetaData("LType", "LType", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LOANDESC = new ColumnMetaData("LoanDesc", "LoanDesc", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface ILoanType : IBusinessEntity {
	IdType LoanTypeId {
	    get;
	}
	IntegerType LType {
	    get;
	}
	StringType LoanDesc {
	    get;
	}
    }
}
