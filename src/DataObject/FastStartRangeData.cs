using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class FastStartRangeData : Spring2.Core.DataObject.DataObject {
	public static readonly FastStartRangeData DEFAULT = new FastStartRangeData();

	private IdType fastStartRangeId = IdType.DEFAULT;
	private IntegerType value = IntegerType.DEFAULT;
	private StringType description = StringType.DEFAULT;

	public IdType FastStartRangeId {
	    get { return this.fastStartRangeId; }
	    set { this.fastStartRangeId = value; }
	}

	public IntegerType Value {
	    get { return this.value; }
	    set { this.value = value; }
	}

	public StringType Description {
	    get { return this.description; }
	    set { this.description = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
