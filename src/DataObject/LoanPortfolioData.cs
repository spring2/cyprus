using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class LoanPortfolioData : Spring2.Core.DataObject.DataObject {
	public static readonly LoanPortfolioData DEFAULT = new LoanPortfolioData();

	private IdType loanPortfolioId = IdType.DEFAULT;
	private DateTimeType upDated = DateTimeType.DEFAULT;
	private DateType eOMDate = DateType.DEFAULT;
	private StringType accountID = StringType.DEFAULT;
	private StringType member = StringType.DEFAULT;
	private IntegerType mClass = IntegerType.DEFAULT;
	private IntegerType ltype = IntegerType.DEFAULT;
	private DecimalType ltypeSub = DecimalType.DEFAULT;
	private IntegerType product = IntegerType.DEFAULT;
	private StringType loanDesc = StringType.DEFAULT;
	private DateType loanDate = DateType.DEFAULT;
	private StringType loanDay = StringType.DEFAULT;
	private IntegerType loanMonth = IntegerType.DEFAULT;
	private IntegerType loanYear = IntegerType.DEFAULT;
	private DateType firstPmtDate = DateType.DEFAULT;
	private StringType status = StringType.DEFAULT;
	private IntegerType rptCode = IntegerType.DEFAULT;
	private IntegerType fundedBr = IntegerType.DEFAULT;
	private IntegerType loadedBr = IntegerType.DEFAULT;
	private IntegerType fICO = IntegerType.DEFAULT;
	private StringType fICORange = StringType.DEFAULT;
	private IntegerType fastStart = IntegerType.DEFAULT;
	private StringType fastStartRange = StringType.DEFAULT;
	private IntegerType bankruptcy = IntegerType.DEFAULT;
	private StringType bankruptcyRange = StringType.DEFAULT;
	private DateType membershipDate = DateType.DEFAULT;
	private StringType newMember = StringType.DEFAULT;
	private StringType riskLev = StringType.DEFAULT;
	private IntegerType traffic = IntegerType.DEFAULT;
	private StringType trafficColor = StringType.DEFAULT;
	private IntegerType memAge = IntegerType.DEFAULT;
	private StringType memSex = StringType.DEFAULT;
	private StringType pmtFreq = StringType.DEFAULT;
	private IntegerType origTerm = IntegerType.DEFAULT;
	private DecimalType origRate = DecimalType.DEFAULT;
	private CurrencyType origPmt = CurrencyType.DEFAULT;
	private CurrencyType origAmount = CurrencyType.DEFAULT;
	private CurrencyType origCrLim = CurrencyType.DEFAULT;
	private IntegerType curTerm = IntegerType.DEFAULT;
	private DecimalType curRate = DecimalType.DEFAULT;
	private CurrencyType curPmt = CurrencyType.DEFAULT;
	private CurrencyType curBal = CurrencyType.DEFAULT;
	private CurrencyType curCrLim = CurrencyType.DEFAULT;
	private DateType balloonDate = DateType.DEFAULT;
	private DateType matDate = DateType.DEFAULT;
	private IntegerType insCode = IntegerType.DEFAULT;
	private CurrencyType collateralValue = CurrencyType.DEFAULT;
	private DecimalType lTV = DecimalType.DEFAULT;
	private IntegerType secType = IntegerType.DEFAULT;
	private StringType secDesc1 = StringType.DEFAULT;
	private StringType secDesc2 = StringType.DEFAULT;
	private DecimalType debtRatio = DecimalType.DEFAULT;
	private IntegerType dealer = IntegerType.DEFAULT;
	private StringType dealerName = StringType.DEFAULT;
	private StringType coborComak = StringType.DEFAULT;
	private StringType relPriceGrp = StringType.DEFAULT;
	private IntegerType relPriceDisc = IntegerType.DEFAULT;
	private DecimalType otherDisc = DecimalType.DEFAULT;
	private StringType discReason = StringType.DEFAULT;
	private DecimalType riskOffset = DecimalType.DEFAULT;
	private StringType creditType = StringType.DEFAULT;
	private StringType statedIncome = StringType.DEFAULT;
	private StringType extensions = StringType.DEFAULT;
	private StringType firstExtDate = StringType.DEFAULT;
	private StringType lastExtDate = StringType.DEFAULT;
	private StringType riskLevel = StringType.DEFAULT;
	private StringType riskDate = StringType.DEFAULT;
	private StringType watch = StringType.DEFAULT;
	private StringType watchDate = StringType.DEFAULT;
	private StringType workOut = StringType.DEFAULT;
	private StringType workOutDate = StringType.DEFAULT;
	private StringType firstMortType = StringType.DEFAULT;
	private StringType busPerReg = StringType.DEFAULT;
	private StringType busGroup = StringType.DEFAULT;
	private StringType busDesc = StringType.DEFAULT;
	private StringType busPart = StringType.DEFAULT;
	private StringType busPartPerc = StringType.DEFAULT;
	private StringType sBAguar = StringType.DEFAULT;
	private IntegerType deliDays = IntegerType.DEFAULT;
	private IntegerType deliSect = IntegerType.DEFAULT;
	private StringType deliHist = StringType.DEFAULT;
	private StringType deliHistQ1 = StringType.DEFAULT;
	private StringType deliHistQ2 = StringType.DEFAULT;
	private StringType deliHistQ3 = StringType.DEFAULT;
	private StringType deliHistQ4 = StringType.DEFAULT;
	private StringType deliHistQ5 = StringType.DEFAULT;
	private StringType deliHistQ6 = StringType.DEFAULT;
	private StringType deliHistQ7 = StringType.DEFAULT;
	private StringType deliHistQ8 = StringType.DEFAULT;
	private DateType chrgOffDate = DateType.DEFAULT;
	private IntegerType chrgOffMonth = IntegerType.DEFAULT;
	private IntegerType chrgOffYear = IntegerType.DEFAULT;
	private IntegerType chrgOffMnthsBF = IntegerType.DEFAULT;
	private StringType propType = StringType.DEFAULT;
	private StringType occupancy = StringType.DEFAULT;
	private CurrencyType origApprVal = CurrencyType.DEFAULT;
	private DateType origApprDate = DateType.DEFAULT;
	private CurrencyType currApprVal = CurrencyType.DEFAULT;
	private DateType currApprDate = DateType.DEFAULT;
	private CurrencyType firstMortBal = CurrencyType.DEFAULT;
	private CurrencyType firstMortMoPyt = CurrencyType.DEFAULT;
	private CurrencyType secondMortBal = CurrencyType.DEFAULT;
	private CurrencyType secondMortPymt = CurrencyType.DEFAULT;
	private CurrencyType taxInsNotInc = CurrencyType.DEFAULT;
	private IntegerType loadOff = IntegerType.DEFAULT;
	private IntegerType apprOff = IntegerType.DEFAULT;
	private IntegerType fundingOpp = IntegerType.DEFAULT;
	private StringType chanel = StringType.DEFAULT;
	private StringType riskType = StringType.DEFAULT;
	private StringType gap = StringType.DEFAULT;
	private IntegerType loadOperator = IntegerType.DEFAULT;
	private CurrencyType deliAmt = CurrencyType.DEFAULT;
	private CurrencyType chgOffAmt = CurrencyType.DEFAULT;
	private StringType usSecDesc = StringType.DEFAULT;
	private StringType troubledDebt = StringType.DEFAULT;
	private StringType tdrDate = StringType.DEFAULT;
	private StringType jntFicoUsed = StringType.DEFAULT;
	private StringType singleOrJointLoan = StringType.DEFAULT;
	private StringType approvingOfficerUD = StringType.DEFAULT;
	private StringType insuranceCodeDesc = StringType.DEFAULT;
	private StringType gapIns = StringType.DEFAULT;
	private StringType mmpIns = StringType.DEFAULT;
	private StringType reportCodeDesc = StringType.DEFAULT;
	private StringType ballonPmtLoan = StringType.DEFAULT;
	private StringType fixedOrVariableRateLoan = StringType.DEFAULT;
	private StringType alpsOfficer = StringType.DEFAULT;
	private StringType cUDLAppNumber = StringType.DEFAULT;
	private CurrencyType fasbDefCost = CurrencyType.DEFAULT;
	private CurrencyType fasbLtdAmortFees = CurrencyType.DEFAULT;
	private CurrencyType interestPriorYtd = CurrencyType.DEFAULT;
	private CurrencyType accruedInterest = CurrencyType.DEFAULT;
	private CurrencyType interestLTD = CurrencyType.DEFAULT;
	private CurrencyType interestYtd = CurrencyType.DEFAULT;
	private CurrencyType partialPayAmt = CurrencyType.DEFAULT;
	private CurrencyType interestUncollected = CurrencyType.DEFAULT;
	private DateType closedAcctDate = DateType.DEFAULT;
	private DateType lastTranDate = DateType.DEFAULT;
	private DateType nextDueDate = DateType.DEFAULT;
	private IntegerType insuranceCode = IntegerType.DEFAULT;
	private IntegerType mbrAgeAtApproval = IntegerType.DEFAULT;

	public IdType LoanPortfolioId {
	    get { return this.loanPortfolioId; }
	    set { this.loanPortfolioId = value; }
	}

	public DateTimeType UpDated {
	    get { return this.upDated; }
	    set { this.upDated = value; }
	}

	public DateType EOMDate {
	    get { return this.eOMDate; }
	    set { this.eOMDate = value; }
	}

	public StringType AccountID {
	    get { return this.accountID; }
	    set { this.accountID = value; }
	}

	public StringType Member {
	    get { return this.member; }
	    set { this.member = value; }
	}

	public IntegerType MClass {
	    get { return this.mClass; }
	    set { this.mClass = value; }
	}

	public IntegerType Ltype {
	    get { return this.ltype; }
	    set { this.ltype = value; }
	}

	public DecimalType LtypeSub {
	    get { return this.ltypeSub; }
	    set { this.ltypeSub = value; }
	}

	public IntegerType Product {
	    get { return this.product; }
	    set { this.product = value; }
	}

	public StringType LoanDesc {
	    get { return this.loanDesc; }
	    set { this.loanDesc = value; }
	}

	public DateType LoanDate {
	    get { return this.loanDate; }
	    set { this.loanDate = value; }
	}

	public StringType LoanDay {
	    get { return this.loanDay; }
	    set { this.loanDay = value; }
	}

	public IntegerType LoanMonth {
	    get { return this.loanMonth; }
	    set { this.loanMonth = value; }
	}

	public IntegerType LoanYear {
	    get { return this.loanYear; }
	    set { this.loanYear = value; }
	}

	public DateType FirstPmtDate {
	    get { return this.firstPmtDate; }
	    set { this.firstPmtDate = value; }
	}

	public StringType Status {
	    get { return this.status; }
	    set { this.status = value; }
	}

	public IntegerType RptCode {
	    get { return this.rptCode; }
	    set { this.rptCode = value; }
	}

	public IntegerType FundedBr {
	    get { return this.fundedBr; }
	    set { this.fundedBr = value; }
	}

	public IntegerType LoadedBr {
	    get { return this.loadedBr; }
	    set { this.loadedBr = value; }
	}

	public IntegerType FICO {
	    get { return this.fICO; }
	    set { this.fICO = value; }
	}

	public StringType FICORange {
	    get { return this.fICORange; }
	    set { this.fICORange = value; }
	}

	public IntegerType FastStart {
	    get { return this.fastStart; }
	    set { this.fastStart = value; }
	}

	public StringType FastStartRange {
	    get { return this.fastStartRange; }
	    set { this.fastStartRange = value; }
	}

	public IntegerType Bankruptcy {
	    get { return this.bankruptcy; }
	    set { this.bankruptcy = value; }
	}

	public StringType BankruptcyRange {
	    get { return this.bankruptcyRange; }
	    set { this.bankruptcyRange = value; }
	}

	public DateType MembershipDate {
	    get { return this.membershipDate; }
	    set { this.membershipDate = value; }
	}

	public StringType NewMember {
	    get { return this.newMember; }
	    set { this.newMember = value; }
	}

	public StringType RiskLev {
	    get { return this.riskLev; }
	    set { this.riskLev = value; }
	}

	public IntegerType Traffic {
	    get { return this.traffic; }
	    set { this.traffic = value; }
	}

	public StringType TrafficColor {
	    get { return this.trafficColor; }
	    set { this.trafficColor = value; }
	}

	public IntegerType MemAge {
	    get { return this.memAge; }
	    set { this.memAge = value; }
	}

	public StringType MemSex {
	    get { return this.memSex; }
	    set { this.memSex = value; }
	}

	public StringType PmtFreq {
	    get { return this.pmtFreq; }
	    set { this.pmtFreq = value; }
	}

	public IntegerType OrigTerm {
	    get { return this.origTerm; }
	    set { this.origTerm = value; }
	}

	public DecimalType OrigRate {
	    get { return this.origRate; }
	    set { this.origRate = value; }
	}

	public CurrencyType OrigPmt {
	    get { return this.origPmt; }
	    set { this.origPmt = value; }
	}

	public CurrencyType OrigAmount {
	    get { return this.origAmount; }
	    set { this.origAmount = value; }
	}

	public CurrencyType OrigCrLim {
	    get { return this.origCrLim; }
	    set { this.origCrLim = value; }
	}

	public IntegerType CurTerm {
	    get { return this.curTerm; }
	    set { this.curTerm = value; }
	}

	public DecimalType CurRate {
	    get { return this.curRate; }
	    set { this.curRate = value; }
	}

	public CurrencyType CurPmt {
	    get { return this.curPmt; }
	    set { this.curPmt = value; }
	}

	public CurrencyType CurBal {
	    get { return this.curBal; }
	    set { this.curBal = value; }
	}

	public CurrencyType CurCrLim {
	    get { return this.curCrLim; }
	    set { this.curCrLim = value; }
	}

	public DateType BalloonDate {
	    get { return this.balloonDate; }
	    set { this.balloonDate = value; }
	}

	public DateType MatDate {
	    get { return this.matDate; }
	    set { this.matDate = value; }
	}

	public IntegerType InsCode {
	    get { return this.insCode; }
	    set { this.insCode = value; }
	}

	public CurrencyType CollateralValue {
	    get { return this.collateralValue; }
	    set { this.collateralValue = value; }
	}

	public DecimalType LTV {
	    get { return this.lTV; }
	    set { this.lTV = value; }
	}

	public IntegerType SecType {
	    get { return this.secType; }
	    set { this.secType = value; }
	}

	public StringType SecDesc1 {
	    get { return this.secDesc1; }
	    set { this.secDesc1 = value; }
	}

	public StringType SecDesc2 {
	    get { return this.secDesc2; }
	    set { this.secDesc2 = value; }
	}

	public DecimalType DebtRatio {
	    get { return this.debtRatio; }
	    set { this.debtRatio = value; }
	}

	public IntegerType Dealer {
	    get { return this.dealer; }
	    set { this.dealer = value; }
	}

	public StringType DealerName {
	    get { return this.dealerName; }
	    set { this.dealerName = value; }
	}

	public StringType CoborComak {
	    get { return this.coborComak; }
	    set { this.coborComak = value; }
	}

	public StringType RelPriceGrp {
	    get { return this.relPriceGrp; }
	    set { this.relPriceGrp = value; }
	}

	public IntegerType RelPriceDisc {
	    get { return this.relPriceDisc; }
	    set { this.relPriceDisc = value; }
	}

	public DecimalType OtherDisc {
	    get { return this.otherDisc; }
	    set { this.otherDisc = value; }
	}

	public StringType DiscReason {
	    get { return this.discReason; }
	    set { this.discReason = value; }
	}

	public DecimalType RiskOffset {
	    get { return this.riskOffset; }
	    set { this.riskOffset = value; }
	}

	public StringType CreditType {
	    get { return this.creditType; }
	    set { this.creditType = value; }
	}

	public StringType StatedIncome {
	    get { return this.statedIncome; }
	    set { this.statedIncome = value; }
	}

	public StringType Extensions {
	    get { return this.extensions; }
	    set { this.extensions = value; }
	}

	public StringType FirstExtDate {
	    get { return this.firstExtDate; }
	    set { this.firstExtDate = value; }
	}

	public StringType LastExtDate {
	    get { return this.lastExtDate; }
	    set { this.lastExtDate = value; }
	}

	public StringType RiskLevel {
	    get { return this.riskLevel; }
	    set { this.riskLevel = value; }
	}

	public StringType RiskDate {
	    get { return this.riskDate; }
	    set { this.riskDate = value; }
	}

	public StringType Watch {
	    get { return this.watch; }
	    set { this.watch = value; }
	}

	public StringType WatchDate {
	    get { return this.watchDate; }
	    set { this.watchDate = value; }
	}

	public StringType WorkOut {
	    get { return this.workOut; }
	    set { this.workOut = value; }
	}

	public StringType WorkOutDate {
	    get { return this.workOutDate; }
	    set { this.workOutDate = value; }
	}

	public StringType FirstMortType {
	    get { return this.firstMortType; }
	    set { this.firstMortType = value; }
	}

	public StringType BusPerReg {
	    get { return this.busPerReg; }
	    set { this.busPerReg = value; }
	}

	public StringType BusGroup {
	    get { return this.busGroup; }
	    set { this.busGroup = value; }
	}

	public StringType BusDesc {
	    get { return this.busDesc; }
	    set { this.busDesc = value; }
	}

	public StringType BusPart {
	    get { return this.busPart; }
	    set { this.busPart = value; }
	}

	public StringType BusPartPerc {
	    get { return this.busPartPerc; }
	    set { this.busPartPerc = value; }
	}

	public StringType SBAguar {
	    get { return this.sBAguar; }
	    set { this.sBAguar = value; }
	}

	public IntegerType DeliDays {
	    get { return this.deliDays; }
	    set { this.deliDays = value; }
	}

	public IntegerType DeliSect {
	    get { return this.deliSect; }
	    set { this.deliSect = value; }
	}

	public StringType DeliHist {
	    get { return this.deliHist; }
	    set { this.deliHist = value; }
	}

	public StringType DeliHistQ1 {
	    get { return this.deliHistQ1; }
	    set { this.deliHistQ1 = value; }
	}

	public StringType DeliHistQ2 {
	    get { return this.deliHistQ2; }
	    set { this.deliHistQ2 = value; }
	}

	public StringType DeliHistQ3 {
	    get { return this.deliHistQ3; }
	    set { this.deliHistQ3 = value; }
	}

	public StringType DeliHistQ4 {
	    get { return this.deliHistQ4; }
	    set { this.deliHistQ4 = value; }
	}

	public StringType DeliHistQ5 {
	    get { return this.deliHistQ5; }
	    set { this.deliHistQ5 = value; }
	}

	public StringType DeliHistQ6 {
	    get { return this.deliHistQ6; }
	    set { this.deliHistQ6 = value; }
	}

	public StringType DeliHistQ7 {
	    get { return this.deliHistQ7; }
	    set { this.deliHistQ7 = value; }
	}

	public StringType DeliHistQ8 {
	    get { return this.deliHistQ8; }
	    set { this.deliHistQ8 = value; }
	}

	public DateType ChrgOffDate {
	    get { return this.chrgOffDate; }
	    set { this.chrgOffDate = value; }
	}

	public IntegerType ChrgOffMonth {
	    get { return this.chrgOffMonth; }
	    set { this.chrgOffMonth = value; }
	}

	public IntegerType ChrgOffYear {
	    get { return this.chrgOffYear; }
	    set { this.chrgOffYear = value; }
	}

	public IntegerType ChrgOffMnthsBF {
	    get { return this.chrgOffMnthsBF; }
	    set { this.chrgOffMnthsBF = value; }
	}

	public StringType PropType {
	    get { return this.propType; }
	    set { this.propType = value; }
	}

	public StringType Occupancy {
	    get { return this.occupancy; }
	    set { this.occupancy = value; }
	}

	public CurrencyType OrigApprVal {
	    get { return this.origApprVal; }
	    set { this.origApprVal = value; }
	}

	public DateType OrigApprDate {
	    get { return this.origApprDate; }
	    set { this.origApprDate = value; }
	}

	public CurrencyType CurrApprVal {
	    get { return this.currApprVal; }
	    set { this.currApprVal = value; }
	}

	public DateType CurrApprDate {
	    get { return this.currApprDate; }
	    set { this.currApprDate = value; }
	}

	public CurrencyType FirstMortBal {
	    get { return this.firstMortBal; }
	    set { this.firstMortBal = value; }
	}

	public CurrencyType FirstMortMoPyt {
	    get { return this.firstMortMoPyt; }
	    set { this.firstMortMoPyt = value; }
	}

	public CurrencyType SecondMortBal {
	    get { return this.secondMortBal; }
	    set { this.secondMortBal = value; }
	}

	public CurrencyType SecondMortPymt {
	    get { return this.secondMortPymt; }
	    set { this.secondMortPymt = value; }
	}

	public CurrencyType TaxInsNotInc {
	    get { return this.taxInsNotInc; }
	    set { this.taxInsNotInc = value; }
	}

	public IntegerType LoadOff {
	    get { return this.loadOff; }
	    set { this.loadOff = value; }
	}

	public IntegerType ApprOff {
	    get { return this.apprOff; }
	    set { this.apprOff = value; }
	}

	public IntegerType FundingOpp {
	    get { return this.fundingOpp; }
	    set { this.fundingOpp = value; }
	}

	public StringType Chanel {
	    get { return this.chanel; }
	    set { this.chanel = value; }
	}

	public StringType RiskType {
	    get { return this.riskType; }
	    set { this.riskType = value; }
	}

	public StringType Gap {
	    get { return this.gap; }
	    set { this.gap = value; }
	}

	public IntegerType LoadOperator {
	    get { return this.loadOperator; }
	    set { this.loadOperator = value; }
	}

	public CurrencyType DeliAmt {
	    get { return this.deliAmt; }
	    set { this.deliAmt = value; }
	}

	public CurrencyType ChgOffAmt {
	    get { return this.chgOffAmt; }
	    set { this.chgOffAmt = value; }
	}

	public StringType UsSecDesc {
	    get { return this.usSecDesc; }
	    set { this.usSecDesc = value; }
	}

	public StringType TroubledDebt {
	    get { return this.troubledDebt; }
	    set { this.troubledDebt = value; }
	}

	public StringType TdrDate {
	    get { return this.tdrDate; }
	    set { this.tdrDate = value; }
	}

	public StringType JntFicoUsed {
	    get { return this.jntFicoUsed; }
	    set { this.jntFicoUsed = value; }
	}

	public StringType SingleOrJointLoan {
	    get { return this.singleOrJointLoan; }
	    set { this.singleOrJointLoan = value; }
	}

	public StringType ApprovingOfficerUD {
	    get { return this.approvingOfficerUD; }
	    set { this.approvingOfficerUD = value; }
	}

	public StringType InsuranceCodeDesc {
	    get { return this.insuranceCodeDesc; }
	    set { this.insuranceCodeDesc = value; }
	}

	public StringType GapIns {
	    get { return this.gapIns; }
	    set { this.gapIns = value; }
	}

	public StringType MmpIns {
	    get { return this.mmpIns; }
	    set { this.mmpIns = value; }
	}

	public StringType ReportCodeDesc {
	    get { return this.reportCodeDesc; }
	    set { this.reportCodeDesc = value; }
	}

	public StringType BallonPmtLoan {
	    get { return this.ballonPmtLoan; }
	    set { this.ballonPmtLoan = value; }
	}

	public StringType FixedOrVariableRateLoan {
	    get { return this.fixedOrVariableRateLoan; }
	    set { this.fixedOrVariableRateLoan = value; }
	}

	public StringType AlpsOfficer {
	    get { return this.alpsOfficer; }
	    set { this.alpsOfficer = value; }
	}

	public StringType CUDLAppNumber {
	    get { return this.cUDLAppNumber; }
	    set { this.cUDLAppNumber = value; }
	}

	public CurrencyType FasbDefCost {
	    get { return this.fasbDefCost; }
	    set { this.fasbDefCost = value; }
	}

	public CurrencyType FasbLtdAmortFees {
	    get { return this.fasbLtdAmortFees; }
	    set { this.fasbLtdAmortFees = value; }
	}

	public CurrencyType InterestPriorYtd {
	    get { return this.interestPriorYtd; }
	    set { this.interestPriorYtd = value; }
	}

	public CurrencyType AccruedInterest {
	    get { return this.accruedInterest; }
	    set { this.accruedInterest = value; }
	}

	public CurrencyType InterestLTD {
	    get { return this.interestLTD; }
	    set { this.interestLTD = value; }
	}

	public CurrencyType InterestYtd {
	    get { return this.interestYtd; }
	    set { this.interestYtd = value; }
	}

	public CurrencyType PartialPayAmt {
	    get { return this.partialPayAmt; }
	    set { this.partialPayAmt = value; }
	}

	public CurrencyType InterestUncollected {
	    get { return this.interestUncollected; }
	    set { this.interestUncollected = value; }
	}

	public DateType ClosedAcctDate {
	    get { return this.closedAcctDate; }
	    set { this.closedAcctDate = value; }
	}

	public DateType LastTranDate {
	    get { return this.lastTranDate; }
	    set { this.lastTranDate = value; }
	}

	public DateType NextDueDate {
	    get { return this.nextDueDate; }
	    set { this.nextDueDate = value; }
	}

	public IntegerType InsuranceCode {
	    get { return this.insuranceCode; }
	    set { this.insuranceCode = value; }
	}

	public IntegerType MbrAgeAtApproval {
	    get { return this.mbrAgeAtApproval; }
	    set { this.mbrAgeAtApproval = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
