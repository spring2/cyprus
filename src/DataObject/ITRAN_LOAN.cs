using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class TRAN_LOANFields {
	private TRAN_LOANFields() {}
	public static readonly String ENTITY_NAME = "TRAN_LOAN";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_PREVIOUS_CREDIT_LIMIT = new ColumnMetaData("L_PREVIOUS_CREDIT_LIMIT", "L_PREVIOUS_CREDIT_LIMIT", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData L_REPAY_AMT = new ColumnMetaData("L_REPAY_AMT", "L_REPAY_AMT", DbType.Currency, SqlDbType.Money, 0, 0, 0);
    }

    public interface ITRAN_LOAN : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	CurrencyType L_PREVIOUS_CREDIT_LIMIT {
	    get;
	}
	CurrencyType L_REPAY_AMT {
	    get;
	}
    }
}
