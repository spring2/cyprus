using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class FICORangeFields {
	private FICORangeFields() {}
	public static readonly String ENTITY_NAME = "FICORange";

	public static readonly ColumnMetaData FICORANGEID = new ColumnMetaData("FICORangeId", "FICORangeId", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData VALUE = new ColumnMetaData("Value", "Value", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DESCRIPTION = new ColumnMetaData("Description", "Description", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface IFICORange : IBusinessEntity {
	IdType FICORangeId {
	    get;
	}
	IntegerType Value {
	    get;
	}
	StringType Description {
	    get;
	}
    }
}
