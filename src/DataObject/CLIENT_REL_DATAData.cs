using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class CLIENT_REL_DATAData : Spring2.Core.DataObject.DataObject {
	public static readonly CLIENT_REL_DATAData DEFAULT = new CLIENT_REL_DATAData();

	private IdType mEM_NUM = IdType.DEFAULT;
	private IntegerType aGE = IntegerType.DEFAULT;

	public IdType MEM_NUM {
	    get { return this.mEM_NUM; }
	    set { this.mEM_NUM = value; }
	}

	public IntegerType AGE {
	    get { return this.aGE; }
	    set { this.aGE = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
