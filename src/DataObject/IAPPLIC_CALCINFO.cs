using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class APPLIC_CALCINFOFields {
	private APPLIC_CALCINFOFields() {}
	public static readonly String ENTITY_NAME = "APPLIC_CALCINFO";

	public static readonly ColumnMetaData APPLIC_ID = new ColumnMetaData("APPLIC_ID", "APPLIC_ID", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DEBT_AFTER = new ColumnMetaData("DEBT_AFTER", "DEBT_AFTER", DbType.Decimal, SqlDbType.Decimal, 0, 18, 8);
    }

    public interface IAPPLIC_CALCINFO : IBusinessEntity {
	IdType APPLIC_ID {
	    get;
	}
	DecimalType DEBT_AFTER {
	    get;
	}
    }
}
