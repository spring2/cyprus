using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class DELINQUENCY_DATAFields {
	private DELINQUENCY_DATAFields() {}
	public static readonly String ENTITY_NAME = "DELINQUENCY_DATA";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DLQ_HIST = new ColumnMetaData("DLQ_HIST", "DLQ_HIST", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface IDELINQUENCY_DATA : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	StringType DLQ_HIST {
	    get;
	}
    }
}
