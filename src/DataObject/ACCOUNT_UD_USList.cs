using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IACCOUNT_UD_US generic collection
    /// </summary>
    public class ACCOUNT_UD_USList : CollectionBase {
	[Generate]
	public static readonly ACCOUNT_UD_USList UNSET = new ACCOUNT_UD_USList(true);
	[Generate]
	public static readonly ACCOUNT_UD_USList DEFAULT = new ACCOUNT_UD_USList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private ACCOUNT_UD_USList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public ACCOUNT_UD_USList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IACCOUNT_UD_US this[int index] {
	    get {
		return (IACCOUNT_UD_US)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IACCOUNT_UD_US value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IACCOUNT_UD_US value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IACCOUNT_UD_US value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IACCOUNT_UD_US) {
		    Add((IACCOUNT_UD_US)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IACCOUNT_UD_US");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IACCOUNT_UD_US this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as IACCOUNT_UD_US;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public ACCOUNT_UD_USList RetainAll(ACCOUNT_UD_USList list) {
	    ACCOUNT_UD_USList result = new ACCOUNT_UD_USList();

	    foreach (IACCOUNT_UD_US data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public ACCOUNT_UD_USList RemoveAll(ACCOUNT_UD_USList list) {
	    ACCOUNT_UD_USList result = new ACCOUNT_UD_USList();

	    foreach (IACCOUNT_UD_US data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		IACCOUNT_UD_US objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class US_INCOME_STATEDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_INCOME_STATED.IsValid || !o2.US_INCOME_STATED.IsValid) {
		    return 0;
		}
		return o1.US_INCOME_STATED.CompareTo(o2.US_INCOME_STATED);
	    }
	}

	[Generate]
	public class US_NUM_OF_EXTENSIONSSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_NUM_OF_EXTENSIONS.IsValid || !o2.US_NUM_OF_EXTENSIONS.IsValid) {
		    return 0;
		}
		return o1.US_NUM_OF_EXTENSIONS.CompareTo(o2.US_NUM_OF_EXTENSIONS);
	    }
	}

	[Generate]
	public class US_1ST_EXT_DTESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_1ST_EXT_DTE.IsValid || !o2.US_1ST_EXT_DTE.IsValid) {
		    return 0;
		}
		return o1.US_1ST_EXT_DTE.CompareTo(o2.US_1ST_EXT_DTE);
	    }
	}

	[Generate]
	public class US_MOSTRECENT_EXT_DTESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_MOSTRECENT_EXT_DTE.IsValid || !o2.US_MOSTRECENT_EXT_DTE.IsValid) {
		    return 0;
		}
		return o1.US_MOSTRECENT_EXT_DTE.CompareTo(o2.US_MOSTRECENT_EXT_DTE);
	    }
	}

	[Generate]
	public class US_L_RISKLEVSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_L_RISKLEV.IsValid || !o2.US_L_RISKLEV.IsValid) {
		    return 0;
		}
		return o1.US_L_RISKLEV.CompareTo(o2.US_L_RISKLEV);
	    }
	}

	[Generate]
	public class US_RISKLEVEL_DTESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_RISKLEVEL_DTE.IsValid || !o2.US_RISKLEVEL_DTE.IsValid) {
		    return 0;
		}
		return o1.US_RISKLEVEL_DTE.CompareTo(o2.US_RISKLEVEL_DTE);
	    }
	}

	[Generate]
	public class US_WATCHLISTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_WATCHLIST.IsValid || !o2.US_WATCHLIST.IsValid) {
		    return 0;
		}
		return o1.US_WATCHLIST.CompareTo(o2.US_WATCHLIST);
	    }
	}

	[Generate]
	public class US_DATE_WATCHLISTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_DATE_WATCHLIST.IsValid || !o2.US_DATE_WATCHLIST.IsValid) {
		    return 0;
		}
		return o1.US_DATE_WATCHLIST.CompareTo(o2.US_DATE_WATCHLIST);
	    }
	}

	[Generate]
	public class US_WORKOUT_LOANSSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_WORKOUT_LOANS.IsValid || !o2.US_WORKOUT_LOANS.IsValid) {
		    return 0;
		}
		return o1.US_WORKOUT_LOANS.CompareTo(o2.US_WORKOUT_LOANS);
	    }
	}

	[Generate]
	public class US_WORKOUT_DTESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_WORKOUT_DTE.IsValid || !o2.US_WORKOUT_DTE.IsValid) {
		    return 0;
		}
		return o1.US_WORKOUT_DTE.CompareTo(o2.US_WORKOUT_DTE);
	    }
	}

	[Generate]
	public class US_UD_BUS_LOAN_PER_REGSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_BUS_LOAN_PER_REG.IsValid || !o2.US_UD_BUS_LOAN_PER_REG.IsValid) {
		    return 0;
		}
		return o1.US_UD_BUS_LOAN_PER_REG.CompareTo(o2.US_UD_BUS_LOAN_PER_REG);
	    }
	}

	[Generate]
	public class US_UD_BUS_LOAN_COL_GRPSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_BUS_LOAN_COL_GRP.IsValid || !o2.US_UD_BUS_LOAN_COL_GRP.IsValid) {
		    return 0;
		}
		return o1.US_UD_BUS_LOAN_COL_GRP.CompareTo(o2.US_UD_BUS_LOAN_COL_GRP);
	    }
	}

	[Generate]
	public class US_UD_BUS_LOAN_COL_DESCSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_BUS_LOAN_COL_DESC.IsValid || !o2.US_UD_BUS_LOAN_COL_DESC.IsValid) {
		    return 0;
		}
		return o1.US_UD_BUS_LOAN_COL_DESC.CompareTo(o2.US_UD_BUS_LOAN_COL_DESC);
	    }
	}

	[Generate]
	public class US_UD_BUS_LOAN_PARTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_BUS_LOAN_PART.IsValid || !o2.US_UD_BUS_LOAN_PART.IsValid) {
		    return 0;
		}
		return o1.US_UD_BUS_LOAN_PART.CompareTo(o2.US_UD_BUS_LOAN_PART);
	    }
	}

	[Generate]
	public class US_UD_SBA_GUARANTYSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_SBA_GUARANTY.IsValid || !o2.US_UD_SBA_GUARANTY.IsValid) {
		    return 0;
		}
		return o1.US_UD_SBA_GUARANTY.CompareTo(o2.US_UD_SBA_GUARANTY);
	    }
	}

	[Generate]
	public class US_UD_1ST_MORT_TYPESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_1ST_MORT_TYPE.IsValid || !o2.US_UD_1ST_MORT_TYPE.IsValid) {
		    return 0;
		}
		return o1.US_UD_1ST_MORT_TYPE.CompareTo(o2.US_UD_1ST_MORT_TYPE);
	    }
	}

	[Generate]
	public class US_UD_BUS_LOAN_PRCNT_PARTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_UD_US o1 = (IACCOUNT_UD_US)a;
		IACCOUNT_UD_US o2 = (IACCOUNT_UD_US)b;

		if (o1 == null || o2 == null || !o1.US_UD_BUS_LOAN_PRCNT_PART.IsValid || !o2.US_UD_BUS_LOAN_PRCNT_PART.IsValid) {
		    return 0;
		}
		return o1.US_UD_BUS_LOAN_PRCNT_PART.CompareTo(o2.US_UD_BUS_LOAN_PRCNT_PART);
	    }
	}
    }
}
