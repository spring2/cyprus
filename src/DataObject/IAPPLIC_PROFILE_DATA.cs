using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class APPLIC_PROFILE_DATAFields {
	private APPLIC_PROFILE_DATAFields() {}
	public static readonly String ENTITY_NAME = "APPLIC_PROFILE_DATA";

	public static readonly ColumnMetaData APPLIC_NUM = new ColumnMetaData("APPLIC_NUM", "APPLIC_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData NO_YRS_ADDR = new ColumnMetaData("NO_YRS_ADDR", "NO_YRS_ADDR", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData PREV_YRS_ADDR = new ColumnMetaData("PREV_YRS_ADDR", "PREV_YRS_ADDR", DbType.Int32, SqlDbType.Int, 0, 10, 0);
    }

    public interface IAPPLIC_PROFILE_DATA : IBusinessEntity {
	IdType APPLIC_NUM {
	    get;
	}
	IntegerType NO_YRS_ADDR {
	    get;
	}
	IntegerType PREV_YRS_ADDR {
	    get;
	}
    }
}
