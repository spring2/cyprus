using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class PROFILE_EMPLOYER_INFOFields {
	private PROFILE_EMPLOYER_INFOFields() {}
	public static readonly String ENTITY_NAME = "PROFILE_EMPLOYER_INFO";

	public static readonly ColumnMetaData RECORD_KEY = new ColumnMetaData("RECORD_KEY", "RECORD_KEY", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData EMP_GROSS_INCOME = new ColumnMetaData("EMP_GROSS_INCOME", "EMP_GROSS_INCOME", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData EMP_YRS_PROF = new ColumnMetaData("EMP_YRS_PROF", "EMP_YRS_PROF", DbType.Int32, SqlDbType.Int, 0, 10, 0);
    }

    public interface IPROFILE_EMPLOYER_INFO : IBusinessEntity {
	IdType RECORD_KEY {
	    get;
	}
	CurrencyType EMP_GROSS_INCOME {
	    get;
	}
	IntegerType EMP_YRS_PROF {
	    get;
	}
    }
}
