using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IDELINQUENCY_DATA generic collection
    /// </summary>
    public class DELINQUENCY_DATAList : CollectionBase {
	[Generate]
	public static readonly DELINQUENCY_DATAList UNSET = new DELINQUENCY_DATAList(true);
	[Generate]
	public static readonly DELINQUENCY_DATAList DEFAULT = new DELINQUENCY_DATAList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private DELINQUENCY_DATAList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public DELINQUENCY_DATAList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IDELINQUENCY_DATA this[int index] {
	    get {
		return (IDELINQUENCY_DATA)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IDELINQUENCY_DATA value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IDELINQUENCY_DATA value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IDELINQUENCY_DATA value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IDELINQUENCY_DATA) {
		    Add((IDELINQUENCY_DATA)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IDELINQUENCY_DATA");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IDELINQUENCY_DATA this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as IDELINQUENCY_DATA;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public DELINQUENCY_DATAList RetainAll(DELINQUENCY_DATAList list) {
	    DELINQUENCY_DATAList result = new DELINQUENCY_DATAList();

	    foreach (IDELINQUENCY_DATA data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public DELINQUENCY_DATAList RemoveAll(DELINQUENCY_DATAList list) {
	    DELINQUENCY_DATAList result = new DELINQUENCY_DATAList();

	    foreach (IDELINQUENCY_DATA data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		IDELINQUENCY_DATA objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IDELINQUENCY_DATA o1 = (IDELINQUENCY_DATA)a;
		IDELINQUENCY_DATA o2 = (IDELINQUENCY_DATA)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class DLQ_HISTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IDELINQUENCY_DATA o1 = (IDELINQUENCY_DATA)a;
		IDELINQUENCY_DATA o2 = (IDELINQUENCY_DATA)b;

		if (o1 == null || o2 == null || !o1.DLQ_HIST.IsValid || !o2.DLQ_HIST.IsValid) {
		    return 0;
		}
		return o1.DLQ_HIST.CompareTo(o2.DLQ_HIST);
	    }
	}
    }
}
