using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ITRAN_LOAN generic collection
    /// </summary>
    public class TRAN_LOANList : CollectionBase {
	[Generate]
	public static readonly TRAN_LOANList UNSET = new TRAN_LOANList(true);
	[Generate]
	public static readonly TRAN_LOANList DEFAULT = new TRAN_LOANList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private TRAN_LOANList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public TRAN_LOANList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ITRAN_LOAN this[int index] {
	    get {
		return (ITRAN_LOAN)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ITRAN_LOAN value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ITRAN_LOAN value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ITRAN_LOAN value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ITRAN_LOAN) {
		    Add((ITRAN_LOAN)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ITRAN_LOAN");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ITRAN_LOAN this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as ITRAN_LOAN;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public TRAN_LOANList RetainAll(TRAN_LOANList list) {
	    TRAN_LOANList result = new TRAN_LOANList();

	    foreach (ITRAN_LOAN data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public TRAN_LOANList RemoveAll(TRAN_LOANList list) {
	    TRAN_LOANList result = new TRAN_LOANList();

	    foreach (ITRAN_LOAN data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		ITRAN_LOAN objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_LOAN o1 = (ITRAN_LOAN)a;
		ITRAN_LOAN o2 = (ITRAN_LOAN)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class L_PREVIOUS_CREDIT_LIMITSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_LOAN o1 = (ITRAN_LOAN)a;
		ITRAN_LOAN o2 = (ITRAN_LOAN)b;

		if (o1 == null || o2 == null || !o1.L_PREVIOUS_CREDIT_LIMIT.IsValid || !o2.L_PREVIOUS_CREDIT_LIMIT.IsValid) {
		    return 0;
		}
		return o1.L_PREVIOUS_CREDIT_LIMIT.CompareTo(o2.L_PREVIOUS_CREDIT_LIMIT);
	    }
	}

	[Generate]
	public class L_REPAY_AMTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_LOAN o1 = (ITRAN_LOAN)a;
		ITRAN_LOAN o2 = (ITRAN_LOAN)b;

		if (o1 == null || o2 == null || !o1.L_REPAY_AMT.IsValid || !o2.L_REPAY_AMT.IsValid) {
		    return 0;
		}
		return o1.L_REPAY_AMT.CompareTo(o2.L_REPAY_AMT);
	    }
	}
    }
}
