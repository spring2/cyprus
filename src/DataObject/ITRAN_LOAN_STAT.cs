using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class TRAN_LOAN_STATFields {
	private TRAN_LOAN_STATFields() {}
	public static readonly String ENTITY_NAME = "TRAN_LOAN_STAT";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_STAT_CHANGE_ACTUAL_DATE = new ColumnMetaData("L_STAT_CHANGE_ACTUAL_DATE", "L_STAT_CHANGE_ACTUAL_DATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
    }

    public interface ITRAN_LOAN_STAT : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	DateTimeType L_STAT_CHANGE_ACTUAL_DATE {
	    get;
	}
    }
}
