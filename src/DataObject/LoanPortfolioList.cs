using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ILoanPortfolio generic collection
    /// </summary>
    public class LoanPortfolioList : CollectionBase {
	[Generate]
	public static readonly LoanPortfolioList UNSET = new LoanPortfolioList(true);
	[Generate]
	public static readonly LoanPortfolioList DEFAULT = new LoanPortfolioList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private LoanPortfolioList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public LoanPortfolioList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ILoanPortfolio this[int index] {
	    get {
		return (ILoanPortfolio)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.LoanPortfolioId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ILoanPortfolio value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.LoanPortfolioId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ILoanPortfolio value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.LoanPortfolioId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ILoanPortfolio value = this[index];
		    keys.Remove(value.LoanPortfolioId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ILoanPortfolio) {
		    Add((ILoanPortfolio)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ILoanPortfolio");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType loanPortfolioId) {
	    return keys.Contains(loanPortfolioId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ILoanPortfolio this[IdType loanPortfolioId] {
	    get {
		return keys[loanPortfolioId] as ILoanPortfolio;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public LoanPortfolioList RetainAll(LoanPortfolioList list) {
	    LoanPortfolioList result = new LoanPortfolioList();

	    foreach (ILoanPortfolio data in List) {
		if (list.Contains(data.LoanPortfolioId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public LoanPortfolioList RemoveAll(LoanPortfolioList list) {
	    LoanPortfolioList result = new LoanPortfolioList();

	    foreach (ILoanPortfolio data in List) {
		if (!list.Contains(data.LoanPortfolioId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType loanPortfolioId) {
	    if (!immutable) {
		ILoanPortfolio objectInList = this[loanPortfolioId];
		List.Remove(objectInList);
		keys.Remove(loanPortfolioId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class LoanPortfolioIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoanPortfolioId.IsValid || !o2.LoanPortfolioId.IsValid) {
		    return 0;
		}
		return o1.LoanPortfolioId.CompareTo(o2.LoanPortfolioId);
	    }
	}

	[Generate]
	public class UpDatedSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.UpDated.IsValid || !o2.UpDated.IsValid) {
		    return 0;
		}
		return o1.UpDated.CompareTo(o2.UpDated);
	    }
	}

	[Generate]
	public class EOMDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.EOMDate.IsValid || !o2.EOMDate.IsValid) {
		    return 0;
		}
		return o1.EOMDate.CompareTo(o2.EOMDate);
	    }
	}

	[Generate]
	public class AccountIDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.AccountID.IsValid || !o2.AccountID.IsValid) {
		    return 0;
		}
		return o1.AccountID.CompareTo(o2.AccountID);
	    }
	}

	[Generate]
	public class MemberSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Member.IsValid || !o2.Member.IsValid) {
		    return 0;
		}
		return o1.Member.CompareTo(o2.Member);
	    }
	}

	[Generate]
	public class MClassSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MClass.IsValid || !o2.MClass.IsValid) {
		    return 0;
		}
		return o1.MClass.CompareTo(o2.MClass);
	    }
	}

	[Generate]
	public class LtypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Ltype.IsValid || !o2.Ltype.IsValid) {
		    return 0;
		}
		return o1.Ltype.CompareTo(o2.Ltype);
	    }
	}

	[Generate]
	public class LtypeSubSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LtypeSub.IsValid || !o2.LtypeSub.IsValid) {
		    return 0;
		}
		return o1.LtypeSub.CompareTo(o2.LtypeSub);
	    }
	}

	[Generate]
	public class ProductSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Product.IsValid || !o2.Product.IsValid) {
		    return 0;
		}
		return o1.Product.CompareTo(o2.Product);
	    }
	}

	[Generate]
	public class LoanDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoanDesc.IsValid || !o2.LoanDesc.IsValid) {
		    return 0;
		}
		return o1.LoanDesc.CompareTo(o2.LoanDesc);
	    }
	}

	[Generate]
	public class LoanDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoanDate.IsValid || !o2.LoanDate.IsValid) {
		    return 0;
		}
		return o1.LoanDate.CompareTo(o2.LoanDate);
	    }
	}

	[Generate]
	public class LoanDaySorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoanDay.IsValid || !o2.LoanDay.IsValid) {
		    return 0;
		}
		return o1.LoanDay.CompareTo(o2.LoanDay);
	    }
	}

	[Generate]
	public class LoanMonthSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoanMonth.IsValid || !o2.LoanMonth.IsValid) {
		    return 0;
		}
		return o1.LoanMonth.CompareTo(o2.LoanMonth);
	    }
	}

	[Generate]
	public class LoanYearSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoanYear.IsValid || !o2.LoanYear.IsValid) {
		    return 0;
		}
		return o1.LoanYear.CompareTo(o2.LoanYear);
	    }
	}

	[Generate]
	public class FirstPmtDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FirstPmtDate.IsValid || !o2.FirstPmtDate.IsValid) {
		    return 0;
		}
		return o1.FirstPmtDate.CompareTo(o2.FirstPmtDate);
	    }
	}

	[Generate]
	public class StatusSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Status.IsValid || !o2.Status.IsValid) {
		    return 0;
		}
		return o1.Status.CompareTo(o2.Status);
	    }
	}

	[Generate]
	public class RptCodeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RptCode.IsValid || !o2.RptCode.IsValid) {
		    return 0;
		}
		return o1.RptCode.CompareTo(o2.RptCode);
	    }
	}

	[Generate]
	public class FundedBrSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FundedBr.IsValid || !o2.FundedBr.IsValid) {
		    return 0;
		}
		return o1.FundedBr.CompareTo(o2.FundedBr);
	    }
	}

	[Generate]
	public class LoadedBrSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoadedBr.IsValid || !o2.LoadedBr.IsValid) {
		    return 0;
		}
		return o1.LoadedBr.CompareTo(o2.LoadedBr);
	    }
	}

	[Generate]
	public class FICOSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FICO.IsValid || !o2.FICO.IsValid) {
		    return 0;
		}
		return o1.FICO.CompareTo(o2.FICO);
	    }
	}

	[Generate]
	public class FICORangeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FICORange.IsValid || !o2.FICORange.IsValid) {
		    return 0;
		}
		return o1.FICORange.CompareTo(o2.FICORange);
	    }
	}

	[Generate]
	public class FastStartSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FastStart.IsValid || !o2.FastStart.IsValid) {
		    return 0;
		}
		return o1.FastStart.CompareTo(o2.FastStart);
	    }
	}

	[Generate]
	public class FastStartRangeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FastStartRange.IsValid || !o2.FastStartRange.IsValid) {
		    return 0;
		}
		return o1.FastStartRange.CompareTo(o2.FastStartRange);
	    }
	}

	[Generate]
	public class BankruptcySorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Bankruptcy.IsValid || !o2.Bankruptcy.IsValid) {
		    return 0;
		}
		return o1.Bankruptcy.CompareTo(o2.Bankruptcy);
	    }
	}

	[Generate]
	public class BankruptcyRangeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BankruptcyRange.IsValid || !o2.BankruptcyRange.IsValid) {
		    return 0;
		}
		return o1.BankruptcyRange.CompareTo(o2.BankruptcyRange);
	    }
	}

	[Generate]
	public class MembershipDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MembershipDate.IsValid || !o2.MembershipDate.IsValid) {
		    return 0;
		}
		return o1.MembershipDate.CompareTo(o2.MembershipDate);
	    }
	}

	[Generate]
	public class NewMemberSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.NewMember.IsValid || !o2.NewMember.IsValid) {
		    return 0;
		}
		return o1.NewMember.CompareTo(o2.NewMember);
	    }
	}

	[Generate]
	public class RiskLevSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RiskLev.IsValid || !o2.RiskLev.IsValid) {
		    return 0;
		}
		return o1.RiskLev.CompareTo(o2.RiskLev);
	    }
	}

	[Generate]
	public class TrafficSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Traffic.IsValid || !o2.Traffic.IsValid) {
		    return 0;
		}
		return o1.Traffic.CompareTo(o2.Traffic);
	    }
	}

	[Generate]
	public class TrafficColorSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.TrafficColor.IsValid || !o2.TrafficColor.IsValid) {
		    return 0;
		}
		return o1.TrafficColor.CompareTo(o2.TrafficColor);
	    }
	}

	[Generate]
	public class MemAgeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MemAge.IsValid || !o2.MemAge.IsValid) {
		    return 0;
		}
		return o1.MemAge.CompareTo(o2.MemAge);
	    }
	}

	[Generate]
	public class MemSexSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MemSex.IsValid || !o2.MemSex.IsValid) {
		    return 0;
		}
		return o1.MemSex.CompareTo(o2.MemSex);
	    }
	}

	[Generate]
	public class PmtFreqSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.PmtFreq.IsValid || !o2.PmtFreq.IsValid) {
		    return 0;
		}
		return o1.PmtFreq.CompareTo(o2.PmtFreq);
	    }
	}

	[Generate]
	public class OrigTermSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigTerm.IsValid || !o2.OrigTerm.IsValid) {
		    return 0;
		}
		return o1.OrigTerm.CompareTo(o2.OrigTerm);
	    }
	}

	[Generate]
	public class OrigRateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigRate.IsValid || !o2.OrigRate.IsValid) {
		    return 0;
		}
		return o1.OrigRate.CompareTo(o2.OrigRate);
	    }
	}

	[Generate]
	public class OrigPmtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigPmt.IsValid || !o2.OrigPmt.IsValid) {
		    return 0;
		}
		return o1.OrigPmt.CompareTo(o2.OrigPmt);
	    }
	}

	[Generate]
	public class OrigAmountSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigAmount.IsValid || !o2.OrigAmount.IsValid) {
		    return 0;
		}
		return o1.OrigAmount.CompareTo(o2.OrigAmount);
	    }
	}

	[Generate]
	public class OrigCrLimSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigCrLim.IsValid || !o2.OrigCrLim.IsValid) {
		    return 0;
		}
		return o1.OrigCrLim.CompareTo(o2.OrigCrLim);
	    }
	}

	[Generate]
	public class CurTermSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurTerm.IsValid || !o2.CurTerm.IsValid) {
		    return 0;
		}
		return o1.CurTerm.CompareTo(o2.CurTerm);
	    }
	}

	[Generate]
	public class CurRateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurRate.IsValid || !o2.CurRate.IsValid) {
		    return 0;
		}
		return o1.CurRate.CompareTo(o2.CurRate);
	    }
	}

	[Generate]
	public class CurPmtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurPmt.IsValid || !o2.CurPmt.IsValid) {
		    return 0;
		}
		return o1.CurPmt.CompareTo(o2.CurPmt);
	    }
	}

	[Generate]
	public class CurBalSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurBal.IsValid || !o2.CurBal.IsValid) {
		    return 0;
		}
		return o1.CurBal.CompareTo(o2.CurBal);
	    }
	}

	[Generate]
	public class CurCrLimSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurCrLim.IsValid || !o2.CurCrLim.IsValid) {
		    return 0;
		}
		return o1.CurCrLim.CompareTo(o2.CurCrLim);
	    }
	}

	[Generate]
	public class BalloonDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BalloonDate.IsValid || !o2.BalloonDate.IsValid) {
		    return 0;
		}
		return o1.BalloonDate.CompareTo(o2.BalloonDate);
	    }
	}

	[Generate]
	public class MatDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MatDate.IsValid || !o2.MatDate.IsValid) {
		    return 0;
		}
		return o1.MatDate.CompareTo(o2.MatDate);
	    }
	}

	[Generate]
	public class InsCodeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InsCode.IsValid || !o2.InsCode.IsValid) {
		    return 0;
		}
		return o1.InsCode.CompareTo(o2.InsCode);
	    }
	}

	[Generate]
	public class CollateralValueSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CollateralValue.IsValid || !o2.CollateralValue.IsValid) {
		    return 0;
		}
		return o1.CollateralValue.CompareTo(o2.CollateralValue);
	    }
	}

	[Generate]
	public class LTVSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LTV.IsValid || !o2.LTV.IsValid) {
		    return 0;
		}
		return o1.LTV.CompareTo(o2.LTV);
	    }
	}

	[Generate]
	public class SecTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SecType.IsValid || !o2.SecType.IsValid) {
		    return 0;
		}
		return o1.SecType.CompareTo(o2.SecType);
	    }
	}

	[Generate]
	public class SecDesc1Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SecDesc1.IsValid || !o2.SecDesc1.IsValid) {
		    return 0;
		}
		return o1.SecDesc1.CompareTo(o2.SecDesc1);
	    }
	}

	[Generate]
	public class SecDesc2Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SecDesc2.IsValid || !o2.SecDesc2.IsValid) {
		    return 0;
		}
		return o1.SecDesc2.CompareTo(o2.SecDesc2);
	    }
	}

	[Generate]
	public class DebtRatioSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DebtRatio.IsValid || !o2.DebtRatio.IsValid) {
		    return 0;
		}
		return o1.DebtRatio.CompareTo(o2.DebtRatio);
	    }
	}

	[Generate]
	public class DealerSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Dealer.IsValid || !o2.Dealer.IsValid) {
		    return 0;
		}
		return o1.Dealer.CompareTo(o2.Dealer);
	    }
	}

	[Generate]
	public class DealerNameSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DealerName.IsValid || !o2.DealerName.IsValid) {
		    return 0;
		}
		return o1.DealerName.CompareTo(o2.DealerName);
	    }
	}

	[Generate]
	public class CoborComakSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CoborComak.IsValid || !o2.CoborComak.IsValid) {
		    return 0;
		}
		return o1.CoborComak.CompareTo(o2.CoborComak);
	    }
	}

	[Generate]
	public class RelPriceGrpSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RelPriceGrp.IsValid || !o2.RelPriceGrp.IsValid) {
		    return 0;
		}
		return o1.RelPriceGrp.CompareTo(o2.RelPriceGrp);
	    }
	}

	[Generate]
	public class RelPriceDiscSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RelPriceDisc.IsValid || !o2.RelPriceDisc.IsValid) {
		    return 0;
		}
		return o1.RelPriceDisc.CompareTo(o2.RelPriceDisc);
	    }
	}

	[Generate]
	public class OtherDiscSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OtherDisc.IsValid || !o2.OtherDisc.IsValid) {
		    return 0;
		}
		return o1.OtherDisc.CompareTo(o2.OtherDisc);
	    }
	}

	[Generate]
	public class DiscReasonSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DiscReason.IsValid || !o2.DiscReason.IsValid) {
		    return 0;
		}
		return o1.DiscReason.CompareTo(o2.DiscReason);
	    }
	}

	[Generate]
	public class RiskOffsetSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RiskOffset.IsValid || !o2.RiskOffset.IsValid) {
		    return 0;
		}
		return o1.RiskOffset.CompareTo(o2.RiskOffset);
	    }
	}

	[Generate]
	public class CreditTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CreditType.IsValid || !o2.CreditType.IsValid) {
		    return 0;
		}
		return o1.CreditType.CompareTo(o2.CreditType);
	    }
	}

	[Generate]
	public class StatedIncomeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.StatedIncome.IsValid || !o2.StatedIncome.IsValid) {
		    return 0;
		}
		return o1.StatedIncome.CompareTo(o2.StatedIncome);
	    }
	}

	[Generate]
	public class ExtensionsSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Extensions.IsValid || !o2.Extensions.IsValid) {
		    return 0;
		}
		return o1.Extensions.CompareTo(o2.Extensions);
	    }
	}

	[Generate]
	public class FirstExtDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FirstExtDate.IsValid || !o2.FirstExtDate.IsValid) {
		    return 0;
		}
		return o1.FirstExtDate.CompareTo(o2.FirstExtDate);
	    }
	}

	[Generate]
	public class LastExtDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LastExtDate.IsValid || !o2.LastExtDate.IsValid) {
		    return 0;
		}
		return o1.LastExtDate.CompareTo(o2.LastExtDate);
	    }
	}

	[Generate]
	public class RiskLevelSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RiskLevel.IsValid || !o2.RiskLevel.IsValid) {
		    return 0;
		}
		return o1.RiskLevel.CompareTo(o2.RiskLevel);
	    }
	}

	[Generate]
	public class RiskDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RiskDate.IsValid || !o2.RiskDate.IsValid) {
		    return 0;
		}
		return o1.RiskDate.CompareTo(o2.RiskDate);
	    }
	}

	[Generate]
	public class WatchSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Watch.IsValid || !o2.Watch.IsValid) {
		    return 0;
		}
		return o1.Watch.CompareTo(o2.Watch);
	    }
	}

	[Generate]
	public class WatchDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.WatchDate.IsValid || !o2.WatchDate.IsValid) {
		    return 0;
		}
		return o1.WatchDate.CompareTo(o2.WatchDate);
	    }
	}

	[Generate]
	public class WorkOutSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.WorkOut.IsValid || !o2.WorkOut.IsValid) {
		    return 0;
		}
		return o1.WorkOut.CompareTo(o2.WorkOut);
	    }
	}

	[Generate]
	public class WorkOutDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.WorkOutDate.IsValid || !o2.WorkOutDate.IsValid) {
		    return 0;
		}
		return o1.WorkOutDate.CompareTo(o2.WorkOutDate);
	    }
	}

	[Generate]
	public class FirstMortTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FirstMortType.IsValid || !o2.FirstMortType.IsValid) {
		    return 0;
		}
		return o1.FirstMortType.CompareTo(o2.FirstMortType);
	    }
	}

	[Generate]
	public class BusPerRegSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BusPerReg.IsValid || !o2.BusPerReg.IsValid) {
		    return 0;
		}
		return o1.BusPerReg.CompareTo(o2.BusPerReg);
	    }
	}

	[Generate]
	public class BusGroupSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BusGroup.IsValid || !o2.BusGroup.IsValid) {
		    return 0;
		}
		return o1.BusGroup.CompareTo(o2.BusGroup);
	    }
	}

	[Generate]
	public class BusDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BusDesc.IsValid || !o2.BusDesc.IsValid) {
		    return 0;
		}
		return o1.BusDesc.CompareTo(o2.BusDesc);
	    }
	}

	[Generate]
	public class BusPartSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BusPart.IsValid || !o2.BusPart.IsValid) {
		    return 0;
		}
		return o1.BusPart.CompareTo(o2.BusPart);
	    }
	}

	[Generate]
	public class BusPartPercSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BusPartPerc.IsValid || !o2.BusPartPerc.IsValid) {
		    return 0;
		}
		return o1.BusPartPerc.CompareTo(o2.BusPartPerc);
	    }
	}

	[Generate]
	public class SBAguarSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SBAguar.IsValid || !o2.SBAguar.IsValid) {
		    return 0;
		}
		return o1.SBAguar.CompareTo(o2.SBAguar);
	    }
	}

	[Generate]
	public class DeliDaysSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliDays.IsValid || !o2.DeliDays.IsValid) {
		    return 0;
		}
		return o1.DeliDays.CompareTo(o2.DeliDays);
	    }
	}

	[Generate]
	public class DeliSectSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliSect.IsValid || !o2.DeliSect.IsValid) {
		    return 0;
		}
		return o1.DeliSect.CompareTo(o2.DeliSect);
	    }
	}

	[Generate]
	public class DeliHistSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHist.IsValid || !o2.DeliHist.IsValid) {
		    return 0;
		}
		return o1.DeliHist.CompareTo(o2.DeliHist);
	    }
	}

	[Generate]
	public class DeliHistQ1Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ1.IsValid || !o2.DeliHistQ1.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ1.CompareTo(o2.DeliHistQ1);
	    }
	}

	[Generate]
	public class DeliHistQ2Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ2.IsValid || !o2.DeliHistQ2.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ2.CompareTo(o2.DeliHistQ2);
	    }
	}

	[Generate]
	public class DeliHistQ3Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ3.IsValid || !o2.DeliHistQ3.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ3.CompareTo(o2.DeliHistQ3);
	    }
	}

	[Generate]
	public class DeliHistQ4Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ4.IsValid || !o2.DeliHistQ4.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ4.CompareTo(o2.DeliHistQ4);
	    }
	}

	[Generate]
	public class DeliHistQ5Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ5.IsValid || !o2.DeliHistQ5.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ5.CompareTo(o2.DeliHistQ5);
	    }
	}

	[Generate]
	public class DeliHistQ6Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ6.IsValid || !o2.DeliHistQ6.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ6.CompareTo(o2.DeliHistQ6);
	    }
	}

	[Generate]
	public class DeliHistQ7Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ7.IsValid || !o2.DeliHistQ7.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ7.CompareTo(o2.DeliHistQ7);
	    }
	}

	[Generate]
	public class DeliHistQ8Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliHistQ8.IsValid || !o2.DeliHistQ8.IsValid) {
		    return 0;
		}
		return o1.DeliHistQ8.CompareTo(o2.DeliHistQ8);
	    }
	}

	[Generate]
	public class ChrgOffDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ChrgOffDate.IsValid || !o2.ChrgOffDate.IsValid) {
		    return 0;
		}
		return o1.ChrgOffDate.CompareTo(o2.ChrgOffDate);
	    }
	}

	[Generate]
	public class ChrgOffMonthSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ChrgOffMonth.IsValid || !o2.ChrgOffMonth.IsValid) {
		    return 0;
		}
		return o1.ChrgOffMonth.CompareTo(o2.ChrgOffMonth);
	    }
	}

	[Generate]
	public class ChrgOffYearSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ChrgOffYear.IsValid || !o2.ChrgOffYear.IsValid) {
		    return 0;
		}
		return o1.ChrgOffYear.CompareTo(o2.ChrgOffYear);
	    }
	}

	[Generate]
	public class ChrgOffMnthsBFSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ChrgOffMnthsBF.IsValid || !o2.ChrgOffMnthsBF.IsValid) {
		    return 0;
		}
		return o1.ChrgOffMnthsBF.CompareTo(o2.ChrgOffMnthsBF);
	    }
	}

	[Generate]
	public class PropTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.PropType.IsValid || !o2.PropType.IsValid) {
		    return 0;
		}
		return o1.PropType.CompareTo(o2.PropType);
	    }
	}

	[Generate]
	public class OccupancySorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Occupancy.IsValid || !o2.Occupancy.IsValid) {
		    return 0;
		}
		return o1.Occupancy.CompareTo(o2.Occupancy);
	    }
	}

	[Generate]
	public class OrigApprValSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigApprVal.IsValid || !o2.OrigApprVal.IsValid) {
		    return 0;
		}
		return o1.OrigApprVal.CompareTo(o2.OrigApprVal);
	    }
	}

	[Generate]
	public class OrigApprDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.OrigApprDate.IsValid || !o2.OrigApprDate.IsValid) {
		    return 0;
		}
		return o1.OrigApprDate.CompareTo(o2.OrigApprDate);
	    }
	}

	[Generate]
	public class CurrApprValSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurrApprVal.IsValid || !o2.CurrApprVal.IsValid) {
		    return 0;
		}
		return o1.CurrApprVal.CompareTo(o2.CurrApprVal);
	    }
	}

	[Generate]
	public class CurrApprDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CurrApprDate.IsValid || !o2.CurrApprDate.IsValid) {
		    return 0;
		}
		return o1.CurrApprDate.CompareTo(o2.CurrApprDate);
	    }
	}

	[Generate]
	public class FirstMortBalSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FirstMortBal.IsValid || !o2.FirstMortBal.IsValid) {
		    return 0;
		}
		return o1.FirstMortBal.CompareTo(o2.FirstMortBal);
	    }
	}

	[Generate]
	public class FirstMortMoPytSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FirstMortMoPyt.IsValid || !o2.FirstMortMoPyt.IsValid) {
		    return 0;
		}
		return o1.FirstMortMoPyt.CompareTo(o2.FirstMortMoPyt);
	    }
	}

	[Generate]
	public class SecondMortBalSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SecondMortBal.IsValid || !o2.SecondMortBal.IsValid) {
		    return 0;
		}
		return o1.SecondMortBal.CompareTo(o2.SecondMortBal);
	    }
	}

	[Generate]
	public class SecondMortPymtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SecondMortPymt.IsValid || !o2.SecondMortPymt.IsValid) {
		    return 0;
		}
		return o1.SecondMortPymt.CompareTo(o2.SecondMortPymt);
	    }
	}

	[Generate]
	public class TaxInsNotIncSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.TaxInsNotInc.IsValid || !o2.TaxInsNotInc.IsValid) {
		    return 0;
		}
		return o1.TaxInsNotInc.CompareTo(o2.TaxInsNotInc);
	    }
	}

	[Generate]
	public class LoadOffSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoadOff.IsValid || !o2.LoadOff.IsValid) {
		    return 0;
		}
		return o1.LoadOff.CompareTo(o2.LoadOff);
	    }
	}

	[Generate]
	public class ApprOffSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ApprOff.IsValid || !o2.ApprOff.IsValid) {
		    return 0;
		}
		return o1.ApprOff.CompareTo(o2.ApprOff);
	    }
	}

	[Generate]
	public class FundingOppSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FundingOpp.IsValid || !o2.FundingOpp.IsValid) {
		    return 0;
		}
		return o1.FundingOpp.CompareTo(o2.FundingOpp);
	    }
	}

	[Generate]
	public class ChanelSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Chanel.IsValid || !o2.Chanel.IsValid) {
		    return 0;
		}
		return o1.Chanel.CompareTo(o2.Chanel);
	    }
	}

	[Generate]
	public class RiskTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.RiskType.IsValid || !o2.RiskType.IsValid) {
		    return 0;
		}
		return o1.RiskType.CompareTo(o2.RiskType);
	    }
	}

	[Generate]
	public class GapSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.Gap.IsValid || !o2.Gap.IsValid) {
		    return 0;
		}
		return o1.Gap.CompareTo(o2.Gap);
	    }
	}

	[Generate]
	public class LoadOperatorSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LoadOperator.IsValid || !o2.LoadOperator.IsValid) {
		    return 0;
		}
		return o1.LoadOperator.CompareTo(o2.LoadOperator);
	    }
	}

	[Generate]
	public class DeliAmtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.DeliAmt.IsValid || !o2.DeliAmt.IsValid) {
		    return 0;
		}
		return o1.DeliAmt.CompareTo(o2.DeliAmt);
	    }
	}

	[Generate]
	public class ChgOffAmtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ChgOffAmt.IsValid || !o2.ChgOffAmt.IsValid) {
		    return 0;
		}
		return o1.ChgOffAmt.CompareTo(o2.ChgOffAmt);
	    }
	}

	[Generate]
	public class UsSecDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.UsSecDesc.IsValid || !o2.UsSecDesc.IsValid) {
		    return 0;
		}
		return o1.UsSecDesc.CompareTo(o2.UsSecDesc);
	    }
	}

	[Generate]
	public class TroubledDebtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.TroubledDebt.IsValid || !o2.TroubledDebt.IsValid) {
		    return 0;
		}
		return o1.TroubledDebt.CompareTo(o2.TroubledDebt);
	    }
	}

	[Generate]
	public class TdrDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.TdrDate.IsValid || !o2.TdrDate.IsValid) {
		    return 0;
		}
		return o1.TdrDate.CompareTo(o2.TdrDate);
	    }
	}

	[Generate]
	public class JntFicoUsedSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.JntFicoUsed.IsValid || !o2.JntFicoUsed.IsValid) {
		    return 0;
		}
		return o1.JntFicoUsed.CompareTo(o2.JntFicoUsed);
	    }
	}

	[Generate]
	public class SingleOrJointLoanSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.SingleOrJointLoan.IsValid || !o2.SingleOrJointLoan.IsValid) {
		    return 0;
		}
		return o1.SingleOrJointLoan.CompareTo(o2.SingleOrJointLoan);
	    }
	}

	[Generate]
	public class ApprovingOfficerUDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ApprovingOfficerUD.IsValid || !o2.ApprovingOfficerUD.IsValid) {
		    return 0;
		}
		return o1.ApprovingOfficerUD.CompareTo(o2.ApprovingOfficerUD);
	    }
	}

	[Generate]
	public class InsuranceCodeDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InsuranceCodeDesc.IsValid || !o2.InsuranceCodeDesc.IsValid) {
		    return 0;
		}
		return o1.InsuranceCodeDesc.CompareTo(o2.InsuranceCodeDesc);
	    }
	}

	[Generate]
	public class GapInsSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.GapIns.IsValid || !o2.GapIns.IsValid) {
		    return 0;
		}
		return o1.GapIns.CompareTo(o2.GapIns);
	    }
	}

	[Generate]
	public class MmpInsSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MmpIns.IsValid || !o2.MmpIns.IsValid) {
		    return 0;
		}
		return o1.MmpIns.CompareTo(o2.MmpIns);
	    }
	}

	[Generate]
	public class ReportCodeDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ReportCodeDesc.IsValid || !o2.ReportCodeDesc.IsValid) {
		    return 0;
		}
		return o1.ReportCodeDesc.CompareTo(o2.ReportCodeDesc);
	    }
	}

	[Generate]
	public class BallonPmtLoanSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.BallonPmtLoan.IsValid || !o2.BallonPmtLoan.IsValid) {
		    return 0;
		}
		return o1.BallonPmtLoan.CompareTo(o2.BallonPmtLoan);
	    }
	}

	[Generate]
	public class FixedOrVariableRateLoanSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FixedOrVariableRateLoan.IsValid || !o2.FixedOrVariableRateLoan.IsValid) {
		    return 0;
		}
		return o1.FixedOrVariableRateLoan.CompareTo(o2.FixedOrVariableRateLoan);
	    }
	}

	[Generate]
	public class AlpsOfficerSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.AlpsOfficer.IsValid || !o2.AlpsOfficer.IsValid) {
		    return 0;
		}
		return o1.AlpsOfficer.CompareTo(o2.AlpsOfficer);
	    }
	}

	[Generate]
	public class CUDLAppNumberSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.CUDLAppNumber.IsValid || !o2.CUDLAppNumber.IsValid) {
		    return 0;
		}
		return o1.CUDLAppNumber.CompareTo(o2.CUDLAppNumber);
	    }
	}

	[Generate]
	public class FasbDefCostSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FasbDefCost.IsValid || !o2.FasbDefCost.IsValid) {
		    return 0;
		}
		return o1.FasbDefCost.CompareTo(o2.FasbDefCost);
	    }
	}

	[Generate]
	public class FasbLtdAmortFeesSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.FasbLtdAmortFees.IsValid || !o2.FasbLtdAmortFees.IsValid) {
		    return 0;
		}
		return o1.FasbLtdAmortFees.CompareTo(o2.FasbLtdAmortFees);
	    }
	}

	[Generate]
	public class InterestPriorYtdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InterestPriorYtd.IsValid || !o2.InterestPriorYtd.IsValid) {
		    return 0;
		}
		return o1.InterestPriorYtd.CompareTo(o2.InterestPriorYtd);
	    }
	}

	[Generate]
	public class AccruedInterestSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.AccruedInterest.IsValid || !o2.AccruedInterest.IsValid) {
		    return 0;
		}
		return o1.AccruedInterest.CompareTo(o2.AccruedInterest);
	    }
	}

	[Generate]
	public class InterestLTDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InterestLTD.IsValid || !o2.InterestLTD.IsValid) {
		    return 0;
		}
		return o1.InterestLTD.CompareTo(o2.InterestLTD);
	    }
	}

	[Generate]
	public class InterestYtdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InterestYtd.IsValid || !o2.InterestYtd.IsValid) {
		    return 0;
		}
		return o1.InterestYtd.CompareTo(o2.InterestYtd);
	    }
	}

	[Generate]
	public class PartialPayAmtSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.PartialPayAmt.IsValid || !o2.PartialPayAmt.IsValid) {
		    return 0;
		}
		return o1.PartialPayAmt.CompareTo(o2.PartialPayAmt);
	    }
	}

	[Generate]
	public class InterestUncollectedSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InterestUncollected.IsValid || !o2.InterestUncollected.IsValid) {
		    return 0;
		}
		return o1.InterestUncollected.CompareTo(o2.InterestUncollected);
	    }
	}

	[Generate]
	public class ClosedAcctDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.ClosedAcctDate.IsValid || !o2.ClosedAcctDate.IsValid) {
		    return 0;
		}
		return o1.ClosedAcctDate.CompareTo(o2.ClosedAcctDate);
	    }
	}

	[Generate]
	public class LastTranDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.LastTranDate.IsValid || !o2.LastTranDate.IsValid) {
		    return 0;
		}
		return o1.LastTranDate.CompareTo(o2.LastTranDate);
	    }
	}

	[Generate]
	public class NextDueDateSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.NextDueDate.IsValid || !o2.NextDueDate.IsValid) {
		    return 0;
		}
		return o1.NextDueDate.CompareTo(o2.NextDueDate);
	    }
	}

	[Generate]
	public class InsuranceCodeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.InsuranceCode.IsValid || !o2.InsuranceCode.IsValid) {
		    return 0;
		}
		return o1.InsuranceCode.CompareTo(o2.InsuranceCode);
	    }
	}

	[Generate]
	public class MbrAgeAtApprovalSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanPortfolio o1 = (ILoanPortfolio)a;
		ILoanPortfolio o2 = (ILoanPortfolio)b;

		if (o1 == null || o2 == null || !o1.MbrAgeAtApproval.IsValid || !o2.MbrAgeAtApproval.IsValid) {
		    return 0;
		}
		return o1.MbrAgeAtApproval.CompareTo(o2.MbrAgeAtApproval);
	    }
	}
    }
}
