using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ICLIENT_REL_DATA generic collection
    /// </summary>
    public class CLIENT_REL_DATAList : CollectionBase {
	[Generate]
	public static readonly CLIENT_REL_DATAList UNSET = new CLIENT_REL_DATAList(true);
	[Generate]
	public static readonly CLIENT_REL_DATAList DEFAULT = new CLIENT_REL_DATAList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private CLIENT_REL_DATAList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public CLIENT_REL_DATAList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ICLIENT_REL_DATA this[int index] {
	    get {
		return (ICLIENT_REL_DATA)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.MEM_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ICLIENT_REL_DATA value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.MEM_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ICLIENT_REL_DATA value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.MEM_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ICLIENT_REL_DATA value = this[index];
		    keys.Remove(value.MEM_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ICLIENT_REL_DATA) {
		    Add((ICLIENT_REL_DATA)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ICLIENT_REL_DATA");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType mEM_NUM) {
	    return keys.Contains(mEM_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ICLIENT_REL_DATA this[IdType mEM_NUM] {
	    get {
		return keys[mEM_NUM] as ICLIENT_REL_DATA;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public CLIENT_REL_DATAList RetainAll(CLIENT_REL_DATAList list) {
	    CLIENT_REL_DATAList result = new CLIENT_REL_DATAList();

	    foreach (ICLIENT_REL_DATA data in List) {
		if (list.Contains(data.MEM_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public CLIENT_REL_DATAList RemoveAll(CLIENT_REL_DATAList list) {
	    CLIENT_REL_DATAList result = new CLIENT_REL_DATAList();

	    foreach (ICLIENT_REL_DATA data in List) {
		if (!list.Contains(data.MEM_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType mEM_NUM) {
	    if (!immutable) {
		ICLIENT_REL_DATA objectInList = this[mEM_NUM];
		List.Remove(objectInList);
		keys.Remove(mEM_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class MEM_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ICLIENT_REL_DATA o1 = (ICLIENT_REL_DATA)a;
		ICLIENT_REL_DATA o2 = (ICLIENT_REL_DATA)b;

		if (o1 == null || o2 == null || !o1.MEM_NUM.IsValid || !o2.MEM_NUM.IsValid) {
		    return 0;
		}
		return o1.MEM_NUM.CompareTo(o2.MEM_NUM);
	    }
	}

	[Generate]
	public class AGESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ICLIENT_REL_DATA o1 = (ICLIENT_REL_DATA)a;
		ICLIENT_REL_DATA o2 = (ICLIENT_REL_DATA)b;

		if (o1 == null || o2 == null || !o1.AGE.IsValid || !o2.AGE.IsValid) {
		    return 0;
		}
		return o1.AGE.CompareTo(o2.AGE);
	    }
	}
    }
}
