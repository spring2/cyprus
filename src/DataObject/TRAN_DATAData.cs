using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class TRAN_DATAData : Spring2.Core.DataObject.DataObject {
	public static readonly TRAN_DATAData DEFAULT = new TRAN_DATAData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private CurrencyType cURRENT_BALANCE = CurrencyType.DEFAULT;
	private DecimalType iNT_RATE = DecimalType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public CurrencyType CURRENT_BALANCE {
	    get { return this.cURRENT_BALANCE; }
	    set { this.cURRENT_BALANCE = value; }
	}

	public DecimalType INT_RATE {
	    get { return this.iNT_RATE; }
	    set { this.iNT_RATE = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
