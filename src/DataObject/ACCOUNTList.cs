using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IACCOUNT generic collection
    /// </summary>
    public class ACCOUNTList : CollectionBase {
	[Generate]
	public static readonly ACCOUNTList UNSET = new ACCOUNTList(true);
	[Generate]
	public static readonly ACCOUNTList DEFAULT = new ACCOUNTList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private ACCOUNTList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public ACCOUNTList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IACCOUNT this[int index] {
	    get {
		return (IACCOUNT)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IACCOUNT value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IACCOUNT value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IACCOUNT value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IACCOUNT) {
		    Add((IACCOUNT)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IACCOUNT");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IACCOUNT this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as IACCOUNT;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public ACCOUNTList RetainAll(ACCOUNTList list) {
	    ACCOUNTList result = new ACCOUNTList();

	    foreach (IACCOUNT data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public ACCOUNTList RemoveAll(ACCOUNTList list) {
	    ACCOUNTList result = new ACCOUNTList();

	    foreach (IACCOUNT data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		IACCOUNT objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class MEM_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.MEM_NUM.IsValid || !o2.MEM_NUM.IsValid) {
		    return 0;
		}
		return o1.MEM_NUM.CompareTo(o2.MEM_NUM);
	    }
	}

	[Generate]
	public class L_ACCT_LEVELSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_ACCT_LEVEL.IsValid || !o2.L_ACCT_LEVEL.IsValid) {
		    return 0;
		}
		return o1.L_ACCT_LEVEL.CompareTo(o2.L_ACCT_LEVEL);
	    }
	}

	[Generate]
	public class ACCT_SUBLEVSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.ACCT_SUBLEV.IsValid || !o2.ACCT_SUBLEV.IsValid) {
		    return 0;
		}
		return o1.ACCT_SUBLEV.CompareTo(o2.ACCT_SUBLEV);
	    }
	}

	[Generate]
	public class ACCOUNT_OPEN_DATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.ACCOUNT_OPEN_DATE.IsValid || !o2.ACCOUNT_OPEN_DATE.IsValid) {
		    return 0;
		}
		return o1.ACCOUNT_OPEN_DATE.CompareTo(o2.ACCOUNT_OPEN_DATE);
	    }
	}

	[Generate]
	public class SPEC_REPORT_CODESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.SPEC_REPORT_CODE.IsValid || !o2.SPEC_REPORT_CODE.IsValid) {
		    return 0;
		}
		return o1.SPEC_REPORT_CODE.CompareTo(o2.SPEC_REPORT_CODE);
	    }
	}

	[Generate]
	public class ORIG_PAYSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.ORIG_PAY.IsValid || !o2.ORIG_PAY.IsValid) {
		    return 0;
		}
		return o1.ORIG_PAY.CompareTo(o2.ORIG_PAY);
	    }
	}

	[Generate]
	public class APRSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.APR.IsValid || !o2.APR.IsValid) {
		    return 0;
		}
		return o1.APR.CompareTo(o2.APR);
	    }
	}

	[Generate]
	public class CURR_CREDIT_LIMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.CURR_CREDIT_LIM.IsValid || !o2.CURR_CREDIT_LIM.IsValid) {
		    return 0;
		}
		return o1.CURR_CREDIT_LIM.CompareTo(o2.CURR_CREDIT_LIM);
	    }
	}

	[Generate]
	public class PG_CODESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.PG_CODE.IsValid || !o2.PG_CODE.IsValid) {
		    return 0;
		}
		return o1.PG_CODE.CompareTo(o2.PG_CODE);
	    }
	}

	[Generate]
	public class DISC_REASONSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.DISC_REASON.IsValid || !o2.DISC_REASON.IsValid) {
		    return 0;
		}
		return o1.DISC_REASON.CompareTo(o2.DISC_REASON);
	    }
	}

	[Generate]
	public class RISK_OFFSET_VALSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.RISK_OFFSET_VAL.IsValid || !o2.RISK_OFFSET_VAL.IsValid) {
		    return 0;
		}
		return o1.RISK_OFFSET_VAL.CompareTo(o2.RISK_OFFSET_VAL);
	    }
	}

	[Generate]
	public class FIRST_PAYDATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.FIRST_PAYDATE.IsValid || !o2.FIRST_PAYDATE.IsValid) {
		    return 0;
		}
		return o1.FIRST_PAYDATE.CompareTo(o2.FIRST_PAYDATE);
	    }
	}

	[Generate]
	public class ORIG_LOAN_TERMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.ORIG_LOAN_TERM.IsValid || !o2.ORIG_LOAN_TERM.IsValid) {
		    return 0;
		}
		return o1.ORIG_LOAN_TERM.CompareTo(o2.ORIG_LOAN_TERM);
	    }
	}

	[Generate]
	public class L_MATURITY_DATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_MATURITY_DATE.IsValid || !o2.L_MATURITY_DATE.IsValid) {
		    return 0;
		}
		return o1.L_MATURITY_DATE.CompareTo(o2.L_MATURITY_DATE);
	    }
	}

	[Generate]
	public class L_INSCODESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_INSCODE.IsValid || !o2.L_INSCODE.IsValid) {
		    return 0;
		}
		return o1.L_INSCODE.CompareTo(o2.L_INSCODE);
	    }
	}

	[Generate]
	public class L_SECCODESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_SECCODE.IsValid || !o2.L_SECCODE.IsValid) {
		    return 0;
		}
		return o1.L_SECCODE.CompareTo(o2.L_SECCODE);
	    }
	}

	[Generate]
	public class L_SECDESC1Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_SECDESC1.IsValid || !o2.L_SECDESC1.IsValid) {
		    return 0;
		}
		return o1.L_SECDESC1.CompareTo(o2.L_SECDESC1);
	    }
	}

	[Generate]
	public class L_SECDESC2Sorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_SECDESC2.IsValid || !o2.L_SECDESC2.IsValid) {
		    return 0;
		}
		return o1.L_SECDESC2.CompareTo(o2.L_SECDESC2);
	    }
	}

	[Generate]
	public class US_APPLIC_IDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.US_APPLIC_ID.IsValid || !o2.US_APPLIC_ID.IsValid) {
		    return 0;
		}
		return o1.US_APPLIC_ID.CompareTo(o2.US_APPLIC_ID);
	    }
	}

	[Generate]
	public class L_ACCT_TYPESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT o1 = (IACCOUNT)a;
		IACCOUNT o2 = (IACCOUNT)b;

		if (o1 == null || o2 == null || !o1.L_ACCT_TYPE.IsValid || !o2.L_ACCT_TYPE.IsValid) {
		    return 0;
		}
		return o1.L_ACCT_TYPE.CompareTo(o2.L_ACCT_TYPE);
	    }
	}
    }
}
