using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class TRAN_LOANData : Spring2.Core.DataObject.DataObject {
	public static readonly TRAN_LOANData DEFAULT = new TRAN_LOANData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private CurrencyType l_PREVIOUS_CREDIT_LIMIT = CurrencyType.DEFAULT;
	private CurrencyType l_REPAY_AMT = CurrencyType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public CurrencyType L_PREVIOUS_CREDIT_LIMIT {
	    get { return this.l_PREVIOUS_CREDIT_LIMIT; }
	    set { this.l_PREVIOUS_CREDIT_LIMIT = value; }
	}

	public CurrencyType L_REPAY_AMT {
	    get { return this.l_REPAY_AMT; }
	    set { this.l_REPAY_AMT = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
