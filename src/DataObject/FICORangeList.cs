using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IFICORange generic collection
    /// </summary>
    public class FICORangeList : CollectionBase {
	[Generate]
	public static readonly FICORangeList UNSET = new FICORangeList(true);
	[Generate]
	public static readonly FICORangeList DEFAULT = new FICORangeList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private FICORangeList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public FICORangeList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IFICORange this[int index] {
	    get {
		return (IFICORange)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.FICORangeId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IFICORange value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.FICORangeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IFICORange value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.FICORangeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IFICORange value = this[index];
		    keys.Remove(value.FICORangeId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IFICORange) {
		    Add((IFICORange)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IFICORange");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType fICORangeId) {
	    return keys.Contains(fICORangeId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IFICORange this[IdType fICORangeId] {
	    get {
		return keys[fICORangeId] as IFICORange;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public FICORangeList RetainAll(FICORangeList list) {
	    FICORangeList result = new FICORangeList();

	    foreach (IFICORange data in List) {
		if (list.Contains(data.FICORangeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public FICORangeList RemoveAll(FICORangeList list) {
	    FICORangeList result = new FICORangeList();

	    foreach (IFICORange data in List) {
		if (!list.Contains(data.FICORangeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType fICORangeId) {
	    if (!immutable) {
		IFICORange objectInList = this[fICORangeId];
		List.Remove(objectInList);
		keys.Remove(fICORangeId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class FICORangeIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFICORange o1 = (IFICORange)a;
		IFICORange o2 = (IFICORange)b;

		if (o1 == null || o2 == null || !o1.FICORangeId.IsValid || !o2.FICORangeId.IsValid) {
		    return 0;
		}
		return o1.FICORangeId.CompareTo(o2.FICORangeId);
	    }
	}

	[Generate]
	public class ValueSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFICORange o1 = (IFICORange)a;
		IFICORange o2 = (IFICORange)b;

		if (o1 == null || o2 == null || !o1.Value.IsValid || !o2.Value.IsValid) {
		    return 0;
		}
		return o1.Value.CompareTo(o2.Value);
	    }
	}

	[Generate]
	public class DescriptionSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFICORange o1 = (IFICORange)a;
		IFICORange o2 = (IFICORange)b;

		if (o1 == null || o2 == null || !o1.Description.IsValid || !o2.Description.IsValid) {
		    return 0;
		}
		return o1.Description.CompareTo(o2.Description);
	    }
	}

	#region
	public IFICORange GetRangeValue(IntegerType value) {
	    this.Sort(new ValueSorter());
	    if (value.IsValid) {
		foreach (IFICORange range in this) {
		    if (range.Value >= value) {
			return range;
		    }
		}
	    }
	    return null;
	}

	#endregion
    }
}
