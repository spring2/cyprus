using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class CLIENTData : Spring2.Core.DataObject.DataObject {
	public static readonly CLIENTData DEFAULT = new CLIENTData();

	private IdType mEM_NUM = IdType.DEFAULT;
	private IntegerType cLIENT_CLASS = IntegerType.DEFAULT;
	private DateTimeType jOIN_DATE = DateTimeType.DEFAULT;
	private StringType sEX = StringType.DEFAULT;

	public IdType MEM_NUM {
	    get { return this.mEM_NUM; }
	    set { this.mEM_NUM = value; }
	}

	public IntegerType CLIENT_CLASS {
	    get { return this.cLIENT_CLASS; }
	    set { this.cLIENT_CLASS = value; }
	}

	public DateTimeType JOIN_DATE {
	    get { return this.jOIN_DATE; }
	    set { this.jOIN_DATE = value; }
	}

	public StringType SEX {
	    get { return this.sEX; }
	    set { this.sEX = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
