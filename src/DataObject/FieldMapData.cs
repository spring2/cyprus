using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class FieldMapData : Spring2.Core.DataObject.DataObject {
	public static readonly FieldMapData DEFAULT = new FieldMapData();

	private IdType fieldMapId = IdType.DEFAULT;
	private StringType srcTable = StringType.DEFAULT;
	private StringType srcColumn = StringType.DEFAULT;
	private StringType destTable = StringType.DEFAULT;
	private StringType destColumn = StringType.DEFAULT;

	public IdType FieldMapId {
	    get { return this.fieldMapId; }
	    set { this.fieldMapId = value; }
	}

	public StringType SrcTable {
	    get { return this.srcTable; }
	    set { this.srcTable = value; }
	}

	public StringType SrcColumn {
	    get { return this.srcColumn; }
	    set { this.srcColumn = value; }
	}

	public StringType DestTable {
	    get { return this.destTable; }
	    set { this.destTable = value; }
	}

	public StringType DestColumn {
	    get { return this.destColumn; }
	    set { this.destColumn = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
