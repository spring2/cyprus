using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IAPPLIC_CALCINFO generic collection
    /// </summary>
    public class APPLIC_CALCINFOList : CollectionBase {
	[Generate]
	public static readonly APPLIC_CALCINFOList UNSET = new APPLIC_CALCINFOList(true);
	[Generate]
	public static readonly APPLIC_CALCINFOList DEFAULT = new APPLIC_CALCINFOList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private APPLIC_CALCINFOList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public APPLIC_CALCINFOList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IAPPLIC_CALCINFO this[int index] {
	    get {
		return (IAPPLIC_CALCINFO)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.APPLIC_ID] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IAPPLIC_CALCINFO value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.APPLIC_ID] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IAPPLIC_CALCINFO value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.APPLIC_ID] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IAPPLIC_CALCINFO value = this[index];
		    keys.Remove(value.APPLIC_ID);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IAPPLIC_CALCINFO) {
		    Add((IAPPLIC_CALCINFO)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IAPPLIC_CALCINFO");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aPPLIC_ID) {
	    return keys.Contains(aPPLIC_ID);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IAPPLIC_CALCINFO this[IdType aPPLIC_ID] {
	    get {
		return keys[aPPLIC_ID] as IAPPLIC_CALCINFO;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public APPLIC_CALCINFOList RetainAll(APPLIC_CALCINFOList list) {
	    APPLIC_CALCINFOList result = new APPLIC_CALCINFOList();

	    foreach (IAPPLIC_CALCINFO data in List) {
		if (list.Contains(data.APPLIC_ID)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public APPLIC_CALCINFOList RemoveAll(APPLIC_CALCINFOList list) {
	    APPLIC_CALCINFOList result = new APPLIC_CALCINFOList();

	    foreach (IAPPLIC_CALCINFO data in List) {
		if (!list.Contains(data.APPLIC_ID)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aPPLIC_ID) {
	    if (!immutable) {
		IAPPLIC_CALCINFO objectInList = this[aPPLIC_ID];
		List.Remove(objectInList);
		keys.Remove(aPPLIC_ID);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class APPLIC_IDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC_CALCINFO o1 = (IAPPLIC_CALCINFO)a;
		IAPPLIC_CALCINFO o2 = (IAPPLIC_CALCINFO)b;

		if (o1 == null || o2 == null || !o1.APPLIC_ID.IsValid || !o2.APPLIC_ID.IsValid) {
		    return 0;
		}
		return o1.APPLIC_ID.CompareTo(o2.APPLIC_ID);
	    }
	}

	[Generate]
	public class DEBT_AFTERSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC_CALCINFO o1 = (IAPPLIC_CALCINFO)a;
		IAPPLIC_CALCINFO o2 = (IAPPLIC_CALCINFO)b;

		if (o1 == null || o2 == null || !o1.DEBT_AFTER.IsValid || !o2.DEBT_AFTER.IsValid) {
		    return 0;
		}
		return o1.DEBT_AFTER.CompareTo(o2.DEBT_AFTER);
	    }
	}
    }
}
