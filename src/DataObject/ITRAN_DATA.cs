using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class TRAN_DATAFields {
	private TRAN_DATAFields() {}
	public static readonly String ENTITY_NAME = "TRAN_DATA";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CURRENT_BALANCE = new ColumnMetaData("CURRENT_BALANCE", "CURRENT_BALANCE", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData INT_RATE = new ColumnMetaData("INT_RATE", "INT_RATE", DbType.Decimal, SqlDbType.Decimal, 0, 18, 8);
    }

    public interface ITRAN_DATA : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	CurrencyType CURRENT_BALANCE {
	    get;
	}
	DecimalType INT_RATE {
	    get;
	}
    }
}
