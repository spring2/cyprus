using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class RiskTypeData : Spring2.Core.DataObject.DataObject {
	public static readonly RiskTypeData DEFAULT = new RiskTypeData();

	private IdType riskTypeId = IdType.DEFAULT;
	private IntegerType lType = IntegerType.DEFAULT;
	private StringType riskTypeDesc = StringType.DEFAULT;

	public IdType RiskTypeId {
	    get { return this.riskTypeId; }
	    set { this.riskTypeId = value; }
	}

	public IntegerType LType {
	    get { return this.lType; }
	    set { this.lType = value; }
	}

	public StringType RiskTypeDesc {
	    get { return this.riskTypeDesc; }
	    set { this.riskTypeDesc = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
