using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IPROFILE_EMPLOYER_INFO generic collection
    /// </summary>
    public class PROFILE_EMPLOYER_INFOList : CollectionBase {
	[Generate]
	public static readonly PROFILE_EMPLOYER_INFOList UNSET = new PROFILE_EMPLOYER_INFOList(true);
	[Generate]
	public static readonly PROFILE_EMPLOYER_INFOList DEFAULT = new PROFILE_EMPLOYER_INFOList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private PROFILE_EMPLOYER_INFOList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public PROFILE_EMPLOYER_INFOList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IPROFILE_EMPLOYER_INFO this[int index] {
	    get {
		return (IPROFILE_EMPLOYER_INFO)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.RECORD_KEY] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IPROFILE_EMPLOYER_INFO value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.RECORD_KEY] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IPROFILE_EMPLOYER_INFO value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.RECORD_KEY] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IPROFILE_EMPLOYER_INFO value = this[index];
		    keys.Remove(value.RECORD_KEY);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IPROFILE_EMPLOYER_INFO) {
		    Add((IPROFILE_EMPLOYER_INFO)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IPROFILE_EMPLOYER_INFO");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType rECORD_KEY) {
	    return keys.Contains(rECORD_KEY);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IPROFILE_EMPLOYER_INFO this[IdType rECORD_KEY] {
	    get {
		return keys[rECORD_KEY] as IPROFILE_EMPLOYER_INFO;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public PROFILE_EMPLOYER_INFOList RetainAll(PROFILE_EMPLOYER_INFOList list) {
	    PROFILE_EMPLOYER_INFOList result = new PROFILE_EMPLOYER_INFOList();

	    foreach (IPROFILE_EMPLOYER_INFO data in List) {
		if (list.Contains(data.RECORD_KEY)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public PROFILE_EMPLOYER_INFOList RemoveAll(PROFILE_EMPLOYER_INFOList list) {
	    PROFILE_EMPLOYER_INFOList result = new PROFILE_EMPLOYER_INFOList();

	    foreach (IPROFILE_EMPLOYER_INFO data in List) {
		if (!list.Contains(data.RECORD_KEY)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType rECORD_KEY) {
	    if (!immutable) {
		IPROFILE_EMPLOYER_INFO objectInList = this[rECORD_KEY];
		List.Remove(objectInList);
		keys.Remove(rECORD_KEY);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class RECORD_KEYSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IPROFILE_EMPLOYER_INFO o1 = (IPROFILE_EMPLOYER_INFO)a;
		IPROFILE_EMPLOYER_INFO o2 = (IPROFILE_EMPLOYER_INFO)b;

		if (o1 == null || o2 == null || !o1.RECORD_KEY.IsValid || !o2.RECORD_KEY.IsValid) {
		    return 0;
		}
		return o1.RECORD_KEY.CompareTo(o2.RECORD_KEY);
	    }
	}

	[Generate]
	public class EMP_GROSS_INCOMESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IPROFILE_EMPLOYER_INFO o1 = (IPROFILE_EMPLOYER_INFO)a;
		IPROFILE_EMPLOYER_INFO o2 = (IPROFILE_EMPLOYER_INFO)b;

		if (o1 == null || o2 == null || !o1.EMP_GROSS_INCOME.IsValid || !o2.EMP_GROSS_INCOME.IsValid) {
		    return 0;
		}
		return o1.EMP_GROSS_INCOME.CompareTo(o2.EMP_GROSS_INCOME);
	    }
	}

	[Generate]
	public class EMP_YRS_PROFSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IPROFILE_EMPLOYER_INFO o1 = (IPROFILE_EMPLOYER_INFO)a;
		IPROFILE_EMPLOYER_INFO o2 = (IPROFILE_EMPLOYER_INFO)b;

		if (o1 == null || o2 == null || !o1.EMP_YRS_PROF.IsValid || !o2.EMP_YRS_PROF.IsValid) {
		    return 0;
		}
		return o1.EMP_YRS_PROF.CompareTo(o2.EMP_YRS_PROF);
	    }
	}
    }
}
