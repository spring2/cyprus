using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class FieldMapFields {
	private FieldMapFields() {}
	public static readonly String ENTITY_NAME = "FieldMap";

	public static readonly ColumnMetaData FIELDMAPID = new ColumnMetaData("FieldMapId", "FieldMapId", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData SRCTABLE = new ColumnMetaData("SrcTable", "SrcTable", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData SRCCOLUMN = new ColumnMetaData("SrcColumn", "SrcColumn", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData DESTTABLE = new ColumnMetaData("DestTable", "DestTable", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData DESTCOLUMN = new ColumnMetaData("DestColumn", "DestColumn", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
    }

    public interface IFieldMap : IBusinessEntity {
	IdType FieldMapId {
	    get;
	}
	StringType SrcTable {
	    get;
	}
	StringType SrcColumn {
	    get;
	}
	StringType DestTable {
	    get;
	}
	StringType DestColumn {
	    get;
	}
    }
}
