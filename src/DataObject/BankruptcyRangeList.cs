using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IBankruptcyRange generic collection
    /// </summary>
    public class BankruptcyRangeList : CollectionBase {
	[Generate]
	public static readonly BankruptcyRangeList UNSET = new BankruptcyRangeList(true);
	[Generate]
	public static readonly BankruptcyRangeList DEFAULT = new BankruptcyRangeList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private BankruptcyRangeList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public BankruptcyRangeList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IBankruptcyRange this[int index] {
	    get {
		return (IBankruptcyRange)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.BankruptcyRangeId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IBankruptcyRange value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.BankruptcyRangeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IBankruptcyRange value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.BankruptcyRangeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IBankruptcyRange value = this[index];
		    keys.Remove(value.BankruptcyRangeId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IBankruptcyRange) {
		    Add((IBankruptcyRange)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IBankruptcyRange");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType bankruptcyRangeId) {
	    return keys.Contains(bankruptcyRangeId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IBankruptcyRange this[IdType bankruptcyRangeId] {
	    get {
		return keys[bankruptcyRangeId] as IBankruptcyRange;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public BankruptcyRangeList RetainAll(BankruptcyRangeList list) {
	    BankruptcyRangeList result = new BankruptcyRangeList();

	    foreach (IBankruptcyRange data in List) {
		if (list.Contains(data.BankruptcyRangeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public BankruptcyRangeList RemoveAll(BankruptcyRangeList list) {
	    BankruptcyRangeList result = new BankruptcyRangeList();

	    foreach (IBankruptcyRange data in List) {
		if (!list.Contains(data.BankruptcyRangeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType bankruptcyRangeId) {
	    if (!immutable) {
		IBankruptcyRange objectInList = this[bankruptcyRangeId];
		List.Remove(objectInList);
		keys.Remove(bankruptcyRangeId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class BankruptcyRangeIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IBankruptcyRange o1 = (IBankruptcyRange)a;
		IBankruptcyRange o2 = (IBankruptcyRange)b;

		if (o1 == null || o2 == null || !o1.BankruptcyRangeId.IsValid || !o2.BankruptcyRangeId.IsValid) {
		    return 0;
		}
		return o1.BankruptcyRangeId.CompareTo(o2.BankruptcyRangeId);
	    }
	}

	[Generate]
	public class ValueSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IBankruptcyRange o1 = (IBankruptcyRange)a;
		IBankruptcyRange o2 = (IBankruptcyRange)b;

		if (o1 == null || o2 == null || !o1.Value.IsValid || !o2.Value.IsValid) {
		    return 0;
		}
		return o1.Value.CompareTo(o2.Value);
	    }
	}

	[Generate]
	public class DescriptionSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IBankruptcyRange o1 = (IBankruptcyRange)a;
		IBankruptcyRange o2 = (IBankruptcyRange)b;

		if (o1 == null || o2 == null || !o1.Description.IsValid || !o2.Description.IsValid) {
		    return 0;
		}
		return o1.Description.CompareTo(o2.Description);
	    }
	}

	#region
	public IBankruptcyRange GetRangeValue(IntegerType value) {
	    this.Sort(new ValueSorter());
	    if (value.IsValid) {
		foreach (IBankruptcyRange range in this) {
		    if (range.Value >= value) {
			return range;
		    }
		}
	    }
	    return null;
	}

	#endregion
    }
}
