using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IFastStartRange generic collection
    /// </summary>
    public class FastStartRangeList : CollectionBase {
	[Generate]
	public static readonly FastStartRangeList UNSET = new FastStartRangeList(true);
	[Generate]
	public static readonly FastStartRangeList DEFAULT = new FastStartRangeList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private FastStartRangeList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public FastStartRangeList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IFastStartRange this[int index] {
	    get {
		return (IFastStartRange)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.FastStartRangeId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IFastStartRange value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.FastStartRangeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IFastStartRange value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.FastStartRangeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IFastStartRange value = this[index];
		    keys.Remove(value.FastStartRangeId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IFastStartRange) {
		    Add((IFastStartRange)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IFastStartRange");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType fastStartRangeId) {
	    return keys.Contains(fastStartRangeId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IFastStartRange this[IdType fastStartRangeId] {
	    get {
		return keys[fastStartRangeId] as IFastStartRange;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public FastStartRangeList RetainAll(FastStartRangeList list) {
	    FastStartRangeList result = new FastStartRangeList();

	    foreach (IFastStartRange data in List) {
		if (list.Contains(data.FastStartRangeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public FastStartRangeList RemoveAll(FastStartRangeList list) {
	    FastStartRangeList result = new FastStartRangeList();

	    foreach (IFastStartRange data in List) {
		if (!list.Contains(data.FastStartRangeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType fastStartRangeId) {
	    if (!immutable) {
		IFastStartRange objectInList = this[fastStartRangeId];
		List.Remove(objectInList);
		keys.Remove(fastStartRangeId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class FastStartRangeIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFastStartRange o1 = (IFastStartRange)a;
		IFastStartRange o2 = (IFastStartRange)b;

		if (o1 == null || o2 == null || !o1.FastStartRangeId.IsValid || !o2.FastStartRangeId.IsValid) {
		    return 0;
		}
		return o1.FastStartRangeId.CompareTo(o2.FastStartRangeId);
	    }
	}

	[Generate]
	public class ValueSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFastStartRange o1 = (IFastStartRange)a;
		IFastStartRange o2 = (IFastStartRange)b;

		if (o1 == null || o2 == null || !o1.Value.IsValid || !o2.Value.IsValid) {
		    return 0;
		}
		return o1.Value.CompareTo(o2.Value);
	    }
	}

	[Generate]
	public class DescriptionSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFastStartRange o1 = (IFastStartRange)a;
		IFastStartRange o2 = (IFastStartRange)b;

		if (o1 == null || o2 == null || !o1.Description.IsValid || !o2.Description.IsValid) {
		    return 0;
		}
		return o1.Description.CompareTo(o2.Description);
	    }
	}

	#region
	public IFastStartRange GetRangeValue(IntegerType value) {
	    this.Sort(new ValueSorter());
	    if (value.IsValid) {
		foreach (IFastStartRange range in this) {
		    if (range.Value >= value) {
			return range;
		    }
		}
	    }
	    return null;
	}

	#endregion
    }
}
