using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IChanel generic collection
    /// </summary>
    public class ChanelList : CollectionBase {
	[Generate]
	public static readonly ChanelList UNSET = new ChanelList(true);
	[Generate]
	public static readonly ChanelList DEFAULT = new ChanelList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private ChanelList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public ChanelList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IChanel this[int index] {
	    get {
		return (IChanel)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ChanelId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IChanel value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ChanelId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IChanel value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ChanelId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IChanel value = this[index];
		    keys.Remove(value.ChanelId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IChanel) {
		    Add((IChanel)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IChanel");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType chanelId) {
	    return keys.Contains(chanelId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IChanel this[IdType chanelId] {
	    get {
		return keys[chanelId] as IChanel;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public ChanelList RetainAll(ChanelList list) {
	    ChanelList result = new ChanelList();

	    foreach (IChanel data in List) {
		if (list.Contains(data.ChanelId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public ChanelList RemoveAll(ChanelList list) {
	    ChanelList result = new ChanelList();

	    foreach (IChanel data in List) {
		if (!list.Contains(data.ChanelId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType chanelId) {
	    if (!immutable) {
		IChanel objectInList = this[chanelId];
		List.Remove(objectInList);
		keys.Remove(chanelId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ChanelIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IChanel o1 = (IChanel)a;
		IChanel o2 = (IChanel)b;

		if (o1 == null || o2 == null || !o1.ChanelId.IsValid || !o2.ChanelId.IsValid) {
		    return 0;
		}
		return o1.ChanelId.CompareTo(o2.ChanelId);
	    }
	}

	[Generate]
	public class ReportCodeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IChanel o1 = (IChanel)a;
		IChanel o2 = (IChanel)b;

		if (o1 == null || o2 == null || !o1.ReportCode.IsValid || !o2.ReportCode.IsValid) {
		    return 0;
		}
		return o1.ReportCode.CompareTo(o2.ReportCode);
	    }
	}

	[Generate]
	public class ChanelDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IChanel o1 = (IChanel)a;
		IChanel o2 = (IChanel)b;

		if (o1 == null || o2 == null || !o1.ChanelDesc.IsValid || !o2.ChanelDesc.IsValid) {
		    return 0;
		}
		return o1.ChanelDesc.CompareTo(o2.ChanelDesc);
	    }
	}

	#region
	public IChanel FindByReportCode(IntegerType reportCode) {
	    foreach (IChanel chanel in this) {
		if (chanel.ReportCode.Equals(reportCode)) {
		    return chanel;
		}
	    }
	    return null;
	}

	#endregion
    }
}
