using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IRiskType generic collection
    /// </summary>
    public class RiskTypeList : CollectionBase {
	[Generate]
	public static readonly RiskTypeList UNSET = new RiskTypeList(true);
	[Generate]
	public static readonly RiskTypeList DEFAULT = new RiskTypeList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private RiskTypeList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public RiskTypeList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IRiskType this[int index] {
	    get {
		return (IRiskType)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.RiskTypeId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IRiskType value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.RiskTypeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IRiskType value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.RiskTypeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IRiskType value = this[index];
		    keys.Remove(value.RiskTypeId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IRiskType) {
		    Add((IRiskType)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IRiskType");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType riskTypeId) {
	    return keys.Contains(riskTypeId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IRiskType this[IdType riskTypeId] {
	    get {
		return keys[riskTypeId] as IRiskType;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public RiskTypeList RetainAll(RiskTypeList list) {
	    RiskTypeList result = new RiskTypeList();

	    foreach (IRiskType data in List) {
		if (list.Contains(data.RiskTypeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public RiskTypeList RemoveAll(RiskTypeList list) {
	    RiskTypeList result = new RiskTypeList();

	    foreach (IRiskType data in List) {
		if (!list.Contains(data.RiskTypeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType riskTypeId) {
	    if (!immutable) {
		IRiskType objectInList = this[riskTypeId];
		List.Remove(objectInList);
		keys.Remove(riskTypeId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class RiskTypeIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IRiskType o1 = (IRiskType)a;
		IRiskType o2 = (IRiskType)b;

		if (o1 == null || o2 == null || !o1.RiskTypeId.IsValid || !o2.RiskTypeId.IsValid) {
		    return 0;
		}
		return o1.RiskTypeId.CompareTo(o2.RiskTypeId);
	    }
	}

	[Generate]
	public class LTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IRiskType o1 = (IRiskType)a;
		IRiskType o2 = (IRiskType)b;

		if (o1 == null || o2 == null || !o1.LType.IsValid || !o2.LType.IsValid) {
		    return 0;
		}
		return o1.LType.CompareTo(o2.LType);
	    }
	}

	[Generate]
	public class RiskTypeDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IRiskType o1 = (IRiskType)a;
		IRiskType o2 = (IRiskType)b;

		if (o1 == null || o2 == null || !o1.RiskTypeDesc.IsValid || !o2.RiskTypeDesc.IsValid) {
		    return 0;
		}
		return o1.RiskTypeDesc.CompareTo(o2.RiskTypeDesc);
	    }
	}

	#region
	public IRiskType FindByLType(IntegerType lType) {
	    foreach (IRiskType rt in this) {
		if (rt.LType.Equals(lType)) {
		    return rt;
		}
	    }
	    return null;
	}

	#endregion
    }
}
