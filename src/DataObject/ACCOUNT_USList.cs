using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IACCOUNT_US generic collection
    /// </summary>
    public class ACCOUNT_USList : CollectionBase {
	[Generate]
	public static readonly ACCOUNT_USList UNSET = new ACCOUNT_USList(true);
	[Generate]
	public static readonly ACCOUNT_USList DEFAULT = new ACCOUNT_USList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private ACCOUNT_USList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public ACCOUNT_USList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IACCOUNT_US this[int index] {
	    get {
		return (IACCOUNT_US)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IACCOUNT_US value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IACCOUNT_US value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IACCOUNT_US value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IACCOUNT_US) {
		    Add((IACCOUNT_US)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IACCOUNT_US");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IACCOUNT_US this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as IACCOUNT_US;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public ACCOUNT_USList RetainAll(ACCOUNT_USList list) {
	    ACCOUNT_USList result = new ACCOUNT_USList();

	    foreach (IACCOUNT_US data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public ACCOUNT_USList RemoveAll(ACCOUNT_USList list) {
	    ACCOUNT_USList result = new ACCOUNT_USList();

	    foreach (IACCOUNT_US data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		IACCOUNT_US objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_US o1 = (IACCOUNT_US)a;
		IACCOUNT_US o2 = (IACCOUNT_US)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class US_LOANSTATUSSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_US o1 = (IACCOUNT_US)a;
		IACCOUNT_US o2 = (IACCOUNT_US)b;

		if (o1 == null || o2 == null || !o1.US_LOANSTATUS.IsValid || !o2.US_LOANSTATUS.IsValid) {
		    return 0;
		}
		return o1.US_LOANSTATUS.CompareTo(o2.US_LOANSTATUS);
	    }
	}

	[Generate]
	public class US_DELI_HISTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IACCOUNT_US o1 = (IACCOUNT_US)a;
		IACCOUNT_US o2 = (IACCOUNT_US)b;

		if (o1 == null || o2 == null || !o1.US_DELI_HIST.IsValid || !o2.US_DELI_HIST.IsValid) {
		    return 0;
		}
		return o1.US_DELI_HIST.CompareTo(o2.US_DELI_HIST);
	    }
	}
    }
}
