using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class CLIENT_REL_DATAFields {
	private CLIENT_REL_DATAFields() {}
	public static readonly String ENTITY_NAME = "CLIENT_REL_DATA";

	public static readonly ColumnMetaData MEM_NUM = new ColumnMetaData("MEM_NUM", "MEM_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData AGE = new ColumnMetaData("AGE", "AGE", DbType.Int32, SqlDbType.Int, 0, 10, 0);
    }

    public interface ICLIENT_REL_DATA : IBusinessEntity {
	IdType MEM_NUM {
	    get;
	}
	IntegerType AGE {
	    get;
	}
    }
}
