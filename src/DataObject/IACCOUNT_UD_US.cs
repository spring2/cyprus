using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class ACCOUNT_UD_USFields {
	private ACCOUNT_UD_USFields() {}
	public static readonly String ENTITY_NAME = "ACCOUNT_UD_US";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData US_INCOME_STATED = new ColumnMetaData("US_INCOME_STATED", "US_INCOME_STATED", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_NUM_OF_EXTENSIONS = new ColumnMetaData("US_NUM_OF_EXTENSIONS", "US_NUM_OF_EXTENSIONS", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_1ST_EXT_DTE = new ColumnMetaData("US_1ST_EXT_DTE", "US_1ST_EXT_DTE", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_MOSTRECENT_EXT_DTE = new ColumnMetaData("US_MOSTRECENT_EXT_DTE", "US_MOSTRECENT_EXT_DTE", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_L_RISKLEV = new ColumnMetaData("US_L_RISKLEV", "US_L_RISKLEV", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_RISKLEVEL_DTE = new ColumnMetaData("US_RISKLEVEL_DTE", "US_RISKLEVEL_DTE", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_WATCHLIST = new ColumnMetaData("US_WATCHLIST", "US_WATCHLIST", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_DATE_WATCHLIST = new ColumnMetaData("US_DATE_WATCHLIST", "US_DATE_WATCHLIST", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_WORKOUT_LOANS = new ColumnMetaData("US_WORKOUT_LOANS", "US_WORKOUT_LOANS", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_WORKOUT_DTE = new ColumnMetaData("US_WORKOUT_DTE", "US_WORKOUT_DTE", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_BUS_LOAN_PER_REG = new ColumnMetaData("US_UD_BUS_LOAN_PER_REG", "US_UD_BUS_LOAN_PER_REG", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_BUS_LOAN_COL_GRP = new ColumnMetaData("US_UD_BUS_LOAN_COL_GRP", "US_UD_BUS_LOAN_COL_GRP", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_BUS_LOAN_COL_DESC = new ColumnMetaData("US_UD_BUS_LOAN_COL_DESC", "US_UD_BUS_LOAN_COL_DESC", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_BUS_LOAN_PART = new ColumnMetaData("US_UD_BUS_LOAN_PART", "US_UD_BUS_LOAN_PART", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_SBA_GUARANTY = new ColumnMetaData("US_UD_SBA_GUARANTY", "US_UD_SBA_GUARANTY", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_1ST_MORT_TYPE = new ColumnMetaData("US_UD_1ST_MORT_TYPE", "US_UD_1ST_MORT_TYPE", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
	public static readonly ColumnMetaData US_UD_BUS_LOAN_PRCNT_PART = new ColumnMetaData("US_UD_BUS_LOAN_PRCNT_PART", "US_UD_BUS_LOAN_PRCNT_PART", DbType.AnsiString, SqlDbType.VarChar, 1000, 0, 0);
    }

    public interface IACCOUNT_UD_US : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	StringType US_INCOME_STATED {
	    get;
	}
	StringType US_NUM_OF_EXTENSIONS {
	    get;
	}
	StringType US_1ST_EXT_DTE {
	    get;
	}
	StringType US_MOSTRECENT_EXT_DTE {
	    get;
	}
	StringType US_L_RISKLEV {
	    get;
	}
	StringType US_RISKLEVEL_DTE {
	    get;
	}
	StringType US_WATCHLIST {
	    get;
	}
	StringType US_DATE_WATCHLIST {
	    get;
	}
	StringType US_WORKOUT_LOANS {
	    get;
	}
	StringType US_WORKOUT_DTE {
	    get;
	}
	StringType US_UD_BUS_LOAN_PER_REG {
	    get;
	}
	StringType US_UD_BUS_LOAN_COL_GRP {
	    get;
	}
	StringType US_UD_BUS_LOAN_COL_DESC {
	    get;
	}
	StringType US_UD_BUS_LOAN_PART {
	    get;
	}
	StringType US_UD_SBA_GUARANTY {
	    get;
	}
	StringType US_UD_1ST_MORT_TYPE {
	    get;
	}
	StringType US_UD_BUS_LOAN_PRCNT_PART {
	    get;
	}
    }
}
