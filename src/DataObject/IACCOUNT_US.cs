using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class ACCOUNT_USFields {
	private ACCOUNT_USFields() {}
	public static readonly String ENTITY_NAME = "ACCOUNT_US";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData US_LOANSTATUS = new ColumnMetaData("US_LOANSTATUS", "US_LOANSTATUS", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData US_DELI_HIST = new ColumnMetaData("US_DELI_HIST", "US_DELI_HIST", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface IACCOUNT_US : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	StringType US_LOANSTATUS {
	    get;
	}
	StringType US_DELI_HIST {
	    get;
	}
    }
}
