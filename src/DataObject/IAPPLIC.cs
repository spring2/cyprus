using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class APPLICFields {
	private APPLICFields() {}
	public static readonly String ENTITY_NAME = "APPLIC";

	public static readonly ColumnMetaData APPLIC_ID = new ColumnMetaData("APPLIC_ID", "APPLIC_ID", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData APPLIC_NUM = new ColumnMetaData("APPLIC_NUM", "APPLIC_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FINAL_STAMP_BR = new ColumnMetaData("FINAL_STAMP_BR", "FINAL_STAMP_BR", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LOAD_STAMP_BR = new ColumnMetaData("LOAD_STAMP_BR", "LOAD_STAMP_BR", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData STAGE2_CRSCORE = new ColumnMetaData("STAGE2_CRSCORE", "STAGE2_CRSCORE", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData AP_RISK_SCORE = new ColumnMetaData("AP_RISK_SCORE", "AP_RISK_SCORE", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData BANKRUPT_SCORE = new ColumnMetaData("BANKRUPT_SCORE", "BANKRUPT_SCORE", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData AP_RISK_LEVEL = new ColumnMetaData("AP_RISK_LEVEL", "AP_RISK_LEVEL", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData ALPS_COLOR = new ColumnMetaData("ALPS_COLOR", "ALPS_COLOR", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData PAYMENT_FREQ = new ColumnMetaData("PAYMENT_FREQ", "PAYMENT_FREQ", DbType.AnsiString, SqlDbType.VarChar, 2, 0, 0);
	public static readonly ColumnMetaData TOT_FIN = new ColumnMetaData("TOT_FIN", "TOT_FIN", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData BALLOON_DATE = new ColumnMetaData("BALLOON_DATE", "BALLOON_DATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData DEALER_NUMBER = new ColumnMetaData("DEALER_NUMBER", "DEALER_NUMBER", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CUDL_DEALER_NAME = new ColumnMetaData("CUDL_DEALER_NAME", "CUDL_DEALER_NAME", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData AP_RP_DISCOUNT = new ColumnMetaData("AP_RP_DISCOUNT", "AP_RP_DISCOUNT", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CRSCORE_TYPE = new ColumnMetaData("CRSCORE_TYPE", "CRSCORE_TYPE", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData HELOC_TYPE = new ColumnMetaData("HELOC_TYPE", "HELOC_TYPE", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData HELOC_OCCUPANCY = new ColumnMetaData("HELOC_OCCUPANCY", "HELOC_OCCUPANCY", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData HELOC_VALUE = new ColumnMetaData("HELOC_VALUE", "HELOC_VALUE", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData HELOC_1STMORT_BAL = new ColumnMetaData("HELOC_1STMORT_BAL", "HELOC_1STMORT_BAL", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData HELOC_1STMORT_PAY = new ColumnMetaData("HELOC_1STMORT_PAY", "HELOC_1STMORT_PAY", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData HELOC_2NDMORT_BAL = new ColumnMetaData("HELOC_2NDMORT_BAL", "HELOC_2NDMORT_BAL", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData HELOC_2NDMORT_PAY = new ColumnMetaData("HELOC_2NDMORT_PAY", "HELOC_2NDMORT_PAY", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData HELOC_ORIG_DATE = new ColumnMetaData("HELOC_ORIG_DATE", "HELOC_ORIG_DATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData INSURE_CODE = new ColumnMetaData("INSURE_CODE", "INSURE_CODE", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData AP_INTEREST_RATE = new ColumnMetaData("AP_INTEREST_RATE", "AP_INTEREST_RATE", DbType.Decimal, SqlDbType.Decimal, 0, 18, 8);
    }

    public interface IAPPLIC : IBusinessEntity {
	IdType APPLIC_ID {
	    get;
	}
	IdType APPLIC_NUM {
	    get;
	}
	IntegerType FINAL_STAMP_BR {
	    get;
	}
	IntegerType LOAD_STAMP_BR {
	    get;
	}
	IntegerType STAGE2_CRSCORE {
	    get;
	}
	IntegerType AP_RISK_SCORE {
	    get;
	}
	IntegerType BANKRUPT_SCORE {
	    get;
	}
	StringType AP_RISK_LEVEL {
	    get;
	}
	IntegerType ALPS_COLOR {
	    get;
	}
	StringType PAYMENT_FREQ {
	    get;
	}
	CurrencyType TOT_FIN {
	    get;
	}
	DateTimeType BALLOON_DATE {
	    get;
	}
	IntegerType DEALER_NUMBER {
	    get;
	}
	StringType CUDL_DEALER_NAME {
	    get;
	}
	IntegerType AP_RP_DISCOUNT {
	    get;
	}
	StringType CRSCORE_TYPE {
	    get;
	}
	StringType HELOC_TYPE {
	    get;
	}
	StringType HELOC_OCCUPANCY {
	    get;
	}
	StringType HELOC_VALUE {
	    get;
	}
	CurrencyType HELOC_1STMORT_BAL {
	    get;
	}
	CurrencyType HELOC_1STMORT_PAY {
	    get;
	}
	CurrencyType HELOC_2NDMORT_BAL {
	    get;
	}
	CurrencyType HELOC_2NDMORT_PAY {
	    get;
	}
	DateTimeType HELOC_ORIG_DATE {
	    get;
	}
	StringType INSURE_CODE {
	    get;
	}
	DecimalType AP_INTEREST_RATE {
	    get;
	}
    }
}
