using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class ACCOUNTFields {
	private ACCOUNTFields() {}
	public static readonly String ENTITY_NAME = "ACCOUNT";

	public static readonly ColumnMetaData ACCT_NUM = new ColumnMetaData("ACCT_NUM", "ACCT_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData MEM_NUM = new ColumnMetaData("MEM_NUM", "MEM_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_ACCT_LEVEL = new ColumnMetaData("L_ACCT_LEVEL", "L_ACCT_LEVEL", DbType.Decimal, SqlDbType.Decimal, 0, 18, 8);
	public static readonly ColumnMetaData ACCT_SUBLEV = new ColumnMetaData("ACCT_SUBLEV", "ACCT_SUBLEV", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData ACCOUNT_OPEN_DATE = new ColumnMetaData("ACCOUNT_OPEN_DATE", "ACCOUNT_OPEN_DATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData SPEC_REPORT_CODE = new ColumnMetaData("SPEC_REPORT_CODE", "SPEC_REPORT_CODE", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData ORIG_PAY = new ColumnMetaData("ORIG_PAY", "ORIG_PAY", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData APR = new ColumnMetaData("APR", "APR", DbType.Decimal, SqlDbType.Decimal, 0, 18, 8);
	public static readonly ColumnMetaData CURR_CREDIT_LIM = new ColumnMetaData("CURR_CREDIT_LIM", "CURR_CREDIT_LIM", DbType.Currency, SqlDbType.Money, 0, 0, 0);
	public static readonly ColumnMetaData PG_CODE = new ColumnMetaData("PG_CODE", "PG_CODE", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData DISC_REASON = new ColumnMetaData("DISC_REASON", "DISC_REASON", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData RISK_OFFSET_VAL = new ColumnMetaData("RISK_OFFSET_VAL", "RISK_OFFSET_VAL", DbType.AnsiString, SqlDbType.VarChar, 150, 0, 0);
	public static readonly ColumnMetaData FIRST_PAYDATE = new ColumnMetaData("FIRST_PAYDATE", "FIRST_PAYDATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData ORIG_LOAN_TERM = new ColumnMetaData("ORIG_LOAN_TERM", "ORIG_LOAN_TERM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_MATURITY_DATE = new ColumnMetaData("L_MATURITY_DATE", "L_MATURITY_DATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData L_INSCODE = new ColumnMetaData("L_INSCODE", "L_INSCODE", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_SECCODE = new ColumnMetaData("L_SECCODE", "L_SECCODE", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_SECDESC1 = new ColumnMetaData("L_SECDESC1", "L_SECDESC1", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData L_SECDESC2 = new ColumnMetaData("L_SECDESC2", "L_SECDESC2", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData US_APPLIC_ID = new ColumnMetaData("US_APPLIC_ID", "US_APPLIC_ID", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData L_ACCT_TYPE = new ColumnMetaData("L_ACCT_TYPE", "L_ACCT_TYPE", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
    }

    public interface IACCOUNT : IBusinessEntity {
	IdType ACCT_NUM {
	    get;
	}
	IdType MEM_NUM {
	    get;
	}
	DecimalType L_ACCT_LEVEL {
	    get;
	}
	IntegerType ACCT_SUBLEV {
	    get;
	}
	DateTimeType ACCOUNT_OPEN_DATE {
	    get;
	}
	StringType SPEC_REPORT_CODE {
	    get;
	}
	CurrencyType ORIG_PAY {
	    get;
	}
	DecimalType APR {
	    get;
	}
	CurrencyType CURR_CREDIT_LIM {
	    get;
	}
	StringType PG_CODE {
	    get;
	}
	StringType DISC_REASON {
	    get;
	}
	StringType RISK_OFFSET_VAL {
	    get;
	}
	DateTimeType FIRST_PAYDATE {
	    get;
	}
	IntegerType ORIG_LOAN_TERM {
	    get;
	}
	DateTimeType L_MATURITY_DATE {
	    get;
	}
	IntegerType L_INSCODE {
	    get;
	}
	IntegerType L_SECCODE {
	    get;
	}
	StringType L_SECDESC1 {
	    get;
	}
	StringType L_SECDESC2 {
	    get;
	}
	IdType US_APPLIC_ID {
	    get;
	}
	StringType L_ACCT_TYPE {
	    get;
	}
    }
}
