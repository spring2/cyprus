using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ICLIENT generic collection
    /// </summary>
    public class CLIENTList : CollectionBase {
	[Generate]
	public static readonly CLIENTList UNSET = new CLIENTList(true);
	[Generate]
	public static readonly CLIENTList DEFAULT = new CLIENTList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private CLIENTList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public CLIENTList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ICLIENT this[int index] {
	    get {
		return (ICLIENT)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.MEM_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ICLIENT value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.MEM_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ICLIENT value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.MEM_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ICLIENT value = this[index];
		    keys.Remove(value.MEM_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ICLIENT) {
		    Add((ICLIENT)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ICLIENT");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType mEM_NUM) {
	    return keys.Contains(mEM_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ICLIENT this[IdType mEM_NUM] {
	    get {
		return keys[mEM_NUM] as ICLIENT;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public CLIENTList RetainAll(CLIENTList list) {
	    CLIENTList result = new CLIENTList();

	    foreach (ICLIENT data in List) {
		if (list.Contains(data.MEM_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public CLIENTList RemoveAll(CLIENTList list) {
	    CLIENTList result = new CLIENTList();

	    foreach (ICLIENT data in List) {
		if (!list.Contains(data.MEM_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType mEM_NUM) {
	    if (!immutable) {
		ICLIENT objectInList = this[mEM_NUM];
		List.Remove(objectInList);
		keys.Remove(mEM_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class MEM_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ICLIENT o1 = (ICLIENT)a;
		ICLIENT o2 = (ICLIENT)b;

		if (o1 == null || o2 == null || !o1.MEM_NUM.IsValid || !o2.MEM_NUM.IsValid) {
		    return 0;
		}
		return o1.MEM_NUM.CompareTo(o2.MEM_NUM);
	    }
	}

	[Generate]
	public class CLIENT_CLASSSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ICLIENT o1 = (ICLIENT)a;
		ICLIENT o2 = (ICLIENT)b;

		if (o1 == null || o2 == null || !o1.CLIENT_CLASS.IsValid || !o2.CLIENT_CLASS.IsValid) {
		    return 0;
		}
		return o1.CLIENT_CLASS.CompareTo(o2.CLIENT_CLASS);
	    }
	}

	[Generate]
	public class JOIN_DATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ICLIENT o1 = (ICLIENT)a;
		ICLIENT o2 = (ICLIENT)b;

		if (o1 == null || o2 == null || !o1.JOIN_DATE.IsValid || !o2.JOIN_DATE.IsValid) {
		    return 0;
		}
		return o1.JOIN_DATE.CompareTo(o2.JOIN_DATE);
	    }
	}

	[Generate]
	public class SEXSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ICLIENT o1 = (ICLIENT)a;
		ICLIENT o2 = (ICLIENT)b;

		if (o1 == null || o2 == null || !o1.SEX.IsValid || !o2.SEX.IsValid) {
		    return 0;
		}
		return o1.SEX.CompareTo(o2.SEX);
	    }
	}
    }
}
