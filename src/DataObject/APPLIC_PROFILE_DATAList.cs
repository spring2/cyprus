using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IAPPLIC_PROFILE_DATA generic collection
    /// </summary>
    public class APPLIC_PROFILE_DATAList : CollectionBase {
	[Generate]
	public static readonly APPLIC_PROFILE_DATAList UNSET = new APPLIC_PROFILE_DATAList(true);
	[Generate]
	public static readonly APPLIC_PROFILE_DATAList DEFAULT = new APPLIC_PROFILE_DATAList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private APPLIC_PROFILE_DATAList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public APPLIC_PROFILE_DATAList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IAPPLIC_PROFILE_DATA this[int index] {
	    get {
		return (IAPPLIC_PROFILE_DATA)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.APPLIC_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IAPPLIC_PROFILE_DATA value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.APPLIC_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IAPPLIC_PROFILE_DATA value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.APPLIC_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IAPPLIC_PROFILE_DATA value = this[index];
		    keys.Remove(value.APPLIC_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IAPPLIC_PROFILE_DATA) {
		    Add((IAPPLIC_PROFILE_DATA)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IAPPLIC_PROFILE_DATA");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aPPLIC_NUM) {
	    return keys.Contains(aPPLIC_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IAPPLIC_PROFILE_DATA this[IdType aPPLIC_NUM] {
	    get {
		return keys[aPPLIC_NUM] as IAPPLIC_PROFILE_DATA;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public APPLIC_PROFILE_DATAList RetainAll(APPLIC_PROFILE_DATAList list) {
	    APPLIC_PROFILE_DATAList result = new APPLIC_PROFILE_DATAList();

	    foreach (IAPPLIC_PROFILE_DATA data in List) {
		if (list.Contains(data.APPLIC_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public APPLIC_PROFILE_DATAList RemoveAll(APPLIC_PROFILE_DATAList list) {
	    APPLIC_PROFILE_DATAList result = new APPLIC_PROFILE_DATAList();

	    foreach (IAPPLIC_PROFILE_DATA data in List) {
		if (!list.Contains(data.APPLIC_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aPPLIC_NUM) {
	    if (!immutable) {
		IAPPLIC_PROFILE_DATA objectInList = this[aPPLIC_NUM];
		List.Remove(objectInList);
		keys.Remove(aPPLIC_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class APPLIC_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC_PROFILE_DATA o1 = (IAPPLIC_PROFILE_DATA)a;
		IAPPLIC_PROFILE_DATA o2 = (IAPPLIC_PROFILE_DATA)b;

		if (o1 == null || o2 == null || !o1.APPLIC_NUM.IsValid || !o2.APPLIC_NUM.IsValid) {
		    return 0;
		}
		return o1.APPLIC_NUM.CompareTo(o2.APPLIC_NUM);
	    }
	}

	[Generate]
	public class NO_YRS_ADDRSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC_PROFILE_DATA o1 = (IAPPLIC_PROFILE_DATA)a;
		IAPPLIC_PROFILE_DATA o2 = (IAPPLIC_PROFILE_DATA)b;

		if (o1 == null || o2 == null || !o1.NO_YRS_ADDR.IsValid || !o2.NO_YRS_ADDR.IsValid) {
		    return 0;
		}
		return o1.NO_YRS_ADDR.CompareTo(o2.NO_YRS_ADDR);
	    }
	}

	[Generate]
	public class PREV_YRS_ADDRSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC_PROFILE_DATA o1 = (IAPPLIC_PROFILE_DATA)a;
		IAPPLIC_PROFILE_DATA o2 = (IAPPLIC_PROFILE_DATA)b;

		if (o1 == null || o2 == null || !o1.PREV_YRS_ADDR.IsValid || !o2.PREV_YRS_ADDR.IsValid) {
		    return 0;
		}
		return o1.PREV_YRS_ADDR.CompareTo(o2.PREV_YRS_ADDR);
	    }
	}
    }
}
