using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class RiskTypeFields {
	private RiskTypeFields() {}
	public static readonly String ENTITY_NAME = "RiskType";

	public static readonly ColumnMetaData RISKTYPEID = new ColumnMetaData("RiskTypeId", "RiskTypeId", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LTYPE = new ColumnMetaData("LType", "LType", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData RISKTYPEDESC = new ColumnMetaData("RiskTypeDesc", "RiskTypeDesc", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface IRiskType : IBusinessEntity {
	IdType RiskTypeId {
	    get;
	}
	IntegerType LType {
	    get;
	}
	StringType RiskTypeDesc {
	    get;
	}
    }
}
