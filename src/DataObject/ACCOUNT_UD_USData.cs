using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class ACCOUNT_UD_USData : Spring2.Core.DataObject.DataObject {
	public static readonly ACCOUNT_UD_USData DEFAULT = new ACCOUNT_UD_USData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private StringType uS_INCOME_STATED = StringType.DEFAULT;
	private StringType uS_NUM_OF_EXTENSIONS = StringType.DEFAULT;
	private StringType uS_1ST_EXT_DTE = StringType.DEFAULT;
	private StringType uS_MOSTRECENT_EXT_DTE = StringType.DEFAULT;
	private StringType uS_L_RISKLEV = StringType.DEFAULT;
	private StringType uS_RISKLEVEL_DTE = StringType.DEFAULT;
	private StringType uS_WATCHLIST = StringType.DEFAULT;
	private StringType uS_DATE_WATCHLIST = StringType.DEFAULT;
	private StringType uS_WORKOUT_LOANS = StringType.DEFAULT;
	private StringType uS_WORKOUT_DTE = StringType.DEFAULT;
	private StringType uS_UD_BUS_LOAN_PER_REG = StringType.DEFAULT;
	private StringType uS_UD_BUS_LOAN_COL_GRP = StringType.DEFAULT;
	private StringType uS_UD_BUS_LOAN_COL_DESC = StringType.DEFAULT;
	private StringType uS_UD_BUS_LOAN_PART = StringType.DEFAULT;
	private StringType uS_UD_SBA_GUARANTY = StringType.DEFAULT;
	private StringType uS_UD_1ST_MORT_TYPE = StringType.DEFAULT;
	private StringType uS_UD_BUS_LOAN_PRCNT_PART = StringType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public StringType US_INCOME_STATED {
	    get { return this.uS_INCOME_STATED; }
	    set { this.uS_INCOME_STATED = value; }
	}

	public StringType US_NUM_OF_EXTENSIONS {
	    get { return this.uS_NUM_OF_EXTENSIONS; }
	    set { this.uS_NUM_OF_EXTENSIONS = value; }
	}

	public StringType US_1ST_EXT_DTE {
	    get { return this.uS_1ST_EXT_DTE; }
	    set { this.uS_1ST_EXT_DTE = value; }
	}

	public StringType US_MOSTRECENT_EXT_DTE {
	    get { return this.uS_MOSTRECENT_EXT_DTE; }
	    set { this.uS_MOSTRECENT_EXT_DTE = value; }
	}

	public StringType US_L_RISKLEV {
	    get { return this.uS_L_RISKLEV; }
	    set { this.uS_L_RISKLEV = value; }
	}

	public StringType US_RISKLEVEL_DTE {
	    get { return this.uS_RISKLEVEL_DTE; }
	    set { this.uS_RISKLEVEL_DTE = value; }
	}

	public StringType US_WATCHLIST {
	    get { return this.uS_WATCHLIST; }
	    set { this.uS_WATCHLIST = value; }
	}

	public StringType US_DATE_WATCHLIST {
	    get { return this.uS_DATE_WATCHLIST; }
	    set { this.uS_DATE_WATCHLIST = value; }
	}

	public StringType US_WORKOUT_LOANS {
	    get { return this.uS_WORKOUT_LOANS; }
	    set { this.uS_WORKOUT_LOANS = value; }
	}

	public StringType US_WORKOUT_DTE {
	    get { return this.uS_WORKOUT_DTE; }
	    set { this.uS_WORKOUT_DTE = value; }
	}

	public StringType US_UD_BUS_LOAN_PER_REG {
	    get { return this.uS_UD_BUS_LOAN_PER_REG; }
	    set { this.uS_UD_BUS_LOAN_PER_REG = value; }
	}

	public StringType US_UD_BUS_LOAN_COL_GRP {
	    get { return this.uS_UD_BUS_LOAN_COL_GRP; }
	    set { this.uS_UD_BUS_LOAN_COL_GRP = value; }
	}

	public StringType US_UD_BUS_LOAN_COL_DESC {
	    get { return this.uS_UD_BUS_LOAN_COL_DESC; }
	    set { this.uS_UD_BUS_LOAN_COL_DESC = value; }
	}

	public StringType US_UD_BUS_LOAN_PART {
	    get { return this.uS_UD_BUS_LOAN_PART; }
	    set { this.uS_UD_BUS_LOAN_PART = value; }
	}

	public StringType US_UD_SBA_GUARANTY {
	    get { return this.uS_UD_SBA_GUARANTY; }
	    set { this.uS_UD_SBA_GUARANTY = value; }
	}

	public StringType US_UD_1ST_MORT_TYPE {
	    get { return this.uS_UD_1ST_MORT_TYPE; }
	    set { this.uS_UD_1ST_MORT_TYPE = value; }
	}

	public StringType US_UD_BUS_LOAN_PRCNT_PART {
	    get { return this.uS_UD_BUS_LOAN_PRCNT_PART; }
	    set { this.uS_UD_BUS_LOAN_PRCNT_PART = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
