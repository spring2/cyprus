using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IAPPLIC generic collection
    /// </summary>
    public class APPLICList : CollectionBase {
	[Generate]
	public static readonly APPLICList UNSET = new APPLICList(true);
	[Generate]
	public static readonly APPLICList DEFAULT = new APPLICList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private APPLICList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public APPLICList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IAPPLIC this[int index] {
	    get {
		return (IAPPLIC)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.APPLIC_ID] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IAPPLIC value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.APPLIC_ID] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IAPPLIC value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.APPLIC_ID] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IAPPLIC value = this[index];
		    keys.Remove(value.APPLIC_ID);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IAPPLIC) {
		    Add((IAPPLIC)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IAPPLIC");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aPPLIC_ID) {
	    return keys.Contains(aPPLIC_ID);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IAPPLIC this[IdType aPPLIC_ID] {
	    get {
		return keys[aPPLIC_ID] as IAPPLIC;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public APPLICList RetainAll(APPLICList list) {
	    APPLICList result = new APPLICList();

	    foreach (IAPPLIC data in List) {
		if (list.Contains(data.APPLIC_ID)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public APPLICList RemoveAll(APPLICList list) {
	    APPLICList result = new APPLICList();

	    foreach (IAPPLIC data in List) {
		if (!list.Contains(data.APPLIC_ID)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aPPLIC_ID) {
	    if (!immutable) {
		IAPPLIC objectInList = this[aPPLIC_ID];
		List.Remove(objectInList);
		keys.Remove(aPPLIC_ID);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class APPLIC_IDSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.APPLIC_ID.IsValid || !o2.APPLIC_ID.IsValid) {
		    return 0;
		}
		return o1.APPLIC_ID.CompareTo(o2.APPLIC_ID);
	    }
	}

	[Generate]
	public class APPLIC_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.APPLIC_NUM.IsValid || !o2.APPLIC_NUM.IsValid) {
		    return 0;
		}
		return o1.APPLIC_NUM.CompareTo(o2.APPLIC_NUM);
	    }
	}

	[Generate]
	public class FINAL_STAMP_BRSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.FINAL_STAMP_BR.IsValid || !o2.FINAL_STAMP_BR.IsValid) {
		    return 0;
		}
		return o1.FINAL_STAMP_BR.CompareTo(o2.FINAL_STAMP_BR);
	    }
	}

	[Generate]
	public class LOAD_STAMP_BRSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.LOAD_STAMP_BR.IsValid || !o2.LOAD_STAMP_BR.IsValid) {
		    return 0;
		}
		return o1.LOAD_STAMP_BR.CompareTo(o2.LOAD_STAMP_BR);
	    }
	}

	[Generate]
	public class STAGE2_CRSCORESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.STAGE2_CRSCORE.IsValid || !o2.STAGE2_CRSCORE.IsValid) {
		    return 0;
		}
		return o1.STAGE2_CRSCORE.CompareTo(o2.STAGE2_CRSCORE);
	    }
	}

	[Generate]
	public class AP_RISK_SCORESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.AP_RISK_SCORE.IsValid || !o2.AP_RISK_SCORE.IsValid) {
		    return 0;
		}
		return o1.AP_RISK_SCORE.CompareTo(o2.AP_RISK_SCORE);
	    }
	}

	[Generate]
	public class BANKRUPT_SCORESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.BANKRUPT_SCORE.IsValid || !o2.BANKRUPT_SCORE.IsValid) {
		    return 0;
		}
		return o1.BANKRUPT_SCORE.CompareTo(o2.BANKRUPT_SCORE);
	    }
	}

	[Generate]
	public class AP_RISK_LEVELSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.AP_RISK_LEVEL.IsValid || !o2.AP_RISK_LEVEL.IsValid) {
		    return 0;
		}
		return o1.AP_RISK_LEVEL.CompareTo(o2.AP_RISK_LEVEL);
	    }
	}

	[Generate]
	public class ALPS_COLORSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.ALPS_COLOR.IsValid || !o2.ALPS_COLOR.IsValid) {
		    return 0;
		}
		return o1.ALPS_COLOR.CompareTo(o2.ALPS_COLOR);
	    }
	}

	[Generate]
	public class PAYMENT_FREQSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.PAYMENT_FREQ.IsValid || !o2.PAYMENT_FREQ.IsValid) {
		    return 0;
		}
		return o1.PAYMENT_FREQ.CompareTo(o2.PAYMENT_FREQ);
	    }
	}

	[Generate]
	public class TOT_FINSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.TOT_FIN.IsValid || !o2.TOT_FIN.IsValid) {
		    return 0;
		}
		return o1.TOT_FIN.CompareTo(o2.TOT_FIN);
	    }
	}

	[Generate]
	public class BALLOON_DATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.BALLOON_DATE.IsValid || !o2.BALLOON_DATE.IsValid) {
		    return 0;
		}
		return o1.BALLOON_DATE.CompareTo(o2.BALLOON_DATE);
	    }
	}

	[Generate]
	public class DEALER_NUMBERSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.DEALER_NUMBER.IsValid || !o2.DEALER_NUMBER.IsValid) {
		    return 0;
		}
		return o1.DEALER_NUMBER.CompareTo(o2.DEALER_NUMBER);
	    }
	}

	[Generate]
	public class CUDL_DEALER_NAMESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.CUDL_DEALER_NAME.IsValid || !o2.CUDL_DEALER_NAME.IsValid) {
		    return 0;
		}
		return o1.CUDL_DEALER_NAME.CompareTo(o2.CUDL_DEALER_NAME);
	    }
	}

	[Generate]
	public class AP_RP_DISCOUNTSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.AP_RP_DISCOUNT.IsValid || !o2.AP_RP_DISCOUNT.IsValid) {
		    return 0;
		}
		return o1.AP_RP_DISCOUNT.CompareTo(o2.AP_RP_DISCOUNT);
	    }
	}

	[Generate]
	public class CRSCORE_TYPESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.CRSCORE_TYPE.IsValid || !o2.CRSCORE_TYPE.IsValid) {
		    return 0;
		}
		return o1.CRSCORE_TYPE.CompareTo(o2.CRSCORE_TYPE);
	    }
	}

	[Generate]
	public class HELOC_TYPESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_TYPE.IsValid || !o2.HELOC_TYPE.IsValid) {
		    return 0;
		}
		return o1.HELOC_TYPE.CompareTo(o2.HELOC_TYPE);
	    }
	}

	[Generate]
	public class HELOC_OCCUPANCYSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_OCCUPANCY.IsValid || !o2.HELOC_OCCUPANCY.IsValid) {
		    return 0;
		}
		return o1.HELOC_OCCUPANCY.CompareTo(o2.HELOC_OCCUPANCY);
	    }
	}

	[Generate]
	public class HELOC_VALUESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_VALUE.IsValid || !o2.HELOC_VALUE.IsValid) {
		    return 0;
		}
		return o1.HELOC_VALUE.CompareTo(o2.HELOC_VALUE);
	    }
	}

	[Generate]
	public class HELOC_1STMORT_BALSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_1STMORT_BAL.IsValid || !o2.HELOC_1STMORT_BAL.IsValid) {
		    return 0;
		}
		return o1.HELOC_1STMORT_BAL.CompareTo(o2.HELOC_1STMORT_BAL);
	    }
	}

	[Generate]
	public class HELOC_1STMORT_PAYSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_1STMORT_PAY.IsValid || !o2.HELOC_1STMORT_PAY.IsValid) {
		    return 0;
		}
		return o1.HELOC_1STMORT_PAY.CompareTo(o2.HELOC_1STMORT_PAY);
	    }
	}

	[Generate]
	public class HELOC_2NDMORT_BALSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_2NDMORT_BAL.IsValid || !o2.HELOC_2NDMORT_BAL.IsValid) {
		    return 0;
		}
		return o1.HELOC_2NDMORT_BAL.CompareTo(o2.HELOC_2NDMORT_BAL);
	    }
	}

	[Generate]
	public class HELOC_2NDMORT_PAYSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_2NDMORT_PAY.IsValid || !o2.HELOC_2NDMORT_PAY.IsValid) {
		    return 0;
		}
		return o1.HELOC_2NDMORT_PAY.CompareTo(o2.HELOC_2NDMORT_PAY);
	    }
	}

	[Generate]
	public class HELOC_ORIG_DATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.HELOC_ORIG_DATE.IsValid || !o2.HELOC_ORIG_DATE.IsValid) {
		    return 0;
		}
		return o1.HELOC_ORIG_DATE.CompareTo(o2.HELOC_ORIG_DATE);
	    }
	}

	[Generate]
	public class INSURE_CODESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.INSURE_CODE.IsValid || !o2.INSURE_CODE.IsValid) {
		    return 0;
		}
		return o1.INSURE_CODE.CompareTo(o2.INSURE_CODE);
	    }
	}

	[Generate]
	public class AP_INTEREST_RATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IAPPLIC o1 = (IAPPLIC)a;
		IAPPLIC o2 = (IAPPLIC)b;

		if (o1 == null || o2 == null || !o1.AP_INTEREST_RATE.IsValid || !o2.AP_INTEREST_RATE.IsValid) {
		    return 0;
		}
		return o1.AP_INTEREST_RATE.CompareTo(o2.AP_INTEREST_RATE);
	    }
	}
    }
}
