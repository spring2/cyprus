using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class CLIENTFields {
	private CLIENTFields() {}
	public static readonly String ENTITY_NAME = "CLIENT";

	public static readonly ColumnMetaData MEM_NUM = new ColumnMetaData("MEM_NUM", "MEM_NUM", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CLIENT_CLASS = new ColumnMetaData("CLIENT_CLASS", "CLIENT_CLASS", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData JOIN_DATE = new ColumnMetaData("JOIN_DATE", "JOIN_DATE", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData SEX = new ColumnMetaData("SEX", "SEX", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
    }

    public interface ICLIENT : IBusinessEntity {
	IdType MEM_NUM {
	    get;
	}
	IntegerType CLIENT_CLASS {
	    get;
	}
	DateTimeType JOIN_DATE {
	    get;
	}
	StringType SEX {
	    get;
	}
    }
}
