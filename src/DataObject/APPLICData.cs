using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class APPLICData : Spring2.Core.DataObject.DataObject {
	public static readonly APPLICData DEFAULT = new APPLICData();

	private IdType aPPLIC_ID = IdType.DEFAULT;
	private IdType aPPLIC_NUM = IdType.DEFAULT;
	private IntegerType fINAL_STAMP_BR = IntegerType.DEFAULT;
	private IntegerType lOAD_STAMP_BR = IntegerType.DEFAULT;
	private IntegerType sTAGE2_CRSCORE = IntegerType.DEFAULT;
	private IntegerType aP_RISK_SCORE = IntegerType.DEFAULT;
	private IntegerType bANKRUPT_SCORE = IntegerType.DEFAULT;
	private StringType aP_RISK_LEVEL = StringType.DEFAULT;
	private IntegerType aLPS_COLOR = IntegerType.DEFAULT;
	private StringType pAYMENT_FREQ = StringType.DEFAULT;
	private CurrencyType tOT_FIN = CurrencyType.DEFAULT;
	private DateTimeType bALLOON_DATE = DateTimeType.DEFAULT;
	private IntegerType dEALER_NUMBER = IntegerType.DEFAULT;
	private StringType cUDL_DEALER_NAME = StringType.DEFAULT;
	private IntegerType aP_RP_DISCOUNT = IntegerType.DEFAULT;
	private StringType cRSCORE_TYPE = StringType.DEFAULT;
	private StringType hELOC_TYPE = StringType.DEFAULT;
	private StringType hELOC_OCCUPANCY = StringType.DEFAULT;
	private StringType hELOC_VALUE = StringType.DEFAULT;
	private CurrencyType hELOC_1STMORT_BAL = CurrencyType.DEFAULT;
	private CurrencyType hELOC_1STMORT_PAY = CurrencyType.DEFAULT;
	private CurrencyType hELOC_2NDMORT_BAL = CurrencyType.DEFAULT;
	private CurrencyType hELOC_2NDMORT_PAY = CurrencyType.DEFAULT;
	private DateTimeType hELOC_ORIG_DATE = DateTimeType.DEFAULT;
	private StringType iNSURE_CODE = StringType.DEFAULT;
	private DecimalType aP_INTEREST_RATE = DecimalType.DEFAULT;

	public IdType APPLIC_ID {
	    get { return this.aPPLIC_ID; }
	    set { this.aPPLIC_ID = value; }
	}

	public IdType APPLIC_NUM {
	    get { return this.aPPLIC_NUM; }
	    set { this.aPPLIC_NUM = value; }
	}

	public IntegerType FINAL_STAMP_BR {
	    get { return this.fINAL_STAMP_BR; }
	    set { this.fINAL_STAMP_BR = value; }
	}

	public IntegerType LOAD_STAMP_BR {
	    get { return this.lOAD_STAMP_BR; }
	    set { this.lOAD_STAMP_BR = value; }
	}

	public IntegerType STAGE2_CRSCORE {
	    get { return this.sTAGE2_CRSCORE; }
	    set { this.sTAGE2_CRSCORE = value; }
	}

	public IntegerType AP_RISK_SCORE {
	    get { return this.aP_RISK_SCORE; }
	    set { this.aP_RISK_SCORE = value; }
	}

	public IntegerType BANKRUPT_SCORE {
	    get { return this.bANKRUPT_SCORE; }
	    set { this.bANKRUPT_SCORE = value; }
	}

	public StringType AP_RISK_LEVEL {
	    get { return this.aP_RISK_LEVEL; }
	    set { this.aP_RISK_LEVEL = value; }
	}

	public IntegerType ALPS_COLOR {
	    get { return this.aLPS_COLOR; }
	    set { this.aLPS_COLOR = value; }
	}

	public StringType PAYMENT_FREQ {
	    get { return this.pAYMENT_FREQ; }
	    set { this.pAYMENT_FREQ = value; }
	}

	public CurrencyType TOT_FIN {
	    get { return this.tOT_FIN; }
	    set { this.tOT_FIN = value; }
	}

	public DateTimeType BALLOON_DATE {
	    get { return this.bALLOON_DATE; }
	    set { this.bALLOON_DATE = value; }
	}

	public IntegerType DEALER_NUMBER {
	    get { return this.dEALER_NUMBER; }
	    set { this.dEALER_NUMBER = value; }
	}

	public StringType CUDL_DEALER_NAME {
	    get { return this.cUDL_DEALER_NAME; }
	    set { this.cUDL_DEALER_NAME = value; }
	}

	public IntegerType AP_RP_DISCOUNT {
	    get { return this.aP_RP_DISCOUNT; }
	    set { this.aP_RP_DISCOUNT = value; }
	}

	public StringType CRSCORE_TYPE {
	    get { return this.cRSCORE_TYPE; }
	    set { this.cRSCORE_TYPE = value; }
	}

	public StringType HELOC_TYPE {
	    get { return this.hELOC_TYPE; }
	    set { this.hELOC_TYPE = value; }
	}

	public StringType HELOC_OCCUPANCY {
	    get { return this.hELOC_OCCUPANCY; }
	    set { this.hELOC_OCCUPANCY = value; }
	}

	public StringType HELOC_VALUE {
	    get { return this.hELOC_VALUE; }
	    set { this.hELOC_VALUE = value; }
	}

	public CurrencyType HELOC_1STMORT_BAL {
	    get { return this.hELOC_1STMORT_BAL; }
	    set { this.hELOC_1STMORT_BAL = value; }
	}

	public CurrencyType HELOC_1STMORT_PAY {
	    get { return this.hELOC_1STMORT_PAY; }
	    set { this.hELOC_1STMORT_PAY = value; }
	}

	public CurrencyType HELOC_2NDMORT_BAL {
	    get { return this.hELOC_2NDMORT_BAL; }
	    set { this.hELOC_2NDMORT_BAL = value; }
	}

	public CurrencyType HELOC_2NDMORT_PAY {
	    get { return this.hELOC_2NDMORT_PAY; }
	    set { this.hELOC_2NDMORT_PAY = value; }
	}

	public DateTimeType HELOC_ORIG_DATE {
	    get { return this.hELOC_ORIG_DATE; }
	    set { this.hELOC_ORIG_DATE = value; }
	}

	public StringType INSURE_CODE {
	    get { return this.iNSURE_CODE; }
	    set { this.iNSURE_CODE = value; }
	}

	public DecimalType AP_INTEREST_RATE {
	    get { return this.aP_INTEREST_RATE; }
	    set { this.aP_INTEREST_RATE = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
