using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class APPLIC_CALCINFOData : Spring2.Core.DataObject.DataObject {
	public static readonly APPLIC_CALCINFOData DEFAULT = new APPLIC_CALCINFOData();

	private IdType aPPLIC_ID = IdType.DEFAULT;
	private DecimalType dEBT_AFTER = DecimalType.DEFAULT;

	public IdType APPLIC_ID {
	    get { return this.aPPLIC_ID; }
	    set { this.aPPLIC_ID = value; }
	}

	public DecimalType DEBT_AFTER {
	    get { return this.dEBT_AFTER; }
	    set { this.dEBT_AFTER = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
