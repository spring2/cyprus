using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ITRAN_DATA generic collection
    /// </summary>
    public class TRAN_DATAList : CollectionBase {
	[Generate]
	public static readonly TRAN_DATAList UNSET = new TRAN_DATAList(true);
	[Generate]
	public static readonly TRAN_DATAList DEFAULT = new TRAN_DATAList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private TRAN_DATAList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public TRAN_DATAList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ITRAN_DATA this[int index] {
	    get {
		return (ITRAN_DATA)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.ACCT_NUM] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ITRAN_DATA value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ITRAN_DATA value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.ACCT_NUM] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ITRAN_DATA value = this[index];
		    keys.Remove(value.ACCT_NUM);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ITRAN_DATA) {
		    Add((ITRAN_DATA)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ITRAN_DATA");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType aCCT_NUM) {
	    return keys.Contains(aCCT_NUM);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ITRAN_DATA this[IdType aCCT_NUM] {
	    get {
		return keys[aCCT_NUM] as ITRAN_DATA;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public TRAN_DATAList RetainAll(TRAN_DATAList list) {
	    TRAN_DATAList result = new TRAN_DATAList();

	    foreach (ITRAN_DATA data in List) {
		if (list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public TRAN_DATAList RemoveAll(TRAN_DATAList list) {
	    TRAN_DATAList result = new TRAN_DATAList();

	    foreach (ITRAN_DATA data in List) {
		if (!list.Contains(data.ACCT_NUM)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType aCCT_NUM) {
	    if (!immutable) {
		ITRAN_DATA objectInList = this[aCCT_NUM];
		List.Remove(objectInList);
		keys.Remove(aCCT_NUM);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class ACCT_NUMSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_DATA o1 = (ITRAN_DATA)a;
		ITRAN_DATA o2 = (ITRAN_DATA)b;

		if (o1 == null || o2 == null || !o1.ACCT_NUM.IsValid || !o2.ACCT_NUM.IsValid) {
		    return 0;
		}
		return o1.ACCT_NUM.CompareTo(o2.ACCT_NUM);
	    }
	}

	[Generate]
	public class CURRENT_BALANCESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_DATA o1 = (ITRAN_DATA)a;
		ITRAN_DATA o2 = (ITRAN_DATA)b;

		if (o1 == null || o2 == null || !o1.CURRENT_BALANCE.IsValid || !o2.CURRENT_BALANCE.IsValid) {
		    return 0;
		}
		return o1.CURRENT_BALANCE.CompareTo(o2.CURRENT_BALANCE);
	    }
	}

	[Generate]
	public class INT_RATESorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ITRAN_DATA o1 = (ITRAN_DATA)a;
		ITRAN_DATA o2 = (ITRAN_DATA)b;

		if (o1 == null || o2 == null || !o1.INT_RATE.IsValid || !o2.INT_RATE.IsValid) {
		    return 0;
		}
		return o1.INT_RATE.CompareTo(o2.INT_RATE);
	    }
	}
    }
}
