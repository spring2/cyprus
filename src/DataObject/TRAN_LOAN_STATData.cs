using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class TRAN_LOAN_STATData : Spring2.Core.DataObject.DataObject {
	public static readonly TRAN_LOAN_STATData DEFAULT = new TRAN_LOAN_STATData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private DateTimeType l_STAT_CHANGE_ACTUAL_DATE = DateTimeType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public DateTimeType L_STAT_CHANGE_ACTUAL_DATE {
	    get { return this.l_STAT_CHANGE_ACTUAL_DATE; }
	    set { this.l_STAT_CHANGE_ACTUAL_DATE = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
