using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class FICORangeData : Spring2.Core.DataObject.DataObject {
	public static readonly FICORangeData DEFAULT = new FICORangeData();

	private IdType fICORangeId = IdType.DEFAULT;
	private IntegerType value = IntegerType.DEFAULT;
	private StringType description = StringType.DEFAULT;

	public IdType FICORangeId {
	    get { return this.fICORangeId; }
	    set { this.fICORangeId = value; }
	}

	public IntegerType Value {
	    get { return this.value; }
	    set { this.value = value; }
	}

	public StringType Description {
	    get { return this.description; }
	    set { this.description = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
