using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class APPLIC_PROFILE_DATAData : Spring2.Core.DataObject.DataObject {
	public static readonly APPLIC_PROFILE_DATAData DEFAULT = new APPLIC_PROFILE_DATAData();

	private IdType aPPLIC_NUM = IdType.DEFAULT;
	private IntegerType nO_YRS_ADDR = IntegerType.DEFAULT;
	private IntegerType pREV_YRS_ADDR = IntegerType.DEFAULT;

	public IdType APPLIC_NUM {
	    get { return this.aPPLIC_NUM; }
	    set { this.aPPLIC_NUM = value; }
	}

	public IntegerType NO_YRS_ADDR {
	    get { return this.nO_YRS_ADDR; }
	    set { this.nO_YRS_ADDR = value; }
	}

	public IntegerType PREV_YRS_ADDR {
	    get { return this.pREV_YRS_ADDR; }
	    set { this.pREV_YRS_ADDR = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
