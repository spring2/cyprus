using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class ChanelData : Spring2.Core.DataObject.DataObject {
	public static readonly ChanelData DEFAULT = new ChanelData();

	private IdType chanelId = IdType.DEFAULT;
	private IntegerType reportCode = IntegerType.DEFAULT;
	private StringType chanelDesc = StringType.DEFAULT;

	public IdType ChanelId {
	    get { return this.chanelId; }
	    set { this.chanelId = value; }
	}

	public IntegerType ReportCode {
	    get { return this.reportCode; }
	    set { this.reportCode = value; }
	}

	public StringType ChanelDesc {
	    get { return this.chanelDesc; }
	    set { this.chanelDesc = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
