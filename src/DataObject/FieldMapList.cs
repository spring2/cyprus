using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// IFieldMap generic collection
    /// </summary>
    public class FieldMapList : CollectionBase {
	[Generate]
	public static readonly FieldMapList UNSET = new FieldMapList(true);
	[Generate]
	public static readonly FieldMapList DEFAULT = new FieldMapList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private FieldMapList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public FieldMapList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public IFieldMap this[int index] {
	    get {
		return (IFieldMap)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.FieldMapId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(IFieldMap value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.FieldMapId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, IFieldMap value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.FieldMapId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    IFieldMap value = this[index];
		    keys.Remove(value.FieldMapId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is IFieldMap) {
		    Add((IFieldMap)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to IFieldMap");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType fieldMapId) {
	    return keys.Contains(fieldMapId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public IFieldMap this[IdType fieldMapId] {
	    get {
		return keys[fieldMapId] as IFieldMap;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public FieldMapList RetainAll(FieldMapList list) {
	    FieldMapList result = new FieldMapList();

	    foreach (IFieldMap data in List) {
		if (list.Contains(data.FieldMapId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public FieldMapList RemoveAll(FieldMapList list) {
	    FieldMapList result = new FieldMapList();

	    foreach (IFieldMap data in List) {
		if (!list.Contains(data.FieldMapId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType fieldMapId) {
	    if (!immutable) {
		IFieldMap objectInList = this[fieldMapId];
		List.Remove(objectInList);
		keys.Remove(fieldMapId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class FieldMapIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFieldMap o1 = (IFieldMap)a;
		IFieldMap o2 = (IFieldMap)b;

		if (o1 == null || o2 == null || !o1.FieldMapId.IsValid || !o2.FieldMapId.IsValid) {
		    return 0;
		}
		return o1.FieldMapId.CompareTo(o2.FieldMapId);
	    }
	}

	[Generate]
	public class SrcTableSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFieldMap o1 = (IFieldMap)a;
		IFieldMap o2 = (IFieldMap)b;

		if (o1 == null || o2 == null || !o1.SrcTable.IsValid || !o2.SrcTable.IsValid) {
		    return 0;
		}
		return o1.SrcTable.CompareTo(o2.SrcTable);
	    }
	}

	[Generate]
	public class SrcColumnSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFieldMap o1 = (IFieldMap)a;
		IFieldMap o2 = (IFieldMap)b;

		if (o1 == null || o2 == null || !o1.SrcColumn.IsValid || !o2.SrcColumn.IsValid) {
		    return 0;
		}
		return o1.SrcColumn.CompareTo(o2.SrcColumn);
	    }
	}

	[Generate]
	public class DestTableSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFieldMap o1 = (IFieldMap)a;
		IFieldMap o2 = (IFieldMap)b;

		if (o1 == null || o2 == null || !o1.DestTable.IsValid || !o2.DestTable.IsValid) {
		    return 0;
		}
		return o1.DestTable.CompareTo(o2.DestTable);
	    }
	}

	[Generate]
	public class DestColumnSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		IFieldMap o1 = (IFieldMap)a;
		IFieldMap o2 = (IFieldMap)b;

		if (o1 == null || o2 == null || !o1.DestColumn.IsValid || !o2.DestColumn.IsValid) {
		    return 0;
		}
		return o1.DestColumn.CompareTo(o2.DestColumn);
	    }
	}
    }
}
