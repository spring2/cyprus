using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class LoanPortfolioFields {
	private LoanPortfolioFields() {}
	public static readonly String ENTITY_NAME = "LoanPortfolio";

	public static readonly ColumnMetaData LOANPORTFOLIOID = new ColumnMetaData("LoanPortfolioId", "LoanPortfolio_Id", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData UPDATED = new ColumnMetaData("UpDated", "UpDated", DbType.DateTime, SqlDbType.DateTime, 0, 0, 0);
	public static readonly ColumnMetaData EOMDATE = new ColumnMetaData("EOMDate", "EOMDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData ACCOUNTID = new ColumnMetaData("AccountID", "AccountID", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData MEMBER = new ColumnMetaData("Member", "Member", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData MCLASS = new ColumnMetaData("MClass", "MClass", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LTYPE = new ColumnMetaData("Ltype", "Ltype", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LTYPESUB = new ColumnMetaData("LtypeSub", "LtypeSub", DbType.Decimal, SqlDbType.Decimal, 0, 18, 8);
	public static readonly ColumnMetaData PRODUCT = new ColumnMetaData("Product", "Product", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LOANDESC = new ColumnMetaData("LoanDesc", "LoanDesc", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData LOANDATE = new ColumnMetaData("LoanDate", "LoanDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData LOANDAY = new ColumnMetaData("LoanDay", "LoanDay", DbType.AnsiString, SqlDbType.VarChar, 3, 0, 0);
	public static readonly ColumnMetaData LOANMONTH = new ColumnMetaData("LoanMonth", "LoanMonth", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LOANYEAR = new ColumnMetaData("LoanYear", "LoanYear", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FIRSTPMTDATE = new ColumnMetaData("FirstPmtDate", "FirstPmtDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData STATUS = new ColumnMetaData("Status", "Status", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData RPTCODE = new ColumnMetaData("RptCode", "RptCode", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FUNDEDBR = new ColumnMetaData("FundedBr", "FundedBr", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData LOADEDBR = new ColumnMetaData("LoadedBr", "LoadedBr", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FICO = new ColumnMetaData("FICO", "FICO", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FICORANGE = new ColumnMetaData("FICORange", "FICORange", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData FASTSTART = new ColumnMetaData("FastStart", "FastStart", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FASTSTARTRANGE = new ColumnMetaData("FastStartRange", "FastStartRange", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData BANKRUPTCY = new ColumnMetaData("Bankruptcy", "Bankruptcy", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData BANKRUPTCYRANGE = new ColumnMetaData("BankruptcyRange", "BankruptcyRange", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData MEMBERSHIPDATE = new ColumnMetaData("MembershipDate", "MembershipDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData NEWMEMBER = new ColumnMetaData("NewMember", "NewMember", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData RISKLEV = new ColumnMetaData("RiskLev", "RiskLev", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData TRAFFIC = new ColumnMetaData("Traffic", "Traffic", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData TRAFFICCOLOR = new ColumnMetaData("TrafficColor", "TrafficColor", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData MEMAGE = new ColumnMetaData("MemAge", "MemAge", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData MEMSEX = new ColumnMetaData("MemSex", "MemSex", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData PMTFREQ = new ColumnMetaData("PmtFreq", "PmtFreq", DbType.AnsiString, SqlDbType.VarChar, 2, 0, 0);
	public static readonly ColumnMetaData ORIGTERM = new ColumnMetaData("OrigTerm", "OrigTerm", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData ORIGRATE = new ColumnMetaData("OrigRate", "OrigRate", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData ORIGPMT = new ColumnMetaData("OrigPmt", "OrigPmt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData ORIGAMOUNT = new ColumnMetaData("OrigAmount", "OrigAmount", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData ORIGCRLIM = new ColumnMetaData("OrigCrLim", "OrigCrLim", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CURTERM = new ColumnMetaData("CurTerm", "CurTerm", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CURRATE = new ColumnMetaData("CurRate", "CurRate", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CURPMT = new ColumnMetaData("CurPmt", "CurPmt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CURBAL = new ColumnMetaData("CurBal", "CurBal", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CURCRLIM = new ColumnMetaData("CurCrLim", "CurCrLim", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData BALLOONDATE = new ColumnMetaData("BalloonDate", "BalloonDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData MATDATE = new ColumnMetaData("MatDate", "MatDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData INSCODE = new ColumnMetaData("InsCode", "InsCode", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData COLLATERALVALUE = new ColumnMetaData("CollateralValue", "CollateralValue", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData LTV = new ColumnMetaData("LTV", "LTV", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData SECTYPE = new ColumnMetaData("SecType", "SecType", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData SECDESC1 = new ColumnMetaData("SecDesc1", "SecDesc1", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData SECDESC2 = new ColumnMetaData("SecDesc2", "SecDesc2", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData DEBTRATIO = new ColumnMetaData("DebtRatio", "DebtRatio", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData DEALER = new ColumnMetaData("Dealer", "Dealer", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DEALERNAME = new ColumnMetaData("DealerName", "DealerName", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData COBORCOMAK = new ColumnMetaData("CoborComak", "CoborComak", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData RELPRICEGRP = new ColumnMetaData("RelPriceGrp", "RelPriceGrp", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData RELPRICEDISC = new ColumnMetaData("RelPriceDisc", "RelPriceDisc", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData OTHERDISC = new ColumnMetaData("OtherDisc", "OtherDisc", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData DISCREASON = new ColumnMetaData("DiscReason", "DiscReason", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData RISKOFFSET = new ColumnMetaData("RiskOffset", "RiskOffset", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CREDITTYPE = new ColumnMetaData("CreditType", "CreditType", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData STATEDINCOME = new ColumnMetaData("StatedIncome", "StatedIncome", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData EXTENSIONS = new ColumnMetaData("Extensions", "Extensions", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData FIRSTEXTDATE = new ColumnMetaData("FirstExtDate", "FirstExtDate", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData LASTEXTDATE = new ColumnMetaData("LastExtDate", "LastExtDate", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData RISKLEVEL = new ColumnMetaData("RiskLevel", "RiskLevel", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData RISKDATE = new ColumnMetaData("RiskDate", "RiskDate", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData WATCH = new ColumnMetaData("Watch", "Watch", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData WATCHDATE = new ColumnMetaData("WatchDate", "WatchDate", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData WORKOUT = new ColumnMetaData("WorkOut", "WorkOut", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData WORKOUTDATE = new ColumnMetaData("WorkOutDate", "WorkOutDate", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData FIRSTMORTTYPE = new ColumnMetaData("FirstMortType", "FirstMortType", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData BUSPERREG = new ColumnMetaData("BusPerReg", "BusPerReg", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData BUSGROUP = new ColumnMetaData("BusGroup", "BusGroup", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData BUSDESC = new ColumnMetaData("BusDesc", "BusDesc", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData BUSPART = new ColumnMetaData("BusPart", "BusPart", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData BUSPARTPERC = new ColumnMetaData("BusPartPerc", "BusPartPerc", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData SBAGUAR = new ColumnMetaData("SBAguar", "SBAguar", DbType.AnsiString, SqlDbType.VarChar, 100, 0, 0);
	public static readonly ColumnMetaData DELIDAYS = new ColumnMetaData("DeliDays", "DeliDays", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DELISECT = new ColumnMetaData("DeliSect", "DeliSect", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DELIHIST = new ColumnMetaData("DeliHist", "DeliHist", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ1 = new ColumnMetaData("DeliHistQ1", "DeliHistQ1", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ2 = new ColumnMetaData("DeliHistQ2", "DeliHistQ2", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ3 = new ColumnMetaData("DeliHistQ3", "DeliHistQ3", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ4 = new ColumnMetaData("DeliHistQ4", "DeliHistQ4", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ5 = new ColumnMetaData("DeliHistQ5", "DeliHistQ5", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ6 = new ColumnMetaData("DeliHistQ6", "DeliHistQ6", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ7 = new ColumnMetaData("DeliHistQ7", "DeliHistQ7", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData DELIHISTQ8 = new ColumnMetaData("DeliHistQ8", "DeliHistQ8", DbType.AnsiString, SqlDbType.VarChar, 1, 0, 0);
	public static readonly ColumnMetaData CHRGOFFDATE = new ColumnMetaData("ChrgOffDate", "ChrgOffDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData CHRGOFFMONTH = new ColumnMetaData("ChrgOffMonth", "ChrgOffMonth", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CHRGOFFYEAR = new ColumnMetaData("ChrgOffYear", "ChrgOffYear", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CHRGOFFMNTHSBF = new ColumnMetaData("ChrgOffMnthsBF", "ChrgOffMnthsBF", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData PROPTYPE = new ColumnMetaData("PropType", "PropType", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData OCCUPANCY = new ColumnMetaData("Occupancy", "Occupancy", DbType.AnsiString, SqlDbType.VarChar, 50, 0, 0);
	public static readonly ColumnMetaData ORIGAPPRVAL = new ColumnMetaData("OrigApprVal", "OrigApprVal", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData ORIGAPPRDATE = new ColumnMetaData("OrigApprDate", "OrigApprDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData CURRAPPRVAL = new ColumnMetaData("CurrApprVal", "CurrApprVal", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CURRAPPRDATE = new ColumnMetaData("CurrApprDate", "CurrApprDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData FIRSTMORTBAL = new ColumnMetaData("FirstMortBal", "FirstMortBal", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData FIRSTMORTMOPYT = new ColumnMetaData("FirstMortMoPyt", "FirstMortMoPyt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData SECONDMORTBAL = new ColumnMetaData("SecondMortBal", "SecondMortBal", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData SECONDMORTPYMT = new ColumnMetaData("SecondMortPymt", "SecondMortPymt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData TAXINSNOTINC = new ColumnMetaData("TaxInsNotInc", "TaxInsNotInc", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData LOADOFF = new ColumnMetaData("LoadOff", "LoadOff", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData APPROFF = new ColumnMetaData("ApprOff", "ApprOff", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData FUNDINGOPP = new ColumnMetaData("FundingOpp", "FundingOpp", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CHANEL = new ColumnMetaData("Chanel", "Chanel", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData RISKTYPE = new ColumnMetaData("RiskType", "RiskType", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData GAP = new ColumnMetaData("Gap", "Gap", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData LOADOPERATOR = new ColumnMetaData("LoadOperator", "LoadOperator", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData DELIAMT = new ColumnMetaData("DeliAmt", "DeliAmt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CHGOFFAMT = new ColumnMetaData("ChgOffAmt", "ChgOffAmt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData USSECDESC = new ColumnMetaData("UsSecDesc", "UsSecDesc", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData TROUBLEDDEBT = new ColumnMetaData("TroubledDebt", "TroubledDebt", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData TDRDATE = new ColumnMetaData("TdrDate", "TdrDate", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData JNTFICOUSED = new ColumnMetaData("JntFicoUsed", "JntFicoUsed", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData SINGLEORJOINTLOAN = new ColumnMetaData("SingleOrJointLoan", "SingleOrJointLoan", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData APPROVINGOFFICERUD = new ColumnMetaData("ApprovingOfficerUD", "ApprovingOfficerUD", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData INSURANCECODEDESC = new ColumnMetaData("InsuranceCodeDesc", "InsuranceCodeDesc", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData GAPINS = new ColumnMetaData("GapIns", "GapIns", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData MMPINS = new ColumnMetaData("MmpIns", "MmpIns", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData REPORTCODEDESC = new ColumnMetaData("ReportCodeDesc", "ReportCodeDesc", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData BALLONPMTLOAN = new ColumnMetaData("BallonPmtLoan", "BallonPmtLoan", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData FIXEDORVARIABLERATELOAN = new ColumnMetaData("FixedOrVariableRateLoan", "FixedOrVariableRateLoan", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData ALPSOFFICER = new ColumnMetaData("AlpsOfficer", "AlpsOfficer", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData CUDLAPPNUMBER = new ColumnMetaData("CUDLAppNumber", "CUDLAppNumber", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
	public static readonly ColumnMetaData FASBDEFCOST = new ColumnMetaData("FasbDefCost", "FasbDefCost", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData FASBLTDAMORTFEES = new ColumnMetaData("FasbLtdAmortFees", "FasbLtdAmortFees", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData INTERESTPRIORYTD = new ColumnMetaData("InterestPriorYtd", "InterestPriorYtd", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData ACCRUEDINTEREST = new ColumnMetaData("AccruedInterest", "AccruedInterest", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData INTERESTLTD = new ColumnMetaData("InterestLTD", "InterestLTD", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData INTERESTYTD = new ColumnMetaData("InterestYtd", "InterestYtd", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData PARTIALPAYAMT = new ColumnMetaData("PartialPayAmt", "PartialPayAmt", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData INTERESTUNCOLLECTED = new ColumnMetaData("InterestUncollected", "InterestUncollected", DbType.Decimal, SqlDbType.Decimal, 0, 18, 2);
	public static readonly ColumnMetaData CLOSEDACCTDATE = new ColumnMetaData("ClosedAcctDate", "ClosedAcctDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData LASTTRANDATE = new ColumnMetaData("LastTranDate", "LastTranDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData NEXTDUEDATE = new ColumnMetaData("NextDueDate", "NextDueDate", DbType.Date, SqlDbType.Date, 0, 0, 0);
	public static readonly ColumnMetaData INSURANCECODE = new ColumnMetaData("InsuranceCode", "InsuranceCode", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData MBRAGEATAPPROVAL = new ColumnMetaData("MbrAgeAtApproval", "MbrAgeAtApproval", DbType.Int32, SqlDbType.Int, 0, 10, 0);
    }

    public interface ILoanPortfolio : IBusinessEntity {
	IdType LoanPortfolioId {
	    get;
	}
	DateTimeType UpDated {
	    get;
	}
	DateType EOMDate {
	    get;
	}
	StringType AccountID {
	    get;
	}
	StringType Member {
	    get;
	}
	IntegerType MClass {
	    get;
	}
	IntegerType Ltype {
	    get;
	}
	DecimalType LtypeSub {
	    get;
	}
	IntegerType Product {
	    get;
	}
	StringType LoanDesc {
	    get;
	}
	DateType LoanDate {
	    get;
	}
	StringType LoanDay {
	    get;
	}
	IntegerType LoanMonth {
	    get;
	}
	IntegerType LoanYear {
	    get;
	}
	DateType FirstPmtDate {
	    get;
	}
	StringType Status {
	    get;
	}
	IntegerType RptCode {
	    get;
	}
	IntegerType FundedBr {
	    get;
	}
	IntegerType LoadedBr {
	    get;
	}
	IntegerType FICO {
	    get;
	}
	StringType FICORange {
	    get;
	}
	IntegerType FastStart {
	    get;
	}
	StringType FastStartRange {
	    get;
	}
	IntegerType Bankruptcy {
	    get;
	}
	StringType BankruptcyRange {
	    get;
	}
	DateType MembershipDate {
	    get;
	}
	StringType NewMember {
	    get;
	}
	StringType RiskLev {
	    get;
	}
	IntegerType Traffic {
	    get;
	}
	StringType TrafficColor {
	    get;
	}
	IntegerType MemAge {
	    get;
	}
	StringType MemSex {
	    get;
	}
	StringType PmtFreq {
	    get;
	}
	IntegerType OrigTerm {
	    get;
	}
	DecimalType OrigRate {
	    get;
	}
	CurrencyType OrigPmt {
	    get;
	}
	CurrencyType OrigAmount {
	    get;
	}
	CurrencyType OrigCrLim {
	    get;
	}
	IntegerType CurTerm {
	    get;
	}
	DecimalType CurRate {
	    get;
	}
	CurrencyType CurPmt {
	    get;
	}
	CurrencyType CurBal {
	    get;
	}
	CurrencyType CurCrLim {
	    get;
	}
	DateType BalloonDate {
	    get;
	}
	DateType MatDate {
	    get;
	}
	IntegerType InsCode {
	    get;
	}
	CurrencyType CollateralValue {
	    get;
	}
	DecimalType LTV {
	    get;
	}
	IntegerType SecType {
	    get;
	}
	StringType SecDesc1 {
	    get;
	}
	StringType SecDesc2 {
	    get;
	}
	DecimalType DebtRatio {
	    get;
	}
	IntegerType Dealer {
	    get;
	}
	StringType DealerName {
	    get;
	}
	StringType CoborComak {
	    get;
	}
	StringType RelPriceGrp {
	    get;
	}
	IntegerType RelPriceDisc {
	    get;
	}
	DecimalType OtherDisc {
	    get;
	}
	StringType DiscReason {
	    get;
	}
	DecimalType RiskOffset {
	    get;
	}
	StringType CreditType {
	    get;
	}
	StringType StatedIncome {
	    get;
	}
	StringType Extensions {
	    get;
	}
	StringType FirstExtDate {
	    get;
	}
	StringType LastExtDate {
	    get;
	}
	StringType RiskLevel {
	    get;
	}
	StringType RiskDate {
	    get;
	}
	StringType Watch {
	    get;
	}
	StringType WatchDate {
	    get;
	}
	StringType WorkOut {
	    get;
	}
	StringType WorkOutDate {
	    get;
	}
	StringType FirstMortType {
	    get;
	}
	StringType BusPerReg {
	    get;
	}
	StringType BusGroup {
	    get;
	}
	StringType BusDesc {
	    get;
	}
	StringType BusPart {
	    get;
	}
	StringType BusPartPerc {
	    get;
	}
	StringType SBAguar {
	    get;
	}
	IntegerType DeliDays {
	    get;
	}
	IntegerType DeliSect {
	    get;
	}
	StringType DeliHist {
	    get;
	}
	StringType DeliHistQ1 {
	    get;
	}
	StringType DeliHistQ2 {
	    get;
	}
	StringType DeliHistQ3 {
	    get;
	}
	StringType DeliHistQ4 {
	    get;
	}
	StringType DeliHistQ5 {
	    get;
	}
	StringType DeliHistQ6 {
	    get;
	}
	StringType DeliHistQ7 {
	    get;
	}
	StringType DeliHistQ8 {
	    get;
	}
	DateType ChrgOffDate {
	    get;
	}
	IntegerType ChrgOffMonth {
	    get;
	}
	IntegerType ChrgOffYear {
	    get;
	}
	IntegerType ChrgOffMnthsBF {
	    get;
	}
	StringType PropType {
	    get;
	}
	StringType Occupancy {
	    get;
	}
	CurrencyType OrigApprVal {
	    get;
	}
	DateType OrigApprDate {
	    get;
	}
	CurrencyType CurrApprVal {
	    get;
	}
	DateType CurrApprDate {
	    get;
	}
	CurrencyType FirstMortBal {
	    get;
	}
	CurrencyType FirstMortMoPyt {
	    get;
	}
	CurrencyType SecondMortBal {
	    get;
	}
	CurrencyType SecondMortPymt {
	    get;
	}
	CurrencyType TaxInsNotInc {
	    get;
	}
	IntegerType LoadOff {
	    get;
	}
	IntegerType ApprOff {
	    get;
	}
	IntegerType FundingOpp {
	    get;
	}
	StringType Chanel {
	    get;
	}
	StringType RiskType {
	    get;
	}
	StringType Gap {
	    get;
	}
	IntegerType LoadOperator {
	    get;
	}
	CurrencyType DeliAmt {
	    get;
	}
	CurrencyType ChgOffAmt {
	    get;
	}
	StringType UsSecDesc {
	    get;
	}
	StringType TroubledDebt {
	    get;
	}
	StringType TdrDate {
	    get;
	}
	StringType JntFicoUsed {
	    get;
	}
	StringType SingleOrJointLoan {
	    get;
	}
	StringType ApprovingOfficerUD {
	    get;
	}
	StringType InsuranceCodeDesc {
	    get;
	}
	StringType GapIns {
	    get;
	}
	StringType MmpIns {
	    get;
	}
	StringType ReportCodeDesc {
	    get;
	}
	StringType BallonPmtLoan {
	    get;
	}
	StringType FixedOrVariableRateLoan {
	    get;
	}
	StringType AlpsOfficer {
	    get;
	}
	StringType CUDLAppNumber {
	    get;
	}
	CurrencyType FasbDefCost {
	    get;
	}
	CurrencyType FasbLtdAmortFees {
	    get;
	}
	CurrencyType InterestPriorYtd {
	    get;
	}
	CurrencyType AccruedInterest {
	    get;
	}
	CurrencyType InterestLTD {
	    get;
	}
	CurrencyType InterestYtd {
	    get;
	}
	CurrencyType PartialPayAmt {
	    get;
	}
	CurrencyType InterestUncollected {
	    get;
	}
	DateType ClosedAcctDate {
	    get;
	}
	DateType LastTranDate {
	    get;
	}
	DateType NextDueDate {
	    get;
	}
	IntegerType InsuranceCode {
	    get;
	}
	IntegerType MbrAgeAtApproval {
	    get;
	}
    }
}
