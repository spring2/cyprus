using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;

using Spring2.Core.BusinessEntity;

namespace Spring2.DataObject {
    public class ChanelFields {
	private ChanelFields() {}
	public static readonly String ENTITY_NAME = "Chanel";

	public static readonly ColumnMetaData CHANELID = new ColumnMetaData("ChanelId", "ChanelId", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData REPORTCODE = new ColumnMetaData("ReportCode", "ReportCode", DbType.Int32, SqlDbType.Int, 0, 10, 0);
	public static readonly ColumnMetaData CHANELDESC = new ColumnMetaData("ChanelDesc", "ChanelDesc", DbType.AnsiString, SqlDbType.VarChar, 250, 0, 0);
    }

    public interface IChanel : IBusinessEntity {
	IdType ChanelId {
	    get;
	}
	IntegerType ReportCode {
	    get;
	}
	StringType ChanelDesc {
	    get;
	}
    }
}
