using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class DELINQUENCY_DATAData : Spring2.Core.DataObject.DataObject {
	public static readonly DELINQUENCY_DATAData DEFAULT = new DELINQUENCY_DATAData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private StringType dLQ_HIST = StringType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public StringType DLQ_HIST {
	    get { return this.dLQ_HIST; }
	    set { this.dLQ_HIST = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
