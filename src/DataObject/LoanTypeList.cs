using System.Collections;
using System;

using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Types;

namespace Spring2.DataObject {
    /// <summary>
    /// ILoanType generic collection
    /// </summary>
    public class LoanTypeList : CollectionBase {
	[Generate]
	public static readonly LoanTypeList UNSET = new LoanTypeList(true);
	[Generate]
	public static readonly LoanTypeList DEFAULT = new LoanTypeList(true);

	[Generate]
	private Boolean immutable = false;

	[Generate]
	private Hashtable keys = new Hashtable();

	[Generate]
	private LoanTypeList(Boolean immutable) {
	    this.immutable = immutable;
	}

	[Generate]
	public LoanTypeList() {}

	[Generate]
	public ICollection Keys {
	    get {
		return new ArrayList(keys.Keys);
	    }
	}

	// Indexer implementation.
	[Generate]
	public ILoanType this[int index] {
	    get {
		return (ILoanType)List[index];
	    }
	    set {
		if (!immutable) {
		    List[index] = value;
		    keys[value.LoanTypeId] = value;
		} else {
		    throw new System.Data.ReadOnlyException();
		}
	    }
	}

	[Generate]
	public void Add(ILoanType value) {
	    if (!immutable) {
		List.Add(value);
		keys[value.LoanTypeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Insert(Int32 index, ILoanType value) {
	    if (!immutable) {
		List.Insert(index, value);
		keys[value.LoanTypeId] = value;
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void Remove(int index) {
	    if (!immutable) {
		if (index > Count - 1 || index < 0) {
		    throw new IndexOutOfRangeException();
		} else {
		    ILoanType value = this[index];
		    keys.Remove(value.LoanTypeId);
		    List.RemoveAt(index);
		}
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	[Generate]
	public void AddRange(IList list) {
	    foreach (Object o in list) {
		if (o is ILoanType) {
		    Add((ILoanType)o);
		} else {
		    throw new InvalidCastException("object in list could not be cast to ILoanType");
		}
	    }
	}

	[Generate]
	public Boolean IsDefault {
	    get {
		return ReferenceEquals(this, DEFAULT);
	    }
	}

	[Generate]
	public Boolean IsUnset {
	    get {
		return ReferenceEquals(this, UNSET);
	    }
	}

	[Generate]
	public Boolean IsValid {
	    get {
		return !(IsDefault || IsUnset);
	    }
	}

	/// <summary>
	/// See if the list contains an instance by identity
	/// </summary>
	[Generate]
	public Boolean Contains(IdType loanTypeId) {
	    return keys.Contains(loanTypeId);
	}

	/// <summary>
	/// returns the instance by identity or null if it not found
	/// </summary>
	[Generate]
	public ILoanType this[IdType loanTypeId] {
	    get {
		return keys[loanTypeId] as ILoanType;
	    }
	}

	/// <summary>
	/// Returns a new list that contains all of the elements that are in both lists
	/// </summary>
	[Generate]
	public LoanTypeList RetainAll(LoanTypeList list) {
	    LoanTypeList result = new LoanTypeList();

	    foreach (ILoanType data in List) {
		if (list.Contains(data.LoanTypeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// return a new list that contains only the elements not contained in the argument list
	/// </summary>
	[Generate]
	public LoanTypeList RemoveAll(LoanTypeList list) {
	    LoanTypeList result = new LoanTypeList();

	    foreach (ILoanType data in List) {
		if (!list.Contains(data.LoanTypeId)) {
		    result.Add(data);
		}
	    }

	    return result;
	}

	/// <summary>
	/// removes value by identity
	/// </summary>
	[Generate]
	public void Remove(IdType loanTypeId) {
	    if (!immutable) {
		ILoanType objectInList = this[loanTypeId];
		List.Remove(objectInList);
		keys.Remove(loanTypeId);
	    } else {
		throw new System.Data.ReadOnlyException();
	    }
	}

	/// <summary>
	/// Sort a list by a column
	/// </summary>
	[Generate]
	public void Sort(IComparer comparer) {
	    InnerList.Sort(comparer);
	}

	/// <summary>
	/// Sort the list given the name of a comparer class.
	/// </summary>
	[Generate]
	public void Sort(String comparerName) {
	    Type type = GetType().GetNestedType(comparerName);
	    if (type == null) {
		throw new ArgumentException(String.Format("Comparer {0} not found in class {1}.", comparerName, GetType().Name));
	    }

	    IComparer comparer = Activator.CreateInstance(type) as IComparer;
	    if (comparer == null) {
		throw new ArgumentException("compareName must be the name of class that implements IComparer.");
	    }

	    InnerList.Sort(comparer);
	}

	[Generate]
	public class LoanTypeIdSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanType o1 = (ILoanType)a;
		ILoanType o2 = (ILoanType)b;

		if (o1 == null || o2 == null || !o1.LoanTypeId.IsValid || !o2.LoanTypeId.IsValid) {
		    return 0;
		}
		return o1.LoanTypeId.CompareTo(o2.LoanTypeId);
	    }
	}

	[Generate]
	public class LTypeSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanType o1 = (ILoanType)a;
		ILoanType o2 = (ILoanType)b;

		if (o1 == null || o2 == null || !o1.LType.IsValid || !o2.LType.IsValid) {
		    return 0;
		}
		return o1.LType.CompareTo(o2.LType);
	    }
	}

	[Generate]
	public class LoanDescSorter : IComparer {
	    public Int32 Compare(Object a, Object b) {
		ILoanType o1 = (ILoanType)a;
		ILoanType o2 = (ILoanType)b;

		if (o1 == null || o2 == null || !o1.LoanDesc.IsValid || !o2.LoanDesc.IsValid) {
		    return 0;
		}
		return o1.LoanDesc.CompareTo(o2.LoanDesc);
	    }
	}

	#region
	public ILoanType FindByLType(IntegerType lType) {
	    foreach (ILoanType lt in this) {
		if (lt.LType.Equals(lType)) {
		    return lt;
		}
	    }
	    return null;
	}

	#endregion
    }
}
