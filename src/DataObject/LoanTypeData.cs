using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class LoanTypeData : Spring2.Core.DataObject.DataObject {
	public static readonly LoanTypeData DEFAULT = new LoanTypeData();

	private IdType loanTypeId = IdType.DEFAULT;
	private IntegerType lType = IntegerType.DEFAULT;
	private StringType loanDesc = StringType.DEFAULT;

	public IdType LoanTypeId {
	    get { return this.loanTypeId; }
	    set { this.loanTypeId = value; }
	}

	public IntegerType LType {
	    get { return this.lType; }
	    set { this.lType = value; }
	}

	public StringType LoanDesc {
	    get { return this.loanDesc; }
	    set { this.loanDesc = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
