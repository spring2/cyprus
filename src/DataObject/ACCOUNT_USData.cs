using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class ACCOUNT_USData : Spring2.Core.DataObject.DataObject {
	public static readonly ACCOUNT_USData DEFAULT = new ACCOUNT_USData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private StringType uS_LOANSTATUS = StringType.DEFAULT;
	private StringType uS_DELI_HIST = StringType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public StringType US_LOANSTATUS {
	    get { return this.uS_LOANSTATUS; }
	    set { this.uS_LOANSTATUS = value; }
	}

	public StringType US_DELI_HIST {
	    get { return this.uS_DELI_HIST; }
	    set { this.uS_DELI_HIST = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
