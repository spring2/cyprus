using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.Types;


namespace Spring2.DataObject {
    public class ACCOUNTData : Spring2.Core.DataObject.DataObject {
	public static readonly ACCOUNTData DEFAULT = new ACCOUNTData();

	private IdType aCCT_NUM = IdType.DEFAULT;
	private IdType mEM_NUM = IdType.DEFAULT;
	private DecimalType l_ACCT_LEVEL = DecimalType.DEFAULT;
	private IntegerType aCCT_SUBLEV = IntegerType.DEFAULT;
	private DateTimeType aCCOUNT_OPEN_DATE = DateTimeType.DEFAULT;
	private StringType sPEC_REPORT_CODE = StringType.DEFAULT;
	private CurrencyType oRIG_PAY = CurrencyType.DEFAULT;
	private DecimalType aPR = DecimalType.DEFAULT;
	private CurrencyType cURR_CREDIT_LIM = CurrencyType.DEFAULT;
	private StringType pG_CODE = StringType.DEFAULT;
	private StringType dISC_REASON = StringType.DEFAULT;
	private StringType rISK_OFFSET_VAL = StringType.DEFAULT;
	private DateTimeType fIRST_PAYDATE = DateTimeType.DEFAULT;
	private IntegerType oRIG_LOAN_TERM = IntegerType.DEFAULT;
	private DateTimeType l_MATURITY_DATE = DateTimeType.DEFAULT;
	private IntegerType l_INSCODE = IntegerType.DEFAULT;
	private IntegerType l_SECCODE = IntegerType.DEFAULT;
	private StringType l_SECDESC1 = StringType.DEFAULT;
	private StringType l_SECDESC2 = StringType.DEFAULT;
	private IdType uS_APPLIC_ID = IdType.DEFAULT;
	private StringType l_ACCT_TYPE = StringType.DEFAULT;

	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	public IdType MEM_NUM {
	    get { return this.mEM_NUM; }
	    set { this.mEM_NUM = value; }
	}

	public DecimalType L_ACCT_LEVEL {
	    get { return this.l_ACCT_LEVEL; }
	    set { this.l_ACCT_LEVEL = value; }
	}

	public IntegerType ACCT_SUBLEV {
	    get { return this.aCCT_SUBLEV; }
	    set { this.aCCT_SUBLEV = value; }
	}

	public DateTimeType ACCOUNT_OPEN_DATE {
	    get { return this.aCCOUNT_OPEN_DATE; }
	    set { this.aCCOUNT_OPEN_DATE = value; }
	}

	public StringType SPEC_REPORT_CODE {
	    get { return this.sPEC_REPORT_CODE; }
	    set { this.sPEC_REPORT_CODE = value; }
	}

	public CurrencyType ORIG_PAY {
	    get { return this.oRIG_PAY; }
	    set { this.oRIG_PAY = value; }
	}

	public DecimalType APR {
	    get { return this.aPR; }
	    set { this.aPR = value; }
	}

	public CurrencyType CURR_CREDIT_LIM {
	    get { return this.cURR_CREDIT_LIM; }
	    set { this.cURR_CREDIT_LIM = value; }
	}

	public StringType PG_CODE {
	    get { return this.pG_CODE; }
	    set { this.pG_CODE = value; }
	}

	public StringType DISC_REASON {
	    get { return this.dISC_REASON; }
	    set { this.dISC_REASON = value; }
	}

	public StringType RISK_OFFSET_VAL {
	    get { return this.rISK_OFFSET_VAL; }
	    set { this.rISK_OFFSET_VAL = value; }
	}

	public DateTimeType FIRST_PAYDATE {
	    get { return this.fIRST_PAYDATE; }
	    set { this.fIRST_PAYDATE = value; }
	}

	public IntegerType ORIG_LOAN_TERM {
	    get { return this.oRIG_LOAN_TERM; }
	    set { this.oRIG_LOAN_TERM = value; }
	}

	public DateTimeType L_MATURITY_DATE {
	    get { return this.l_MATURITY_DATE; }
	    set { this.l_MATURITY_DATE = value; }
	}

	public IntegerType L_INSCODE {
	    get { return this.l_INSCODE; }
	    set { this.l_INSCODE = value; }
	}

	public IntegerType L_SECCODE {
	    get { return this.l_SECCODE; }
	    set { this.l_SECCODE = value; }
	}

	public StringType L_SECDESC1 {
	    get { return this.l_SECDESC1; }
	    set { this.l_SECDESC1 = value; }
	}

	public StringType L_SECDESC2 {
	    get { return this.l_SECDESC2; }
	    set { this.l_SECDESC2 = value; }
	}

	public IdType US_APPLIC_ID {
	    get { return this.uS_APPLIC_ID; }
	    set { this.uS_APPLIC_ID = value; }
	}

	public StringType L_ACCT_TYPE {
	    get { return this.l_ACCT_TYPE; }
	    set { this.l_ACCT_TYPE = value; }
	}

	public Boolean IsDefault {
	    get {
		return Object.ReferenceEquals(DEFAULT, this);
	    }
	}
    }
}
