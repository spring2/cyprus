using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class ACCOUNT_US : BusinessEntity, IACCOUNT_US {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private StringType uS_LOANSTATUS = StringType.DEFAULT;

	[Generate()]
	internal ACCOUNT_US() {}

	[Generate()]
	internal ACCOUNT_US(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static ACCOUNT_US NewInstance() {
	    return new ACCOUNT_US();
	}

	[Generate()]
	public static ACCOUNT_US GetInstance(IdType aCCT_NUM) {
	    return ACCOUNT_USDAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	public void Update(ACCOUNT_USData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    uS_LOANSTATUS = data.US_LOANSTATUS.IsDefault ? uS_LOANSTATUS : data.US_LOANSTATUS;
	    uS_DELI_HIST = data.US_DELI_HIST.IsDefault ? uS_DELI_HIST : data.US_DELI_HIST;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = ACCOUNT_USDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		ACCOUNT_USDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public StringType US_LOANSTATUS {
	    get { return this.uS_LOANSTATUS; }
	    set { this.uS_LOANSTATUS = value; }
	}


	[Generate()]
	public void Reload() {
	    ACCOUNT_USDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}

	[Generate()]
	private StringType uS_DELI_HIST = StringType.DEFAULT;

	[Generate()]
	public StringType US_DELI_HIST {
	    get { return this.uS_DELI_HIST; }
	    set { this.uS_DELI_HIST = value; }
	}
    }
}
