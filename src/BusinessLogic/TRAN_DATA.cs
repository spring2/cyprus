using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class TRAN_DATA : BusinessEntity, ITRAN_DATA {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private CurrencyType cURRENT_BALANCE = CurrencyType.DEFAULT;

	[Generate()]
	internal TRAN_DATA() {}

	[Generate()]
	internal TRAN_DATA(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static TRAN_DATA NewInstance() {
	    return new TRAN_DATA();
	}

	[Generate()]
	public static TRAN_DATA GetInstance(IdType aCCT_NUM) {
	    return TRAN_DATADAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	public void Update(TRAN_DATAData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    cURRENT_BALANCE = data.CURRENT_BALANCE.IsDefault ? cURRENT_BALANCE : data.CURRENT_BALANCE;
	    iNT_RATE = data.INT_RATE.IsDefault ? iNT_RATE : data.INT_RATE;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = TRAN_DATADAO.DAO.Insert(this);
		isNew = false;
	    } else {
		TRAN_DATADAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public CurrencyType CURRENT_BALANCE {
	    get { return this.cURRENT_BALANCE; }
	    set { this.cURRENT_BALANCE = value; }
	}


	[Generate()]
	public void Reload() {
	    TRAN_DATADAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}

	[Generate()]
	private DecimalType iNT_RATE = DecimalType.DEFAULT;

	[Generate()]
	public DecimalType INT_RATE {
	    get { return this.iNT_RATE; }
	    set { this.iNT_RATE = value; }
	}
    }
}
