using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class FastStartRange : BusinessEntity, IFastStartRange {
	[Generate()]
	private IdType fastStartRangeId = IdType.DEFAULT;
	[Generate()]
	private IntegerType value = IntegerType.DEFAULT;
	[Generate()]
	private StringType description = StringType.DEFAULT;

	[Generate()]
	internal FastStartRange() {}

	[Generate()]
	internal FastStartRange(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static FastStartRange NewInstance() {
	    return new FastStartRange();
	}

	[Generate()]
	public static FastStartRange GetInstance(IdType fastStartRangeId) {
	    return FastStartRangeDAO.DAO.Load(fastStartRangeId);
	}

	[Generate()]
	public void Update(FastStartRangeData data) {
	    fastStartRangeId = data.FastStartRangeId.IsDefault ? fastStartRangeId : data.FastStartRangeId;
	    value = data.Value.IsDefault ? value : data.Value;
	    description = data.Description.IsDefault ? description : data.Description;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.FastStartRangeId = FastStartRangeDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		FastStartRangeDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType FastStartRangeId {
	    get { return this.fastStartRangeId; }
	    set { this.fastStartRangeId = value; }
	}

	[Generate()]
	public IntegerType Value {
	    get { return this.value; }
	    set { this.value = value; }
	}

	[Generate()]
	public StringType Description {
	    get { return this.description; }
	    set { this.description = value; }
	}


	[Generate()]
	public void Reload() {
	    FastStartRangeDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + FastStartRangeId.ToString();
	}
    }
}
