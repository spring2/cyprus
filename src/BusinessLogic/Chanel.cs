using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class Chanel : BusinessEntity, IChanel {
	[Generate()]
	private IdType chanelId = IdType.DEFAULT;
	[Generate()]
	private IntegerType reportCode = IntegerType.DEFAULT;

	[Generate()]
	internal Chanel() {}

	[Generate()]
	internal Chanel(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static Chanel NewInstance() {
	    return new Chanel();
	}

	[Generate()]
	public static Chanel GetInstance(IdType chanelId) {
	    return ChanelDAO.DAO.Load(chanelId);
	}

	[Generate()]
	public void Update(ChanelData data) {
	    chanelId = data.ChanelId.IsDefault ? chanelId : data.ChanelId;
	    reportCode = data.ReportCode.IsDefault ? reportCode : data.ReportCode;
	    chanelDesc = data.ChanelDesc.IsDefault ? chanelDesc : data.ChanelDesc;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ChanelId = ChanelDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		ChanelDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ChanelId {
	    get { return this.chanelId; }
	    set { this.chanelId = value; }
	}

	[Generate()]
	public IntegerType ReportCode {
	    get { return this.reportCode; }
	    set { this.reportCode = value; }
	}


	[Generate()]
	public void Reload() {
	    ChanelDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ChanelId.ToString();
	}

	[Generate()]
	private StringType chanelDesc = StringType.DEFAULT;

	[Generate()]
	public StringType ChanelDesc {
	    get { return this.chanelDesc; }
	    set { this.chanelDesc = value; }
	}
    }
}
