﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;
using rm = Rhino.Mocks.Constraints;
using Spring2.Core.Configuration;
using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Mail.BusinessLogic;
using Spring2.Core.Mail.Dao;
using Spring2.Core.Mail.DataObject;
using Spring2.Core.Mail.Types;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Exceptions;
using Spring2.Types;
using Spring2.UnitTest;

namespace Spring2.BusinessLogic.UnitTest {
    [TestFixture]
    public class LoanPortfolioTest : BaseTest {
	[SetUp]
	public void SetUp() {
	    MockSetUp();
	}

	[TearDown]
	public void TearDown() {
	    MockTearDown();
	}

	[Test]
	public void DelinquentInQuarterShouldReturnTrueIfQuarterContains3OrGreater() {
	    LoanPortfolio lp = LoanPortfolio.NewInstance();
	    lp.DeliHist = "CCCCCCCCCC3CC3CC3CC3CCCC";
	    bool q1 = lp.DelinquentInQuarter(1);
	    bool q2 = lp.DelinquentInQuarter(2);
	    bool q3 = lp.DelinquentInQuarter(3);
	    bool q4 = lp.DelinquentInQuarter(4);
	    bool q5 = lp.DelinquentInQuarter(5);
	    bool q6 = lp.DelinquentInQuarter(6);
	    bool q7 = lp.DelinquentInQuarter(7);
	    bool q8 = lp.DelinquentInQuarter(8);

	    Assert.IsFalse(q1);
	    Assert.IsFalse(q2);
	    Assert.IsFalse(q3);
	    Assert.IsTrue(q4);
	    Assert.IsTrue(q5);
	    Assert.IsTrue(q6);
	    Assert.IsTrue(q7);
	    Assert.IsFalse(q8);
	}
    }
}
