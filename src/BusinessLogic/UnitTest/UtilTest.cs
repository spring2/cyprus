﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;
using rm = Rhino.Mocks.Constraints;
using Spring2.Core.Configuration;
using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Mail.BusinessLogic;
using Spring2.Core.Mail.Dao;
using Spring2.Core.Mail.DataObject;
using Spring2.Core.Mail.Types;
using Spring2.Core.Message;
using Spring2.Core.Types;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Exceptions;
using Spring2.Types;
using Spring2.UnitTest;

using Spring2.Util;

namespace Spring2.BusinessLogic.UnitTest {
    [TestFixture]
    public class UtilTest : BaseTest {
	[SetUp]
	public void SetUp() {
	    MockSetUp();
	}

	[TearDown]
	public void TearDown() {
	    MockTearDown();
	}

	[Test]
	public void MonthsDifferenceCurrentYearTest() {
	    DateTime firstDayOfYear = new DateTime(DateTime.Now.Year, 1, 1);
	    DateTime lastDayOfYear = new DateTime(DateTime.Now.Year, 12, 31);
	    int monthsDifference = ServiceUtil.MonthDifference(firstDayOfYear, lastDayOfYear);

	    Assert.AreEqual(11, monthsDifference);
	}

	[Test]
	public void MonthsDifferenceFirstDateAfterSecondDateTest() {
	    DateTime firstDayOfYear = new DateTime(DateTime.Now.Year, 1, 1);
	    DateTime lastDayOfYear = new DateTime(DateTime.Now.Year, 12, 31);
	    int monthsDifference = ServiceUtil.MonthDifference(lastDayOfYear, firstDayOfYear);

	    Assert.AreEqual(11, monthsDifference);
	}


	[Test]
	public void MonthsDifferenceSameMonthTest() {
	    DateTime firstDayOfYear = new DateTime(DateTime.Now.Year, 12, 1);
	    DateTime lastDayOfYear = new DateTime(DateTime.Now.Year, 12, 31);
	    int monthsDifference = ServiceUtil.MonthDifference(firstDayOfYear, lastDayOfYear);

	    Assert.AreEqual(0, monthsDifference);
	}
    }
}
