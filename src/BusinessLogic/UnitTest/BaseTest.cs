using NUnit.Framework;
using Spring2.Core.Configuration;
using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Exceptions;
using Spring2.Types;
using System.Collections;
using System.Configuration;
using System.Security.Principal;
using System;
using Rhino.Mocks;
using System.Collections.Generic;
using Spring2.Core.IoC;
using Spring2.UnitTest;
using Spring2.DataObject;
using System.Threading;
using Spring2.Util;

namespace Spring2.BusinessLogic.UnitTest {
    /// <summary>
    /// Summary description for BaseTest.
    /// </summary>
    public class BaseTest {
	protected TestMockBusinessLogicUtility testUtility = null;

	public virtual void MockSetUp() {
	    testUtility = new TestMockBusinessLogicUtility();
	    ConfigurationSettings.AppSettings["ConfigurationProvider"] = "Spring2.Core.Configuration.SimpleConfigurationProvider";
	}

	public virtual void MockTearDown() {
	    Thread.CurrentPrincipal = null;
	    ConfigurationProvider.SetProvider(null);
	    testUtility.TearDown();
	    testUtility = null;
	    //CacheUtility.FlushAll();
	}

	protected Boolean ContainMessage(MessageList errors, Message err) {
	    Boolean result = false;
	    foreach (Message error in errors) {
		if (error.Message == err.Message) {
		    result = true;
		}
	    }
	    return result;
	}
    }
}
