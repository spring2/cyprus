﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;
using rm = Rhino.Mocks.Constraints;
using Spring2.Core.Configuration;
using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Mail.BusinessLogic;
using Spring2.Core.Mail.Dao;
using Spring2.Core.Mail.DataObject;
using Spring2.Core.Mail.Types;
using Spring2.Core.Message;
using Spring2.Core.Types;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Exceptions;
using Spring2.Types;
using Spring2.UnitTest;

namespace Spring2.BusinessLogic.UnitTest {
    [TestFixture]
    public class RangeListTest : BaseTest {
	[SetUp]
	public void SetUp() {
	    MockSetUp();
	}

	[TearDown]
	public void TearDown() {
	    MockTearDown();
	}

	[Test]
	public void FICORangeListValueNotValid() {
	    FICORange range2 = FICORange.NewInstance();
	    range2.Value = 2;
	    range2.Description = "0-2";

	    FICORange range5 = FICORange.NewInstance();
	    range5.Value = 5;
	    range5.Description = "3-5";

	    FICORange range100 = FICORange.NewInstance();
	    range100.Value = 100;
	    range100.Description = "6-100";

	    FICORange rangeMax = FICORange.NewInstance();
	    rangeMax.Value = 9999;
	    rangeMax.Description = "> 100";

	    FICORangeList ranges = new FICORangeList();
	    ranges.Add(range2);
	    ranges.Add(range5);
	    ranges.Add(range100);
	    ranges.Add(rangeMax);

	    Assert.IsNull(ranges.GetRangeValue(IntegerType.UNSET));
	}

	[Test]
	public void FICORangeListValueLessThanLowestRangeValue() {
	    FICORange range2 = FICORange.NewInstance();
	    range2.Value = 2;
	    range2.Description = "0-2";

	    FICORange range5 = FICORange.NewInstance();
	    range5.Value = 5;
	    range5.Description = "3-5";

	    FICORange range100 = FICORange.NewInstance();
	    range100.Value = 100;
	    range100.Description = "6-100";

	    FICORange rangeMax = FICORange.NewInstance();
	    rangeMax.Value = 9999;
	    rangeMax.Description = "> 100";

	    FICORangeList ranges = new FICORangeList();
	    ranges.Add(range2);
	    ranges.Add(range5);
	    ranges.Add(range100);
	    ranges.Add(rangeMax);

	    IFICORange actual = ranges.GetRangeValue(0);
	    Assert.IsNotNull(actual);
	    Assert.AreEqual(range2.Description, actual.Description);
	}

	[Test]
	public void FICORangeListValueGreaterThanHighestRangeValue() {
	    FICORange range2 = FICORange.NewInstance();
	    range2.Value = 2;
	    range2.Description = "0-2";

	    FICORange range5 = FICORange.NewInstance();
	    range5.Value = 5;
	    range5.Description = "3-5";

	    FICORange range100 = FICORange.NewInstance();
	    range100.Value = 100;
	    range100.Description = "6-100";

	    FICORange rangeMax = FICORange.NewInstance();
	    rangeMax.Value = 9999;
	    rangeMax.Description = "> 100";

	    FICORangeList ranges = new FICORangeList();
	    ranges.Add(range2);
	    ranges.Add(range5);
	    ranges.Add(range100);
	    ranges.Add(rangeMax);

	    IFICORange actual = ranges.GetRangeValue(3);
	    Assert.IsNotNull(actual);
	    Assert.AreEqual(range5.Description, actual.Description);
	}

	[Test]
	public void FICORangeListValueEqualToRangeValue() {
	    FICORange range2 = FICORange.NewInstance();
	    range2.Value = 2;
	    range2.Description = "0-2";

	    FICORange range5 = FICORange.NewInstance();
	    range5.Value = 5;
	    range5.Description = "3-5";

	    FICORange range100 = FICORange.NewInstance();
	    range100.Value = 100;
	    range100.Description = "6-100";

	    FICORange rangeMax = FICORange.NewInstance();
	    rangeMax.Value = 9999;
	    rangeMax.Description = "> 100";

	    FICORangeList ranges = new FICORangeList();
	    ranges.Add(range2);
	    ranges.Add(range5);
	    ranges.Add(range100);
	    ranges.Add(rangeMax);

	    IFICORange actual = ranges.GetRangeValue(5);
	    Assert.IsNotNull(actual);
	    Assert.AreEqual(range5.Description, actual.Description);
	}

	[Test]
	public void FICORangeListValueGreaterThanMaxRangeValue() {
	    FICORange range2 = FICORange.NewInstance();
	    range2.Value = 2;
	    range2.Description = "0-2";

	    FICORange range5 = FICORange.NewInstance();
	    range5.Value = 5;
	    range5.Description = "3-5";

	    FICORange range100 = FICORange.NewInstance();
	    range100.Value = 100;
	    range100.Description = "6-100";

	    FICORange rangeMax = FICORange.NewInstance();
	    rangeMax.Value = 9999;
	    rangeMax.Description = "> 100";

	    FICORangeList ranges = new FICORangeList();
	    ranges.Add(range2);
	    ranges.Add(range5);
	    ranges.Add(range100);
	    ranges.Add(rangeMax);

	    IFICORange actual = ranges.GetRangeValue(10000);
	    Assert.IsNull(actual);
	}
    }
}
