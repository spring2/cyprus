﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring2.DataObject;
using Spring2.UnitTest;
using Spring2.BusinessLogic;
using Spring2.Types;
using Rhino.Mocks;
using Spring2.Core.Types;
using Spring2.Dao;

namespace Spring2.BusinessLogic.UnitTest {
    /// <summary>
    /// Test utility specifically for testing Business Logic classes.
    /// </summary>
    /// <remarks>
    /// These methods need to be fleshed out some more. I've only added things that I've been needing to make my tests pass.
    /// You will need to add what ever it is necessary if it doesn't exist in the method.
    /// </remarks>
    public class TestMockBusinessLogicUtility : TestMockDataUtility {
	#region Set Default Property
	
	#endregion

	#region Mock Business Object Interfaces
	
	#endregion

	#region Helper stubs
	public FICORangeList TestFICORanges(){
	    FICORangeList results = new FICORangeList();

	    FICORange range0 = FICORange.NewInstance();
	    range0.Value = 1;
	    range0.Description = "range0";

	    FICORange range1 = FICORange.NewInstance();
	    range1.Value = 1;
	    range1.Description = "range1";

	    FICORange range2 = FICORange.NewInstance();
	    range2.Value = 2;
	    range2.Description = "range2";

	    FICORange range100 = FICORange.NewInstance();
	    range100.Value = 100;
	    range100.Description = "range100";

	    results.Add(range0);
	    results.Add(range1);
	    results.Add(range2);
	    results.Add(range100);

	    return results;
	}
	#endregion
    }
}
