using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class FICORange : BusinessEntity, IFICORange {
	[Generate()]
	private IdType fICORangeId = IdType.DEFAULT;
	[Generate()]
	private IntegerType value = IntegerType.DEFAULT;
	[Generate()]
	private StringType description = StringType.DEFAULT;

	[Generate()]
	internal FICORange() {}

	[Generate()]
	internal FICORange(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static FICORange NewInstance() {
	    return new FICORange();
	}

	[Generate()]
	public static FICORange GetInstance(IdType fICORangeId) {
	    return FICORangeDAO.DAO.Load(fICORangeId);
	}

	[Generate()]
	public void Update(FICORangeData data) {
	    fICORangeId = data.FICORangeId.IsDefault ? fICORangeId : data.FICORangeId;
	    value = data.Value.IsDefault ? value : data.Value;
	    description = data.Description.IsDefault ? description : data.Description;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.FICORangeId = FICORangeDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		FICORangeDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType FICORangeId {
	    get { return this.fICORangeId; }
	    set { this.fICORangeId = value; }
	}

	[Generate()]
	public IntegerType Value {
	    get { return this.value; }
	    set { this.value = value; }
	}

	[Generate()]
	public StringType Description {
	    get { return this.description; }
	    set { this.description = value; }
	}


	[Generate()]
	public void Reload() {
	    FICORangeDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + FICORangeId.ToString();
	}
    }
}
