using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class TRAN_LOAN : BusinessEntity, ITRAN_LOAN {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private CurrencyType l_PREVIOUS_CREDIT_LIMIT = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType l_REPAY_AMT = CurrencyType.DEFAULT;

	[Generate()]
	internal TRAN_LOAN() {}

	[Generate()]
	internal TRAN_LOAN(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static TRAN_LOAN NewInstance() {
	    return new TRAN_LOAN();
	}

	[Generate()]
	public static TRAN_LOAN GetInstance(IdType aCCT_NUM) {
	    return TRAN_LOANDAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	public void Update(TRAN_LOANData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    l_PREVIOUS_CREDIT_LIMIT = data.L_PREVIOUS_CREDIT_LIMIT.IsDefault ? l_PREVIOUS_CREDIT_LIMIT : data.L_PREVIOUS_CREDIT_LIMIT;
	    l_REPAY_AMT = data.L_REPAY_AMT.IsDefault ? l_REPAY_AMT : data.L_REPAY_AMT;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = TRAN_LOANDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		TRAN_LOANDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public CurrencyType L_PREVIOUS_CREDIT_LIMIT {
	    get { return this.l_PREVIOUS_CREDIT_LIMIT; }
	    set { this.l_PREVIOUS_CREDIT_LIMIT = value; }
	}

	[Generate()]
	public CurrencyType L_REPAY_AMT {
	    get { return this.l_REPAY_AMT; }
	    set { this.l_REPAY_AMT = value; }
	}


	[Generate()]
	public void Reload() {
	    TRAN_LOANDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}
    }
}
