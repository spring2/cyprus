using System.Data;
using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    /// <summary>
    /// A collection of information pertaining to a Loan
    /// </summary>
    public class LoanPortfolio : BusinessEntity, ILoanPortfolio {
	[Generate()]
	private IdType loanPortfolioId = IdType.DEFAULT;
	[Generate()]
	private DateTimeType upDated = DateTimeType.DEFAULT;
	[Generate()]
	private StringType accountID = StringType.DEFAULT;
	[Generate()]
	private StringType member = StringType.DEFAULT;
	[Generate()]
	private IntegerType mClass = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType ltype = IntegerType.DEFAULT;
	[Generate()]
	private DecimalType ltypeSub = DecimalType.DEFAULT;
	[Generate()]
	private IntegerType product = IntegerType.DEFAULT;
	[Generate()]
	private StringType loanDesc = StringType.DEFAULT;
	[Generate()]
	private DateType loanDate = DateType.DEFAULT;
	[Generate()]
	private StringType loanDay = StringType.DEFAULT;
	[Generate()]
	private IntegerType loanMonth = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType loanYear = IntegerType.DEFAULT;
	[Generate()]
	private DateType firstPmtDate = DateType.DEFAULT;
	[Generate()]
	private StringType status = StringType.DEFAULT;
	[Generate()]
	private IntegerType rptCode = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType fundedBr = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType loadedBr = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType fICO = IntegerType.DEFAULT;
	[Generate()]
	private StringType fICORange = StringType.DEFAULT;
	[Generate()]
	private IntegerType fastStart = IntegerType.DEFAULT;
	[Generate()]
	private StringType fastStartRange = StringType.DEFAULT;
	[Generate()]
	private IntegerType bankruptcy = IntegerType.DEFAULT;
	[Generate()]
	private StringType bankruptcyRange = StringType.DEFAULT;
	[Generate()]
	private DateType membershipDate = DateType.DEFAULT;
	[Generate()]
	private StringType newMember = StringType.DEFAULT;
	[Generate()]
	private StringType riskLev = StringType.DEFAULT;
	[Generate()]
	private IntegerType traffic = IntegerType.DEFAULT;
	[Generate()]
	private StringType trafficColor = StringType.DEFAULT;
	[Generate()]
	private IntegerType memAge = IntegerType.DEFAULT;
	[Generate()]
	private StringType memSex = StringType.DEFAULT;
	[Generate()]
	private StringType pmtFreq = StringType.DEFAULT;
	[Generate()]
	private IntegerType origTerm = IntegerType.DEFAULT;
	[Generate()]
	private DecimalType origRate = DecimalType.DEFAULT;
	[Generate()]
	private CurrencyType origPmt = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType origAmount = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType origCrLim = CurrencyType.DEFAULT;
	[Generate()]
	private IntegerType curTerm = IntegerType.DEFAULT;
	[Generate()]
	private DecimalType curRate = DecimalType.DEFAULT;
	[Generate()]
	private CurrencyType curPmt = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType curBal = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType curCrLim = CurrencyType.DEFAULT;
	[Generate()]
	private DateType balloonDate = DateType.DEFAULT;
	[Generate()]
	private DateType matDate = DateType.DEFAULT;
	[Generate()]
	private IntegerType insCode = IntegerType.DEFAULT;
	[Generate()]
	private CurrencyType collateralValue = CurrencyType.DEFAULT;
	[Generate()]
	private DecimalType lTV = DecimalType.DEFAULT;
	[Generate()]
	private IntegerType secType = IntegerType.DEFAULT;
	[Generate()]
	private StringType secDesc1 = StringType.DEFAULT;
	[Generate()]
	private StringType secDesc2 = StringType.DEFAULT;
	[Generate()]
	private DecimalType debtRatio = DecimalType.DEFAULT;
	[Generate()]
	private IntegerType dealer = IntegerType.DEFAULT;
	[Generate()]
	private StringType dealerName = StringType.DEFAULT;
	[Generate()]
	private StringType coborComak = StringType.DEFAULT;
	[Generate()]
	private StringType relPriceGrp = StringType.DEFAULT;
	[Generate()]
	private IntegerType relPriceDisc = IntegerType.DEFAULT;
	[Generate()]
	private DecimalType otherDisc = DecimalType.DEFAULT;
	[Generate()]
	private StringType discReason = StringType.DEFAULT;
	[Generate()]
	private DecimalType riskOffset = DecimalType.DEFAULT;
	[Generate()]
	private StringType creditType = StringType.DEFAULT;
	[Generate()]
	private IntegerType apprOff = IntegerType.DEFAULT;
	[Generate()]
	private StringType statedIncome = StringType.DEFAULT;
	[Generate()]
	private StringType extensions = StringType.DEFAULT;
	[Generate()]
	private StringType firstExtDate = StringType.DEFAULT;
	[Generate()]
	private StringType lastExtDate = StringType.DEFAULT;
	[Generate()]
	private StringType riskLevel = StringType.DEFAULT;
	[Generate()]
	private StringType riskDate = StringType.DEFAULT;
	[Generate()]
	private StringType watch = StringType.DEFAULT;
	[Generate()]
	private StringType watchDate = StringType.DEFAULT;
	[Generate()]
	private StringType workOut = StringType.DEFAULT;
	[Generate()]
	private StringType workOutDate = StringType.DEFAULT;
	[Generate()]
	private StringType firstMortType = StringType.DEFAULT;
	[Generate()]
	private StringType busPerReg = StringType.DEFAULT;
	[Generate()]
	private StringType busGroup = StringType.DEFAULT;
	[Generate()]
	private StringType busDesc = StringType.DEFAULT;
	[Generate()]
	private StringType busPart = StringType.DEFAULT;
	[Generate()]
	private StringType busPartPerc = StringType.DEFAULT;
	[Generate()]
	private StringType sBAguar = StringType.DEFAULT;
	[Generate()]
	private IntegerType deliDays = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType deliSect = IntegerType.DEFAULT;
	[Generate()]
	private StringType deliHist = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ1 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ2 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ3 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ4 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ5 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ6 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ7 = StringType.DEFAULT;
	[Generate()]
	private StringType deliHistQ8 = StringType.DEFAULT;
	[Generate()]
	private DateType chrgOffDate = DateType.DEFAULT;
	[Generate()]
	private IntegerType chrgOffMonth = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType chrgOffYear = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType chrgOffMnthsBF = IntegerType.DEFAULT;
	[Generate()]
	private StringType propType = StringType.DEFAULT;
	[Generate()]
	private StringType occupancy = StringType.DEFAULT;
	[Generate()]
	private CurrencyType currApprVal = CurrencyType.DEFAULT;
	[Generate()]
	private DateType currApprDate = DateType.DEFAULT;
	[Generate()]
	private CurrencyType firstMortBal = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType firstMortMoPyt = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType secondMortBal = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType secondMortPymt = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType taxInsNotInc = CurrencyType.DEFAULT;

	[Generate()]
	internal LoanPortfolio() {}

	[Generate()]
	internal LoanPortfolio(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static LoanPortfolio NewInstance() {
	    return new LoanPortfolio();
	}

	[Generate()]
	public static LoanPortfolio GetInstance(IdType loanPortfolioId) {
	    return LoanPortfolioDAO.DAO.Load(loanPortfolioId);
	}

	//[Generate()]
	public void Update(LoanPortfolioData data) {
	    Update(data, true);
	}

	public void Update(LoanPortfolioData data, Boolean store) {
	    loanPortfolioId = data.LoanPortfolioId.IsDefault ? loanPortfolioId : data.LoanPortfolioId;
	    upDated = data.UpDated.IsDefault ? upDated : data.UpDated;
	    eOMDate = data.EOMDate.IsDefault ? eOMDate : data.EOMDate;
	    accountID = data.AccountID.IsDefault ? accountID : data.AccountID;
	    member = data.Member.IsDefault ? member : data.Member;
	    mClass = data.MClass.IsDefault ? mClass : data.MClass;
	    ltype = data.Ltype.IsDefault ? ltype : data.Ltype;
	    ltypeSub = data.LtypeSub.IsDefault ? ltypeSub : data.LtypeSub;
	    product = data.Product.IsDefault ? product : data.Product;
	    loanDesc = data.LoanDesc.IsDefault ? loanDesc : data.LoanDesc;
	    loanDate = data.LoanDate.IsDefault ? loanDate : data.LoanDate;
	    loanDay = data.LoanDay.IsDefault ? loanDay : data.LoanDay;
	    loanMonth = data.LoanMonth.IsDefault ? loanMonth : data.LoanMonth;
	    loanYear = data.LoanYear.IsDefault ? loanYear : data.LoanYear;
	    firstPmtDate = data.FirstPmtDate.IsDefault ? firstPmtDate : data.FirstPmtDate;
	    status = data.Status.IsDefault ? status : data.Status;
	    rptCode = data.RptCode.IsDefault ? rptCode : data.RptCode;
	    fundedBr = data.FundedBr.IsDefault ? fundedBr : data.FundedBr;
	    loadedBr = data.LoadedBr.IsDefault ? loadedBr : data.LoadedBr;
	    fICO = data.FICO.IsDefault ? fICO : data.FICO;
	    fICORange = data.FICORange.IsDefault ? fICORange : data.FICORange;
	    fastStart = data.FastStart.IsDefault ? fastStart : data.FastStart;
	    fastStartRange = data.FastStartRange.IsDefault ? fastStartRange : data.FastStartRange;
	    bankruptcy = data.Bankruptcy.IsDefault ? bankruptcy : data.Bankruptcy;
	    bankruptcyRange = data.BankruptcyRange.IsDefault ? bankruptcyRange : data.BankruptcyRange;
	    membershipDate = data.MembershipDate.IsDefault ? membershipDate : data.MembershipDate;
	    newMember = data.NewMember.IsDefault ? newMember : data.NewMember;
	    riskLev = data.RiskLev.IsDefault ? riskLev : data.RiskLev;
	    traffic = data.Traffic.IsDefault ? traffic : data.Traffic;
	    trafficColor = data.TrafficColor.IsDefault ? trafficColor : data.TrafficColor;
	    memAge = data.MemAge.IsDefault ? memAge : data.MemAge;
	    memSex = data.MemSex.IsDefault ? memSex : data.MemSex;
	    pmtFreq = data.PmtFreq.IsDefault ? pmtFreq : data.PmtFreq;
	    origTerm = data.OrigTerm.IsDefault ? origTerm : data.OrigTerm;
	    origRate = data.OrigRate.IsDefault ? origRate : data.OrigRate;
	    origPmt = data.OrigPmt.IsDefault ? origPmt : data.OrigPmt;
	    origAmount = data.OrigAmount.IsDefault ? origAmount : data.OrigAmount;
	    origCrLim = data.OrigCrLim.IsDefault ? origCrLim : data.OrigCrLim;
	    curTerm = data.CurTerm.IsDefault ? curTerm : data.CurTerm;
	    curRate = data.CurRate.IsDefault ? curRate : data.CurRate;
	    curPmt = data.CurPmt.IsDefault ? curPmt : data.CurPmt;
	    curBal = data.CurBal.IsDefault ? curBal : data.CurBal;
	    curCrLim = data.CurCrLim.IsDefault ? curCrLim : data.CurCrLim;
	    balloonDate = data.BalloonDate.IsDefault ? balloonDate : data.BalloonDate;
	    matDate = data.MatDate.IsDefault ? matDate : data.MatDate;
	    insCode = data.InsCode.IsDefault ? insCode : data.InsCode;
	    collateralValue = data.CollateralValue.IsDefault ? collateralValue : data.CollateralValue;
	    lTV = data.LTV.IsDefault ? lTV : data.LTV;
	    secType = data.SecType.IsDefault ? secType : data.SecType;
	    secDesc1 = data.SecDesc1.IsDefault ? secDesc1 : data.SecDesc1;
	    secDesc2 = data.SecDesc2.IsDefault ? secDesc2 : data.SecDesc2;
	    debtRatio = data.DebtRatio.IsDefault ? debtRatio : data.DebtRatio;
	    dealer = data.Dealer.IsDefault ? dealer : data.Dealer;
	    dealerName = data.DealerName.IsDefault ? dealerName : data.DealerName;
	    coborComak = data.CoborComak.IsDefault ? coborComak : data.CoborComak;
	    relPriceGrp = data.RelPriceGrp.IsDefault ? relPriceGrp : data.RelPriceGrp;
	    relPriceDisc = data.RelPriceDisc.IsDefault ? relPriceDisc : data.RelPriceDisc;
	    otherDisc = data.OtherDisc.IsDefault ? otherDisc : data.OtherDisc;
	    discReason = data.DiscReason.IsDefault ? discReason : data.DiscReason;
	    riskOffset = data.RiskOffset.IsDefault ? riskOffset : data.RiskOffset;
	    creditType = data.CreditType.IsDefault ? creditType : data.CreditType;
	    statedIncome = data.StatedIncome.IsDefault ? statedIncome : data.StatedIncome;
	    extensions = data.Extensions.IsDefault ? extensions : data.Extensions;
	    firstExtDate = data.FirstExtDate.IsDefault ? firstExtDate : data.FirstExtDate;
	    lastExtDate = data.LastExtDate.IsDefault ? lastExtDate : data.LastExtDate;
	    riskLevel = data.RiskLevel.IsDefault ? riskLevel : data.RiskLevel;
	    riskDate = data.RiskDate.IsDefault ? riskDate : data.RiskDate;
	    watch = data.Watch.IsDefault ? watch : data.Watch;
	    watchDate = data.WatchDate.IsDefault ? watchDate : data.WatchDate;
	    workOut = data.WorkOut.IsDefault ? workOut : data.WorkOut;
	    workOutDate = data.WorkOutDate.IsDefault ? workOutDate : data.WorkOutDate;
	    firstMortType = data.FirstMortType.IsDefault ? firstMortType : data.FirstMortType;
	    busPerReg = data.BusPerReg.IsDefault ? busPerReg : data.BusPerReg;
	    busGroup = data.BusGroup.IsDefault ? busGroup : data.BusGroup;
	    busDesc = data.BusDesc.IsDefault ? busDesc : data.BusDesc;
	    busPart = data.BusPart.IsDefault ? busPart : data.BusPart;
	    busPartPerc = data.BusPartPerc.IsDefault ? busPartPerc : data.BusPartPerc;
	    sBAguar = data.SBAguar.IsDefault ? sBAguar : data.SBAguar;
	    deliDays = data.DeliDays.IsDefault ? deliDays : data.DeliDays;
	    deliSect = data.DeliSect.IsDefault ? deliSect : data.DeliSect;
	    deliHist = data.DeliHist.IsDefault ? deliHist : data.DeliHist;
	    deliHistQ1 = data.DeliHistQ1.IsDefault ? deliHistQ1 : data.DeliHistQ1;
	    deliHistQ2 = data.DeliHistQ2.IsDefault ? deliHistQ2 : data.DeliHistQ2;
	    deliHistQ3 = data.DeliHistQ3.IsDefault ? deliHistQ3 : data.DeliHistQ3;
	    deliHistQ4 = data.DeliHistQ4.IsDefault ? deliHistQ4 : data.DeliHistQ4;
	    deliHistQ5 = data.DeliHistQ5.IsDefault ? deliHistQ5 : data.DeliHistQ5;
	    deliHistQ6 = data.DeliHistQ6.IsDefault ? deliHistQ6 : data.DeliHistQ6;
	    deliHistQ7 = data.DeliHistQ7.IsDefault ? deliHistQ7 : data.DeliHistQ7;
	    deliHistQ8 = data.DeliHistQ8.IsDefault ? deliHistQ8 : data.DeliHistQ8;
	    chrgOffDate = data.ChrgOffDate.IsDefault ? chrgOffDate : data.ChrgOffDate;
	    chrgOffMonth = data.ChrgOffMonth.IsDefault ? chrgOffMonth : data.ChrgOffMonth;
	    chrgOffYear = data.ChrgOffYear.IsDefault ? chrgOffYear : data.ChrgOffYear;
	    chrgOffMnthsBF = data.ChrgOffMnthsBF.IsDefault ? chrgOffMnthsBF : data.ChrgOffMnthsBF;
	    propType = data.PropType.IsDefault ? propType : data.PropType;
	    occupancy = data.Occupancy.IsDefault ? occupancy : data.Occupancy;
	    origApprVal = data.OrigApprVal.IsDefault ? origApprVal : data.OrigApprVal;
	    origApprDate = data.OrigApprDate.IsDefault ? origApprDate : data.OrigApprDate;
	    currApprVal = data.CurrApprVal.IsDefault ? currApprVal : data.CurrApprVal;
	    currApprDate = data.CurrApprDate.IsDefault ? currApprDate : data.CurrApprDate;
	    firstMortBal = data.FirstMortBal.IsDefault ? firstMortBal : data.FirstMortBal;
	    firstMortMoPyt = data.FirstMortMoPyt.IsDefault ? firstMortMoPyt : data.FirstMortMoPyt;
	    secondMortBal = data.SecondMortBal.IsDefault ? secondMortBal : data.SecondMortBal;
	    secondMortPymt = data.SecondMortPymt.IsDefault ? secondMortPymt : data.SecondMortPymt;
	    taxInsNotInc = data.TaxInsNotInc.IsDefault ? taxInsNotInc : data.TaxInsNotInc;
	    loadOff = data.LoadOff.IsDefault ? loadOff : data.LoadOff;
	    apprOff = data.ApprOff.IsDefault ? apprOff : data.ApprOff;
	    fundingOpp = data.FundingOpp.IsDefault ? fundingOpp : data.FundingOpp;
	    chanel = data.Chanel.IsDefault ? chanel : data.Chanel;
	    riskType = data.RiskType.IsDefault ? riskType : data.RiskType;
	    gap = data.Gap.IsDefault ? gap : data.Gap;
	    loadOperator = data.LoadOperator.IsDefault ? loadOperator : data.LoadOperator;
	    deliAmt = data.DeliAmt.IsDefault ? deliAmt : data.DeliAmt;
	    chgOffAmt = data.ChgOffAmt.IsDefault ? chgOffAmt : data.ChgOffAmt;

	    usSecDesc = data.UsSecDesc.IsDefault ? usSecDesc : data.UsSecDesc;
	    troubledDebt = data.TroubledDebt.IsDefault ? troubledDebt : data.TroubledDebt;
	    tdrDate = data.TdrDate.IsDefault ? tdrDate : data.TdrDate;

	    if (store) {
		Store();
	    }
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.LoanPortfolioId = LoanPortfolioDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		LoanPortfolioDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType LoanPortfolioId {
	    get { return this.loanPortfolioId; }
	    set { this.loanPortfolioId = value; }
	}

	[Generate()]
	public DateTimeType UpDated {
	    get { return this.upDated; }
	    set { this.upDated = value; }
	}

	[Generate()]
	public StringType AccountID {
	    get { return this.accountID; }
	    set { this.accountID = value; }
	}

	[Generate()]
	public StringType Member {
	    get { return this.member; }
	    set { this.member = value; }
	}

	[Generate()]
	public IntegerType MClass {
	    get { return this.mClass; }
	    set { this.mClass = value; }
	}

	[Generate()]
	public IntegerType Ltype {
	    get { return this.ltype; }
	    set { this.ltype = value; }
	}

	[Generate()]
	public DecimalType LtypeSub {
	    get { return this.ltypeSub; }
	    set { this.ltypeSub = value; }
	}

	[Generate()]
	public IntegerType Product {
	    get { return this.product; }
	    set { this.product = value; }
	}

	[Generate()]
	public StringType LoanDesc {
	    get { return this.loanDesc; }
	    set { this.loanDesc = value; }
	}

	[Generate()]
	public DateType LoanDate {
	    get { return this.loanDate; }
	    set { this.loanDate = value; }
	}

	[Generate()]
	public StringType LoanDay {
	    get { return this.loanDay; }
	    set { this.loanDay = value; }
	}

	[Generate()]
	public IntegerType LoanMonth {
	    get { return this.loanMonth; }
	    set { this.loanMonth = value; }
	}

	[Generate()]
	public IntegerType LoanYear {
	    get { return this.loanYear; }
	    set { this.loanYear = value; }
	}

	[Generate()]
	public DateType FirstPmtDate {
	    get { return this.firstPmtDate; }
	    set { this.firstPmtDate = value; }
	}

	[Generate()]
	public StringType Status {
	    get { return this.status; }
	    set { this.status = value; }
	}

	[Generate()]
	public IntegerType RptCode {
	    get { return this.rptCode; }
	    set { this.rptCode = value; }
	}

	[Generate()]
	public IntegerType FundedBr {
	    get { return this.fundedBr; }
	    set { this.fundedBr = value; }
	}

	[Generate()]
	public IntegerType LoadedBr {
	    get { return this.loadedBr; }
	    set { this.loadedBr = value; }
	}

	[Generate()]
	public IntegerType FICO {
	    get { return this.fICO; }
	    set { this.fICO = value; }
	}

	[Generate()]
	public StringType FICORange {
	    get { return this.fICORange; }
	    set { this.fICORange = value; }
	}

	[Generate()]
	public IntegerType FastStart {
	    get { return this.fastStart; }
	    set { this.fastStart = value; }
	}

	[Generate()]
	public StringType FastStartRange {
	    get { return this.fastStartRange; }
	    set { this.fastStartRange = value; }
	}

	[Generate()]
	public IntegerType Bankruptcy {
	    get { return this.bankruptcy; }
	    set { this.bankruptcy = value; }
	}

	[Generate()]
	public StringType BankruptcyRange {
	    get { return this.bankruptcyRange; }
	    set { this.bankruptcyRange = value; }
	}

	[Generate()]
	public DateType MembershipDate {
	    get { return this.membershipDate; }
	    set { this.membershipDate = value; }
	}

	[Generate()]
	public StringType NewMember {
	    get { return this.newMember; }
	    set { this.newMember = value; }
	}

	[Generate()]
	public StringType RiskLev {
	    get { return this.riskLev; }
	    set { this.riskLev = value; }
	}

	[Generate()]
	public IntegerType Traffic {
	    get { return this.traffic; }
	    set { this.traffic = value; }
	}

	[Generate()]
	public StringType TrafficColor {
	    get { return this.trafficColor; }
	    set { this.trafficColor = value; }
	}

	[Generate()]
	public IntegerType MemAge {
	    get { return this.memAge; }
	    set { this.memAge = value; }
	}

	[Generate()]
	public StringType MemSex {
	    get { return this.memSex; }
	    set { this.memSex = value; }
	}

	[Generate()]
	public StringType PmtFreq {
	    get { return this.pmtFreq; }
	    set { this.pmtFreq = value; }
	}

	[Generate()]
	public IntegerType OrigTerm {
	    get { return this.origTerm; }
	    set { this.origTerm = value; }
	}

	[Generate()]
	public DecimalType OrigRate {
	    get { return this.origRate; }
	    set { this.origRate = value; }
	}

	[Generate()]
	public CurrencyType OrigPmt {
	    get { return this.origPmt; }
	    set { this.origPmt = value; }
	}

	[Generate()]
	public CurrencyType OrigAmount {
	    get { return this.origAmount; }
	    set { this.origAmount = value; }
	}

	[Generate()]
	public CurrencyType OrigCrLim {
	    get { return this.origCrLim; }
	    set { this.origCrLim = value; }
	}

	[Generate()]
	public IntegerType CurTerm {
	    get { return this.curTerm; }
	    set { this.curTerm = value; }
	}

	[Generate()]
	public DecimalType CurRate {
	    get { return this.curRate; }
	    set { this.curRate = value; }
	}

	[Generate()]
	public CurrencyType CurPmt {
	    get { return this.curPmt; }
	    set { this.curPmt = value; }
	}

	[Generate()]
	public CurrencyType CurBal {
	    get { return this.curBal; }
	    set { this.curBal = value; }
	}

	[Generate()]
	public CurrencyType CurCrLim {
	    get { return this.curCrLim; }
	    set { this.curCrLim = value; }
	}

	[Generate()]
	public DateType BalloonDate {
	    get { return this.balloonDate; }
	    set { this.balloonDate = value; }
	}

	[Generate()]
	public DateType MatDate {
	    get { return this.matDate; }
	    set { this.matDate = value; }
	}

	[Generate()]
	public IntegerType InsCode {
	    get { return this.insCode; }
	    set { this.insCode = value; }
	}

	[Generate()]
	public CurrencyType CollateralValue {
	    get { return this.collateralValue; }
	    set { this.collateralValue = value; }
	}

	[Generate()]
	public DecimalType LTV {
	    get { return this.lTV; }
	    set { this.lTV = value; }
	}

	[Generate()]
	public IntegerType SecType {
	    get { return this.secType; }
	    set { this.secType = value; }
	}

	[Generate()]
	public StringType SecDesc1 {
	    get { return this.secDesc1; }
	    set { this.secDesc1 = value; }
	}

	[Generate()]
	public StringType SecDesc2 {
	    get { return this.secDesc2; }
	    set { this.secDesc2 = value; }
	}

	[Generate()]
	public DecimalType DebtRatio {
	    get { return this.debtRatio; }
	    set { this.debtRatio = value; }
	}

	[Generate()]
	public IntegerType Dealer {
	    get { return this.dealer; }
	    set { this.dealer = value; }
	}

	[Generate()]
	public StringType DealerName {
	    get { return this.dealerName; }
	    set { this.dealerName = value; }
	}

	[Generate()]
	public StringType CoborComak {
	    get { return this.coborComak; }
	    set { this.coborComak = value; }
	}

	[Generate()]
	public StringType RelPriceGrp {
	    get { return this.relPriceGrp; }
	    set { this.relPriceGrp = value; }
	}

	[Generate()]
	public IntegerType RelPriceDisc {
	    get { return this.relPriceDisc; }
	    set { this.relPriceDisc = value; }
	}

	[Generate()]
	public DecimalType OtherDisc {
	    get { return this.otherDisc; }
	    set { this.otherDisc = value; }
	}

	[Generate()]
	public StringType DiscReason {
	    get { return this.discReason; }
	    set { this.discReason = value; }
	}

	[Generate()]
	public DecimalType RiskOffset {
	    get { return this.riskOffset; }
	    set { this.riskOffset = value; }
	}

	[Generate()]
	public StringType CreditType {
	    get { return this.creditType; }
	    set { this.creditType = value; }
	}

	[Generate()]
	public IntegerType ApprOff {
	    get { return this.apprOff; }
	    set { this.apprOff = value; }
	}

	[Generate()]
	public StringType StatedIncome {
	    get { return this.statedIncome; }
	    set { this.statedIncome = value; }
	}

	[Generate()]
	public StringType Extensions {
	    get { return this.extensions; }
	    set { this.extensions = value; }
	}

	[Generate()]
	public StringType FirstExtDate {
	    get { return this.firstExtDate; }
	    set { this.firstExtDate = value; }
	}

	[Generate()]
	public StringType LastExtDate {
	    get { return this.lastExtDate; }
	    set { this.lastExtDate = value; }
	}

	[Generate()]
	public StringType RiskLevel {
	    get { return this.riskLevel; }
	    set { this.riskLevel = value; }
	}

	[Generate()]
	public StringType RiskDate {
	    get { return this.riskDate; }
	    set { this.riskDate = value; }
	}

	[Generate()]
	public StringType Watch {
	    get { return this.watch; }
	    set { this.watch = value; }
	}

	[Generate()]
	public StringType WatchDate {
	    get { return this.watchDate; }
	    set { this.watchDate = value; }
	}

	[Generate()]
	public StringType WorkOut {
	    get { return this.workOut; }
	    set { this.workOut = value; }
	}

	[Generate()]
	public StringType WorkOutDate {
	    get { return this.workOutDate; }
	    set { this.workOutDate = value; }
	}

	[Generate()]
	public StringType FirstMortType {
	    get { return this.firstMortType; }
	    set { this.firstMortType = value; }
	}

	[Generate()]
	public StringType BusPerReg {
	    get { return this.busPerReg; }
	    set { this.busPerReg = value; }
	}

	[Generate()]
	public StringType BusGroup {
	    get { return this.busGroup; }
	    set { this.busGroup = value; }
	}

	[Generate()]
	public StringType BusDesc {
	    get { return this.busDesc; }
	    set { this.busDesc = value; }
	}

	[Generate()]
	public StringType BusPart {
	    get { return this.busPart; }
	    set { this.busPart = value; }
	}

	[Generate()]
	public StringType BusPartPerc {
	    get { return this.busPartPerc; }
	    set { this.busPartPerc = value; }
	}

	[Generate()]
	public StringType SBAguar {
	    get { return this.sBAguar; }
	    set { this.sBAguar = value; }
	}

	[Generate()]
	public IntegerType DeliDays {
	    get { return this.deliDays; }
	    set { this.deliDays = value; }
	}

	[Generate()]
	public IntegerType DeliSect {
	    get { return this.deliSect; }
	    set { this.deliSect = value; }
	}

	[Generate()]
	public StringType DeliHist {
	    get { return this.deliHist; }
	    set { this.deliHist = value; }
	}

	[Generate()]
	public StringType DeliHistQ1 {
	    get { return this.deliHistQ1; }
	    set { this.deliHistQ1 = value; }
	}

	[Generate()]
	public StringType DeliHistQ2 {
	    get { return this.deliHistQ2; }
	    set { this.deliHistQ2 = value; }
	}

	[Generate()]
	public StringType DeliHistQ3 {
	    get { return this.deliHistQ3; }
	    set { this.deliHistQ3 = value; }
	}

	[Generate()]
	public StringType DeliHistQ4 {
	    get { return this.deliHistQ4; }
	    set { this.deliHistQ4 = value; }
	}

	[Generate()]
	public StringType DeliHistQ5 {
	    get { return this.deliHistQ5; }
	    set { this.deliHistQ5 = value; }
	}

	[Generate()]
	public StringType DeliHistQ6 {
	    get { return this.deliHistQ6; }
	    set { this.deliHistQ6 = value; }
	}

	[Generate()]
	public StringType DeliHistQ7 {
	    get { return this.deliHistQ7; }
	    set { this.deliHistQ7 = value; }
	}

	[Generate()]
	public StringType DeliHistQ8 {
	    get { return this.deliHistQ8; }
	    set { this.deliHistQ8 = value; }
	}

	[Generate()]
	public DateType ChrgOffDate {
	    get { return this.chrgOffDate; }
	    set { this.chrgOffDate = value; }
	}

	[Generate()]
	public IntegerType ChrgOffMonth {
	    get { return this.chrgOffMonth; }
	    set { this.chrgOffMonth = value; }
	}

	[Generate()]
	public IntegerType ChrgOffYear {
	    get { return this.chrgOffYear; }
	    set { this.chrgOffYear = value; }
	}

	[Generate()]
	public IntegerType ChrgOffMnthsBF {
	    get { return this.chrgOffMnthsBF; }
	    set { this.chrgOffMnthsBF = value; }
	}

	[Generate()]
	public StringType PropType {
	    get { return this.propType; }
	    set { this.propType = value; }
	}

	[Generate()]
	public StringType Occupancy {
	    get { return this.occupancy; }
	    set { this.occupancy = value; }
	}

	[Generate()]
	public CurrencyType CurrApprVal {
	    get { return this.currApprVal; }
	    set { this.currApprVal = value; }
	}

	[Generate()]
	public DateType CurrApprDate {
	    get { return this.currApprDate; }
	    set { this.currApprDate = value; }
	}

	[Generate()]
	public CurrencyType FirstMortBal {
	    get { return this.firstMortBal; }
	    set { this.firstMortBal = value; }
	}

	[Generate()]
	public CurrencyType FirstMortMoPyt {
	    get { return this.firstMortMoPyt; }
	    set { this.firstMortMoPyt = value; }
	}

	[Generate()]
	public CurrencyType SecondMortBal {
	    get { return this.secondMortBal; }
	    set { this.secondMortBal = value; }
	}

	[Generate()]
	public CurrencyType SecondMortPymt {
	    get { return this.secondMortPymt; }
	    set { this.secondMortPymt = value; }
	}

	[Generate()]
	public CurrencyType TaxInsNotInc {
	    get { return this.taxInsNotInc; }
	    set { this.taxInsNotInc = value; }
	}


	[Generate()]
	public void Reload() {
	    LoanPortfolioDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + LoanPortfolioId.ToString();
	}

	[Generate()]
	private IntegerType loadOff = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType fundingOpp = IntegerType.DEFAULT;

	[Generate()]
	public IntegerType LoadOff {
	    get { return this.loadOff; }
	    set { this.loadOff = value; }
	}

	[Generate()]
	public IntegerType FundingOpp {
	    get { return this.fundingOpp; }
	    set { this.fundingOpp = value; }
	}
	[Generate()]
	private CurrencyType origApprVal = CurrencyType.DEFAULT;
	[Generate()]
	private DateType origApprDate = DateType.DEFAULT;

	[Generate()]
	public CurrencyType OrigApprVal {
	    get { return this.origApprVal; }
	    set { this.origApprVal = value; }
	}

	[Generate()]
	public DateType OrigApprDate {
	    get { return this.origApprDate; }
	    set { this.origApprDate = value; }
	}
	[Generate()]
	private DateType eOMDate = DateType.DEFAULT;

	[Generate()]
	public DateType EOMDate {
	    get { return this.eOMDate; }
	    set { this.eOMDate = value; }
	}

	public Boolean DelinquentInQuarter(Int32 quarter) {
	    String qtrValues = String.Empty;
	    if (quarter.Equals(1) && this.deliHist.Length > 0) {
		qtrValues = deliHist.Length < 3 ? deliHist.Substring(0, deliHist.Length) : deliHist.Substring(0, 3);
	    } else if (quarter.Equals(2) && this.deliHist.Length > 3) {
		qtrValues = deliHist.Length < 6 ? deliHist.Substring(3, deliHist.Length - 3) : deliHist.Substring(3, 3);
	    } else if (quarter.Equals(3) && this.deliHist.Length > 6) {
		qtrValues = deliHist.Length < 9 ? deliHist.Substring(6, deliHist.Length - 6) : deliHist.Substring(6, 3);
	    } else if (quarter.Equals(4) && this.deliHist.Length > 9) {
		qtrValues = deliHist.Length < 12 ? deliHist.Substring(9, deliHist.Length - 9) : deliHist.Substring(9, 3);
	    } else if (quarter.Equals(5) && this.deliHist.Length > 12) {
		qtrValues = deliHist.Length < 15 ? deliHist.Substring(12, deliHist.Length - 12) : deliHist.Substring(12, 3);
	    } else if (quarter.Equals(6) && this.deliHist.Length > 15) {
		qtrValues = deliHist.Length < 18 ? deliHist.Substring(15, deliHist.Length - 15) : deliHist.Substring(15, 3);
	    } else if (quarter.Equals(7) && this.deliHist.Length > 18) {
		qtrValues = deliHist.Length < 21 ? deliHist.Substring(18, deliHist.Length - 18) : deliHist.Substring(18, 3);
	    } else if (quarter.Equals(8) && this.deliHist.Length > 21) {
		qtrValues = deliHist.Length < 24 ? deliHist.Substring(21, deliHist.Length - 21) : deliHist.Substring(21, 3);
	    }
	    return ContainsDelinquentCharacters(qtrValues);
	}

	private Boolean ContainsDelinquentCharacters(String value) {
	    foreach (Char c in value) {
		if ("3456789".Contains(c.ToString())) {
		    return true;
		}
	    }

	    return false;
	}

	[Generate()]
	private StringType chanel = StringType.DEFAULT;

	[Generate()]
	public StringType Chanel {
	    get { return this.chanel; }
	    set { this.chanel = value; }
	}
	[Generate()]
	private StringType riskType = StringType.DEFAULT;

	[Generate()]
	public StringType RiskType {
	    get { return this.riskType; }
	    set { this.riskType = value; }
	}
	[Generate()]
	private StringType usSecDesc = StringType.DEFAULT;

	[Generate()]
	public StringType UsSecDesc {
	    get { return this.usSecDesc; }
	    set { this.usSecDesc = value; }
	}
	[Generate()]
	private StringType troubledDebt = StringType.DEFAULT;

	[Generate()]
	public StringType TroubledDebt {
	    get { return this.troubledDebt; }
	    set { this.troubledDebt = value; }
	}
	[Generate()]
	private StringType tdrDate = StringType.DEFAULT;

	[Generate()]
	public StringType TdrDate {
	    get { return this.tdrDate; }
	    set { this.tdrDate = value; }
	}
	[Generate()]
	private StringType gap = StringType.DEFAULT;

	[Generate()]
	public StringType Gap {
	    get { return this.gap; }
	    set { this.gap = value; }
	}
	[Generate()]
	private IntegerType loadOperator = IntegerType.DEFAULT;

	[Generate()]
	public IntegerType LoadOperator {
	    get { return this.loadOperator; }
	    set { this.loadOperator = value; }
	}
	[Generate()]
	private CurrencyType deliAmt = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType chgOffAmt = CurrencyType.DEFAULT;

	[Generate()]
	public CurrencyType DeliAmt {
	    get { return this.deliAmt; }
	    set { this.deliAmt = value; }
	}

	[Generate()]
	public CurrencyType ChgOffAmt {
	    get { return this.chgOffAmt; }
	    set { this.chgOffAmt = value; }
	}
	[Generate()]
	private StringType jntFicoUsed = StringType.DEFAULT;
	[Generate()]
	private StringType singleOrJointLoan = StringType.DEFAULT;
	[Generate()]
	private StringType approvingOfficerUD = StringType.DEFAULT;
	[Generate()]
	private StringType insuranceCodeDesc = StringType.DEFAULT;
	[Generate()]
	private StringType gapIns = StringType.DEFAULT;
	[Generate()]
	private StringType mmpIns = StringType.DEFAULT;
	[Generate()]
	private StringType reportCodeDesc = StringType.DEFAULT;
	[Generate()]
	private StringType ballonPmtLoan = StringType.DEFAULT;
	[Generate()]
	private StringType fixedOrVariableRateLoan = StringType.DEFAULT;
	[Generate()]
	private StringType alpsOfficer = StringType.DEFAULT;
	[Generate()]
	private StringType cUDLAppNumber = StringType.DEFAULT;
	[Generate()]
	private CurrencyType fasbDefCost = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType fasbLtdAmortFees = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType interestPriorYtd = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType accruedInterest = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType interestLTD = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType interestYtd = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType partialPayAmt = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType interestUncollected = CurrencyType.DEFAULT;
	[Generate()]
	private DateType closedAcctDate = DateType.DEFAULT;
	[Generate()]
	private DateType lastTranDate = DateType.DEFAULT;
	[Generate()]
	private DateType nextDueDate = DateType.DEFAULT;
	[Generate()]
	private IntegerType insuranceCode = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType mbrAgeAtApproval = IntegerType.DEFAULT;

	[Generate()]
	public StringType JntFicoUsed {
	    get { return this.jntFicoUsed; }
	    set { this.jntFicoUsed = value; }
	}

	[Generate()]
	public StringType SingleOrJointLoan {
	    get { return this.singleOrJointLoan; }
	    set { this.singleOrJointLoan = value; }
	}

	[Generate()]
	public StringType ApprovingOfficerUD {
	    get { return this.approvingOfficerUD; }
	    set { this.approvingOfficerUD = value; }
	}

	[Generate()]
	public StringType InsuranceCodeDesc {
	    get { return this.insuranceCodeDesc; }
	    set { this.insuranceCodeDesc = value; }
	}

	[Generate()]
	public StringType GapIns {
	    get { return this.gapIns; }
	    set { this.gapIns = value; }
	}

	[Generate()]
	public StringType MmpIns {
	    get { return this.mmpIns; }
	    set { this.mmpIns = value; }
	}

	[Generate()]
	public StringType ReportCodeDesc {
	    get { return this.reportCodeDesc; }
	    set { this.reportCodeDesc = value; }
	}

	[Generate()]
	public StringType BallonPmtLoan {
	    get { return this.ballonPmtLoan; }
	    set { this.ballonPmtLoan = value; }
	}

	[Generate()]
	public StringType FixedOrVariableRateLoan {
	    get { return this.fixedOrVariableRateLoan; }
	    set { this.fixedOrVariableRateLoan = value; }
	}

	[Generate()]
	public StringType AlpsOfficer {
	    get { return this.alpsOfficer; }
	    set { this.alpsOfficer = value; }
	}

	[Generate()]
	public StringType CUDLAppNumber {
	    get { return this.cUDLAppNumber; }
	    set { this.cUDLAppNumber = value; }
	}

	[Generate()]
	public CurrencyType FasbDefCost {
	    get { return this.fasbDefCost; }
	    set { this.fasbDefCost = value; }
	}

	[Generate()]
	public CurrencyType FasbLtdAmortFees {
	    get { return this.fasbLtdAmortFees; }
	    set { this.fasbLtdAmortFees = value; }
	}

	[Generate()]
	public CurrencyType InterestPriorYtd {
	    get { return this.interestPriorYtd; }
	    set { this.interestPriorYtd = value; }
	}

	[Generate()]
	public CurrencyType AccruedInterest {
	    get { return this.accruedInterest; }
	    set { this.accruedInterest = value; }
	}

	[Generate()]
	public CurrencyType InterestLTD {
	    get { return this.interestLTD; }
	    set { this.interestLTD = value; }
	}

	[Generate()]
	public CurrencyType InterestYtd {
	    get { return this.interestYtd; }
	    set { this.interestYtd = value; }
	}

	[Generate()]
	public CurrencyType PartialPayAmt {
	    get { return this.partialPayAmt; }
	    set { this.partialPayAmt = value; }
	}

	[Generate()]
	public CurrencyType InterestUncollected {
	    get { return this.interestUncollected; }
	    set { this.interestUncollected = value; }
	}

	[Generate()]
	public DateType ClosedAcctDate {
	    get { return this.closedAcctDate; }
	    set { this.closedAcctDate = value; }
	}

	[Generate()]
	public DateType LastTranDate {
	    get { return this.lastTranDate; }
	    set { this.lastTranDate = value; }
	}

	[Generate()]
	public DateType NextDueDate {
	    get { return this.nextDueDate; }
	    set { this.nextDueDate = value; }
	}

	[Generate()]
	public IntegerType InsuranceCode {
	    get { return this.insuranceCode; }
	    set { this.insuranceCode = value; }
	}

	[Generate()]
	public IntegerType MbrAgeAtApproval {
	    get { return this.mbrAgeAtApproval; }
	    set { this.mbrAgeAtApproval = value; }
	}
    }
}
