using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class APPLIC : BusinessEntity, IAPPLIC {
	[Generate()]
	private IdType aPPLIC_ID = IdType.DEFAULT;
	[Generate()]
	private IntegerType fINAL_STAMP_BR = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType lOAD_STAMP_BR = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType sTAGE2_CRSCORE = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType aP_RISK_SCORE = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType bANKRUPT_SCORE = IntegerType.DEFAULT;
	[Generate()]
	private StringType aP_RISK_LEVEL = StringType.DEFAULT;
	[Generate()]
	private IntegerType aLPS_COLOR = IntegerType.DEFAULT;
	[Generate()]
	private StringType pAYMENT_FREQ = StringType.DEFAULT;
	[Generate()]
	private CurrencyType tOT_FIN = CurrencyType.DEFAULT;
	[Generate()]
	private DateTimeType bALLOON_DATE = DateTimeType.DEFAULT;
	[Generate()]
	private IntegerType dEALER_NUMBER = IntegerType.DEFAULT;
	[Generate()]
	private StringType cUDL_DEALER_NAME = StringType.DEFAULT;
	[Generate()]
	private IntegerType aP_RP_DISCOUNT = IntegerType.DEFAULT;
	[Generate()]
	private StringType cRSCORE_TYPE = StringType.DEFAULT;
	[Generate()]
	private StringType hELOC_TYPE = StringType.DEFAULT;
	[Generate()]
	private StringType hELOC_OCCUPANCY = StringType.DEFAULT;
	[Generate()]
	private StringType hELOC_VALUE = StringType.DEFAULT;
	[Generate()]
	private CurrencyType hELOC_1STMORT_BAL = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType hELOC_1STMORT_PAY = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType hELOC_2NDMORT_BAL = CurrencyType.DEFAULT;
	[Generate()]
	private CurrencyType hELOC_2NDMORT_PAY = CurrencyType.DEFAULT;
	[Generate()]
	private StringType iNSURE_CODE = StringType.DEFAULT;

	[Generate()]
	internal APPLIC() {}

	[Generate()]
	internal APPLIC(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static APPLIC NewInstance() {
	    return new APPLIC();
	}

	[Generate()]
	public static APPLIC GetInstance(IdType aPPLIC_ID) {
	    return APPLICDAO.DAO.Load(aPPLIC_ID);
	}

	[Generate()]
	public void Update(APPLICData data) {
	    aPPLIC_ID = data.APPLIC_ID.IsDefault ? aPPLIC_ID : data.APPLIC_ID;
	    aPPLIC_NUM = data.APPLIC_NUM.IsDefault ? aPPLIC_NUM : data.APPLIC_NUM;
	    fINAL_STAMP_BR = data.FINAL_STAMP_BR.IsDefault ? fINAL_STAMP_BR : data.FINAL_STAMP_BR;
	    lOAD_STAMP_BR = data.LOAD_STAMP_BR.IsDefault ? lOAD_STAMP_BR : data.LOAD_STAMP_BR;
	    sTAGE2_CRSCORE = data.STAGE2_CRSCORE.IsDefault ? sTAGE2_CRSCORE : data.STAGE2_CRSCORE;
	    aP_RISK_SCORE = data.AP_RISK_SCORE.IsDefault ? aP_RISK_SCORE : data.AP_RISK_SCORE;
	    bANKRUPT_SCORE = data.BANKRUPT_SCORE.IsDefault ? bANKRUPT_SCORE : data.BANKRUPT_SCORE;
	    aP_RISK_LEVEL = data.AP_RISK_LEVEL.IsDefault ? aP_RISK_LEVEL : data.AP_RISK_LEVEL;
	    aLPS_COLOR = data.ALPS_COLOR.IsDefault ? aLPS_COLOR : data.ALPS_COLOR;
	    pAYMENT_FREQ = data.PAYMENT_FREQ.IsDefault ? pAYMENT_FREQ : data.PAYMENT_FREQ;
	    tOT_FIN = data.TOT_FIN.IsDefault ? tOT_FIN : data.TOT_FIN;
	    bALLOON_DATE = data.BALLOON_DATE.IsDefault ? bALLOON_DATE : data.BALLOON_DATE;
	    dEALER_NUMBER = data.DEALER_NUMBER.IsDefault ? dEALER_NUMBER : data.DEALER_NUMBER;
	    cUDL_DEALER_NAME = data.CUDL_DEALER_NAME.IsDefault ? cUDL_DEALER_NAME : data.CUDL_DEALER_NAME;
	    aP_RP_DISCOUNT = data.AP_RP_DISCOUNT.IsDefault ? aP_RP_DISCOUNT : data.AP_RP_DISCOUNT;
	    cRSCORE_TYPE = data.CRSCORE_TYPE.IsDefault ? cRSCORE_TYPE : data.CRSCORE_TYPE;
	    hELOC_TYPE = data.HELOC_TYPE.IsDefault ? hELOC_TYPE : data.HELOC_TYPE;
	    hELOC_OCCUPANCY = data.HELOC_OCCUPANCY.IsDefault ? hELOC_OCCUPANCY : data.HELOC_OCCUPANCY;
	    hELOC_VALUE = data.HELOC_VALUE.IsDefault ? hELOC_VALUE : data.HELOC_VALUE;
	    hELOC_1STMORT_BAL = data.HELOC_1STMORT_BAL.IsDefault ? hELOC_1STMORT_BAL : data.HELOC_1STMORT_BAL;
	    hELOC_1STMORT_PAY = data.HELOC_1STMORT_PAY.IsDefault ? hELOC_1STMORT_PAY : data.HELOC_1STMORT_PAY;
	    hELOC_2NDMORT_BAL = data.HELOC_2NDMORT_BAL.IsDefault ? hELOC_2NDMORT_BAL : data.HELOC_2NDMORT_BAL;
	    hELOC_2NDMORT_PAY = data.HELOC_2NDMORT_PAY.IsDefault ? hELOC_2NDMORT_PAY : data.HELOC_2NDMORT_PAY;
	    hELOC_ORIG_DATE = data.HELOC_ORIG_DATE.IsDefault ? hELOC_ORIG_DATE : data.HELOC_ORIG_DATE;
	    iNSURE_CODE = data.INSURE_CODE.IsDefault ? iNSURE_CODE : data.INSURE_CODE;
	    aP_INTEREST_RATE = data.AP_INTEREST_RATE.IsDefault ? aP_INTEREST_RATE : data.AP_INTEREST_RATE;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.APPLIC_ID = APPLICDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		APPLICDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType APPLIC_ID {
	    get { return this.aPPLIC_ID; }
	    set { this.aPPLIC_ID = value; }
	}

	[Generate()]
	public IntegerType FINAL_STAMP_BR {
	    get { return this.fINAL_STAMP_BR; }
	    set { this.fINAL_STAMP_BR = value; }
	}

	[Generate()]
	public IntegerType LOAD_STAMP_BR {
	    get { return this.lOAD_STAMP_BR; }
	    set { this.lOAD_STAMP_BR = value; }
	}

	[Generate()]
	public IntegerType STAGE2_CRSCORE {
	    get { return this.sTAGE2_CRSCORE; }
	    set { this.sTAGE2_CRSCORE = value; }
	}

	[Generate()]
	public IntegerType AP_RISK_SCORE {
	    get { return this.aP_RISK_SCORE; }
	    set { this.aP_RISK_SCORE = value; }
	}

	[Generate()]
	public IntegerType BANKRUPT_SCORE {
	    get { return this.bANKRUPT_SCORE; }
	    set { this.bANKRUPT_SCORE = value; }
	}

	[Generate()]
	public StringType AP_RISK_LEVEL {
	    get { return this.aP_RISK_LEVEL; }
	    set { this.aP_RISK_LEVEL = value; }
	}

	[Generate()]
	public IntegerType ALPS_COLOR {
	    get { return this.aLPS_COLOR; }
	    set { this.aLPS_COLOR = value; }
	}

	[Generate()]
	public StringType PAYMENT_FREQ {
	    get { return this.pAYMENT_FREQ; }
	    set { this.pAYMENT_FREQ = value; }
	}

	[Generate()]
	public CurrencyType TOT_FIN {
	    get { return this.tOT_FIN; }
	    set { this.tOT_FIN = value; }
	}

	[Generate()]
	public DateTimeType BALLOON_DATE {
	    get { return this.bALLOON_DATE; }
	    set { this.bALLOON_DATE = value; }
	}

	[Generate()]
	public IntegerType DEALER_NUMBER {
	    get { return this.dEALER_NUMBER; }
	    set { this.dEALER_NUMBER = value; }
	}

	[Generate()]
	public StringType CUDL_DEALER_NAME {
	    get { return this.cUDL_DEALER_NAME; }
	    set { this.cUDL_DEALER_NAME = value; }
	}

	[Generate()]
	public IntegerType AP_RP_DISCOUNT {
	    get { return this.aP_RP_DISCOUNT; }
	    set { this.aP_RP_DISCOUNT = value; }
	}

	[Generate()]
	public StringType CRSCORE_TYPE {
	    get { return this.cRSCORE_TYPE; }
	    set { this.cRSCORE_TYPE = value; }
	}

	[Generate()]
	public StringType HELOC_TYPE {
	    get { return this.hELOC_TYPE; }
	    set { this.hELOC_TYPE = value; }
	}

	[Generate()]
	public StringType HELOC_OCCUPANCY {
	    get { return this.hELOC_OCCUPANCY; }
	    set { this.hELOC_OCCUPANCY = value; }
	}

	[Generate()]
	public StringType HELOC_VALUE {
	    get { return this.hELOC_VALUE; }
	    set { this.hELOC_VALUE = value; }
	}

	[Generate()]
	public CurrencyType HELOC_1STMORT_BAL {
	    get { return this.hELOC_1STMORT_BAL; }
	    set { this.hELOC_1STMORT_BAL = value; }
	}

	[Generate()]
	public CurrencyType HELOC_1STMORT_PAY {
	    get { return this.hELOC_1STMORT_PAY; }
	    set { this.hELOC_1STMORT_PAY = value; }
	}

	[Generate()]
	public CurrencyType HELOC_2NDMORT_BAL {
	    get { return this.hELOC_2NDMORT_BAL; }
	    set { this.hELOC_2NDMORT_BAL = value; }
	}

	[Generate()]
	public CurrencyType HELOC_2NDMORT_PAY {
	    get { return this.hELOC_2NDMORT_PAY; }
	    set { this.hELOC_2NDMORT_PAY = value; }
	}

	[Generate()]
	public StringType INSURE_CODE {
	    get { return this.iNSURE_CODE; }
	    set { this.iNSURE_CODE = value; }
	}


	[Generate()]
	public void Reload() {
	    APPLICDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + APPLIC_ID.ToString();
	}

	[Generate()]
	private IdType aPPLIC_NUM = IdType.DEFAULT;

	[Generate()]
	public IdType APPLIC_NUM {
	    get { return this.aPPLIC_NUM; }
	    set { this.aPPLIC_NUM = value; }
	}
	[Generate()]
	private DateTimeType hELOC_ORIG_DATE = DateTimeType.DEFAULT;
	[Generate()]
	private DecimalType aP_INTEREST_RATE = DecimalType.DEFAULT;

	[Generate()]
	public DateTimeType HELOC_ORIG_DATE {
	    get { return this.hELOC_ORIG_DATE; }
	    set { this.hELOC_ORIG_DATE = value; }
	}

	[Generate()]
	public DecimalType AP_INTEREST_RATE {
	    get { return this.aP_INTEREST_RATE; }
	    set { this.aP_INTEREST_RATE = value; }
	}
    }
}
