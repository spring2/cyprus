using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class LoanType : BusinessEntity, ILoanType {
	[Generate()]
	private IdType loanTypeId = IdType.DEFAULT;
	[Generate()]
	private IntegerType lType = IntegerType.DEFAULT;
	[Generate()]
	private StringType loanDesc = StringType.DEFAULT;

	[Generate()]
	internal LoanType() {}

	[Generate()]
	internal LoanType(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static LoanType NewInstance() {
	    return new LoanType();
	}

	[Generate()]
	public static LoanType GetInstance(IdType loanTypeId) {
	    return LoanTypeDAO.DAO.Load(loanTypeId);
	}

	[Generate()]
	public void Update(LoanTypeData data) {
	    loanTypeId = data.LoanTypeId.IsDefault ? loanTypeId : data.LoanTypeId;
	    lType = data.LType.IsDefault ? lType : data.LType;
	    loanDesc = data.LoanDesc.IsDefault ? loanDesc : data.LoanDesc;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.LoanTypeId = LoanTypeDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		LoanTypeDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType LoanTypeId {
	    get { return this.loanTypeId; }
	    set { this.loanTypeId = value; }
	}

	[Generate()]
	public IntegerType LType {
	    get { return this.lType; }
	    set { this.lType = value; }
	}

	[Generate()]
	public StringType LoanDesc {
	    get { return this.loanDesc; }
	    set { this.loanDesc = value; }
	}


	[Generate()]
	public void Reload() {
	    LoanTypeDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + LoanTypeId.ToString();
	}
    }
}
