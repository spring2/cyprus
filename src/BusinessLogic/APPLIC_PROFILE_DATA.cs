using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class APPLIC_PROFILE_DATA : BusinessEntity, IAPPLIC_PROFILE_DATA {
	[Generate()]
	private IntegerType nO_YRS_ADDR = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType pREV_YRS_ADDR = IntegerType.DEFAULT;

	[Generate()]
	internal APPLIC_PROFILE_DATA() {}

	[Generate()]
	internal APPLIC_PROFILE_DATA(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static APPLIC_PROFILE_DATA NewInstance() {
	    return new APPLIC_PROFILE_DATA();
	}

	[Generate()]
	public static APPLIC_PROFILE_DATA GetInstance(IdType aPPLIC_NUM) {
	    return APPLIC_PROFILE_DATADAO.DAO.Load(aPPLIC_NUM);
	}

	[Generate()]
	public void Update(APPLIC_PROFILE_DATAData data) {
	    aPPLIC_NUM = data.APPLIC_NUM.IsDefault ? aPPLIC_NUM : data.APPLIC_NUM;
	    nO_YRS_ADDR = data.NO_YRS_ADDR.IsDefault ? nO_YRS_ADDR : data.NO_YRS_ADDR;
	    pREV_YRS_ADDR = data.PREV_YRS_ADDR.IsDefault ? pREV_YRS_ADDR : data.PREV_YRS_ADDR;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.APPLIC_NUM = APPLIC_PROFILE_DATADAO.DAO.Insert(this);
		isNew = false;
	    } else {
		APPLIC_PROFILE_DATADAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IntegerType NO_YRS_ADDR {
	    get { return this.nO_YRS_ADDR; }
	    set { this.nO_YRS_ADDR = value; }
	}

	[Generate()]
	public IntegerType PREV_YRS_ADDR {
	    get { return this.pREV_YRS_ADDR; }
	    set { this.pREV_YRS_ADDR = value; }
	}


	[Generate()]
	public void Reload() {
	    APPLIC_PROFILE_DATADAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + APPLIC_NUM.ToString();
	}

	[Generate()]
	private IdType aPPLIC_NUM = IdType.DEFAULT;


	[Generate()]
	public IdType APPLIC_NUM {
	    get { return this.aPPLIC_NUM; }
	    set { this.aPPLIC_NUM = value; }
	}
    }
}
