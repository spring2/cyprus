using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class DELINQUENCY_DATA : BusinessEntity, IDELINQUENCY_DATA {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private StringType dLQ_HIST = StringType.DEFAULT;

	[Generate()]
	internal DELINQUENCY_DATA() {}

	[Generate()]
	internal DELINQUENCY_DATA(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static DELINQUENCY_DATA NewInstance() {
	    return new DELINQUENCY_DATA();
	}

	[Generate()]
	public static DELINQUENCY_DATA GetInstance(IdType aCCT_NUM) {
	    return DELINQUENCY_DATADAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	public void Update(DELINQUENCY_DATAData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    dLQ_HIST = data.DLQ_HIST.IsDefault ? dLQ_HIST : data.DLQ_HIST;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = DELINQUENCY_DATADAO.DAO.Insert(this);
		isNew = false;
	    } else {
		DELINQUENCY_DATADAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public StringType DLQ_HIST {
	    get { return this.dLQ_HIST; }
	    set { this.dLQ_HIST = value; }
	}


	[Generate()]
	public void Reload() {
	    DELINQUENCY_DATADAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}
    }
}
