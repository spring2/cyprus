using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class PROFILE_EMPLOYER_INFO : BusinessEntity, IPROFILE_EMPLOYER_INFO {
	[Generate()]
	private IdType rECORD_KEY = IdType.DEFAULT;
	[Generate()]
	private CurrencyType eMP_GROSS_INCOME = CurrencyType.DEFAULT;

	[Generate()]
	internal PROFILE_EMPLOYER_INFO() {}

	[Generate()]
	internal PROFILE_EMPLOYER_INFO(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static PROFILE_EMPLOYER_INFO NewInstance() {
	    return new PROFILE_EMPLOYER_INFO();
	}

	[Generate()]
	public static PROFILE_EMPLOYER_INFO GetInstance(IdType rECORD_KEY) {
	    return PROFILE_EMPLOYER_INFODAO.DAO.Load(rECORD_KEY);
	}

	[Generate()]
	public void Update(PROFILE_EMPLOYER_INFOData data) {
	    rECORD_KEY = data.RECORD_KEY.IsDefault ? rECORD_KEY : data.RECORD_KEY;
	    eMP_GROSS_INCOME = data.EMP_GROSS_INCOME.IsDefault ? eMP_GROSS_INCOME : data.EMP_GROSS_INCOME;
	    eMP_YRS_PROF = data.EMP_YRS_PROF.IsDefault ? eMP_YRS_PROF : data.EMP_YRS_PROF;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.RECORD_KEY = PROFILE_EMPLOYER_INFODAO.DAO.Insert(this);
		isNew = false;
	    } else {
		PROFILE_EMPLOYER_INFODAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType RECORD_KEY {
	    get { return this.rECORD_KEY; }
	    set { this.rECORD_KEY = value; }
	}

	[Generate()]
	public CurrencyType EMP_GROSS_INCOME {
	    get { return this.eMP_GROSS_INCOME; }
	    set { this.eMP_GROSS_INCOME = value; }
	}


	[Generate()]
	public void Reload() {
	    PROFILE_EMPLOYER_INFODAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + RECORD_KEY.ToString();
	}

	[Generate()]
	private IntegerType eMP_YRS_PROF = IntegerType.DEFAULT;

	[Generate()]
	public IntegerType EMP_YRS_PROF {
	    get { return this.eMP_YRS_PROF; }
	    set { this.eMP_YRS_PROF = value; }
	}
    }
}
