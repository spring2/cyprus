using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class ACCOUNT : BusinessEntity, IACCOUNT {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private IdType mEM_NUM = IdType.DEFAULT;
	[Generate()]
	private DecimalType l_ACCT_LEVEL = DecimalType.DEFAULT;
	[Generate()]
	private IntegerType aCCT_SUBLEV = IntegerType.DEFAULT;
	[Generate()]
	private DateTimeType aCCOUNT_OPEN_DATE = DateTimeType.DEFAULT;
	[Generate()]
	private StringType sPEC_REPORT_CODE = StringType.DEFAULT;
	[Generate()]
	private CurrencyType oRIG_PAY = CurrencyType.DEFAULT;
	[Generate()]
	private DecimalType aPR = DecimalType.DEFAULT;
	[Generate()]
	private CurrencyType cURR_CREDIT_LIM = CurrencyType.DEFAULT;
	[Generate()]
	private StringType pG_CODE = StringType.DEFAULT;
	[Generate()]
	private StringType dISC_REASON = StringType.DEFAULT;
	[Generate()]
	private StringType rISK_OFFSET_VAL = StringType.DEFAULT;
	[Generate()]
	private DateTimeType fIRST_PAYDATE = DateTimeType.DEFAULT;
	[Generate()]
	private IntegerType oRIG_LOAN_TERM = IntegerType.DEFAULT;
	[Generate()]
	private DateTimeType l_MATURITY_DATE = DateTimeType.DEFAULT;
	[Generate()]
	private IntegerType l_INSCODE = IntegerType.DEFAULT;
	[Generate()]
	private IntegerType l_SECCODE = IntegerType.DEFAULT;
	[Generate()]
	private StringType l_SECDESC1 = StringType.DEFAULT;
	[Generate()]
	private StringType l_SECDESC2 = StringType.DEFAULT;
	[Generate()]
	private IdType uS_APPLIC_ID = IdType.DEFAULT;

	[Generate()]
	internal ACCOUNT() {}

	[Generate()]
	internal ACCOUNT(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static ACCOUNT NewInstance() {
	    return new ACCOUNT();
	}

	[Generate()]
	public void Update(ACCOUNTData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    mEM_NUM = data.MEM_NUM.IsDefault ? mEM_NUM : data.MEM_NUM;
	    l_ACCT_LEVEL = data.L_ACCT_LEVEL.IsDefault ? l_ACCT_LEVEL : data.L_ACCT_LEVEL;
	    aCCT_SUBLEV = data.ACCT_SUBLEV.IsDefault ? aCCT_SUBLEV : data.ACCT_SUBLEV;
	    aCCOUNT_OPEN_DATE = data.ACCOUNT_OPEN_DATE.IsDefault ? aCCOUNT_OPEN_DATE : data.ACCOUNT_OPEN_DATE;
	    sPEC_REPORT_CODE = data.SPEC_REPORT_CODE.IsDefault ? sPEC_REPORT_CODE : data.SPEC_REPORT_CODE;
	    oRIG_PAY = data.ORIG_PAY.IsDefault ? oRIG_PAY : data.ORIG_PAY;
	    aPR = data.APR.IsDefault ? aPR : data.APR;
	    cURR_CREDIT_LIM = data.CURR_CREDIT_LIM.IsDefault ? cURR_CREDIT_LIM : data.CURR_CREDIT_LIM;
	    pG_CODE = data.PG_CODE.IsDefault ? pG_CODE : data.PG_CODE;
	    dISC_REASON = data.DISC_REASON.IsDefault ? dISC_REASON : data.DISC_REASON;
	    rISK_OFFSET_VAL = data.RISK_OFFSET_VAL.IsDefault ? rISK_OFFSET_VAL : data.RISK_OFFSET_VAL;
	    fIRST_PAYDATE = data.FIRST_PAYDATE.IsDefault ? fIRST_PAYDATE : data.FIRST_PAYDATE;
	    oRIG_LOAN_TERM = data.ORIG_LOAN_TERM.IsDefault ? oRIG_LOAN_TERM : data.ORIG_LOAN_TERM;
	    l_MATURITY_DATE = data.L_MATURITY_DATE.IsDefault ? l_MATURITY_DATE : data.L_MATURITY_DATE;
	    l_INSCODE = data.L_INSCODE.IsDefault ? l_INSCODE : data.L_INSCODE;
	    l_SECCODE = data.L_SECCODE.IsDefault ? l_SECCODE : data.L_SECCODE;
	    l_SECDESC1 = data.L_SECDESC1.IsDefault ? l_SECDESC1 : data.L_SECDESC1;
	    l_SECDESC2 = data.L_SECDESC2.IsDefault ? l_SECDESC2 : data.L_SECDESC2;
	    uS_APPLIC_ID = data.US_APPLIC_ID.IsDefault ? uS_APPLIC_ID : data.US_APPLIC_ID;
	    l_ACCT_TYPE = data.L_ACCT_TYPE.IsDefault ? l_ACCT_TYPE : data.L_ACCT_TYPE;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = ACCOUNTDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		ACCOUNTDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public IdType MEM_NUM {
	    get { return this.mEM_NUM; }
	    set { this.mEM_NUM = value; }
	}

	[Generate()]
	public DecimalType L_ACCT_LEVEL {
	    get { return this.l_ACCT_LEVEL; }
	    set { this.l_ACCT_LEVEL = value; }
	}

	[Generate()]
	public IntegerType ACCT_SUBLEV {
	    get { return this.aCCT_SUBLEV; }
	    set { this.aCCT_SUBLEV = value; }
	}

	[Generate()]
	public DateTimeType ACCOUNT_OPEN_DATE {
	    get { return this.aCCOUNT_OPEN_DATE; }
	    set { this.aCCOUNT_OPEN_DATE = value; }
	}

	[Generate()]
	public StringType SPEC_REPORT_CODE {
	    get { return this.sPEC_REPORT_CODE; }
	    set { this.sPEC_REPORT_CODE = value; }
	}

	[Generate()]
	public CurrencyType ORIG_PAY {
	    get { return this.oRIG_PAY; }
	    set { this.oRIG_PAY = value; }
	}

	[Generate()]
	public DecimalType APR {
	    get { return this.aPR; }
	    set { this.aPR = value; }
	}

	[Generate()]
	public CurrencyType CURR_CREDIT_LIM {
	    get { return this.cURR_CREDIT_LIM; }
	    set { this.cURR_CREDIT_LIM = value; }
	}

	[Generate()]
	public StringType PG_CODE {
	    get { return this.pG_CODE; }
	    set { this.pG_CODE = value; }
	}

	[Generate()]
	public StringType DISC_REASON {
	    get { return this.dISC_REASON; }
	    set { this.dISC_REASON = value; }
	}

	[Generate()]
	public StringType RISK_OFFSET_VAL {
	    get { return this.rISK_OFFSET_VAL; }
	    set { this.rISK_OFFSET_VAL = value; }
	}

	[Generate()]
	public DateTimeType FIRST_PAYDATE {
	    get { return this.fIRST_PAYDATE; }
	    set { this.fIRST_PAYDATE = value; }
	}

	[Generate()]
	public IntegerType ORIG_LOAN_TERM {
	    get { return this.oRIG_LOAN_TERM; }
	    set { this.oRIG_LOAN_TERM = value; }
	}

	[Generate()]
	public DateTimeType L_MATURITY_DATE {
	    get { return this.l_MATURITY_DATE; }
	    set { this.l_MATURITY_DATE = value; }
	}

	[Generate()]
	public IntegerType L_INSCODE {
	    get { return this.l_INSCODE; }
	    set { this.l_INSCODE = value; }
	}

	[Generate()]
	public IntegerType L_SECCODE {
	    get { return this.l_SECCODE; }
	    set { this.l_SECCODE = value; }
	}

	[Generate()]
	public StringType L_SECDESC1 {
	    get { return this.l_SECDESC1; }
	    set { this.l_SECDESC1 = value; }
	}

	[Generate()]
	public StringType L_SECDESC2 {
	    get { return this.l_SECDESC2; }
	    set { this.l_SECDESC2 = value; }
	}

	[Generate()]
	public IdType US_APPLIC_ID {
	    get { return this.uS_APPLIC_ID; }
	    set { this.uS_APPLIC_ID = value; }
	}


	[Generate()]
	public void Reload() {
	    ACCOUNTDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}

	[Generate()]
	public static ACCOUNT GetInstance(IdType aCCT_NUM) {
	    return ACCOUNTDAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	private StringType l_ACCT_TYPE = StringType.DEFAULT;

	[Generate()]
	public StringType L_ACCT_TYPE {
	    get { return this.l_ACCT_TYPE; }
	    set { this.l_ACCT_TYPE = value; }
	}
    }
}
