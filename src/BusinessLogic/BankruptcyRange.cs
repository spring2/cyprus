using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class BankruptcyRange : BusinessEntity, IBankruptcyRange {
	[Generate()]
	private IdType bankruptcyRangeId = IdType.DEFAULT;
	[Generate()]
	private IntegerType value = IntegerType.DEFAULT;
	[Generate()]
	private StringType description = StringType.DEFAULT;

	[Generate()]
	internal BankruptcyRange() {}

	[Generate()]
	internal BankruptcyRange(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static BankruptcyRange NewInstance() {
	    return new BankruptcyRange();
	}

	[Generate()]
	public static BankruptcyRange GetInstance(IdType bankruptcyRangeId) {
	    return BankruptcyRangeDAO.DAO.Load(bankruptcyRangeId);
	}

	[Generate()]
	public void Update(BankruptcyRangeData data) {
	    bankruptcyRangeId = data.BankruptcyRangeId.IsDefault ? bankruptcyRangeId : data.BankruptcyRangeId;
	    value = data.Value.IsDefault ? value : data.Value;
	    description = data.Description.IsDefault ? description : data.Description;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.BankruptcyRangeId = BankruptcyRangeDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		BankruptcyRangeDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType BankruptcyRangeId {
	    get { return this.bankruptcyRangeId; }
	    set { this.bankruptcyRangeId = value; }
	}

	[Generate()]
	public IntegerType Value {
	    get { return this.value; }
	    set { this.value = value; }
	}

	[Generate()]
	public StringType Description {
	    get { return this.description; }
	    set { this.description = value; }
	}


	[Generate()]
	public void Reload() {
	    BankruptcyRangeDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + BankruptcyRangeId.ToString();
	}
    }
}
