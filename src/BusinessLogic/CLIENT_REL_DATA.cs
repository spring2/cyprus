using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class CLIENT_REL_DATA : BusinessEntity, ICLIENT_REL_DATA {
	[Generate()]
	private IdType mEM_NUM = IdType.DEFAULT;
	[Generate()]
	private IntegerType aGE = IntegerType.DEFAULT;

	[Generate()]
	internal CLIENT_REL_DATA() {}

	[Generate()]
	internal CLIENT_REL_DATA(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static CLIENT_REL_DATA NewInstance() {
	    return new CLIENT_REL_DATA();
	}

	[Generate()]
	public static CLIENT_REL_DATA GetInstance(IdType mEM_NUM) {
	    return CLIENT_REL_DATADAO.DAO.Load(mEM_NUM);
	}

	[Generate()]
	public void Update(CLIENT_REL_DATAData data) {
	    mEM_NUM = data.MEM_NUM.IsDefault ? mEM_NUM : data.MEM_NUM;
	    aGE = data.AGE.IsDefault ? aGE : data.AGE;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.MEM_NUM = CLIENT_REL_DATADAO.DAO.Insert(this);
		isNew = false;
	    } else {
		CLIENT_REL_DATADAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType MEM_NUM {
	    get { return this.mEM_NUM; }
	    set { this.mEM_NUM = value; }
	}

	[Generate()]
	public IntegerType AGE {
	    get { return this.aGE; }
	    set { this.aGE = value; }
	}


	[Generate()]
	public void Reload() {
	    CLIENT_REL_DATADAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + MEM_NUM.ToString();
	}
    }
}
