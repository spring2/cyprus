using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class ACCOUNT_UD_US : BusinessEntity, IACCOUNT_UD_US {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private StringType uS_INCOME_STATED = StringType.DEFAULT;
	[Generate()]
	private StringType uS_NUM_OF_EXTENSIONS = StringType.DEFAULT;
	[Generate()]
	private StringType uS_1ST_EXT_DTE = StringType.DEFAULT;
	[Generate()]
	private StringType uS_MOSTRECENT_EXT_DTE = StringType.DEFAULT;
	[Generate()]
	private StringType uS_L_RISKLEV = StringType.DEFAULT;
	[Generate()]
	private StringType uS_RISKLEVEL_DTE = StringType.DEFAULT;
	[Generate()]
	private StringType uS_WATCHLIST = StringType.DEFAULT;
	[Generate()]
	private StringType uS_DATE_WATCHLIST = StringType.DEFAULT;
	[Generate()]
	private StringType uS_WORKOUT_LOANS = StringType.DEFAULT;
	[Generate()]
	private StringType uS_WORKOUT_DTE = StringType.DEFAULT;
	[Generate()]
	private StringType uS_UD_BUS_LOAN_PER_REG = StringType.DEFAULT;
	[Generate()]
	private StringType uS_UD_BUS_LOAN_COL_GRP = StringType.DEFAULT;
	[Generate()]
	private StringType uS_UD_BUS_LOAN_COL_DESC = StringType.DEFAULT;
	[Generate()]
	private StringType uS_UD_BUS_LOAN_PART = StringType.DEFAULT;
	[Generate()]
	private StringType uS_UD_SBA_GUARANTY = StringType.DEFAULT;

	[Generate()]
	internal ACCOUNT_UD_US() {}

	[Generate()]
	internal ACCOUNT_UD_US(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static ACCOUNT_UD_US NewInstance() {
	    return new ACCOUNT_UD_US();
	}

	[Generate()]
	public static ACCOUNT_UD_US GetInstance(IdType aCCT_NUM) {
	    return ACCOUNT_UD_USDAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	public void Update(ACCOUNT_UD_USData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    uS_INCOME_STATED = data.US_INCOME_STATED.IsDefault ? uS_INCOME_STATED : data.US_INCOME_STATED;
	    uS_NUM_OF_EXTENSIONS = data.US_NUM_OF_EXTENSIONS.IsDefault ? uS_NUM_OF_EXTENSIONS : data.US_NUM_OF_EXTENSIONS;
	    uS_1ST_EXT_DTE = data.US_1ST_EXT_DTE.IsDefault ? uS_1ST_EXT_DTE : data.US_1ST_EXT_DTE;
	    uS_MOSTRECENT_EXT_DTE = data.US_MOSTRECENT_EXT_DTE.IsDefault ? uS_MOSTRECENT_EXT_DTE : data.US_MOSTRECENT_EXT_DTE;
	    uS_L_RISKLEV = data.US_L_RISKLEV.IsDefault ? uS_L_RISKLEV : data.US_L_RISKLEV;
	    uS_RISKLEVEL_DTE = data.US_RISKLEVEL_DTE.IsDefault ? uS_RISKLEVEL_DTE : data.US_RISKLEVEL_DTE;
	    uS_WATCHLIST = data.US_WATCHLIST.IsDefault ? uS_WATCHLIST : data.US_WATCHLIST;
	    uS_DATE_WATCHLIST = data.US_DATE_WATCHLIST.IsDefault ? uS_DATE_WATCHLIST : data.US_DATE_WATCHLIST;
	    uS_WORKOUT_LOANS = data.US_WORKOUT_LOANS.IsDefault ? uS_WORKOUT_LOANS : data.US_WORKOUT_LOANS;
	    uS_WORKOUT_DTE = data.US_WORKOUT_DTE.IsDefault ? uS_WORKOUT_DTE : data.US_WORKOUT_DTE;
	    uS_UD_BUS_LOAN_PER_REG = data.US_UD_BUS_LOAN_PER_REG.IsDefault ? uS_UD_BUS_LOAN_PER_REG : data.US_UD_BUS_LOAN_PER_REG;
	    uS_UD_BUS_LOAN_COL_GRP = data.US_UD_BUS_LOAN_COL_GRP.IsDefault ? uS_UD_BUS_LOAN_COL_GRP : data.US_UD_BUS_LOAN_COL_GRP;
	    uS_UD_BUS_LOAN_COL_DESC = data.US_UD_BUS_LOAN_COL_DESC.IsDefault ? uS_UD_BUS_LOAN_COL_DESC : data.US_UD_BUS_LOAN_COL_DESC;
	    uS_UD_BUS_LOAN_PART = data.US_UD_BUS_LOAN_PART.IsDefault ? uS_UD_BUS_LOAN_PART : data.US_UD_BUS_LOAN_PART;
	    uS_UD_SBA_GUARANTY = data.US_UD_SBA_GUARANTY.IsDefault ? uS_UD_SBA_GUARANTY : data.US_UD_SBA_GUARANTY;
	    uS_UD_1ST_MORT_TYPE = data.US_UD_1ST_MORT_TYPE.IsDefault ? uS_UD_1ST_MORT_TYPE : data.US_UD_1ST_MORT_TYPE;
	    uS_UD_BUS_LOAN_PRCNT_PART = data.US_UD_BUS_LOAN_PRCNT_PART.IsDefault ? uS_UD_BUS_LOAN_PRCNT_PART : data.US_UD_BUS_LOAN_PRCNT_PART;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = ACCOUNT_UD_USDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		ACCOUNT_UD_USDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public StringType US_INCOME_STATED {
	    get { return this.uS_INCOME_STATED; }
	    set { this.uS_INCOME_STATED = value; }
	}

	[Generate()]
	public StringType US_NUM_OF_EXTENSIONS {
	    get { return this.uS_NUM_OF_EXTENSIONS; }
	    set { this.uS_NUM_OF_EXTENSIONS = value; }
	}

	[Generate()]
	public StringType US_1ST_EXT_DTE {
	    get { return this.uS_1ST_EXT_DTE; }
	    set { this.uS_1ST_EXT_DTE = value; }
	}

	[Generate()]
	public StringType US_MOSTRECENT_EXT_DTE {
	    get { return this.uS_MOSTRECENT_EXT_DTE; }
	    set { this.uS_MOSTRECENT_EXT_DTE = value; }
	}

	[Generate()]
	public StringType US_L_RISKLEV {
	    get { return this.uS_L_RISKLEV; }
	    set { this.uS_L_RISKLEV = value; }
	}

	[Generate()]
	public StringType US_RISKLEVEL_DTE {
	    get { return this.uS_RISKLEVEL_DTE; }
	    set { this.uS_RISKLEVEL_DTE = value; }
	}

	[Generate()]
	public StringType US_WATCHLIST {
	    get { return this.uS_WATCHLIST; }
	    set { this.uS_WATCHLIST = value; }
	}

	[Generate()]
	public StringType US_DATE_WATCHLIST {
	    get { return this.uS_DATE_WATCHLIST; }
	    set { this.uS_DATE_WATCHLIST = value; }
	}

	[Generate()]
	public StringType US_WORKOUT_LOANS {
	    get { return this.uS_WORKOUT_LOANS; }
	    set { this.uS_WORKOUT_LOANS = value; }
	}

	[Generate()]
	public StringType US_WORKOUT_DTE {
	    get { return this.uS_WORKOUT_DTE; }
	    set { this.uS_WORKOUT_DTE = value; }
	}

	[Generate()]
	public StringType US_UD_BUS_LOAN_PER_REG {
	    get { return this.uS_UD_BUS_LOAN_PER_REG; }
	    set { this.uS_UD_BUS_LOAN_PER_REG = value; }
	}

	[Generate()]
	public StringType US_UD_BUS_LOAN_COL_GRP {
	    get { return this.uS_UD_BUS_LOAN_COL_GRP; }
	    set { this.uS_UD_BUS_LOAN_COL_GRP = value; }
	}

	[Generate()]
	public StringType US_UD_BUS_LOAN_COL_DESC {
	    get { return this.uS_UD_BUS_LOAN_COL_DESC; }
	    set { this.uS_UD_BUS_LOAN_COL_DESC = value; }
	}

	[Generate()]
	public StringType US_UD_BUS_LOAN_PART {
	    get { return this.uS_UD_BUS_LOAN_PART; }
	    set { this.uS_UD_BUS_LOAN_PART = value; }
	}

	[Generate()]
	public StringType US_UD_SBA_GUARANTY {
	    get { return this.uS_UD_SBA_GUARANTY; }
	    set { this.uS_UD_SBA_GUARANTY = value; }
	}


	[Generate()]
	public void Reload() {
	    ACCOUNT_UD_USDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}

	[Generate()]
	private StringType uS_UD_1ST_MORT_TYPE = StringType.DEFAULT;
	[Generate()]
	private StringType uS_UD_BUS_LOAN_PRCNT_PART = StringType.DEFAULT;

	[Generate()]
	public StringType US_UD_1ST_MORT_TYPE {
	    get { return this.uS_UD_1ST_MORT_TYPE; }
	    set { this.uS_UD_1ST_MORT_TYPE = value; }
	}

	[Generate()]
	public StringType US_UD_BUS_LOAN_PRCNT_PART {
	    get { return this.uS_UD_BUS_LOAN_PRCNT_PART; }
	    set { this.uS_UD_BUS_LOAN_PRCNT_PART = value; }
	}
    }
}
