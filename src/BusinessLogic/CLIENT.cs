using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class CLIENT : BusinessEntity, ICLIENT {
	[Generate()]
	private IdType mEM_NUM = IdType.DEFAULT;
	[Generate()]
	private IntegerType cLIENT_CLASS = IntegerType.DEFAULT;
	[Generate()]
	private DateTimeType jOIN_DATE = DateTimeType.DEFAULT;
	[Generate()]
	private StringType sEX = StringType.DEFAULT;

	[Generate()]
	internal CLIENT() {}

	[Generate()]
	internal CLIENT(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static CLIENT NewInstance() {
	    return new CLIENT();
	}

	[Generate()]
	public static CLIENT GetInstance(IdType mEM_NUM) {
	    return CLIENTDAO.DAO.Load(mEM_NUM);
	}

	[Generate()]
	public void Update(CLIENTData data) {
	    mEM_NUM = data.MEM_NUM.IsDefault ? mEM_NUM : data.MEM_NUM;
	    cLIENT_CLASS = data.CLIENT_CLASS.IsDefault ? cLIENT_CLASS : data.CLIENT_CLASS;
	    jOIN_DATE = data.JOIN_DATE.IsDefault ? jOIN_DATE : data.JOIN_DATE;
	    sEX = data.SEX.IsDefault ? sEX : data.SEX;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.MEM_NUM = CLIENTDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		CLIENTDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType MEM_NUM {
	    get { return this.mEM_NUM; }
	    set { this.mEM_NUM = value; }
	}

	[Generate()]
	public IntegerType CLIENT_CLASS {
	    get { return this.cLIENT_CLASS; }
	    set { this.cLIENT_CLASS = value; }
	}

	[Generate()]
	public DateTimeType JOIN_DATE {
	    get { return this.jOIN_DATE; }
	    set { this.jOIN_DATE = value; }
	}

	[Generate()]
	public StringType SEX {
	    get { return this.sEX; }
	    set { this.sEX = value; }
	}


	[Generate()]
	public void Reload() {
	    CLIENTDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + MEM_NUM.ToString();
	}
    }
}
