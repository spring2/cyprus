using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class RiskType : BusinessEntity, IRiskType {
	[Generate()]
	private IdType riskTypeId = IdType.DEFAULT;
	[Generate()]
	private IntegerType lType = IntegerType.DEFAULT;
	[Generate()]
	private StringType riskTypeDesc = StringType.DEFAULT;

	[Generate()]
	internal RiskType() {}

	[Generate()]
	internal RiskType(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static RiskType NewInstance() {
	    return new RiskType();
	}

	[Generate()]
	public static RiskType GetInstance(IdType riskTypeId) {
	    return RiskTypeDAO.DAO.Load(riskTypeId);
	}

	[Generate()]
	public void Update(RiskTypeData data) {
	    riskTypeId = data.RiskTypeId.IsDefault ? riskTypeId : data.RiskTypeId;
	    lType = data.LType.IsDefault ? lType : data.LType;
	    riskTypeDesc = data.RiskTypeDesc.IsDefault ? riskTypeDesc : data.RiskTypeDesc;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.RiskTypeId = RiskTypeDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		RiskTypeDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType RiskTypeId {
	    get { return this.riskTypeId; }
	    set { this.riskTypeId = value; }
	}

	[Generate()]
	public IntegerType LType {
	    get { return this.lType; }
	    set { this.lType = value; }
	}

	[Generate()]
	public StringType RiskTypeDesc {
	    get { return this.riskTypeDesc; }
	    set { this.riskTypeDesc = value; }
	}


	[Generate()]
	public void Reload() {
	    RiskTypeDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + RiskTypeId.ToString();
	}
    }
}
