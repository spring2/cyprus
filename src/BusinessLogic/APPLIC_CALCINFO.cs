using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class APPLIC_CALCINFO : BusinessEntity, IAPPLIC_CALCINFO {
	[Generate()]
	private IdType aPPLIC_ID = IdType.DEFAULT;
	[Generate()]
	private DecimalType dEBT_AFTER = DecimalType.DEFAULT;

	[Generate()]
	internal APPLIC_CALCINFO() {}

	[Generate()]
	internal APPLIC_CALCINFO(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static APPLIC_CALCINFO NewInstance() {
	    return new APPLIC_CALCINFO();
	}

	[Generate()]
	public static APPLIC_CALCINFO GetInstance(IdType aPPLIC_ID) {
	    return APPLIC_CALCINFODAO.DAO.Load(aPPLIC_ID);
	}

	[Generate()]
	public void Update(APPLIC_CALCINFOData data) {
	    aPPLIC_ID = data.APPLIC_ID.IsDefault ? aPPLIC_ID : data.APPLIC_ID;
	    dEBT_AFTER = data.DEBT_AFTER.IsDefault ? dEBT_AFTER : data.DEBT_AFTER;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.APPLIC_ID = APPLIC_CALCINFODAO.DAO.Insert(this);
		isNew = false;
	    } else {
		APPLIC_CALCINFODAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType APPLIC_ID {
	    get { return this.aPPLIC_ID; }
	    set { this.aPPLIC_ID = value; }
	}

	[Generate()]
	public DecimalType DEBT_AFTER {
	    get { return this.dEBT_AFTER; }
	    set { this.dEBT_AFTER = value; }
	}


	[Generate()]
	public void Reload() {
	    APPLIC_CALCINFODAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + APPLIC_ID.ToString();
	}
    }
}
