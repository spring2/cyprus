using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class FieldMap : BusinessEntity, IFieldMap {
	[Generate()]
	private IdType fieldMapId = IdType.DEFAULT;
	[Generate()]
	private StringType srcTable = StringType.DEFAULT;
	[Generate()]
	private StringType srcColumn = StringType.DEFAULT;
	[Generate()]
	private StringType destTable = StringType.DEFAULT;
	[Generate()]
	private StringType destColumn = StringType.DEFAULT;

	[Generate()]
	internal FieldMap() {}

	[Generate()]
	internal FieldMap(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static FieldMap NewInstance() {
	    return new FieldMap();
	}

	[Generate()]
	public static FieldMap GetInstance(IdType fieldMapId) {
	    return FieldMapDAO.DAO.Load(fieldMapId);
	}

	[Generate()]
	public void Update(FieldMapData data) {
	    fieldMapId = data.FieldMapId.IsDefault ? fieldMapId : data.FieldMapId;
	    srcTable = data.SrcTable.IsDefault ? srcTable : data.SrcTable;
	    srcColumn = data.SrcColumn.IsDefault ? srcColumn : data.SrcColumn;
	    destTable = data.DestTable.IsDefault ? destTable : data.DestTable;
	    destColumn = data.DestColumn.IsDefault ? destColumn : data.DestColumn;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.FieldMapId = FieldMapDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		FieldMapDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType FieldMapId {
	    get { return this.fieldMapId; }
	    set { this.fieldMapId = value; }
	}

	[Generate()]
	public StringType SrcTable {
	    get { return this.srcTable; }
	    set { this.srcTable = value; }
	}

	[Generate()]
	public StringType SrcColumn {
	    get { return this.srcColumn; }
	    set { this.srcColumn = value; }
	}

	[Generate()]
	public StringType DestTable {
	    get { return this.destTable; }
	    set { this.destTable = value; }
	}

	[Generate()]
	public StringType DestColumn {
	    get { return this.destColumn; }
	    set { this.destColumn = value; }
	}


	[Generate()]
	public void Reload() {
	    FieldMapDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + FieldMapId.ToString();
	}
    }
}
