using System;

using Spring2.Core.BusinessEntity;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.BusinessLogic {
    public class TRAN_LOAN_STAT : BusinessEntity, ITRAN_LOAN_STAT {
	[Generate()]
	private IdType aCCT_NUM = IdType.DEFAULT;
	[Generate()]
	private DateTimeType l_STAT_CHANGE_ACTUAL_DATE = DateTimeType.DEFAULT;

	[Generate()]
	internal TRAN_LOAN_STAT() {}

	[Generate()]
	internal TRAN_LOAN_STAT(Boolean isNew) {
	    this.isNew = isNew;
	}

	[Generate()]
	public static TRAN_LOAN_STAT NewInstance() {
	    return new TRAN_LOAN_STAT();
	}

	[Generate()]
	public static TRAN_LOAN_STAT GetInstance(IdType aCCT_NUM) {
	    return TRAN_LOAN_STATDAO.DAO.Load(aCCT_NUM);
	}

	[Generate()]
	public void Update(TRAN_LOAN_STATData data) {
	    aCCT_NUM = data.ACCT_NUM.IsDefault ? aCCT_NUM : data.ACCT_NUM;
	    l_STAT_CHANGE_ACTUAL_DATE = data.L_STAT_CHANGE_ACTUAL_DATE.IsDefault ? l_STAT_CHANGE_ACTUAL_DATE : data.L_STAT_CHANGE_ACTUAL_DATE;
	    Store();
	}

	[Generate()]
	public void Store() {
	    MessageList errors = Validate();

	    if (errors.Count > 0) {
		if (!isNew) {
		    this.Reload();
		}
		throw new MessageListException(errors);
	    }

	    if (isNew) {
		this.ACCT_NUM = TRAN_LOAN_STATDAO.DAO.Insert(this);
		isNew = false;
	    } else {
		TRAN_LOAN_STATDAO.DAO.Update(this);
	    }
	}

	[Generate()]
	public virtual MessageList Validate() {
	    MessageList errors = new MessageList();
	    return errors;
	}

	[Generate()]
	public IdType ACCT_NUM {
	    get { return this.aCCT_NUM; }
	    set { this.aCCT_NUM = value; }
	}

	[Generate()]
	public DateTimeType L_STAT_CHANGE_ACTUAL_DATE {
	    get { return this.l_STAT_CHANGE_ACTUAL_DATE; }
	    set { this.l_STAT_CHANGE_ACTUAL_DATE = value; }
	}


	[Generate()]
	public void Reload() {
	    TRAN_LOAN_STATDAO.DAO.Reload(this);
	}

	[Generate()]
	public override String ToString() {
	    return GetType().ToString() + "@" + ACCT_NUM.ToString();
	}
    }
}
