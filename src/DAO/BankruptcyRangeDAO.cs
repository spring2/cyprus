using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for BankruptcyRange business entity.
    /// </summary>
    public class BankruptcyRangeDAO : Spring2.Core.DAO.SqlEntityDAO, IBankruptcyRangeDAO {
	private static IBankruptcyRangeDAO instance = new BankruptcyRangeDAO();
	public static IBankruptcyRangeDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IBankruptcyRangeDAO))) {
		    ClassRegistry.Register<IBankruptcyRangeDAO>(instance);
		}
		return ClassRegistry.Resolve<IBankruptcyRangeDAO>();
	    }
	}

	private static readonly String VIEW = "vwBankruptcyRange";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 BankruptcyRangeId;
	    public Int32 Value;
	    public Int32 Description;

	    internal ColumnOrdinals(IDataReader reader) {
		BankruptcyRangeId = reader.GetOrdinal("BankruptcyRangeId");
		Value = reader.GetOrdinal("Value");
		Description = reader.GetOrdinal("Description");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		BankruptcyRangeId = reader.GetOrdinal(prefix + "BankruptcyRangeId");
		Value = reader.GetOrdinal(prefix + "Value");
		Description = reader.GetOrdinal(prefix + "Description");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static BankruptcyRangeDAO() {
	    AddPropertyMapping("BankruptcyRangeId", @"BankruptcyRangeId");
	    AddPropertyMapping("Value", @"Value");
	    AddPropertyMapping("Description", @"Description");
	}

	private BankruptcyRangeDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all BankruptcyRange rows.
	/// </summary>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public BankruptcyRangeList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public BankruptcyRangeList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of BankruptcyRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public BankruptcyRangeList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public BankruptcyRangeList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    BankruptcyRangeList list = new BankruptcyRangeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all BankruptcyRange rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public BankruptcyRangeList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public BankruptcyRangeList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of BankruptcyRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public BankruptcyRangeList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public BankruptcyRangeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    BankruptcyRangeList list = new BankruptcyRangeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a BankruptcyRange entity using it's primary key.
	/// </summary>
	/// <param name="bankruptcyRangeId">A key field.</param>
	/// <returns>A BankruptcyRange object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public BankruptcyRange Load(IdType bankruptcyRangeId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(BankruptcyRangeFields.BANKRUPTCYRANGEID, EqualityOperatorEnum.Equal, bankruptcyRangeId.IsValid ? bankruptcyRangeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(BankruptcyRange instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(BankruptcyRangeFields.BANKRUPTCYRANGEID, EqualityOperatorEnum.Equal, instance.BankruptcyRangeId.IsValid ? instance.BankruptcyRangeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for BankruptcyRange.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private BankruptcyRangeList GetList(IDataReader reader) {
	    BankruptcyRangeList list = new BankruptcyRangeList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private BankruptcyRange GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private BankruptcyRange GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    BankruptcyRange data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public BankruptcyRange GetDataObjectFromReader(BankruptcyRange data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public BankruptcyRange GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    BankruptcyRange data = new BankruptcyRange(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public BankruptcyRange GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    BankruptcyRange data = new BankruptcyRange(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public BankruptcyRange GetDataObjectFromReader(BankruptcyRange data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.BankruptcyRangeId)) {
		data.BankruptcyRangeId = IdType.UNSET;
	    } else {
		data.BankruptcyRangeId = new IdType(dataReader.GetInt32(ordinals.BankruptcyRangeId));
	    }
	    if (dataReader.IsDBNull(ordinals.Value)) {
		data.Value = IntegerType.UNSET;
	    } else {
		data.Value = new IntegerType(dataReader.GetInt32(ordinals.Value));
	    }
	    if (dataReader.IsDBNull(ordinals.Description)) {
		data.Description = StringType.UNSET;
	    } else {
		data.Description = StringType.Parse(dataReader.GetString(ordinals.Description));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the BankruptcyRange table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(BankruptcyRange data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the BankruptcyRange table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(BankruptcyRange data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spBankruptcyRange_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(BankruptcyRangeFields.VALUE, data.Value.IsValid ? data.Value.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(BankruptcyRangeFields.DESCRIPTION, data.Description.IsValid ? data.Description.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the BankruptcyRange table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(BankruptcyRange data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the BankruptcyRange table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(BankruptcyRange data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spBankruptcyRange_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(BankruptcyRangeFields.BANKRUPTCYRANGEID, data.BankruptcyRangeId.IsValid ? data.BankruptcyRangeId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(BankruptcyRangeFields.VALUE, data.Value.IsValid ? data.Value.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(BankruptcyRangeFields.DESCRIPTION, data.Description.IsValid ? data.Description.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the BankruptcyRange table by BankruptcyRangeId.
	/// </summary>
	/// <param name="bankruptcyRangeId">A key field.</param>
	public void Delete(IdType bankruptcyRangeId) {
	    Delete(bankruptcyRangeId, null);
	}

	/// <summary>
	/// Deletes a record from the BankruptcyRange table by BankruptcyRangeId.
	/// </summary>
	/// <param name="bankruptcyRangeId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType bankruptcyRangeId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spBankruptcyRange_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(BankruptcyRangeFields.BANKRUPTCYRANGEID, bankruptcyRangeId.IsValid ? bankruptcyRangeId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
