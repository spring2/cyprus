using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for CLIENT business entity.
    /// </summary>
    public interface ICLIENTDAO {
	/// <summary>
	/// Returns a list of all CLIENT rows.
	/// </summary>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	CLIENTList GetList();

	/// <summary>
	/// Returns a filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	CLIENTList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of CLIENT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	CLIENTList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	CLIENTList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all CLIENT rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	CLIENTList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	CLIENTList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of CLIENT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	CLIENTList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	CLIENTList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a CLIENT entity using it's primary key.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	/// <returns>A CLIENT object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	CLIENT Load(IdType mEM_NUM);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(CLIENT instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	CLIENT GetDataObjectFromReader(CLIENT data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	CLIENT GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	CLIENT GetDataObjectFromReader(IDataReader dataReader, CLIENTDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	CLIENT GetDataObjectFromReader(CLIENT data, IDataReader dataReader, CLIENTDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the CLIENT table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(CLIENT data);


	/// <summary>
	/// Updates a record in the CLIENT table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(CLIENT data);

	/// <summary>
	/// Deletes a record from the CLIENT table by MEM_NUM.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	[Generate]
	void Delete(IdType mEM_NUM);


	//}
    }
}
