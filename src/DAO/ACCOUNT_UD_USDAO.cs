using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for ACCOUNT_UD_US business entity.
    /// </summary>
    public class ACCOUNT_UD_USDAO : Spring2.Core.DAO.SqlEntityDAO, IACCOUNT_UD_USDAO {
	private static IACCOUNT_UD_USDAO instance = new ACCOUNT_UD_USDAO();
	public static IACCOUNT_UD_USDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IACCOUNT_UD_USDAO))) {
		    ClassRegistry.Register<IACCOUNT_UD_USDAO>(instance);
		}
		return ClassRegistry.Resolve<IACCOUNT_UD_USDAO>();
	    }
	}

	private static readonly String VIEW = "vwACCOUNT_UD_US";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 ACCT_NUM;
	    public Int32 US_INCOME_STATED;
	    public Int32 US_NUM_OF_EXTENSIONS;
	    public Int32 US_1ST_EXT_DTE;
	    public Int32 US_MOSTRECENT_EXT_DTE;
	    public Int32 US_L_RISKLEV;
	    public Int32 US_RISKLEVEL_DTE;
	    public Int32 US_WATCHLIST;
	    public Int32 US_DATE_WATCHLIST;
	    public Int32 US_WORKOUT_LOANS;
	    public Int32 US_WORKOUT_DTE;
	    public Int32 US_UD_BUS_LOAN_PER_REG;
	    public Int32 US_UD_BUS_LOAN_COL_GRP;
	    public Int32 US_UD_BUS_LOAN_COL_DESC;
	    public Int32 US_UD_BUS_LOAN_PART;
	    public Int32 US_UD_SBA_GUARANTY;
	    public Int32 US_UD_1ST_MORT_TYPE;
	    public Int32 US_UD_BUS_LOAN_PRCNT_PART;

	    internal ColumnOrdinals(IDataReader reader) {
		ACCT_NUM = reader.GetOrdinal("ACCT_NUM");
		US_INCOME_STATED = reader.GetOrdinal("US_INCOME_STATED");
		US_NUM_OF_EXTENSIONS = reader.GetOrdinal("US_NUM_OF_EXTENSIONS");
		US_1ST_EXT_DTE = reader.GetOrdinal("US_1ST_EXT_DTE");
		US_MOSTRECENT_EXT_DTE = reader.GetOrdinal("US_MOSTRECENT_EXT_DTE");
		US_L_RISKLEV = reader.GetOrdinal("US_L_RISKLEV");
		US_RISKLEVEL_DTE = reader.GetOrdinal("US_RISKLEVEL_DTE");
		US_WATCHLIST = reader.GetOrdinal("US_WATCHLIST");
		US_DATE_WATCHLIST = reader.GetOrdinal("US_DATE_WATCHLIST");
		US_WORKOUT_LOANS = reader.GetOrdinal("US_WORKOUT_LOANS");
		US_WORKOUT_DTE = reader.GetOrdinal("US_WORKOUT_DTE");
		US_UD_BUS_LOAN_PER_REG = reader.GetOrdinal("US_UD_BUS_LOAN_PER_REG");
		US_UD_BUS_LOAN_COL_GRP = reader.GetOrdinal("US_UD_BUS_LOAN_COL_GRP");
		US_UD_BUS_LOAN_COL_DESC = reader.GetOrdinal("US_UD_BUS_LOAN_COL_DESC");
		US_UD_BUS_LOAN_PART = reader.GetOrdinal("US_UD_BUS_LOAN_PART");
		US_UD_SBA_GUARANTY = reader.GetOrdinal("US_UD_SBA_GUARANTY");
		US_UD_1ST_MORT_TYPE = reader.GetOrdinal("US_UD_1ST_MORT_TYPE");
		US_UD_BUS_LOAN_PRCNT_PART = reader.GetOrdinal("US_UD_BUS_LOAN_PRCNT_PART");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		ACCT_NUM = reader.GetOrdinal(prefix + "ACCT_NUM");
		US_INCOME_STATED = reader.GetOrdinal(prefix + "US_INCOME_STATED");
		US_NUM_OF_EXTENSIONS = reader.GetOrdinal(prefix + "US_NUM_OF_EXTENSIONS");
		US_1ST_EXT_DTE = reader.GetOrdinal(prefix + "US_1ST_EXT_DTE");
		US_MOSTRECENT_EXT_DTE = reader.GetOrdinal(prefix + "US_MOSTRECENT_EXT_DTE");
		US_L_RISKLEV = reader.GetOrdinal(prefix + "US_L_RISKLEV");
		US_RISKLEVEL_DTE = reader.GetOrdinal(prefix + "US_RISKLEVEL_DTE");
		US_WATCHLIST = reader.GetOrdinal(prefix + "US_WATCHLIST");
		US_DATE_WATCHLIST = reader.GetOrdinal(prefix + "US_DATE_WATCHLIST");
		US_WORKOUT_LOANS = reader.GetOrdinal(prefix + "US_WORKOUT_LOANS");
		US_WORKOUT_DTE = reader.GetOrdinal(prefix + "US_WORKOUT_DTE");
		US_UD_BUS_LOAN_PER_REG = reader.GetOrdinal(prefix + "US_UD_BUS_LOAN_PER_REG");
		US_UD_BUS_LOAN_COL_GRP = reader.GetOrdinal(prefix + "US_UD_BUS_LOAN_COL_GRP");
		US_UD_BUS_LOAN_COL_DESC = reader.GetOrdinal(prefix + "US_UD_BUS_LOAN_COL_DESC");
		US_UD_BUS_LOAN_PART = reader.GetOrdinal(prefix + "US_UD_BUS_LOAN_PART");
		US_UD_SBA_GUARANTY = reader.GetOrdinal(prefix + "US_UD_SBA_GUARANTY");
		US_UD_1ST_MORT_TYPE = reader.GetOrdinal(prefix + "US_UD_1ST_MORT_TYPE");
		US_UD_BUS_LOAN_PRCNT_PART = reader.GetOrdinal(prefix + "US_UD_BUS_LOAN_PRCNT_PART");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static ACCOUNT_UD_USDAO() {
	    AddPropertyMapping("ACCT_NUM", @"ACCT_NUM");
	    AddPropertyMapping("US_INCOME_STATED", @"US_INCOME_STATED");
	    AddPropertyMapping("US_NUM_OF_EXTENSIONS", @"US_NUM_OF_EXTENSIONS");
	    AddPropertyMapping("US_1ST_EXT_DTE", @"US_1ST_EXT_DTE");
	    AddPropertyMapping("US_MOSTRECENT_EXT_DTE", @"US_MOSTRECENT_EXT_DTE");
	    AddPropertyMapping("US_L_RISKLEV", @"US_L_RISKLEV");
	    AddPropertyMapping("US_RISKLEVEL_DTE", @"US_RISKLEVEL_DTE");
	    AddPropertyMapping("US_WATCHLIST", @"US_WATCHLIST");
	    AddPropertyMapping("US_DATE_WATCHLIST", @"US_DATE_WATCHLIST");
	    AddPropertyMapping("US_WORKOUT_LOANS", @"US_WORKOUT_LOANS");
	    AddPropertyMapping("US_WORKOUT_DTE", @"US_WORKOUT_DTE");
	    AddPropertyMapping("US_UD_BUS_LOAN_PER_REG", @"US_UD_BUS_LOAN_PER_REG");
	    AddPropertyMapping("US_UD_BUS_LOAN_COL_GRP", @"US_UD_BUS_LOAN_COL_GRP");
	    AddPropertyMapping("US_UD_BUS_LOAN_COL_DESC", @"US_UD_BUS_LOAN_COL_DESC");
	    AddPropertyMapping("US_UD_BUS_LOAN_PART", @"US_UD_BUS_LOAN_PART");
	    AddPropertyMapping("US_UD_SBA_GUARANTY", @"US_UD_SBA_GUARANTY");
	    AddPropertyMapping("US_UD_1ST_MORT_TYPE", @"US_UD_1ST_MORT_TYPE");
	    AddPropertyMapping("US_UD_BUS_LOAN_PRCNT_PART", @"US_UD_BUS_LOAN_PRCNT_PART");
	}

	private ACCOUNT_UD_USDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all ACCOUNT_UD_US rows.
	/// </summary>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_UD_USList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_UD_USList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of ACCOUNT_UD_US rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_UD_USList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_UD_USList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    ACCOUNT_UD_USList list = new ACCOUNT_UD_USList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_UD_USList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_UD_USList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of ACCOUNT_UD_US rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_UD_USList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_UD_USList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    ACCOUNT_UD_USList list = new ACCOUNT_UD_USList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a ACCOUNT_UD_US entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A ACCOUNT_UD_US object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public ACCOUNT_UD_US Load(IdType aCCT_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ACCOUNT_UD_USFields.ACCT_NUM, EqualityOperatorEnum.Equal, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(ACCOUNT_UD_US instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ACCOUNT_UD_USFields.ACCT_NUM, EqualityOperatorEnum.Equal, instance.ACCT_NUM.IsValid ? instance.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for ACCOUNT_UD_US.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private ACCOUNT_UD_USList GetList(IDataReader reader) {
	    ACCOUNT_UD_USList list = new ACCOUNT_UD_USList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private ACCOUNT_UD_US GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private ACCOUNT_UD_US GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    ACCOUNT_UD_US data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_UD_US GetDataObjectFromReader(ACCOUNT_UD_US data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_UD_US GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    ACCOUNT_UD_US data = new ACCOUNT_UD_US(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_UD_US GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    ACCOUNT_UD_US data = new ACCOUNT_UD_US(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_UD_US GetDataObjectFromReader(ACCOUNT_UD_US data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.ACCT_NUM)) {
		data.ACCT_NUM = IdType.UNSET;
	    } else {
		data.ACCT_NUM = new IdType(dataReader.GetInt32(ordinals.ACCT_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.US_INCOME_STATED)) {
		data.US_INCOME_STATED = StringType.UNSET;
	    } else {
		data.US_INCOME_STATED = StringType.Parse(dataReader.GetString(ordinals.US_INCOME_STATED));
	    }
	    if (dataReader.IsDBNull(ordinals.US_NUM_OF_EXTENSIONS)) {
		data.US_NUM_OF_EXTENSIONS = StringType.UNSET;
	    } else {
		data.US_NUM_OF_EXTENSIONS = StringType.Parse(dataReader.GetString(ordinals.US_NUM_OF_EXTENSIONS));
	    }
	    if (dataReader.IsDBNull(ordinals.US_1ST_EXT_DTE)) {
		data.US_1ST_EXT_DTE = StringType.UNSET;
	    } else {
		data.US_1ST_EXT_DTE = StringType.Parse(dataReader.GetString(ordinals.US_1ST_EXT_DTE));
	    }
	    if (dataReader.IsDBNull(ordinals.US_MOSTRECENT_EXT_DTE)) {
		data.US_MOSTRECENT_EXT_DTE = StringType.UNSET;
	    } else {
		data.US_MOSTRECENT_EXT_DTE = StringType.Parse(dataReader.GetString(ordinals.US_MOSTRECENT_EXT_DTE));
	    }
	    if (dataReader.IsDBNull(ordinals.US_L_RISKLEV)) {
		data.US_L_RISKLEV = StringType.UNSET;
	    } else {
		data.US_L_RISKLEV = StringType.Parse(dataReader.GetString(ordinals.US_L_RISKLEV));
	    }
	    if (dataReader.IsDBNull(ordinals.US_RISKLEVEL_DTE)) {
		data.US_RISKLEVEL_DTE = StringType.UNSET;
	    } else {
		data.US_RISKLEVEL_DTE = StringType.Parse(dataReader.GetString(ordinals.US_RISKLEVEL_DTE));
	    }
	    if (dataReader.IsDBNull(ordinals.US_WATCHLIST)) {
		data.US_WATCHLIST = StringType.UNSET;
	    } else {
		data.US_WATCHLIST = StringType.Parse(dataReader.GetString(ordinals.US_WATCHLIST));
	    }
	    if (dataReader.IsDBNull(ordinals.US_DATE_WATCHLIST)) {
		data.US_DATE_WATCHLIST = StringType.UNSET;
	    } else {
		data.US_DATE_WATCHLIST = StringType.Parse(dataReader.GetString(ordinals.US_DATE_WATCHLIST));
	    }
	    if (dataReader.IsDBNull(ordinals.US_WORKOUT_LOANS)) {
		data.US_WORKOUT_LOANS = StringType.UNSET;
	    } else {
		data.US_WORKOUT_LOANS = StringType.Parse(dataReader.GetString(ordinals.US_WORKOUT_LOANS));
	    }
	    if (dataReader.IsDBNull(ordinals.US_WORKOUT_DTE)) {
		data.US_WORKOUT_DTE = StringType.UNSET;
	    } else {
		data.US_WORKOUT_DTE = StringType.Parse(dataReader.GetString(ordinals.US_WORKOUT_DTE));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_BUS_LOAN_PER_REG)) {
		data.US_UD_BUS_LOAN_PER_REG = StringType.UNSET;
	    } else {
		data.US_UD_BUS_LOAN_PER_REG = StringType.Parse(dataReader.GetString(ordinals.US_UD_BUS_LOAN_PER_REG));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_BUS_LOAN_COL_GRP)) {
		data.US_UD_BUS_LOAN_COL_GRP = StringType.UNSET;
	    } else {
		data.US_UD_BUS_LOAN_COL_GRP = StringType.Parse(dataReader.GetString(ordinals.US_UD_BUS_LOAN_COL_GRP));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_BUS_LOAN_COL_DESC)) {
		data.US_UD_BUS_LOAN_COL_DESC = StringType.UNSET;
	    } else {
		data.US_UD_BUS_LOAN_COL_DESC = StringType.Parse(dataReader.GetString(ordinals.US_UD_BUS_LOAN_COL_DESC));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_BUS_LOAN_PART)) {
		data.US_UD_BUS_LOAN_PART = StringType.UNSET;
	    } else {
		data.US_UD_BUS_LOAN_PART = StringType.Parse(dataReader.GetString(ordinals.US_UD_BUS_LOAN_PART));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_SBA_GUARANTY)) {
		data.US_UD_SBA_GUARANTY = StringType.UNSET;
	    } else {
		data.US_UD_SBA_GUARANTY = StringType.Parse(dataReader.GetString(ordinals.US_UD_SBA_GUARANTY));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_1ST_MORT_TYPE)) {
		data.US_UD_1ST_MORT_TYPE = StringType.UNSET;
	    } else {
		data.US_UD_1ST_MORT_TYPE = StringType.Parse(dataReader.GetString(ordinals.US_UD_1ST_MORT_TYPE));
	    }
	    if (dataReader.IsDBNull(ordinals.US_UD_BUS_LOAN_PRCNT_PART)) {
		data.US_UD_BUS_LOAN_PRCNT_PART = StringType.UNSET;
	    } else {
		data.US_UD_BUS_LOAN_PRCNT_PART = StringType.Parse(dataReader.GetString(ordinals.US_UD_BUS_LOAN_PRCNT_PART));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the ACCOUNT_UD_US table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(ACCOUNT_UD_US data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the ACCOUNT_UD_US table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(ACCOUNT_UD_US data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_UD_US_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_INCOME_STATED, data.US_INCOME_STATED.IsValid ? data.US_INCOME_STATED.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_NUM_OF_EXTENSIONS, data.US_NUM_OF_EXTENSIONS.IsValid ? data.US_NUM_OF_EXTENSIONS.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_1ST_EXT_DTE, data.US_1ST_EXT_DTE.IsValid ? data.US_1ST_EXT_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_MOSTRECENT_EXT_DTE, data.US_MOSTRECENT_EXT_DTE.IsValid ? data.US_MOSTRECENT_EXT_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_L_RISKLEV, data.US_L_RISKLEV.IsValid ? data.US_L_RISKLEV.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_RISKLEVEL_DTE, data.US_RISKLEVEL_DTE.IsValid ? data.US_RISKLEVEL_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_WATCHLIST, data.US_WATCHLIST.IsValid ? data.US_WATCHLIST.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_DATE_WATCHLIST, data.US_DATE_WATCHLIST.IsValid ? data.US_DATE_WATCHLIST.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_WORKOUT_LOANS, data.US_WORKOUT_LOANS.IsValid ? data.US_WORKOUT_LOANS.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_WORKOUT_DTE, data.US_WORKOUT_DTE.IsValid ? data.US_WORKOUT_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_PER_REG, data.US_UD_BUS_LOAN_PER_REG.IsValid ? data.US_UD_BUS_LOAN_PER_REG.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_COL_GRP, data.US_UD_BUS_LOAN_COL_GRP.IsValid ? data.US_UD_BUS_LOAN_COL_GRP.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_COL_DESC, data.US_UD_BUS_LOAN_COL_DESC.IsValid ? data.US_UD_BUS_LOAN_COL_DESC.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_PART, data.US_UD_BUS_LOAN_PART.IsValid ? data.US_UD_BUS_LOAN_PART.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_SBA_GUARANTY, data.US_UD_SBA_GUARANTY.IsValid ? data.US_UD_SBA_GUARANTY.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_1ST_MORT_TYPE, data.US_UD_1ST_MORT_TYPE.IsValid ? data.US_UD_1ST_MORT_TYPE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_PRCNT_PART, data.US_UD_BUS_LOAN_PRCNT_PART.IsValid ? data.US_UD_BUS_LOAN_PRCNT_PART.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the ACCOUNT_UD_US table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(ACCOUNT_UD_US data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the ACCOUNT_UD_US table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(ACCOUNT_UD_US data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_UD_US_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.ACCT_NUM, data.ACCT_NUM.IsValid ? data.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_INCOME_STATED, data.US_INCOME_STATED.IsValid ? data.US_INCOME_STATED.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_NUM_OF_EXTENSIONS, data.US_NUM_OF_EXTENSIONS.IsValid ? data.US_NUM_OF_EXTENSIONS.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_1ST_EXT_DTE, data.US_1ST_EXT_DTE.IsValid ? data.US_1ST_EXT_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_MOSTRECENT_EXT_DTE, data.US_MOSTRECENT_EXT_DTE.IsValid ? data.US_MOSTRECENT_EXT_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_L_RISKLEV, data.US_L_RISKLEV.IsValid ? data.US_L_RISKLEV.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_RISKLEVEL_DTE, data.US_RISKLEVEL_DTE.IsValid ? data.US_RISKLEVEL_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_WATCHLIST, data.US_WATCHLIST.IsValid ? data.US_WATCHLIST.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_DATE_WATCHLIST, data.US_DATE_WATCHLIST.IsValid ? data.US_DATE_WATCHLIST.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_WORKOUT_LOANS, data.US_WORKOUT_LOANS.IsValid ? data.US_WORKOUT_LOANS.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_WORKOUT_DTE, data.US_WORKOUT_DTE.IsValid ? data.US_WORKOUT_DTE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_PER_REG, data.US_UD_BUS_LOAN_PER_REG.IsValid ? data.US_UD_BUS_LOAN_PER_REG.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_COL_GRP, data.US_UD_BUS_LOAN_COL_GRP.IsValid ? data.US_UD_BUS_LOAN_COL_GRP.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_COL_DESC, data.US_UD_BUS_LOAN_COL_DESC.IsValid ? data.US_UD_BUS_LOAN_COL_DESC.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_PART, data.US_UD_BUS_LOAN_PART.IsValid ? data.US_UD_BUS_LOAN_PART.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_SBA_GUARANTY, data.US_UD_SBA_GUARANTY.IsValid ? data.US_UD_SBA_GUARANTY.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_1ST_MORT_TYPE, data.US_UD_1ST_MORT_TYPE.IsValid ? data.US_UD_1ST_MORT_TYPE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.US_UD_BUS_LOAN_PRCNT_PART, data.US_UD_BUS_LOAN_PRCNT_PART.IsValid ? data.US_UD_BUS_LOAN_PRCNT_PART.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the ACCOUNT_UD_US table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	public void Delete(IdType aCCT_NUM) {
	    Delete(aCCT_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the ACCOUNT_UD_US table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aCCT_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_UD_US_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_UD_USFields.ACCT_NUM, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
