using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for APPLIC_CALCINFO business entity.
    /// </summary>
    public interface IAPPLIC_CALCINFODAO {
	/// <summary>
	/// Returns a list of all APPLIC_CALCINFO rows.
	/// </summary>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList();

	/// <summary>
	/// Returns a filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of APPLIC_CALCINFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of APPLIC_CALCINFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_CALCINFOList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a APPLIC_CALCINFO entity using it's primary key.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	/// <returns>A APPLIC_CALCINFO object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	APPLIC_CALCINFO Load(IdType aPPLIC_ID);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(APPLIC_CALCINFO instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_CALCINFO GetDataObjectFromReader(APPLIC_CALCINFO data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_CALCINFO GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_CALCINFO GetDataObjectFromReader(IDataReader dataReader, APPLIC_CALCINFODAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_CALCINFO GetDataObjectFromReader(APPLIC_CALCINFO data, IDataReader dataReader, APPLIC_CALCINFODAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the APPLIC_CALCINFO table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(APPLIC_CALCINFO data);


	/// <summary>
	/// Updates a record in the APPLIC_CALCINFO table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(APPLIC_CALCINFO data);

	/// <summary>
	/// Deletes a record from the APPLIC_CALCINFO table by APPLIC_ID.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	[Generate]
	void Delete(IdType aPPLIC_ID);


	//}
    }
}
