using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for APPLIC business entity.
    /// </summary>
    public class APPLICDAO : Spring2.Core.DAO.SqlEntityDAO, IAPPLICDAO {
	private static IAPPLICDAO instance = new APPLICDAO();
	public static IAPPLICDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IAPPLICDAO))) {
		    ClassRegistry.Register<IAPPLICDAO>(instance);
		}
		return ClassRegistry.Resolve<IAPPLICDAO>();
	    }
	}

	private static readonly String VIEW = "vwAPPLIC";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 APPLIC_ID;
	    public Int32 APPLIC_NUM;
	    public Int32 FINAL_STAMP_BR;
	    public Int32 LOAD_STAMP_BR;
	    public Int32 STAGE2_CRSCORE;
	    public Int32 AP_RISK_SCORE;
	    public Int32 BANKRUPT_SCORE;
	    public Int32 AP_RISK_LEVEL;
	    public Int32 ALPS_COLOR;
	    public Int32 PAYMENT_FREQ;
	    public Int32 TOT_FIN;
	    public Int32 BALLOON_DATE;
	    public Int32 DEALER_NUMBER;
	    public Int32 CUDL_DEALER_NAME;
	    public Int32 AP_RP_DISCOUNT;
	    public Int32 CRSCORE_TYPE;
	    public Int32 HELOC_TYPE;
	    public Int32 HELOC_OCCUPANCY;
	    public Int32 HELOC_VALUE;
	    public Int32 HELOC_1STMORT_BAL;
	    public Int32 HELOC_1STMORT_PAY;
	    public Int32 HELOC_2NDMORT_BAL;
	    public Int32 HELOC_2NDMORT_PAY;
	    public Int32 HELOC_ORIG_DATE;
	    public Int32 INSURE_CODE;
	    public Int32 AP_INTEREST_RATE;

	    internal ColumnOrdinals(IDataReader reader) {
		APPLIC_ID = reader.GetOrdinal("APPLIC_ID");
		APPLIC_NUM = reader.GetOrdinal("APPLIC_NUM");
		FINAL_STAMP_BR = reader.GetOrdinal("FINAL_STAMP_BR");
		LOAD_STAMP_BR = reader.GetOrdinal("LOAD_STAMP_BR");
		STAGE2_CRSCORE = reader.GetOrdinal("STAGE2_CRSCORE");
		AP_RISK_SCORE = reader.GetOrdinal("AP_RISK_SCORE");
		BANKRUPT_SCORE = reader.GetOrdinal("BANKRUPT_SCORE");
		AP_RISK_LEVEL = reader.GetOrdinal("AP_RISK_LEVEL");
		ALPS_COLOR = reader.GetOrdinal("ALPS_COLOR");
		PAYMENT_FREQ = reader.GetOrdinal("PAYMENT_FREQ");
		TOT_FIN = reader.GetOrdinal("TOT_FIN");
		BALLOON_DATE = reader.GetOrdinal("BALLOON_DATE");
		DEALER_NUMBER = reader.GetOrdinal("DEALER_NUMBER");
		CUDL_DEALER_NAME = reader.GetOrdinal("CUDL_DEALER_NAME");
		AP_RP_DISCOUNT = reader.GetOrdinal("AP_RP_DISCOUNT");
		CRSCORE_TYPE = reader.GetOrdinal("CRSCORE_TYPE");
		HELOC_TYPE = reader.GetOrdinal("HELOC_TYPE");
		HELOC_OCCUPANCY = reader.GetOrdinal("HELOC_OCCUPANCY");
		HELOC_VALUE = reader.GetOrdinal("HELOC_VALUE");
		HELOC_1STMORT_BAL = reader.GetOrdinal("HELOC_1STMORT_BAL");
		HELOC_1STMORT_PAY = reader.GetOrdinal("HELOC_1STMORT_PAY");
		HELOC_2NDMORT_BAL = reader.GetOrdinal("HELOC_2NDMORT_BAL");
		HELOC_2NDMORT_PAY = reader.GetOrdinal("HELOC_2NDMORT_PAY");
		HELOC_ORIG_DATE = reader.GetOrdinal("HELOC_ORIG_DATE");
		INSURE_CODE = reader.GetOrdinal("INSURE_CODE");
		AP_INTEREST_RATE = reader.GetOrdinal("AP_INTEREST_RATE");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		APPLIC_ID = reader.GetOrdinal(prefix + "APPLIC_ID");
		APPLIC_NUM = reader.GetOrdinal(prefix + "APPLIC_NUM");
		FINAL_STAMP_BR = reader.GetOrdinal(prefix + "FINAL_STAMP_BR");
		LOAD_STAMP_BR = reader.GetOrdinal(prefix + "LOAD_STAMP_BR");
		STAGE2_CRSCORE = reader.GetOrdinal(prefix + "STAGE2_CRSCORE");
		AP_RISK_SCORE = reader.GetOrdinal(prefix + "AP_RISK_SCORE");
		BANKRUPT_SCORE = reader.GetOrdinal(prefix + "BANKRUPT_SCORE");
		AP_RISK_LEVEL = reader.GetOrdinal(prefix + "AP_RISK_LEVEL");
		ALPS_COLOR = reader.GetOrdinal(prefix + "ALPS_COLOR");
		PAYMENT_FREQ = reader.GetOrdinal(prefix + "PAYMENT_FREQ");
		TOT_FIN = reader.GetOrdinal(prefix + "TOT_FIN");
		BALLOON_DATE = reader.GetOrdinal(prefix + "BALLOON_DATE");
		DEALER_NUMBER = reader.GetOrdinal(prefix + "DEALER_NUMBER");
		CUDL_DEALER_NAME = reader.GetOrdinal(prefix + "CUDL_DEALER_NAME");
		AP_RP_DISCOUNT = reader.GetOrdinal(prefix + "AP_RP_DISCOUNT");
		CRSCORE_TYPE = reader.GetOrdinal(prefix + "CRSCORE_TYPE");
		HELOC_TYPE = reader.GetOrdinal(prefix + "HELOC_TYPE");
		HELOC_OCCUPANCY = reader.GetOrdinal(prefix + "HELOC_OCCUPANCY");
		HELOC_VALUE = reader.GetOrdinal(prefix + "HELOC_VALUE");
		HELOC_1STMORT_BAL = reader.GetOrdinal(prefix + "HELOC_1STMORT_BAL");
		HELOC_1STMORT_PAY = reader.GetOrdinal(prefix + "HELOC_1STMORT_PAY");
		HELOC_2NDMORT_BAL = reader.GetOrdinal(prefix + "HELOC_2NDMORT_BAL");
		HELOC_2NDMORT_PAY = reader.GetOrdinal(prefix + "HELOC_2NDMORT_PAY");
		HELOC_ORIG_DATE = reader.GetOrdinal(prefix + "HELOC_ORIG_DATE");
		INSURE_CODE = reader.GetOrdinal(prefix + "INSURE_CODE");
		AP_INTEREST_RATE = reader.GetOrdinal(prefix + "AP_INTEREST_RATE");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static APPLICDAO() {
	    AddPropertyMapping("APPLIC_ID", @"APPLIC_ID");
	    AddPropertyMapping("APPLIC_NUM", @"APPLIC_NUM");
	    AddPropertyMapping("FINAL_STAMP_BR", @"FINAL_STAMP_BR");
	    AddPropertyMapping("LOAD_STAMP_BR", @"LOAD_STAMP_BR");
	    AddPropertyMapping("STAGE2_CRSCORE", @"STAGE2_CRSCORE");
	    AddPropertyMapping("AP_RISK_SCORE", @"AP_RISK_SCORE");
	    AddPropertyMapping("BANKRUPT_SCORE", @"BANKRUPT_SCORE");
	    AddPropertyMapping("AP_RISK_LEVEL", @"AP_RISK_LEVEL");
	    AddPropertyMapping("ALPS_COLOR", @"ALPS_COLOR");
	    AddPropertyMapping("PAYMENT_FREQ", @"PAYMENT_FREQ");
	    AddPropertyMapping("TOT_FIN", @"TOT_FIN");
	    AddPropertyMapping("BALLOON_DATE", @"BALLOON_DATE");
	    AddPropertyMapping("DEALER_NUMBER", @"DEALER_NUMBER");
	    AddPropertyMapping("CUDL_DEALER_NAME", @"CUDL_DEALER_NAME");
	    AddPropertyMapping("AP_RP_DISCOUNT", @"AP_RP_DISCOUNT");
	    AddPropertyMapping("CRSCORE_TYPE", @"CRSCORE_TYPE");
	    AddPropertyMapping("HELOC_TYPE", @"HELOC_TYPE");
	    AddPropertyMapping("HELOC_OCCUPANCY", @"HELOC_OCCUPANCY");
	    AddPropertyMapping("HELOC_VALUE", @"HELOC_VALUE");
	    AddPropertyMapping("HELOC_1STMORT_BAL", @"HELOC_1STMORT_BAL");
	    AddPropertyMapping("HELOC_1STMORT_PAY", @"HELOC_1STMORT_PAY");
	    AddPropertyMapping("HELOC_2NDMORT_BAL", @"HELOC_2NDMORT_BAL");
	    AddPropertyMapping("HELOC_2NDMORT_PAY", @"HELOC_2NDMORT_PAY");
	    AddPropertyMapping("HELOC_ORIG_DATE", @"HELOC_ORIG_DATE");
	    AddPropertyMapping("INSURE_CODE", @"INSURE_CODE");
	    AddPropertyMapping("AP_INTEREST_RATE", @"AP_INTEREST_RATE");
	}

	private APPLICDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all APPLIC rows.
	/// </summary>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLICList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of APPLIC rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLICList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of APPLIC rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLICList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLICList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    APPLICList list = new APPLICList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all APPLIC rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLICList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of APPLIC rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLICList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of APPLIC rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLICList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLICList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    APPLICList list = new APPLICList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a APPLIC entity using it's primary key.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	/// <returns>A APPLIC object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public APPLIC Load(IdType aPPLIC_ID) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(APPLICFields.APPLIC_ID, EqualityOperatorEnum.Equal, aPPLIC_ID.IsValid ? aPPLIC_ID.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(APPLIC instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(APPLICFields.APPLIC_ID, EqualityOperatorEnum.Equal, instance.APPLIC_ID.IsValid ? instance.APPLIC_ID.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for APPLIC.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private APPLICList GetList(IDataReader reader) {
	    APPLICList list = new APPLICList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private APPLIC GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private APPLIC GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    APPLIC data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC GetDataObjectFromReader(APPLIC data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    APPLIC data = new APPLIC(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    APPLIC data = new APPLIC(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC GetDataObjectFromReader(APPLIC data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.APPLIC_ID)) {
		data.APPLIC_ID = IdType.UNSET;
	    } else {
		data.APPLIC_ID = new IdType(dataReader.GetInt32(ordinals.APPLIC_ID));
	    }
	    if (dataReader.IsDBNull(ordinals.APPLIC_NUM)) {
		data.APPLIC_NUM = IdType.UNSET;
	    } else {
		data.APPLIC_NUM = new IdType(dataReader.GetInt32(ordinals.APPLIC_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.FINAL_STAMP_BR)) {
		data.FINAL_STAMP_BR = IntegerType.UNSET;
	    } else {
		data.FINAL_STAMP_BR = new IntegerType(dataReader.GetInt32(ordinals.FINAL_STAMP_BR));
	    }
	    if (dataReader.IsDBNull(ordinals.LOAD_STAMP_BR)) {
		data.LOAD_STAMP_BR = IntegerType.UNSET;
	    } else {
		data.LOAD_STAMP_BR = new IntegerType(dataReader.GetInt32(ordinals.LOAD_STAMP_BR));
	    }
	    if (dataReader.IsDBNull(ordinals.STAGE2_CRSCORE)) {
		data.STAGE2_CRSCORE = IntegerType.UNSET;
	    } else {
		data.STAGE2_CRSCORE = new IntegerType(dataReader.GetInt32(ordinals.STAGE2_CRSCORE));
	    }
	    if (dataReader.IsDBNull(ordinals.AP_RISK_SCORE)) {
		data.AP_RISK_SCORE = IntegerType.UNSET;
	    } else {
		data.AP_RISK_SCORE = new IntegerType(dataReader.GetInt32(ordinals.AP_RISK_SCORE));
	    }
	    if (dataReader.IsDBNull(ordinals.BANKRUPT_SCORE)) {
		data.BANKRUPT_SCORE = IntegerType.UNSET;
	    } else {
		data.BANKRUPT_SCORE = new IntegerType(dataReader.GetInt32(ordinals.BANKRUPT_SCORE));
	    }
	    if (dataReader.IsDBNull(ordinals.AP_RISK_LEVEL)) {
		data.AP_RISK_LEVEL = StringType.UNSET;
	    } else {
		data.AP_RISK_LEVEL = StringType.Parse(dataReader.GetString(ordinals.AP_RISK_LEVEL));
	    }
	    if (dataReader.IsDBNull(ordinals.ALPS_COLOR)) {
		data.ALPS_COLOR = IntegerType.UNSET;
	    } else {
		data.ALPS_COLOR = new IntegerType(dataReader.GetInt32(ordinals.ALPS_COLOR));
	    }
	    if (dataReader.IsDBNull(ordinals.PAYMENT_FREQ)) {
		data.PAYMENT_FREQ = StringType.UNSET;
	    } else {
		data.PAYMENT_FREQ = StringType.Parse(dataReader.GetString(ordinals.PAYMENT_FREQ));
	    }
	    if (dataReader.IsDBNull(ordinals.TOT_FIN)) {
		data.TOT_FIN = CurrencyType.UNSET;
	    } else {
		data.TOT_FIN = new CurrencyType(dataReader.GetDecimal(ordinals.TOT_FIN));
	    }
	    if (dataReader.IsDBNull(ordinals.BALLOON_DATE)) {
		data.BALLOON_DATE = DateTimeType.UNSET;
	    } else {
		data.BALLOON_DATE = new DateTimeType(dataReader.GetDateTime(ordinals.BALLOON_DATE));
	    }
	    if (dataReader.IsDBNull(ordinals.DEALER_NUMBER)) {
		data.DEALER_NUMBER = IntegerType.UNSET;
	    } else {
		data.DEALER_NUMBER = new IntegerType(dataReader.GetInt32(ordinals.DEALER_NUMBER));
	    }
	    if (dataReader.IsDBNull(ordinals.CUDL_DEALER_NAME)) {
		data.CUDL_DEALER_NAME = StringType.UNSET;
	    } else {
		data.CUDL_DEALER_NAME = StringType.Parse(dataReader.GetString(ordinals.CUDL_DEALER_NAME));
	    }
	    if (dataReader.IsDBNull(ordinals.AP_RP_DISCOUNT)) {
		data.AP_RP_DISCOUNT = IntegerType.UNSET;
	    } else {
		data.AP_RP_DISCOUNT = new IntegerType(dataReader.GetInt32(ordinals.AP_RP_DISCOUNT));
	    }
	    if (dataReader.IsDBNull(ordinals.CRSCORE_TYPE)) {
		data.CRSCORE_TYPE = StringType.UNSET;
	    } else {
		data.CRSCORE_TYPE = StringType.Parse(dataReader.GetString(ordinals.CRSCORE_TYPE));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_TYPE)) {
		data.HELOC_TYPE = StringType.UNSET;
	    } else {
		data.HELOC_TYPE = StringType.Parse(dataReader.GetString(ordinals.HELOC_TYPE));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_OCCUPANCY)) {
		data.HELOC_OCCUPANCY = StringType.UNSET;
	    } else {
		data.HELOC_OCCUPANCY = StringType.Parse(dataReader.GetString(ordinals.HELOC_OCCUPANCY));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_VALUE)) {
		data.HELOC_VALUE = StringType.UNSET;
	    } else {
		data.HELOC_VALUE = StringType.Parse(dataReader.GetString(ordinals.HELOC_VALUE));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_1STMORT_BAL)) {
		data.HELOC_1STMORT_BAL = CurrencyType.UNSET;
	    } else {
		data.HELOC_1STMORT_BAL = new CurrencyType(dataReader.GetDecimal(ordinals.HELOC_1STMORT_BAL));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_1STMORT_PAY)) {
		data.HELOC_1STMORT_PAY = CurrencyType.UNSET;
	    } else {
		data.HELOC_1STMORT_PAY = new CurrencyType(dataReader.GetDecimal(ordinals.HELOC_1STMORT_PAY));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_2NDMORT_BAL)) {
		data.HELOC_2NDMORT_BAL = CurrencyType.UNSET;
	    } else {
		data.HELOC_2NDMORT_BAL = new CurrencyType(dataReader.GetDecimal(ordinals.HELOC_2NDMORT_BAL));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_2NDMORT_PAY)) {
		data.HELOC_2NDMORT_PAY = CurrencyType.UNSET;
	    } else {
		data.HELOC_2NDMORT_PAY = new CurrencyType(dataReader.GetDecimal(ordinals.HELOC_2NDMORT_PAY));
	    }
	    if (dataReader.IsDBNull(ordinals.HELOC_ORIG_DATE)) {
		data.HELOC_ORIG_DATE = DateTimeType.UNSET;
	    } else {
		data.HELOC_ORIG_DATE = new DateTimeType(dataReader.GetDateTime(ordinals.HELOC_ORIG_DATE));
	    }
	    if (dataReader.IsDBNull(ordinals.INSURE_CODE)) {
		data.INSURE_CODE = StringType.UNSET;
	    } else {
		data.INSURE_CODE = StringType.Parse(dataReader.GetString(ordinals.INSURE_CODE));
	    }
	    if (dataReader.IsDBNull(ordinals.AP_INTEREST_RATE)) {
		data.AP_INTEREST_RATE = DecimalType.UNSET;
	    } else {
		data.AP_INTEREST_RATE = new DecimalType(dataReader.GetDecimal(ordinals.AP_INTEREST_RATE));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the APPLIC table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(APPLIC data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the APPLIC table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(APPLIC data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.APPLIC_NUM, data.APPLIC_NUM.IsValid ? data.APPLIC_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.FINAL_STAMP_BR, data.FINAL_STAMP_BR.IsValid ? data.FINAL_STAMP_BR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.LOAD_STAMP_BR, data.LOAD_STAMP_BR.IsValid ? data.LOAD_STAMP_BR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.STAGE2_CRSCORE, data.STAGE2_CRSCORE.IsValid ? data.STAGE2_CRSCORE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_RISK_SCORE, data.AP_RISK_SCORE.IsValid ? data.AP_RISK_SCORE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.BANKRUPT_SCORE, data.BANKRUPT_SCORE.IsValid ? data.BANKRUPT_SCORE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_RISK_LEVEL, data.AP_RISK_LEVEL.IsValid ? data.AP_RISK_LEVEL.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.ALPS_COLOR, data.ALPS_COLOR.IsValid ? data.ALPS_COLOR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.PAYMENT_FREQ, data.PAYMENT_FREQ.IsValid ? data.PAYMENT_FREQ.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.TOT_FIN, data.TOT_FIN.IsValid ? data.TOT_FIN.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.BALLOON_DATE, data.BALLOON_DATE.IsValid ? data.BALLOON_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.DEALER_NUMBER, data.DEALER_NUMBER.IsValid ? data.DEALER_NUMBER.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.CUDL_DEALER_NAME, data.CUDL_DEALER_NAME.IsValid ? data.CUDL_DEALER_NAME.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_RP_DISCOUNT, data.AP_RP_DISCOUNT.IsValid ? data.AP_RP_DISCOUNT.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.CRSCORE_TYPE, data.CRSCORE_TYPE.IsValid ? data.CRSCORE_TYPE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_TYPE, data.HELOC_TYPE.IsValid ? data.HELOC_TYPE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_OCCUPANCY, data.HELOC_OCCUPANCY.IsValid ? data.HELOC_OCCUPANCY.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_VALUE, data.HELOC_VALUE.IsValid ? data.HELOC_VALUE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_1STMORT_BAL, data.HELOC_1STMORT_BAL.IsValid ? data.HELOC_1STMORT_BAL.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_1STMORT_PAY, data.HELOC_1STMORT_PAY.IsValid ? data.HELOC_1STMORT_PAY.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_2NDMORT_BAL, data.HELOC_2NDMORT_BAL.IsValid ? data.HELOC_2NDMORT_BAL.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_2NDMORT_PAY, data.HELOC_2NDMORT_PAY.IsValid ? data.HELOC_2NDMORT_PAY.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_ORIG_DATE, data.HELOC_ORIG_DATE.IsValid ? data.HELOC_ORIG_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.INSURE_CODE, data.INSURE_CODE.IsValid ? data.INSURE_CODE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_INTEREST_RATE, data.AP_INTEREST_RATE.IsValid ? data.AP_INTEREST_RATE.ToDecimal() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the APPLIC table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(APPLIC data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the APPLIC table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(APPLIC data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.APPLIC_ID, data.APPLIC_ID.IsValid ? data.APPLIC_ID.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.APPLIC_NUM, data.APPLIC_NUM.IsValid ? data.APPLIC_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.FINAL_STAMP_BR, data.FINAL_STAMP_BR.IsValid ? data.FINAL_STAMP_BR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.LOAD_STAMP_BR, data.LOAD_STAMP_BR.IsValid ? data.LOAD_STAMP_BR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.STAGE2_CRSCORE, data.STAGE2_CRSCORE.IsValid ? data.STAGE2_CRSCORE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_RISK_SCORE, data.AP_RISK_SCORE.IsValid ? data.AP_RISK_SCORE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.BANKRUPT_SCORE, data.BANKRUPT_SCORE.IsValid ? data.BANKRUPT_SCORE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_RISK_LEVEL, data.AP_RISK_LEVEL.IsValid ? data.AP_RISK_LEVEL.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.ALPS_COLOR, data.ALPS_COLOR.IsValid ? data.ALPS_COLOR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.PAYMENT_FREQ, data.PAYMENT_FREQ.IsValid ? data.PAYMENT_FREQ.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.TOT_FIN, data.TOT_FIN.IsValid ? data.TOT_FIN.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.BALLOON_DATE, data.BALLOON_DATE.IsValid ? data.BALLOON_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.DEALER_NUMBER, data.DEALER_NUMBER.IsValid ? data.DEALER_NUMBER.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.CUDL_DEALER_NAME, data.CUDL_DEALER_NAME.IsValid ? data.CUDL_DEALER_NAME.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_RP_DISCOUNT, data.AP_RP_DISCOUNT.IsValid ? data.AP_RP_DISCOUNT.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.CRSCORE_TYPE, data.CRSCORE_TYPE.IsValid ? data.CRSCORE_TYPE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_TYPE, data.HELOC_TYPE.IsValid ? data.HELOC_TYPE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_OCCUPANCY, data.HELOC_OCCUPANCY.IsValid ? data.HELOC_OCCUPANCY.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_VALUE, data.HELOC_VALUE.IsValid ? data.HELOC_VALUE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_1STMORT_BAL, data.HELOC_1STMORT_BAL.IsValid ? data.HELOC_1STMORT_BAL.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_1STMORT_PAY, data.HELOC_1STMORT_PAY.IsValid ? data.HELOC_1STMORT_PAY.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_2NDMORT_BAL, data.HELOC_2NDMORT_BAL.IsValid ? data.HELOC_2NDMORT_BAL.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_2NDMORT_PAY, data.HELOC_2NDMORT_PAY.IsValid ? data.HELOC_2NDMORT_PAY.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.HELOC_ORIG_DATE, data.HELOC_ORIG_DATE.IsValid ? data.HELOC_ORIG_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.INSURE_CODE, data.INSURE_CODE.IsValid ? data.INSURE_CODE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.AP_INTEREST_RATE, data.AP_INTEREST_RATE.IsValid ? data.AP_INTEREST_RATE.ToDecimal() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the APPLIC table by APPLIC_ID.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	public void Delete(IdType aPPLIC_ID) {
	    Delete(aPPLIC_ID, null);
	}

	/// <summary>
	/// Deletes a record from the APPLIC table by APPLIC_ID.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aPPLIC_ID, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(APPLICFields.APPLIC_ID, aPPLIC_ID.IsValid ? aPPLIC_ID.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
