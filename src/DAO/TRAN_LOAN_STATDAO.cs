using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for TRAN_LOAN_STAT business entity.
    /// </summary>
    public class TRAN_LOAN_STATDAO : Spring2.Core.DAO.SqlEntityDAO, ITRAN_LOAN_STATDAO {
	private static ITRAN_LOAN_STATDAO instance = new TRAN_LOAN_STATDAO();
	public static ITRAN_LOAN_STATDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(ITRAN_LOAN_STATDAO))) {
		    ClassRegistry.Register<ITRAN_LOAN_STATDAO>(instance);
		}
		return ClassRegistry.Resolve<ITRAN_LOAN_STATDAO>();
	    }
	}

	private static readonly String VIEW = "vwTRAN_LOAN_STAT";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 ACCT_NUM;
	    public Int32 L_STAT_CHANGE_ACTUAL_DATE;

	    internal ColumnOrdinals(IDataReader reader) {
		ACCT_NUM = reader.GetOrdinal("ACCT_NUM");
		L_STAT_CHANGE_ACTUAL_DATE = reader.GetOrdinal("L_STAT_CHANGE_ACTUAL_DATE");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		ACCT_NUM = reader.GetOrdinal(prefix + "ACCT_NUM");
		L_STAT_CHANGE_ACTUAL_DATE = reader.GetOrdinal(prefix + "L_STAT_CHANGE_ACTUAL_DATE");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static TRAN_LOAN_STATDAO() {
	    AddPropertyMapping("ACCT_NUM", @"ACCT_NUM");
	    AddPropertyMapping("L_STAT_CHANGE_ACTUAL_DATE", @"L_STAT_CHANGE_ACTUAL_DATE");
	}

	private TRAN_LOAN_STATDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all TRAN_LOAN_STAT rows.
	/// </summary>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public TRAN_LOAN_STATList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of TRAN_LOAN_STAT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public TRAN_LOAN_STATList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of TRAN_LOAN_STAT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public TRAN_LOAN_STATList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of TRAN_LOAN_STAT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public TRAN_LOAN_STATList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    TRAN_LOAN_STATList list = new TRAN_LOAN_STATList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all TRAN_LOAN_STAT rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public TRAN_LOAN_STATList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of TRAN_LOAN_STAT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public TRAN_LOAN_STATList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of TRAN_LOAN_STAT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public TRAN_LOAN_STATList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of TRAN_LOAN_STAT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_LOAN_STAT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public TRAN_LOAN_STATList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    TRAN_LOAN_STATList list = new TRAN_LOAN_STATList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a TRAN_LOAN_STAT entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A TRAN_LOAN_STAT object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public TRAN_LOAN_STAT Load(IdType aCCT_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(TRAN_LOAN_STATFields.ACCT_NUM, EqualityOperatorEnum.Equal, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(TRAN_LOAN_STAT instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(TRAN_LOAN_STATFields.ACCT_NUM, EqualityOperatorEnum.Equal, instance.ACCT_NUM.IsValid ? instance.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for TRAN_LOAN_STAT.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private TRAN_LOAN_STATList GetList(IDataReader reader) {
	    TRAN_LOAN_STATList list = new TRAN_LOAN_STATList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private TRAN_LOAN_STAT GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private TRAN_LOAN_STAT GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    TRAN_LOAN_STAT data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public TRAN_LOAN_STAT GetDataObjectFromReader(TRAN_LOAN_STAT data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public TRAN_LOAN_STAT GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    TRAN_LOAN_STAT data = new TRAN_LOAN_STAT(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public TRAN_LOAN_STAT GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    TRAN_LOAN_STAT data = new TRAN_LOAN_STAT(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public TRAN_LOAN_STAT GetDataObjectFromReader(TRAN_LOAN_STAT data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.ACCT_NUM)) {
		data.ACCT_NUM = IdType.UNSET;
	    } else {
		data.ACCT_NUM = new IdType(dataReader.GetInt32(ordinals.ACCT_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.L_STAT_CHANGE_ACTUAL_DATE)) {
		data.L_STAT_CHANGE_ACTUAL_DATE = DateTimeType.UNSET;
	    } else {
		data.L_STAT_CHANGE_ACTUAL_DATE = new DateTimeType(dataReader.GetDateTime(ordinals.L_STAT_CHANGE_ACTUAL_DATE));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the TRAN_LOAN_STAT table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(TRAN_LOAN_STAT data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the TRAN_LOAN_STAT table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(TRAN_LOAN_STAT data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spTRAN_LOAN_STAT_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(TRAN_LOAN_STATFields.L_STAT_CHANGE_ACTUAL_DATE, data.L_STAT_CHANGE_ACTUAL_DATE.IsValid ? data.L_STAT_CHANGE_ACTUAL_DATE.ToDateTime() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the TRAN_LOAN_STAT table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(TRAN_LOAN_STAT data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the TRAN_LOAN_STAT table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(TRAN_LOAN_STAT data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spTRAN_LOAN_STAT_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(TRAN_LOAN_STATFields.ACCT_NUM, data.ACCT_NUM.IsValid ? data.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(TRAN_LOAN_STATFields.L_STAT_CHANGE_ACTUAL_DATE, data.L_STAT_CHANGE_ACTUAL_DATE.IsValid ? data.L_STAT_CHANGE_ACTUAL_DATE.ToDateTime() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the TRAN_LOAN_STAT table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	public void Delete(IdType aCCT_NUM) {
	    Delete(aCCT_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the TRAN_LOAN_STAT table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aCCT_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spTRAN_LOAN_STAT_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(TRAN_LOAN_STATFields.ACCT_NUM, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
