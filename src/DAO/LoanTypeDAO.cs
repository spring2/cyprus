using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for LoanType business entity.
    /// </summary>
    public class LoanTypeDAO : Spring2.Core.DAO.SqlEntityDAO, ILoanTypeDAO {
	private static ILoanTypeDAO instance = new LoanTypeDAO();
	public static ILoanTypeDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(ILoanTypeDAO))) {
		    ClassRegistry.Register<ILoanTypeDAO>(instance);
		}
		return ClassRegistry.Resolve<ILoanTypeDAO>();
	    }
	}

	private static readonly String VIEW = "vwLoanType";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 LoanTypeId;
	    public Int32 LType;
	    public Int32 LoanDesc;

	    internal ColumnOrdinals(IDataReader reader) {
		LoanTypeId = reader.GetOrdinal("LoanTypeId");
		LType = reader.GetOrdinal("LType");
		LoanDesc = reader.GetOrdinal("LoanDesc");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		LoanTypeId = reader.GetOrdinal(prefix + "LoanTypeId");
		LType = reader.GetOrdinal(prefix + "LType");
		LoanDesc = reader.GetOrdinal(prefix + "LoanDesc");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static LoanTypeDAO() {
	    AddPropertyMapping("LoanTypeId", @"LoanTypeId");
	    AddPropertyMapping("LType", @"LType");
	    AddPropertyMapping("LoanDesc", @"LoanDesc");
	}

	private LoanTypeDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all LoanType rows.
	/// </summary>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanTypeList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of LoanType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanTypeList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of LoanType rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanTypeList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of LoanType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanTypeList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    LoanTypeList list = new LoanTypeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all LoanType rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanTypeList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of LoanType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanTypeList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of LoanType rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanTypeList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of LoanType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanTypeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    LoanTypeList list = new LoanTypeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a LoanType entity using it's primary key.
	/// </summary>
	/// <param name="loanTypeId">A key field.</param>
	/// <returns>A LoanType object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public LoanType Load(IdType loanTypeId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(LoanTypeFields.LOANTYPEID, EqualityOperatorEnum.Equal, loanTypeId.IsValid ? loanTypeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(LoanType instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(LoanTypeFields.LOANTYPEID, EqualityOperatorEnum.Equal, instance.LoanTypeId.IsValid ? instance.LoanTypeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for LoanType.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private LoanTypeList GetList(IDataReader reader) {
	    LoanTypeList list = new LoanTypeList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private LoanType GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private LoanType GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    LoanType data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public LoanType GetDataObjectFromReader(LoanType data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public LoanType GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    LoanType data = new LoanType(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public LoanType GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    LoanType data = new LoanType(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public LoanType GetDataObjectFromReader(LoanType data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.LoanTypeId)) {
		data.LoanTypeId = IdType.UNSET;
	    } else {
		data.LoanTypeId = new IdType(dataReader.GetInt32(ordinals.LoanTypeId));
	    }
	    if (dataReader.IsDBNull(ordinals.LType)) {
		data.LType = IntegerType.UNSET;
	    } else {
		data.LType = new IntegerType(dataReader.GetInt32(ordinals.LType));
	    }
	    if (dataReader.IsDBNull(ordinals.LoanDesc)) {
		data.LoanDesc = StringType.UNSET;
	    } else {
		data.LoanDesc = StringType.Parse(dataReader.GetString(ordinals.LoanDesc));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the LoanType table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(LoanType data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the LoanType table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(LoanType data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spLoanType_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(LoanTypeFields.LTYPE, data.LType.IsValid ? data.LType.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanTypeFields.LOANDESC, data.LoanDesc.IsValid ? data.LoanDesc.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the LoanType table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(LoanType data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the LoanType table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(LoanType data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spLoanType_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(LoanTypeFields.LOANTYPEID, data.LoanTypeId.IsValid ? data.LoanTypeId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanTypeFields.LTYPE, data.LType.IsValid ? data.LType.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanTypeFields.LOANDESC, data.LoanDesc.IsValid ? data.LoanDesc.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the LoanType table by LoanTypeId.
	/// </summary>
	/// <param name="loanTypeId">A key field.</param>
	public void Delete(IdType loanTypeId) {
	    Delete(loanTypeId, null);
	}

	/// <summary>
	/// Deletes a record from the LoanType table by LoanTypeId.
	/// </summary>
	/// <param name="loanTypeId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType loanTypeId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spLoanType_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(LoanTypeFields.LOANTYPEID, loanTypeId.IsValid ? loanTypeId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
