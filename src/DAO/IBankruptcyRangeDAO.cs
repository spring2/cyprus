using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for BankruptcyRange business entity.
    /// </summary>
    public interface IBankruptcyRangeDAO {
	/// <summary>
	/// Returns a list of all BankruptcyRange rows.
	/// </summary>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	BankruptcyRangeList GetList();

	/// <summary>
	/// Returns a filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	BankruptcyRangeList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of BankruptcyRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	BankruptcyRangeList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	BankruptcyRangeList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all BankruptcyRange rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	BankruptcyRangeList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	BankruptcyRangeList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of BankruptcyRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	BankruptcyRangeList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of BankruptcyRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of BankruptcyRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	BankruptcyRangeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a BankruptcyRange entity using it's primary key.
	/// </summary>
	/// <param name="bankruptcyRangeId">A key field.</param>
	/// <returns>A BankruptcyRange object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	BankruptcyRange Load(IdType bankruptcyRangeId);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(BankruptcyRange instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	BankruptcyRange GetDataObjectFromReader(BankruptcyRange data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	BankruptcyRange GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	BankruptcyRange GetDataObjectFromReader(IDataReader dataReader, BankruptcyRangeDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	BankruptcyRange GetDataObjectFromReader(BankruptcyRange data, IDataReader dataReader, BankruptcyRangeDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the BankruptcyRange table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(BankruptcyRange data);


	/// <summary>
	/// Updates a record in the BankruptcyRange table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(BankruptcyRange data);

	/// <summary>
	/// Deletes a record from the BankruptcyRange table by BankruptcyRangeId.
	/// </summary>
	/// <param name="bankruptcyRangeId">A key field.</param>
	[Generate]
	void Delete(IdType bankruptcyRangeId);


	//}
    }
}
