using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for APPLIC_CALCINFO business entity.
    /// </summary>
    public class APPLIC_CALCINFODAO : Spring2.Core.DAO.SqlEntityDAO, IAPPLIC_CALCINFODAO {
	private static IAPPLIC_CALCINFODAO instance = new APPLIC_CALCINFODAO();
	public static IAPPLIC_CALCINFODAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IAPPLIC_CALCINFODAO))) {
		    ClassRegistry.Register<IAPPLIC_CALCINFODAO>(instance);
		}
		return ClassRegistry.Resolve<IAPPLIC_CALCINFODAO>();
	    }
	}

	private static readonly String VIEW = "vwAPPLIC_CALCINFO";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 APPLIC_ID;
	    public Int32 DEBT_AFTER;

	    internal ColumnOrdinals(IDataReader reader) {
		APPLIC_ID = reader.GetOrdinal("APPLIC_ID");
		DEBT_AFTER = reader.GetOrdinal("DEBT_AFTER");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		APPLIC_ID = reader.GetOrdinal(prefix + "APPLIC_ID");
		DEBT_AFTER = reader.GetOrdinal(prefix + "DEBT_AFTER");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static APPLIC_CALCINFODAO() {
	    AddPropertyMapping("APPLIC_ID", @"APPLIC_ID");
	    AddPropertyMapping("DEBT_AFTER", @"DEBT_AFTER");
	}

	private APPLIC_CALCINFODAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all APPLIC_CALCINFO rows.
	/// </summary>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_CALCINFOList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_CALCINFOList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of APPLIC_CALCINFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_CALCINFOList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_CALCINFOList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    APPLIC_CALCINFOList list = new APPLIC_CALCINFOList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_CALCINFOList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_CALCINFOList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of APPLIC_CALCINFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_CALCINFOList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_CALCINFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_CALCINFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_CALCINFOList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    APPLIC_CALCINFOList list = new APPLIC_CALCINFOList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a APPLIC_CALCINFO entity using it's primary key.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	/// <returns>A APPLIC_CALCINFO object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public APPLIC_CALCINFO Load(IdType aPPLIC_ID) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(APPLIC_CALCINFOFields.APPLIC_ID, EqualityOperatorEnum.Equal, aPPLIC_ID.IsValid ? aPPLIC_ID.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(APPLIC_CALCINFO instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(APPLIC_CALCINFOFields.APPLIC_ID, EqualityOperatorEnum.Equal, instance.APPLIC_ID.IsValid ? instance.APPLIC_ID.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for APPLIC_CALCINFO.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private APPLIC_CALCINFOList GetList(IDataReader reader) {
	    APPLIC_CALCINFOList list = new APPLIC_CALCINFOList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private APPLIC_CALCINFO GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private APPLIC_CALCINFO GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    APPLIC_CALCINFO data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_CALCINFO GetDataObjectFromReader(APPLIC_CALCINFO data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_CALCINFO GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    APPLIC_CALCINFO data = new APPLIC_CALCINFO(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_CALCINFO GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    APPLIC_CALCINFO data = new APPLIC_CALCINFO(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_CALCINFO GetDataObjectFromReader(APPLIC_CALCINFO data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.APPLIC_ID)) {
		data.APPLIC_ID = IdType.UNSET;
	    } else {
		data.APPLIC_ID = new IdType(dataReader.GetInt32(ordinals.APPLIC_ID));
	    }
	    if (dataReader.IsDBNull(ordinals.DEBT_AFTER)) {
		data.DEBT_AFTER = DecimalType.UNSET;
	    } else {
		data.DEBT_AFTER = new DecimalType(dataReader.GetDecimal(ordinals.DEBT_AFTER));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the APPLIC_CALCINFO table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(APPLIC_CALCINFO data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the APPLIC_CALCINFO table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(APPLIC_CALCINFO data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_CALCINFO_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_CALCINFOFields.DEBT_AFTER, data.DEBT_AFTER.IsValid ? data.DEBT_AFTER.ToDecimal() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the APPLIC_CALCINFO table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(APPLIC_CALCINFO data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the APPLIC_CALCINFO table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(APPLIC_CALCINFO data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_CALCINFO_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_CALCINFOFields.APPLIC_ID, data.APPLIC_ID.IsValid ? data.APPLIC_ID.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_CALCINFOFields.DEBT_AFTER, data.DEBT_AFTER.IsValid ? data.DEBT_AFTER.ToDecimal() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the APPLIC_CALCINFO table by APPLIC_ID.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	public void Delete(IdType aPPLIC_ID) {
	    Delete(aPPLIC_ID, null);
	}

	/// <summary>
	/// Deletes a record from the APPLIC_CALCINFO table by APPLIC_ID.
	/// </summary>
	/// <param name="aPPLIC_ID">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aPPLIC_ID, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_CALCINFO_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_CALCINFOFields.APPLIC_ID, aPPLIC_ID.IsValid ? aPPLIC_ID.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
