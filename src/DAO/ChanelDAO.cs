using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for Chanel business entity.
    /// </summary>
    public class ChanelDAO : Spring2.Core.DAO.SqlEntityDAO, IChanelDAO {
	private static IChanelDAO instance = new ChanelDAO();
	public static IChanelDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IChanelDAO))) {
		    ClassRegistry.Register<IChanelDAO>(instance);
		}
		return ClassRegistry.Resolve<IChanelDAO>();
	    }
	}

	private static readonly String VIEW = "vwChanel";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 ChanelId;
	    public Int32 ReportCode;
	    public Int32 ChanelDesc;

	    internal ColumnOrdinals(IDataReader reader) {
		ChanelId = reader.GetOrdinal("ChanelId");
		ReportCode = reader.GetOrdinal("ReportCode");
		ChanelDesc = reader.GetOrdinal("ChanelDesc");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		ChanelId = reader.GetOrdinal(prefix + "ChanelId");
		ReportCode = reader.GetOrdinal(prefix + "ReportCode");
		ChanelDesc = reader.GetOrdinal(prefix + "ChanelDesc");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static ChanelDAO() {
	    AddPropertyMapping("ChanelId", @"ChanelId");
	    AddPropertyMapping("ReportCode", @"ReportCode");
	    AddPropertyMapping("ChanelDesc", @"ChanelDesc");
	}

	private ChanelDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all Chanel rows.
	/// </summary>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ChanelList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of Chanel rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ChanelList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of Chanel rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ChanelList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of Chanel rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ChanelList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    ChanelList list = new ChanelList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all Chanel rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ChanelList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of Chanel rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ChanelList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of Chanel rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ChanelList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of Chanel rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of Chanel objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ChanelList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    ChanelList list = new ChanelList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a Chanel entity using it's primary key.
	/// </summary>
	/// <param name="chanelId">A key field.</param>
	/// <returns>A Chanel object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public Chanel Load(IdType chanelId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ChanelFields.CHANELID, EqualityOperatorEnum.Equal, chanelId.IsValid ? chanelId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(Chanel instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ChanelFields.CHANELID, EqualityOperatorEnum.Equal, instance.ChanelId.IsValid ? instance.ChanelId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for Chanel.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private ChanelList GetList(IDataReader reader) {
	    ChanelList list = new ChanelList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private Chanel GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private Chanel GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    Chanel data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public Chanel GetDataObjectFromReader(Chanel data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public Chanel GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    Chanel data = new Chanel(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public Chanel GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    Chanel data = new Chanel(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public Chanel GetDataObjectFromReader(Chanel data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.ChanelId)) {
		data.ChanelId = IdType.UNSET;
	    } else {
		data.ChanelId = new IdType(dataReader.GetInt32(ordinals.ChanelId));
	    }
	    if (dataReader.IsDBNull(ordinals.ReportCode)) {
		data.ReportCode = IntegerType.UNSET;
	    } else {
		data.ReportCode = new IntegerType(dataReader.GetInt32(ordinals.ReportCode));
	    }
	    if (dataReader.IsDBNull(ordinals.ChanelDesc)) {
		data.ChanelDesc = StringType.UNSET;
	    } else {
		data.ChanelDesc = StringType.Parse(dataReader.GetString(ordinals.ChanelDesc));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the Chanel table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(Chanel data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the Chanel table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(Chanel data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spChanel_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ChanelFields.REPORTCODE, data.ReportCode.IsValid ? data.ReportCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ChanelFields.CHANELDESC, data.ChanelDesc.IsValid ? data.ChanelDesc.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the Chanel table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(Chanel data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the Chanel table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(Chanel data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spChanel_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ChanelFields.CHANELID, data.ChanelId.IsValid ? data.ChanelId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ChanelFields.REPORTCODE, data.ReportCode.IsValid ? data.ReportCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ChanelFields.CHANELDESC, data.ChanelDesc.IsValid ? data.ChanelDesc.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the Chanel table by ChanelId.
	/// </summary>
	/// <param name="chanelId">A key field.</param>
	public void Delete(IdType chanelId) {
	    Delete(chanelId, null);
	}

	/// <summary>
	/// Deletes a record from the Chanel table by ChanelId.
	/// </summary>
	/// <param name="chanelId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType chanelId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spChanel_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(ChanelFields.CHANELID, chanelId.IsValid ? chanelId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
