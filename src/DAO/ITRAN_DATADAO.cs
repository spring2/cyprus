using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for TRAN_DATA business entity.
    /// </summary>
    public interface ITRAN_DATADAO {
	/// <summary>
	/// Returns a list of all TRAN_DATA rows.
	/// </summary>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	TRAN_DATAList GetList();

	/// <summary>
	/// Returns a filtered list of TRAN_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	TRAN_DATAList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of TRAN_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	TRAN_DATAList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of TRAN_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	TRAN_DATAList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all TRAN_DATA rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	TRAN_DATAList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of TRAN_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	TRAN_DATAList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of TRAN_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	TRAN_DATAList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of TRAN_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of TRAN_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	TRAN_DATAList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a TRAN_DATA entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A TRAN_DATA object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	TRAN_DATA Load(IdType aCCT_NUM);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(TRAN_DATA instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	TRAN_DATA GetDataObjectFromReader(TRAN_DATA data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	TRAN_DATA GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	TRAN_DATA GetDataObjectFromReader(IDataReader dataReader, TRAN_DATADAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	TRAN_DATA GetDataObjectFromReader(TRAN_DATA data, IDataReader dataReader, TRAN_DATADAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the TRAN_DATA table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(TRAN_DATA data);


	/// <summary>
	/// Updates a record in the TRAN_DATA table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(TRAN_DATA data);

	/// <summary>
	/// Deletes a record from the TRAN_DATA table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	[Generate]
	void Delete(IdType aCCT_NUM);


	//}
    }
}
