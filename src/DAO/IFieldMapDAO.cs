using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for FieldMap business entity.
    /// </summary>
    public interface IFieldMapDAO {
	/// <summary>
	/// Returns a list of all FieldMap rows.
	/// </summary>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FieldMapList GetList();

	/// <summary>
	/// Returns a filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FieldMapList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of FieldMap rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FieldMapList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FieldMapList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all FieldMap rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FieldMapList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FieldMapList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of FieldMap rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FieldMapList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FieldMapList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a FieldMap entity using it's primary key.
	/// </summary>
	/// <param name="fieldMapId">A key field.</param>
	/// <returns>A FieldMap object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	FieldMap Load(IdType fieldMapId);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(FieldMap instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FieldMap GetDataObjectFromReader(FieldMap data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FieldMap GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FieldMap GetDataObjectFromReader(IDataReader dataReader, FieldMapDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FieldMap GetDataObjectFromReader(FieldMap data, IDataReader dataReader, FieldMapDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the FieldMap table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(FieldMap data);


	/// <summary>
	/// Updates a record in the FieldMap table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(FieldMap data);

	/// <summary>
	/// Deletes a record from the FieldMap table by FieldMapId.
	/// </summary>
	/// <param name="fieldMapId">A key field.</param>
	[Generate]
	void Delete(IdType fieldMapId);


	//}
    }
}
