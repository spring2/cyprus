using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for RiskType business entity.
    /// </summary>
    public class RiskTypeDAO : Spring2.Core.DAO.SqlEntityDAO, IRiskTypeDAO {
	private static IRiskTypeDAO instance = new RiskTypeDAO();
	public static IRiskTypeDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IRiskTypeDAO))) {
		    ClassRegistry.Register<IRiskTypeDAO>(instance);
		}
		return ClassRegistry.Resolve<IRiskTypeDAO>();
	    }
	}

	private static readonly String VIEW = "vwRiskType";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 RiskTypeId;
	    public Int32 LType;
	    public Int32 RiskTypeDesc;

	    internal ColumnOrdinals(IDataReader reader) {
		RiskTypeId = reader.GetOrdinal("RiskTypeId");
		LType = reader.GetOrdinal("LType");
		RiskTypeDesc = reader.GetOrdinal("RiskTypeDesc");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		RiskTypeId = reader.GetOrdinal(prefix + "RiskTypeId");
		LType = reader.GetOrdinal(prefix + "LType");
		RiskTypeDesc = reader.GetOrdinal(prefix + "RiskTypeDesc");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static RiskTypeDAO() {
	    AddPropertyMapping("RiskTypeId", @"RiskTypeId");
	    AddPropertyMapping("LType", @"LType");
	    AddPropertyMapping("RiskTypeDesc", @"RiskTypeDesc");
	}

	private RiskTypeDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all RiskType rows.
	/// </summary>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public RiskTypeList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public RiskTypeList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of RiskType rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public RiskTypeList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public RiskTypeList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    RiskTypeList list = new RiskTypeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all RiskType rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public RiskTypeList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public RiskTypeList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of RiskType rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public RiskTypeList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public RiskTypeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    RiskTypeList list = new RiskTypeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a RiskType entity using it's primary key.
	/// </summary>
	/// <param name="riskTypeId">A key field.</param>
	/// <returns>A RiskType object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public RiskType Load(IdType riskTypeId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(RiskTypeFields.RISKTYPEID, EqualityOperatorEnum.Equal, riskTypeId.IsValid ? riskTypeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(RiskType instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(RiskTypeFields.RISKTYPEID, EqualityOperatorEnum.Equal, instance.RiskTypeId.IsValid ? instance.RiskTypeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for RiskType.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private RiskTypeList GetList(IDataReader reader) {
	    RiskTypeList list = new RiskTypeList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private RiskType GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private RiskType GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    RiskType data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public RiskType GetDataObjectFromReader(RiskType data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public RiskType GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    RiskType data = new RiskType(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public RiskType GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    RiskType data = new RiskType(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public RiskType GetDataObjectFromReader(RiskType data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.RiskTypeId)) {
		data.RiskTypeId = IdType.UNSET;
	    } else {
		data.RiskTypeId = new IdType(dataReader.GetInt32(ordinals.RiskTypeId));
	    }
	    if (dataReader.IsDBNull(ordinals.LType)) {
		data.LType = IntegerType.UNSET;
	    } else {
		data.LType = new IntegerType(dataReader.GetInt32(ordinals.LType));
	    }
	    if (dataReader.IsDBNull(ordinals.RiskTypeDesc)) {
		data.RiskTypeDesc = StringType.UNSET;
	    } else {
		data.RiskTypeDesc = StringType.Parse(dataReader.GetString(ordinals.RiskTypeDesc));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the RiskType table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(RiskType data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the RiskType table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(RiskType data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spRiskType_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(RiskTypeFields.LTYPE, data.LType.IsValid ? data.LType.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(RiskTypeFields.RISKTYPEDESC, data.RiskTypeDesc.IsValid ? data.RiskTypeDesc.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the RiskType table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(RiskType data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the RiskType table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(RiskType data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spRiskType_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(RiskTypeFields.RISKTYPEID, data.RiskTypeId.IsValid ? data.RiskTypeId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(RiskTypeFields.LTYPE, data.LType.IsValid ? data.LType.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(RiskTypeFields.RISKTYPEDESC, data.RiskTypeDesc.IsValid ? data.RiskTypeDesc.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the RiskType table by RiskTypeId.
	/// </summary>
	/// <param name="riskTypeId">A key field.</param>
	public void Delete(IdType riskTypeId) {
	    Delete(riskTypeId, null);
	}

	/// <summary>
	/// Deletes a record from the RiskType table by RiskTypeId.
	/// </summary>
	/// <param name="riskTypeId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType riskTypeId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spRiskType_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(RiskTypeFields.RISKTYPEID, riskTypeId.IsValid ? riskTypeId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
