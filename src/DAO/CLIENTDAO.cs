using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for CLIENT business entity.
    /// </summary>
    public class CLIENTDAO : Spring2.Core.DAO.SqlEntityDAO, ICLIENTDAO {
	private static ICLIENTDAO instance = new CLIENTDAO();
	public static ICLIENTDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(ICLIENTDAO))) {
		    ClassRegistry.Register<ICLIENTDAO>(instance);
		}
		return ClassRegistry.Resolve<ICLIENTDAO>();
	    }
	}

	private static readonly String VIEW = "vwCLIENT";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 MEM_NUM;
	    public Int32 CLIENT_CLASS;
	    public Int32 JOIN_DATE;
	    public Int32 SEX;

	    internal ColumnOrdinals(IDataReader reader) {
		MEM_NUM = reader.GetOrdinal("MEM_NUM");
		CLIENT_CLASS = reader.GetOrdinal("CLIENT_CLASS");
		JOIN_DATE = reader.GetOrdinal("JOIN_DATE");
		SEX = reader.GetOrdinal("SEX");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		MEM_NUM = reader.GetOrdinal(prefix + "MEM_NUM");
		CLIENT_CLASS = reader.GetOrdinal(prefix + "CLIENT_CLASS");
		JOIN_DATE = reader.GetOrdinal(prefix + "JOIN_DATE");
		SEX = reader.GetOrdinal(prefix + "SEX");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static CLIENTDAO() {
	    AddPropertyMapping("MEM_NUM", @"MEM_NUM");
	    AddPropertyMapping("CLIENT_CLASS", @"CLIENT_CLASS");
	    AddPropertyMapping("JOIN_DATE", @"JOIN_DATE");
	    AddPropertyMapping("SEX", @"SEX");
	}

	private CLIENTDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all CLIENT rows.
	/// </summary>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENTList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENTList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of CLIENT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENTList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENTList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    CLIENTList list = new CLIENTList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all CLIENT rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENTList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENTList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of CLIENT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENTList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of CLIENT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENTList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    CLIENTList list = new CLIENTList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a CLIENT entity using it's primary key.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	/// <returns>A CLIENT object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public CLIENT Load(IdType mEM_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(CLIENTFields.MEM_NUM, EqualityOperatorEnum.Equal, mEM_NUM.IsValid ? mEM_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(CLIENT instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(CLIENTFields.MEM_NUM, EqualityOperatorEnum.Equal, instance.MEM_NUM.IsValid ? instance.MEM_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for CLIENT.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private CLIENTList GetList(IDataReader reader) {
	    CLIENTList list = new CLIENTList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private CLIENT GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private CLIENT GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    CLIENT data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT GetDataObjectFromReader(CLIENT data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    CLIENT data = new CLIENT(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    CLIENT data = new CLIENT(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT GetDataObjectFromReader(CLIENT data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.MEM_NUM)) {
		data.MEM_NUM = IdType.UNSET;
	    } else {
		data.MEM_NUM = new IdType(dataReader.GetInt32(ordinals.MEM_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.CLIENT_CLASS)) {
		data.CLIENT_CLASS = IntegerType.UNSET;
	    } else {
		data.CLIENT_CLASS = new IntegerType(dataReader.GetInt32(ordinals.CLIENT_CLASS));
	    }
	    if (dataReader.IsDBNull(ordinals.JOIN_DATE)) {
		data.JOIN_DATE = DateTimeType.UNSET;
	    } else {
		data.JOIN_DATE = new DateTimeType(dataReader.GetDateTime(ordinals.JOIN_DATE));
	    }
	    if (dataReader.IsDBNull(ordinals.SEX)) {
		data.SEX = StringType.UNSET;
	    } else {
		data.SEX = StringType.Parse(dataReader.GetString(ordinals.SEX));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the CLIENT table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(CLIENT data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the CLIENT table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(CLIENT data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spCLIENT_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.CLIENT_CLASS, data.CLIENT_CLASS.IsValid ? data.CLIENT_CLASS.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.JOIN_DATE, data.JOIN_DATE.IsValid ? data.JOIN_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.SEX, data.SEX.IsValid ? data.SEX.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the CLIENT table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(CLIENT data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the CLIENT table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(CLIENT data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spCLIENT_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.MEM_NUM, data.MEM_NUM.IsValid ? data.MEM_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.CLIENT_CLASS, data.CLIENT_CLASS.IsValid ? data.CLIENT_CLASS.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.JOIN_DATE, data.JOIN_DATE.IsValid ? data.JOIN_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.SEX, data.SEX.IsValid ? data.SEX.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the CLIENT table by MEM_NUM.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	public void Delete(IdType mEM_NUM) {
	    Delete(mEM_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the CLIENT table by MEM_NUM.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType mEM_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spCLIENT_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(CLIENTFields.MEM_NUM, mEM_NUM.IsValid ? mEM_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
