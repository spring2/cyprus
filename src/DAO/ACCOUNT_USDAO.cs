using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for ACCOUNT_US business entity.
    /// </summary>
    public class ACCOUNT_USDAO : Spring2.Core.DAO.SqlEntityDAO, IACCOUNT_USDAO {
	private static IACCOUNT_USDAO instance = new ACCOUNT_USDAO();
	public static IACCOUNT_USDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IACCOUNT_USDAO))) {
		    ClassRegistry.Register<IACCOUNT_USDAO>(instance);
		}
		return ClassRegistry.Resolve<IACCOUNT_USDAO>();
	    }
	}

	private static readonly String VIEW = "vwACCOUNT_US";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 ACCT_NUM;
	    public Int32 US_LOANSTATUS;
	    public Int32 US_DELI_HIST;

	    internal ColumnOrdinals(IDataReader reader) {
		ACCT_NUM = reader.GetOrdinal("ACCT_NUM");
		US_LOANSTATUS = reader.GetOrdinal("US_LOANSTATUS");
		US_DELI_HIST = reader.GetOrdinal("US_DELI_HIST");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		ACCT_NUM = reader.GetOrdinal(prefix + "ACCT_NUM");
		US_LOANSTATUS = reader.GetOrdinal(prefix + "US_LOANSTATUS");
		US_DELI_HIST = reader.GetOrdinal(prefix + "US_DELI_HIST");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static ACCOUNT_USDAO() {
	    AddPropertyMapping("ACCT_NUM", @"ACCT_NUM");
	    AddPropertyMapping("US_LOANSTATUS", @"US_LOANSTATUS");
	    AddPropertyMapping("US_DELI_HIST", @"US_DELI_HIST");
	}

	private ACCOUNT_USDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all ACCOUNT_US rows.
	/// </summary>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_USList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of ACCOUNT_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_USList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of ACCOUNT_US rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_USList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_USList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    ACCOUNT_USList list = new ACCOUNT_USList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all ACCOUNT_US rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_USList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of ACCOUNT_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_USList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of ACCOUNT_US rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNT_USList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNT_USList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    ACCOUNT_USList list = new ACCOUNT_USList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a ACCOUNT_US entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A ACCOUNT_US object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public ACCOUNT_US Load(IdType aCCT_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ACCOUNT_USFields.ACCT_NUM, EqualityOperatorEnum.Equal, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(ACCOUNT_US instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ACCOUNT_USFields.ACCT_NUM, EqualityOperatorEnum.Equal, instance.ACCT_NUM.IsValid ? instance.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for ACCOUNT_US.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private ACCOUNT_USList GetList(IDataReader reader) {
	    ACCOUNT_USList list = new ACCOUNT_USList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private ACCOUNT_US GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private ACCOUNT_US GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    ACCOUNT_US data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_US GetDataObjectFromReader(ACCOUNT_US data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_US GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    ACCOUNT_US data = new ACCOUNT_US(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_US GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    ACCOUNT_US data = new ACCOUNT_US(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT_US GetDataObjectFromReader(ACCOUNT_US data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.ACCT_NUM)) {
		data.ACCT_NUM = IdType.UNSET;
	    } else {
		data.ACCT_NUM = new IdType(dataReader.GetInt32(ordinals.ACCT_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.US_LOANSTATUS)) {
		data.US_LOANSTATUS = StringType.UNSET;
	    } else {
		data.US_LOANSTATUS = StringType.Parse(dataReader.GetString(ordinals.US_LOANSTATUS));
	    }
	    if (dataReader.IsDBNull(ordinals.US_DELI_HIST)) {
		data.US_DELI_HIST = StringType.UNSET;
	    } else {
		data.US_DELI_HIST = StringType.Parse(dataReader.GetString(ordinals.US_DELI_HIST));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the ACCOUNT_US table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(ACCOUNT_US data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the ACCOUNT_US table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(ACCOUNT_US data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_US_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_USFields.US_LOANSTATUS, data.US_LOANSTATUS.IsValid ? data.US_LOANSTATUS.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_USFields.US_DELI_HIST, data.US_DELI_HIST.IsValid ? data.US_DELI_HIST.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the ACCOUNT_US table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(ACCOUNT_US data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the ACCOUNT_US table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(ACCOUNT_US data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_US_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_USFields.ACCT_NUM, data.ACCT_NUM.IsValid ? data.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_USFields.US_LOANSTATUS, data.US_LOANSTATUS.IsValid ? data.US_LOANSTATUS.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_USFields.US_DELI_HIST, data.US_DELI_HIST.IsValid ? data.US_DELI_HIST.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the ACCOUNT_US table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	public void Delete(IdType aCCT_NUM) {
	    Delete(aCCT_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the ACCOUNT_US table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aCCT_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_US_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNT_USFields.ACCT_NUM, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
