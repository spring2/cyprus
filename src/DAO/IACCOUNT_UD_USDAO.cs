using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for ACCOUNT_UD_US business entity.
    /// </summary>
    public interface IACCOUNT_UD_USDAO {
	/// <summary>
	/// Returns a list of all ACCOUNT_UD_US rows.
	/// </summary>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList();

	/// <summary>
	/// Returns a filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of ACCOUNT_UD_US rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of ACCOUNT_UD_US rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT_UD_US rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT_UD_US objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	ACCOUNT_UD_USList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a ACCOUNT_UD_US entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A ACCOUNT_UD_US object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	ACCOUNT_UD_US Load(IdType aCCT_NUM);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(ACCOUNT_UD_US instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	ACCOUNT_UD_US GetDataObjectFromReader(ACCOUNT_UD_US data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	ACCOUNT_UD_US GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	ACCOUNT_UD_US GetDataObjectFromReader(IDataReader dataReader, ACCOUNT_UD_USDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	ACCOUNT_UD_US GetDataObjectFromReader(ACCOUNT_UD_US data, IDataReader dataReader, ACCOUNT_UD_USDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the ACCOUNT_UD_US table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(ACCOUNT_UD_US data);


	/// <summary>
	/// Updates a record in the ACCOUNT_UD_US table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(ACCOUNT_UD_US data);

	/// <summary>
	/// Deletes a record from the ACCOUNT_UD_US table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	[Generate]
	void Delete(IdType aCCT_NUM);


	//}
    }
}
