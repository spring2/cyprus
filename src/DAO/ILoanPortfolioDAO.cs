using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for LoanPortfolio business entity.
    /// </summary>
    public interface ILoanPortfolioDAO {
	/// <summary>
	/// Returns a list of all LoanPortfolio rows.
	/// </summary>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	LoanPortfolioList GetList();

	/// <summary>
	/// Returns a filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	LoanPortfolioList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of LoanPortfolio rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	LoanPortfolioList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	LoanPortfolioList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all LoanPortfolio rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	LoanPortfolioList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	LoanPortfolioList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of LoanPortfolio rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	LoanPortfolioList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	LoanPortfolioList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a LoanPortfolio entity using it's primary key.
	/// </summary>
	/// <param name="loanPortfolio_Id">A key field.</param>
	/// <returns>A LoanPortfolio object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	LoanPortfolio Load(IdType loanPortfolioId);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(LoanPortfolio instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	LoanPortfolio GetDataObjectFromReader(LoanPortfolio data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	LoanPortfolio GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	LoanPortfolio GetDataObjectFromReader(IDataReader dataReader, LoanPortfolioDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	LoanPortfolio GetDataObjectFromReader(LoanPortfolio data, IDataReader dataReader, LoanPortfolioDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the LoanPortfolio table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(LoanPortfolio data);


	/// <summary>
	/// Updates a record in the LoanPortfolio table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(LoanPortfolio data);

	/// <summary>
	/// Deletes a record from the LoanPortfolio table by LoanPortfolioId.
	/// </summary>
	/// <param name="loanPortfolio_Id">A key field.</param>
	[Generate]
	void Delete(IdType loanPortfolioId);


	//}
    }
}
