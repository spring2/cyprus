using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for DELINQUENCY_DATA business entity.
    /// </summary>
    public class DELINQUENCY_DATADAO : Spring2.Core.DAO.SqlEntityDAO, IDELINQUENCY_DATADAO {
	private static IDELINQUENCY_DATADAO instance = new DELINQUENCY_DATADAO();
	public static IDELINQUENCY_DATADAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IDELINQUENCY_DATADAO))) {
		    ClassRegistry.Register<IDELINQUENCY_DATADAO>(instance);
		}
		return ClassRegistry.Resolve<IDELINQUENCY_DATADAO>();
	    }
	}

	private static readonly String VIEW = "vwDELINQUENCY_DATA";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 ACCT_NUM;
	    public Int32 DLQ_HIST;

	    internal ColumnOrdinals(IDataReader reader) {
		ACCT_NUM = reader.GetOrdinal("ACCT_NUM");
		DLQ_HIST = reader.GetOrdinal("DLQ_HIST");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		ACCT_NUM = reader.GetOrdinal(prefix + "ACCT_NUM");
		DLQ_HIST = reader.GetOrdinal(prefix + "DLQ_HIST");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static DELINQUENCY_DATADAO() {
	    AddPropertyMapping("ACCT_NUM", @"ACCT_NUM");
	    AddPropertyMapping("DLQ_HIST", @"DLQ_HIST");
	}

	private DELINQUENCY_DATADAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all DELINQUENCY_DATA rows.
	/// </summary>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public DELINQUENCY_DATAList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of DELINQUENCY_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public DELINQUENCY_DATAList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of DELINQUENCY_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public DELINQUENCY_DATAList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of DELINQUENCY_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public DELINQUENCY_DATAList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    DELINQUENCY_DATAList list = new DELINQUENCY_DATAList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all DELINQUENCY_DATA rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public DELINQUENCY_DATAList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of DELINQUENCY_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public DELINQUENCY_DATAList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of DELINQUENCY_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public DELINQUENCY_DATAList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of DELINQUENCY_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of DELINQUENCY_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public DELINQUENCY_DATAList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    DELINQUENCY_DATAList list = new DELINQUENCY_DATAList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a DELINQUENCY_DATA entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A DELINQUENCY_DATA object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public DELINQUENCY_DATA Load(IdType aCCT_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(DELINQUENCY_DATAFields.ACCT_NUM, EqualityOperatorEnum.Equal, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(DELINQUENCY_DATA instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(DELINQUENCY_DATAFields.ACCT_NUM, EqualityOperatorEnum.Equal, instance.ACCT_NUM.IsValid ? instance.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for DELINQUENCY_DATA.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private DELINQUENCY_DATAList GetList(IDataReader reader) {
	    DELINQUENCY_DATAList list = new DELINQUENCY_DATAList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private DELINQUENCY_DATA GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private DELINQUENCY_DATA GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    DELINQUENCY_DATA data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public DELINQUENCY_DATA GetDataObjectFromReader(DELINQUENCY_DATA data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public DELINQUENCY_DATA GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    DELINQUENCY_DATA data = new DELINQUENCY_DATA(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public DELINQUENCY_DATA GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    DELINQUENCY_DATA data = new DELINQUENCY_DATA(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public DELINQUENCY_DATA GetDataObjectFromReader(DELINQUENCY_DATA data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.ACCT_NUM)) {
		data.ACCT_NUM = IdType.UNSET;
	    } else {
		data.ACCT_NUM = new IdType(dataReader.GetInt32(ordinals.ACCT_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.DLQ_HIST)) {
		data.DLQ_HIST = StringType.UNSET;
	    } else {
		data.DLQ_HIST = StringType.Parse(dataReader.GetString(ordinals.DLQ_HIST));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the DELINQUENCY_DATA table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(DELINQUENCY_DATA data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the DELINQUENCY_DATA table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(DELINQUENCY_DATA data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spDELINQUENCY_DATA_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(DELINQUENCY_DATAFields.DLQ_HIST, data.DLQ_HIST.IsValid ? data.DLQ_HIST.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the DELINQUENCY_DATA table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(DELINQUENCY_DATA data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the DELINQUENCY_DATA table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(DELINQUENCY_DATA data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spDELINQUENCY_DATA_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(DELINQUENCY_DATAFields.ACCT_NUM, data.ACCT_NUM.IsValid ? data.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(DELINQUENCY_DATAFields.DLQ_HIST, data.DLQ_HIST.IsValid ? data.DLQ_HIST.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the DELINQUENCY_DATA table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	public void Delete(IdType aCCT_NUM) {
	    Delete(aCCT_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the DELINQUENCY_DATA table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aCCT_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spDELINQUENCY_DATA_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(DELINQUENCY_DATAFields.ACCT_NUM, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
