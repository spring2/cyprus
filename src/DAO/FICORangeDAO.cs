using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for FICORange business entity.
    /// </summary>
    public class FICORangeDAO : Spring2.Core.DAO.SqlEntityDAO, IFICORangeDAO {
	private static IFICORangeDAO instance = new FICORangeDAO();
	public static IFICORangeDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IFICORangeDAO))) {
		    ClassRegistry.Register<IFICORangeDAO>(instance);
		}
		return ClassRegistry.Resolve<IFICORangeDAO>();
	    }
	}

	private static readonly String VIEW = "vwFICORange";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 FICORangeId;
	    public Int32 Value;
	    public Int32 Description;

	    internal ColumnOrdinals(IDataReader reader) {
		FICORangeId = reader.GetOrdinal("FICORangeId");
		Value = reader.GetOrdinal("Value");
		Description = reader.GetOrdinal("Description");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		FICORangeId = reader.GetOrdinal(prefix + "FICORangeId");
		Value = reader.GetOrdinal(prefix + "Value");
		Description = reader.GetOrdinal(prefix + "Description");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static FICORangeDAO() {
	    AddPropertyMapping("FICORangeId", @"FICORangeId");
	    AddPropertyMapping("Value", @"Value");
	    AddPropertyMapping("Description", @"Description");
	}

	private FICORangeDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all FICORange rows.
	/// </summary>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FICORangeList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of FICORange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FICORangeList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of FICORange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FICORangeList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of FICORange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FICORangeList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    FICORangeList list = new FICORangeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all FICORange rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FICORangeList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of FICORange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FICORangeList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of FICORange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FICORangeList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of FICORange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FICORange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FICORangeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    FICORangeList list = new FICORangeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a FICORange entity using it's primary key.
	/// </summary>
	/// <param name="fICORangeId">A key field.</param>
	/// <returns>A FICORange object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public FICORange Load(IdType fICORangeId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(FICORangeFields.FICORANGEID, EqualityOperatorEnum.Equal, fICORangeId.IsValid ? fICORangeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(FICORange instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(FICORangeFields.FICORANGEID, EqualityOperatorEnum.Equal, instance.FICORangeId.IsValid ? instance.FICORangeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for FICORange.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private FICORangeList GetList(IDataReader reader) {
	    FICORangeList list = new FICORangeList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private FICORange GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private FICORange GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    FICORange data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public FICORange GetDataObjectFromReader(FICORange data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public FICORange GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    FICORange data = new FICORange(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public FICORange GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    FICORange data = new FICORange(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public FICORange GetDataObjectFromReader(FICORange data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.FICORangeId)) {
		data.FICORangeId = IdType.UNSET;
	    } else {
		data.FICORangeId = new IdType(dataReader.GetInt32(ordinals.FICORangeId));
	    }
	    if (dataReader.IsDBNull(ordinals.Value)) {
		data.Value = IntegerType.UNSET;
	    } else {
		data.Value = new IntegerType(dataReader.GetInt32(ordinals.Value));
	    }
	    if (dataReader.IsDBNull(ordinals.Description)) {
		data.Description = StringType.UNSET;
	    } else {
		data.Description = StringType.Parse(dataReader.GetString(ordinals.Description));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the FICORange table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(FICORange data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the FICORange table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(FICORange data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFICORange_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(FICORangeFields.VALUE, data.Value.IsValid ? data.Value.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FICORangeFields.DESCRIPTION, data.Description.IsValid ? data.Description.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the FICORange table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(FICORange data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the FICORange table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(FICORange data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFICORange_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(FICORangeFields.FICORANGEID, data.FICORangeId.IsValid ? data.FICORangeId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FICORangeFields.VALUE, data.Value.IsValid ? data.Value.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FICORangeFields.DESCRIPTION, data.Description.IsValid ? data.Description.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the FICORange table by FICORangeId.
	/// </summary>
	/// <param name="fICORangeId">A key field.</param>
	public void Delete(IdType fICORangeId) {
	    Delete(fICORangeId, null);
	}

	/// <summary>
	/// Deletes a record from the FICORange table by FICORangeId.
	/// </summary>
	/// <param name="fICORangeId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType fICORangeId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFICORange_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(FICORangeFields.FICORANGEID, fICORangeId.IsValid ? fICORangeId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
