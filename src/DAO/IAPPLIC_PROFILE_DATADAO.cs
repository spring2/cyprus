using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for APPLIC_PROFILE_DATA business entity.
    /// </summary>
    public interface IAPPLIC_PROFILE_DATADAO {
	/// <summary>
	/// Returns a list of all APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList();

	/// <summary>
	/// Returns a filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of APPLIC_PROFILE_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of APPLIC_PROFILE_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	APPLIC_PROFILE_DATAList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a APPLIC_PROFILE_DATA entity using it's primary key.
	/// </summary>
	/// <param name="aPPLIC_NUM">A key field.</param>
	/// <returns>A APPLIC_PROFILE_DATA object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	APPLIC_PROFILE_DATA Load(IdType aPPLIC_NUM);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(APPLIC_PROFILE_DATA instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_PROFILE_DATA GetDataObjectFromReader(APPLIC_PROFILE_DATA data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_PROFILE_DATA GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_PROFILE_DATA GetDataObjectFromReader(IDataReader dataReader, APPLIC_PROFILE_DATADAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	APPLIC_PROFILE_DATA GetDataObjectFromReader(APPLIC_PROFILE_DATA data, IDataReader dataReader, APPLIC_PROFILE_DATADAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the APPLIC_PROFILE_DATA table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(APPLIC_PROFILE_DATA data);


	/// <summary>
	/// Updates a record in the APPLIC_PROFILE_DATA table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(APPLIC_PROFILE_DATA data);

	/// <summary>
	/// Deletes a record from the APPLIC_PROFILE_DATA table by APPLIC_NUM.
	/// </summary>
	/// <param name="aPPLIC_NUM">A key field.</param>
	[Generate]
	void Delete(IdType aPPLIC_NUM);


	//}
    }
}
