using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for FastStartRange business entity.
    /// </summary>
    public interface IFastStartRangeDAO {
	/// <summary>
	/// Returns a list of all FastStartRange rows.
	/// </summary>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FastStartRangeList GetList();

	/// <summary>
	/// Returns a filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FastStartRangeList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of FastStartRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FastStartRangeList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FastStartRangeList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all FastStartRange rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FastStartRangeList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FastStartRangeList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of FastStartRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	FastStartRangeList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	FastStartRangeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a FastStartRange entity using it's primary key.
	/// </summary>
	/// <param name="fastStartRangeId">A key field.</param>
	/// <returns>A FastStartRange object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	FastStartRange Load(IdType fastStartRangeId);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(FastStartRange instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FastStartRange GetDataObjectFromReader(FastStartRange data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FastStartRange GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FastStartRange GetDataObjectFromReader(IDataReader dataReader, FastStartRangeDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	FastStartRange GetDataObjectFromReader(FastStartRange data, IDataReader dataReader, FastStartRangeDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the FastStartRange table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(FastStartRange data);


	/// <summary>
	/// Updates a record in the FastStartRange table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(FastStartRange data);

	/// <summary>
	/// Deletes a record from the FastStartRange table by FastStartRangeId.
	/// </summary>
	/// <param name="fastStartRangeId">A key field.</param>
	[Generate]
	void Delete(IdType fastStartRangeId);


	//}
    }
}
