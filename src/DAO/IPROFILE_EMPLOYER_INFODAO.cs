using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for PROFILE_EMPLOYER_INFO business entity.
    /// </summary>
    public interface IPROFILE_EMPLOYER_INFODAO {
	/// <summary>
	/// Returns a list of all PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList();

	/// <summary>
	/// Returns a filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of PROFILE_EMPLOYER_INFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of PROFILE_EMPLOYER_INFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a PROFILE_EMPLOYER_INFO entity using it's primary key.
	/// </summary>
	/// <param name="rECORD_KEY">A key field.</param>
	/// <returns>A PROFILE_EMPLOYER_INFO object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	PROFILE_EMPLOYER_INFO Load(IdType rECORD_KEY);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(PROFILE_EMPLOYER_INFO instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	PROFILE_EMPLOYER_INFO GetDataObjectFromReader(PROFILE_EMPLOYER_INFO data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	PROFILE_EMPLOYER_INFO GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	PROFILE_EMPLOYER_INFO GetDataObjectFromReader(IDataReader dataReader, PROFILE_EMPLOYER_INFODAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	PROFILE_EMPLOYER_INFO GetDataObjectFromReader(PROFILE_EMPLOYER_INFO data, IDataReader dataReader, PROFILE_EMPLOYER_INFODAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the PROFILE_EMPLOYER_INFO table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(PROFILE_EMPLOYER_INFO data);


	/// <summary>
	/// Updates a record in the PROFILE_EMPLOYER_INFO table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(PROFILE_EMPLOYER_INFO data);

	/// <summary>
	/// Deletes a record from the PROFILE_EMPLOYER_INFO table by RECORD_KEY.
	/// </summary>
	/// <param name="rECORD_KEY">A key field.</param>
	[Generate]
	void Delete(IdType rECORD_KEY);


	//}
    }
}
