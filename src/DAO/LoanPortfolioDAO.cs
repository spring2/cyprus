using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for LoanPortfolio business entity.
    /// </summary>
    public class LoanPortfolioDAO : Spring2.Core.DAO.SqlEntityDAO, ILoanPortfolioDAO {
	private static ILoanPortfolioDAO instance = new LoanPortfolioDAO();
	public static ILoanPortfolioDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(ILoanPortfolioDAO))) {
		    ClassRegistry.Register<ILoanPortfolioDAO>(instance);
		}
		return ClassRegistry.Resolve<ILoanPortfolioDAO>();
	    }
	}

	private static readonly String VIEW = "vwLoanPortfolio";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 LoanPortfolio_Id;
	    public Int32 UpDated;
	    public Int32 EOMDate;
	    public Int32 AccountID;
	    public Int32 Member;
	    public Int32 MClass;
	    public Int32 Ltype;
	    public Int32 LtypeSub;
	    public Int32 Product;
	    public Int32 LoanDesc;
	    public Int32 LoanDate;
	    public Int32 LoanDay;
	    public Int32 LoanMonth;
	    public Int32 LoanYear;
	    public Int32 FirstPmtDate;
	    public Int32 Status;
	    public Int32 RptCode;
	    public Int32 FundedBr;
	    public Int32 LoadedBr;
	    public Int32 FICO;
	    public Int32 FICORange;
	    public Int32 FastStart;
	    public Int32 FastStartRange;
	    public Int32 Bankruptcy;
	    public Int32 BankruptcyRange;
	    public Int32 MembershipDate;
	    public Int32 NewMember;
	    public Int32 RiskLev;
	    public Int32 Traffic;
	    public Int32 TrafficColor;
	    public Int32 MemAge;
	    public Int32 MemSex;
	    public Int32 PmtFreq;
	    public Int32 OrigTerm;
	    public Int32 OrigRate;
	    public Int32 OrigPmt;
	    public Int32 OrigAmount;
	    public Int32 OrigCrLim;
	    public Int32 CurTerm;
	    public Int32 CurRate;
	    public Int32 CurPmt;
	    public Int32 CurBal;
	    public Int32 CurCrLim;
	    public Int32 BalloonDate;
	    public Int32 MatDate;
	    public Int32 InsCode;
	    public Int32 CollateralValue;
	    public Int32 LTV;
	    public Int32 SecType;
	    public Int32 SecDesc1;
	    public Int32 SecDesc2;
	    public Int32 DebtRatio;
	    public Int32 Dealer;
	    public Int32 DealerName;
	    public Int32 CoborComak;
	    public Int32 RelPriceGrp;
	    public Int32 RelPriceDisc;
	    public Int32 OtherDisc;
	    public Int32 DiscReason;
	    public Int32 RiskOffset;
	    public Int32 CreditType;
	    public Int32 StatedIncome;
	    public Int32 Extensions;
	    public Int32 FirstExtDate;
	    public Int32 LastExtDate;
	    public Int32 RiskLevel;
	    public Int32 RiskDate;
	    public Int32 Watch;
	    public Int32 WatchDate;
	    public Int32 WorkOut;
	    public Int32 WorkOutDate;
	    public Int32 FirstMortType;
	    public Int32 BusPerReg;
	    public Int32 BusGroup;
	    public Int32 BusDesc;
	    public Int32 BusPart;
	    public Int32 BusPartPerc;
	    public Int32 SBAguar;
	    public Int32 DeliDays;
	    public Int32 DeliSect;
	    public Int32 DeliHist;
	    public Int32 DeliHistQ1;
	    public Int32 DeliHistQ2;
	    public Int32 DeliHistQ3;
	    public Int32 DeliHistQ4;
	    public Int32 DeliHistQ5;
	    public Int32 DeliHistQ6;
	    public Int32 DeliHistQ7;
	    public Int32 DeliHistQ8;
	    public Int32 ChrgOffDate;
	    public Int32 ChrgOffMonth;
	    public Int32 ChrgOffYear;
	    public Int32 ChrgOffMnthsBF;
	    public Int32 PropType;
	    public Int32 Occupancy;
	    public Int32 OrigApprVal;
	    public Int32 OrigApprDate;
	    public Int32 CurrApprVal;
	    public Int32 CurrApprDate;
	    public Int32 FirstMortBal;
	    public Int32 FirstMortMoPyt;
	    public Int32 SecondMortBal;
	    public Int32 SecondMortPymt;
	    public Int32 TaxInsNotInc;
	    public Int32 LoadOff;
	    public Int32 ApprOff;
	    public Int32 FundingOpp;
	    public Int32 Chanel;
	    public Int32 RiskType;
	    public Int32 Gap;
	    public Int32 LoadOperator;
	    public Int32 DeliAmt;
	    public Int32 ChgOffAmt;
	    public Int32 UsSecDesc;
	    public Int32 TroubledDebt;
	    public Int32 TdrDate;
	    public Int32 JntFicoUsed;
	    public Int32 SingleOrJointLoan;
	    public Int32 ApprovingOfficerUD;
	    public Int32 InsuranceCodeDesc;
	    public Int32 GapIns;
	    public Int32 MmpIns;
	    public Int32 ReportCodeDesc;
	    public Int32 BallonPmtLoan;
	    public Int32 FixedOrVariableRateLoan;
	    public Int32 AlpsOfficer;
	    public Int32 CUDLAppNumber;
	    public Int32 FasbDefCost;
	    public Int32 FasbLtdAmortFees;
	    public Int32 InterestPriorYtd;
	    public Int32 AccruedInterest;
	    public Int32 InterestLTD;
	    public Int32 InterestYtd;
	    public Int32 PartialPayAmt;
	    public Int32 InterestUncollected;
	    public Int32 ClosedAcctDate;
	    public Int32 LastTranDate;
	    public Int32 NextDueDate;
	    public Int32 InsuranceCode;
	    public Int32 MbrAgeAtApproval;

	    internal ColumnOrdinals(IDataReader reader) {
		LoanPortfolio_Id = reader.GetOrdinal("LoanPortfolio_Id");
		UpDated = reader.GetOrdinal("UpDated");
		EOMDate = reader.GetOrdinal("EOMDate");
		AccountID = reader.GetOrdinal("AccountID");
		Member = reader.GetOrdinal("Member");
		MClass = reader.GetOrdinal("MClass");
		Ltype = reader.GetOrdinal("Ltype");
		LtypeSub = reader.GetOrdinal("LtypeSub");
		Product = reader.GetOrdinal("Product");
		LoanDesc = reader.GetOrdinal("LoanDesc");
		LoanDate = reader.GetOrdinal("LoanDate");
		LoanDay = reader.GetOrdinal("LoanDay");
		LoanMonth = reader.GetOrdinal("LoanMonth");
		LoanYear = reader.GetOrdinal("LoanYear");
		FirstPmtDate = reader.GetOrdinal("FirstPmtDate");
		Status = reader.GetOrdinal("Status");
		RptCode = reader.GetOrdinal("RptCode");
		FundedBr = reader.GetOrdinal("FundedBr");
		LoadedBr = reader.GetOrdinal("LoadedBr");
		FICO = reader.GetOrdinal("FICO");
		FICORange = reader.GetOrdinal("FICORange");
		FastStart = reader.GetOrdinal("FastStart");
		FastStartRange = reader.GetOrdinal("FastStartRange");
		Bankruptcy = reader.GetOrdinal("Bankruptcy");
		BankruptcyRange = reader.GetOrdinal("BankruptcyRange");
		MembershipDate = reader.GetOrdinal("MembershipDate");
		NewMember = reader.GetOrdinal("NewMember");
		RiskLev = reader.GetOrdinal("RiskLev");
		Traffic = reader.GetOrdinal("Traffic");
		TrafficColor = reader.GetOrdinal("TrafficColor");
		MemAge = reader.GetOrdinal("MemAge");
		MemSex = reader.GetOrdinal("MemSex");
		PmtFreq = reader.GetOrdinal("PmtFreq");
		OrigTerm = reader.GetOrdinal("OrigTerm");
		OrigRate = reader.GetOrdinal("OrigRate");
		OrigPmt = reader.GetOrdinal("OrigPmt");
		OrigAmount = reader.GetOrdinal("OrigAmount");
		OrigCrLim = reader.GetOrdinal("OrigCrLim");
		CurTerm = reader.GetOrdinal("CurTerm");
		CurRate = reader.GetOrdinal("CurRate");
		CurPmt = reader.GetOrdinal("CurPmt");
		CurBal = reader.GetOrdinal("CurBal");
		CurCrLim = reader.GetOrdinal("CurCrLim");
		BalloonDate = reader.GetOrdinal("BalloonDate");
		MatDate = reader.GetOrdinal("MatDate");
		InsCode = reader.GetOrdinal("InsCode");
		CollateralValue = reader.GetOrdinal("CollateralValue");
		LTV = reader.GetOrdinal("LTV");
		SecType = reader.GetOrdinal("SecType");
		SecDesc1 = reader.GetOrdinal("SecDesc1");
		SecDesc2 = reader.GetOrdinal("SecDesc2");
		DebtRatio = reader.GetOrdinal("DebtRatio");
		Dealer = reader.GetOrdinal("Dealer");
		DealerName = reader.GetOrdinal("DealerName");
		CoborComak = reader.GetOrdinal("CoborComak");
		RelPriceGrp = reader.GetOrdinal("RelPriceGrp");
		RelPriceDisc = reader.GetOrdinal("RelPriceDisc");
		OtherDisc = reader.GetOrdinal("OtherDisc");
		DiscReason = reader.GetOrdinal("DiscReason");
		RiskOffset = reader.GetOrdinal("RiskOffset");
		CreditType = reader.GetOrdinal("CreditType");
		StatedIncome = reader.GetOrdinal("StatedIncome");
		Extensions = reader.GetOrdinal("Extensions");
		FirstExtDate = reader.GetOrdinal("FirstExtDate");
		LastExtDate = reader.GetOrdinal("LastExtDate");
		RiskLevel = reader.GetOrdinal("RiskLevel");
		RiskDate = reader.GetOrdinal("RiskDate");
		Watch = reader.GetOrdinal("Watch");
		WatchDate = reader.GetOrdinal("WatchDate");
		WorkOut = reader.GetOrdinal("WorkOut");
		WorkOutDate = reader.GetOrdinal("WorkOutDate");
		FirstMortType = reader.GetOrdinal("FirstMortType");
		BusPerReg = reader.GetOrdinal("BusPerReg");
		BusGroup = reader.GetOrdinal("BusGroup");
		BusDesc = reader.GetOrdinal("BusDesc");
		BusPart = reader.GetOrdinal("BusPart");
		BusPartPerc = reader.GetOrdinal("BusPartPerc");
		SBAguar = reader.GetOrdinal("SBAguar");
		DeliDays = reader.GetOrdinal("DeliDays");
		DeliSect = reader.GetOrdinal("DeliSect");
		DeliHist = reader.GetOrdinal("DeliHist");
		DeliHistQ1 = reader.GetOrdinal("DeliHistQ1");
		DeliHistQ2 = reader.GetOrdinal("DeliHistQ2");
		DeliHistQ3 = reader.GetOrdinal("DeliHistQ3");
		DeliHistQ4 = reader.GetOrdinal("DeliHistQ4");
		DeliHistQ5 = reader.GetOrdinal("DeliHistQ5");
		DeliHistQ6 = reader.GetOrdinal("DeliHistQ6");
		DeliHistQ7 = reader.GetOrdinal("DeliHistQ7");
		DeliHistQ8 = reader.GetOrdinal("DeliHistQ8");
		ChrgOffDate = reader.GetOrdinal("ChrgOffDate");
		ChrgOffMonth = reader.GetOrdinal("ChrgOffMonth");
		ChrgOffYear = reader.GetOrdinal("ChrgOffYear");
		ChrgOffMnthsBF = reader.GetOrdinal("ChrgOffMnthsBF");
		PropType = reader.GetOrdinal("PropType");
		Occupancy = reader.GetOrdinal("Occupancy");
		OrigApprVal = reader.GetOrdinal("OrigApprVal");
		OrigApprDate = reader.GetOrdinal("OrigApprDate");
		CurrApprVal = reader.GetOrdinal("CurrApprVal");
		CurrApprDate = reader.GetOrdinal("CurrApprDate");
		FirstMortBal = reader.GetOrdinal("FirstMortBal");
		FirstMortMoPyt = reader.GetOrdinal("FirstMortMoPyt");
		SecondMortBal = reader.GetOrdinal("SecondMortBal");
		SecondMortPymt = reader.GetOrdinal("SecondMortPymt");
		TaxInsNotInc = reader.GetOrdinal("TaxInsNotInc");
		LoadOff = reader.GetOrdinal("LoadOff");
		ApprOff = reader.GetOrdinal("ApprOff");
		FundingOpp = reader.GetOrdinal("FundingOpp");
		Chanel = reader.GetOrdinal("Chanel");
		RiskType = reader.GetOrdinal("RiskType");
		Gap = reader.GetOrdinal("Gap");
		LoadOperator = reader.GetOrdinal("LoadOperator");
		DeliAmt = reader.GetOrdinal("DeliAmt");
		ChgOffAmt = reader.GetOrdinal("ChgOffAmt");
		UsSecDesc = reader.GetOrdinal("UsSecDesc");
		TroubledDebt = reader.GetOrdinal("TroubledDebt");
		TdrDate = reader.GetOrdinal("TdrDate");
		JntFicoUsed = reader.GetOrdinal("JntFicoUsed");
		SingleOrJointLoan = reader.GetOrdinal("SingleOrJointLoan");
		ApprovingOfficerUD = reader.GetOrdinal("ApprovingOfficerUD");
		InsuranceCodeDesc = reader.GetOrdinal("InsuranceCodeDesc");
		GapIns = reader.GetOrdinal("GapIns");
		MmpIns = reader.GetOrdinal("MmpIns");
		ReportCodeDesc = reader.GetOrdinal("ReportCodeDesc");
		BallonPmtLoan = reader.GetOrdinal("BallonPmtLoan");
		FixedOrVariableRateLoan = reader.GetOrdinal("FixedOrVariableRateLoan");
		AlpsOfficer = reader.GetOrdinal("AlpsOfficer");
		CUDLAppNumber = reader.GetOrdinal("CUDLAppNumber");
		FasbDefCost = reader.GetOrdinal("FasbDefCost");
		FasbLtdAmortFees = reader.GetOrdinal("FasbLtdAmortFees");
		InterestPriorYtd = reader.GetOrdinal("InterestPriorYtd");
		AccruedInterest = reader.GetOrdinal("AccruedInterest");
		InterestLTD = reader.GetOrdinal("InterestLTD");
		InterestYtd = reader.GetOrdinal("InterestYtd");
		PartialPayAmt = reader.GetOrdinal("PartialPayAmt");
		InterestUncollected = reader.GetOrdinal("InterestUncollected");
		ClosedAcctDate = reader.GetOrdinal("ClosedAcctDate");
		LastTranDate = reader.GetOrdinal("LastTranDate");
		NextDueDate = reader.GetOrdinal("NextDueDate");
		InsuranceCode = reader.GetOrdinal("InsuranceCode");
		MbrAgeAtApproval = reader.GetOrdinal("MbrAgeAtApproval");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		LoanPortfolio_Id = reader.GetOrdinal(prefix + "LoanPortfolio_Id");
		UpDated = reader.GetOrdinal(prefix + "UpDated");
		EOMDate = reader.GetOrdinal(prefix + "EOMDate");
		AccountID = reader.GetOrdinal(prefix + "AccountID");
		Member = reader.GetOrdinal(prefix + "Member");
		MClass = reader.GetOrdinal(prefix + "MClass");
		Ltype = reader.GetOrdinal(prefix + "Ltype");
		LtypeSub = reader.GetOrdinal(prefix + "LtypeSub");
		Product = reader.GetOrdinal(prefix + "Product");
		LoanDesc = reader.GetOrdinal(prefix + "LoanDesc");
		LoanDate = reader.GetOrdinal(prefix + "LoanDate");
		LoanDay = reader.GetOrdinal(prefix + "LoanDay");
		LoanMonth = reader.GetOrdinal(prefix + "LoanMonth");
		LoanYear = reader.GetOrdinal(prefix + "LoanYear");
		FirstPmtDate = reader.GetOrdinal(prefix + "FirstPmtDate");
		Status = reader.GetOrdinal(prefix + "Status");
		RptCode = reader.GetOrdinal(prefix + "RptCode");
		FundedBr = reader.GetOrdinal(prefix + "FundedBr");
		LoadedBr = reader.GetOrdinal(prefix + "LoadedBr");
		FICO = reader.GetOrdinal(prefix + "FICO");
		FICORange = reader.GetOrdinal(prefix + "FICORange");
		FastStart = reader.GetOrdinal(prefix + "FastStart");
		FastStartRange = reader.GetOrdinal(prefix + "FastStartRange");
		Bankruptcy = reader.GetOrdinal(prefix + "Bankruptcy");
		BankruptcyRange = reader.GetOrdinal(prefix + "BankruptcyRange");
		MembershipDate = reader.GetOrdinal(prefix + "MembershipDate");
		NewMember = reader.GetOrdinal(prefix + "NewMember");
		RiskLev = reader.GetOrdinal(prefix + "RiskLev");
		Traffic = reader.GetOrdinal(prefix + "Traffic");
		TrafficColor = reader.GetOrdinal(prefix + "TrafficColor");
		MemAge = reader.GetOrdinal(prefix + "MemAge");
		MemSex = reader.GetOrdinal(prefix + "MemSex");
		PmtFreq = reader.GetOrdinal(prefix + "PmtFreq");
		OrigTerm = reader.GetOrdinal(prefix + "OrigTerm");
		OrigRate = reader.GetOrdinal(prefix + "OrigRate");
		OrigPmt = reader.GetOrdinal(prefix + "OrigPmt");
		OrigAmount = reader.GetOrdinal(prefix + "OrigAmount");
		OrigCrLim = reader.GetOrdinal(prefix + "OrigCrLim");
		CurTerm = reader.GetOrdinal(prefix + "CurTerm");
		CurRate = reader.GetOrdinal(prefix + "CurRate");
		CurPmt = reader.GetOrdinal(prefix + "CurPmt");
		CurBal = reader.GetOrdinal(prefix + "CurBal");
		CurCrLim = reader.GetOrdinal(prefix + "CurCrLim");
		BalloonDate = reader.GetOrdinal(prefix + "BalloonDate");
		MatDate = reader.GetOrdinal(prefix + "MatDate");
		InsCode = reader.GetOrdinal(prefix + "InsCode");
		CollateralValue = reader.GetOrdinal(prefix + "CollateralValue");
		LTV = reader.GetOrdinal(prefix + "LTV");
		SecType = reader.GetOrdinal(prefix + "SecType");
		SecDesc1 = reader.GetOrdinal(prefix + "SecDesc1");
		SecDesc2 = reader.GetOrdinal(prefix + "SecDesc2");
		DebtRatio = reader.GetOrdinal(prefix + "DebtRatio");
		Dealer = reader.GetOrdinal(prefix + "Dealer");
		DealerName = reader.GetOrdinal(prefix + "DealerName");
		CoborComak = reader.GetOrdinal(prefix + "CoborComak");
		RelPriceGrp = reader.GetOrdinal(prefix + "RelPriceGrp");
		RelPriceDisc = reader.GetOrdinal(prefix + "RelPriceDisc");
		OtherDisc = reader.GetOrdinal(prefix + "OtherDisc");
		DiscReason = reader.GetOrdinal(prefix + "DiscReason");
		RiskOffset = reader.GetOrdinal(prefix + "RiskOffset");
		CreditType = reader.GetOrdinal(prefix + "CreditType");
		StatedIncome = reader.GetOrdinal(prefix + "StatedIncome");
		Extensions = reader.GetOrdinal(prefix + "Extensions");
		FirstExtDate = reader.GetOrdinal(prefix + "FirstExtDate");
		LastExtDate = reader.GetOrdinal(prefix + "LastExtDate");
		RiskLevel = reader.GetOrdinal(prefix + "RiskLevel");
		RiskDate = reader.GetOrdinal(prefix + "RiskDate");
		Watch = reader.GetOrdinal(prefix + "Watch");
		WatchDate = reader.GetOrdinal(prefix + "WatchDate");
		WorkOut = reader.GetOrdinal(prefix + "WorkOut");
		WorkOutDate = reader.GetOrdinal(prefix + "WorkOutDate");
		FirstMortType = reader.GetOrdinal(prefix + "FirstMortType");
		BusPerReg = reader.GetOrdinal(prefix + "BusPerReg");
		BusGroup = reader.GetOrdinal(prefix + "BusGroup");
		BusDesc = reader.GetOrdinal(prefix + "BusDesc");
		BusPart = reader.GetOrdinal(prefix + "BusPart");
		BusPartPerc = reader.GetOrdinal(prefix + "BusPartPerc");
		SBAguar = reader.GetOrdinal(prefix + "SBAguar");
		DeliDays = reader.GetOrdinal(prefix + "DeliDays");
		DeliSect = reader.GetOrdinal(prefix + "DeliSect");
		DeliHist = reader.GetOrdinal(prefix + "DeliHist");
		DeliHistQ1 = reader.GetOrdinal(prefix + "DeliHistQ1");
		DeliHistQ2 = reader.GetOrdinal(prefix + "DeliHistQ2");
		DeliHistQ3 = reader.GetOrdinal(prefix + "DeliHistQ3");
		DeliHistQ4 = reader.GetOrdinal(prefix + "DeliHistQ4");
		DeliHistQ5 = reader.GetOrdinal(prefix + "DeliHistQ5");
		DeliHistQ6 = reader.GetOrdinal(prefix + "DeliHistQ6");
		DeliHistQ7 = reader.GetOrdinal(prefix + "DeliHistQ7");
		DeliHistQ8 = reader.GetOrdinal(prefix + "DeliHistQ8");
		ChrgOffDate = reader.GetOrdinal(prefix + "ChrgOffDate");
		ChrgOffMonth = reader.GetOrdinal(prefix + "ChrgOffMonth");
		ChrgOffYear = reader.GetOrdinal(prefix + "ChrgOffYear");
		ChrgOffMnthsBF = reader.GetOrdinal(prefix + "ChrgOffMnthsBF");
		PropType = reader.GetOrdinal(prefix + "PropType");
		Occupancy = reader.GetOrdinal(prefix + "Occupancy");
		OrigApprVal = reader.GetOrdinal(prefix + "OrigApprVal");
		OrigApprDate = reader.GetOrdinal(prefix + "OrigApprDate");
		CurrApprVal = reader.GetOrdinal(prefix + "CurrApprVal");
		CurrApprDate = reader.GetOrdinal(prefix + "CurrApprDate");
		FirstMortBal = reader.GetOrdinal(prefix + "FirstMortBal");
		FirstMortMoPyt = reader.GetOrdinal(prefix + "FirstMortMoPyt");
		SecondMortBal = reader.GetOrdinal(prefix + "SecondMortBal");
		SecondMortPymt = reader.GetOrdinal(prefix + "SecondMortPymt");
		TaxInsNotInc = reader.GetOrdinal(prefix + "TaxInsNotInc");
		LoadOff = reader.GetOrdinal(prefix + "LoadOff");
		ApprOff = reader.GetOrdinal(prefix + "ApprOff");
		FundingOpp = reader.GetOrdinal(prefix + "FundingOpp");
		Chanel = reader.GetOrdinal(prefix + "Chanel");
		RiskType = reader.GetOrdinal(prefix + "RiskType");
		Gap = reader.GetOrdinal(prefix + "Gap");
		LoadOperator = reader.GetOrdinal(prefix + "LoadOperator");
		DeliAmt = reader.GetOrdinal(prefix + "DeliAmt");
		ChgOffAmt = reader.GetOrdinal(prefix + "ChgOffAmt");
		UsSecDesc = reader.GetOrdinal(prefix + "UsSecDesc");
		TroubledDebt = reader.GetOrdinal(prefix + "TroubledDebt");
		TdrDate = reader.GetOrdinal(prefix + "TdrDate");
		JntFicoUsed = reader.GetOrdinal(prefix + "JntFicoUsed");
		SingleOrJointLoan = reader.GetOrdinal(prefix + "SingleOrJointLoan");
		ApprovingOfficerUD = reader.GetOrdinal(prefix + "ApprovingOfficerUD");
		InsuranceCodeDesc = reader.GetOrdinal(prefix + "InsuranceCodeDesc");
		GapIns = reader.GetOrdinal(prefix + "GapIns");
		MmpIns = reader.GetOrdinal(prefix + "MmpIns");
		ReportCodeDesc = reader.GetOrdinal(prefix + "ReportCodeDesc");
		BallonPmtLoan = reader.GetOrdinal(prefix + "BallonPmtLoan");
		FixedOrVariableRateLoan = reader.GetOrdinal(prefix + "FixedOrVariableRateLoan");
		AlpsOfficer = reader.GetOrdinal(prefix + "AlpsOfficer");
		CUDLAppNumber = reader.GetOrdinal(prefix + "CUDLAppNumber");
		FasbDefCost = reader.GetOrdinal(prefix + "FasbDefCost");
		FasbLtdAmortFees = reader.GetOrdinal(prefix + "FasbLtdAmortFees");
		InterestPriorYtd = reader.GetOrdinal(prefix + "InterestPriorYtd");
		AccruedInterest = reader.GetOrdinal(prefix + "AccruedInterest");
		InterestLTD = reader.GetOrdinal(prefix + "InterestLTD");
		InterestYtd = reader.GetOrdinal(prefix + "InterestYtd");
		PartialPayAmt = reader.GetOrdinal(prefix + "PartialPayAmt");
		InterestUncollected = reader.GetOrdinal(prefix + "InterestUncollected");
		ClosedAcctDate = reader.GetOrdinal(prefix + "ClosedAcctDate");
		LastTranDate = reader.GetOrdinal(prefix + "LastTranDate");
		NextDueDate = reader.GetOrdinal(prefix + "NextDueDate");
		InsuranceCode = reader.GetOrdinal(prefix + "InsuranceCode");
		MbrAgeAtApproval = reader.GetOrdinal(prefix + "MbrAgeAtApproval");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static LoanPortfolioDAO() {
	    AddPropertyMapping("LoanPortfolioId", @"LoanPortfolio_Id");
	    AddPropertyMapping("UpDated", @"UpDated");
	    AddPropertyMapping("EOMDate", @"EOMDate");
	    AddPropertyMapping("AccountID", @"AccountID");
	    AddPropertyMapping("Member", @"Member");
	    AddPropertyMapping("MClass", @"MClass");
	    AddPropertyMapping("Ltype", @"Ltype");
	    AddPropertyMapping("LtypeSub", @"LtypeSub");
	    AddPropertyMapping("Product", @"Product");
	    AddPropertyMapping("LoanDesc", @"LoanDesc");
	    AddPropertyMapping("LoanDate", @"LoanDate");
	    AddPropertyMapping("LoanDay", @"LoanDay");
	    AddPropertyMapping("LoanMonth", @"LoanMonth");
	    AddPropertyMapping("LoanYear", @"LoanYear");
	    AddPropertyMapping("FirstPmtDate", @"FirstPmtDate");
	    AddPropertyMapping("Status", @"Status");
	    AddPropertyMapping("RptCode", @"RptCode");
	    AddPropertyMapping("FundedBr", @"FundedBr");
	    AddPropertyMapping("LoadedBr", @"LoadedBr");
	    AddPropertyMapping("FICO", @"FICO");
	    AddPropertyMapping("FICORange", @"FICORange");
	    AddPropertyMapping("FastStart", @"FastStart");
	    AddPropertyMapping("FastStartRange", @"FastStartRange");
	    AddPropertyMapping("Bankruptcy", @"Bankruptcy");
	    AddPropertyMapping("BankruptcyRange", @"BankruptcyRange");
	    AddPropertyMapping("MembershipDate", @"MembershipDate");
	    AddPropertyMapping("NewMember", @"NewMember");
	    AddPropertyMapping("RiskLev", @"RiskLev");
	    AddPropertyMapping("Traffic", @"Traffic");
	    AddPropertyMapping("TrafficColor", @"TrafficColor");
	    AddPropertyMapping("MemAge", @"MemAge");
	    AddPropertyMapping("MemSex", @"MemSex");
	    AddPropertyMapping("PmtFreq", @"PmtFreq");
	    AddPropertyMapping("OrigTerm", @"OrigTerm");
	    AddPropertyMapping("OrigRate", @"OrigRate");
	    AddPropertyMapping("OrigPmt", @"OrigPmt");
	    AddPropertyMapping("OrigAmount", @"OrigAmount");
	    AddPropertyMapping("OrigCrLim", @"OrigCrLim");
	    AddPropertyMapping("CurTerm", @"CurTerm");
	    AddPropertyMapping("CurRate", @"CurRate");
	    AddPropertyMapping("CurPmt", @"CurPmt");
	    AddPropertyMapping("CurBal", @"CurBal");
	    AddPropertyMapping("CurCrLim", @"CurCrLim");
	    AddPropertyMapping("BalloonDate", @"BalloonDate");
	    AddPropertyMapping("MatDate", @"MatDate");
	    AddPropertyMapping("InsCode", @"InsCode");
	    AddPropertyMapping("CollateralValue", @"CollateralValue");
	    AddPropertyMapping("LTV", @"LTV");
	    AddPropertyMapping("SecType", @"SecType");
	    AddPropertyMapping("SecDesc1", @"SecDesc1");
	    AddPropertyMapping("SecDesc2", @"SecDesc2");
	    AddPropertyMapping("DebtRatio", @"DebtRatio");
	    AddPropertyMapping("Dealer", @"Dealer");
	    AddPropertyMapping("DealerName", @"DealerName");
	    AddPropertyMapping("CoborComak", @"CoborComak");
	    AddPropertyMapping("RelPriceGrp", @"RelPriceGrp");
	    AddPropertyMapping("RelPriceDisc", @"RelPriceDisc");
	    AddPropertyMapping("OtherDisc", @"OtherDisc");
	    AddPropertyMapping("DiscReason", @"DiscReason");
	    AddPropertyMapping("RiskOffset", @"RiskOffset");
	    AddPropertyMapping("CreditType", @"CreditType");
	    AddPropertyMapping("StatedIncome", @"StatedIncome");
	    AddPropertyMapping("Extensions", @"Extensions");
	    AddPropertyMapping("FirstExtDate", @"FirstExtDate");
	    AddPropertyMapping("LastExtDate", @"LastExtDate");
	    AddPropertyMapping("RiskLevel", @"RiskLevel");
	    AddPropertyMapping("RiskDate", @"RiskDate");
	    AddPropertyMapping("Watch", @"Watch");
	    AddPropertyMapping("WatchDate", @"WatchDate");
	    AddPropertyMapping("WorkOut", @"WorkOut");
	    AddPropertyMapping("WorkOutDate", @"WorkOutDate");
	    AddPropertyMapping("FirstMortType", @"FirstMortType");
	    AddPropertyMapping("BusPerReg", @"BusPerReg");
	    AddPropertyMapping("BusGroup", @"BusGroup");
	    AddPropertyMapping("BusDesc", @"BusDesc");
	    AddPropertyMapping("BusPart", @"BusPart");
	    AddPropertyMapping("BusPartPerc", @"BusPartPerc");
	    AddPropertyMapping("SBAguar", @"SBAguar");
	    AddPropertyMapping("DeliDays", @"DeliDays");
	    AddPropertyMapping("DeliSect", @"DeliSect");
	    AddPropertyMapping("DeliHist", @"DeliHist");
	    AddPropertyMapping("DeliHistQ1", @"DeliHistQ1");
	    AddPropertyMapping("DeliHistQ2", @"DeliHistQ2");
	    AddPropertyMapping("DeliHistQ3", @"DeliHistQ3");
	    AddPropertyMapping("DeliHistQ4", @"DeliHistQ4");
	    AddPropertyMapping("DeliHistQ5", @"DeliHistQ5");
	    AddPropertyMapping("DeliHistQ6", @"DeliHistQ6");
	    AddPropertyMapping("DeliHistQ7", @"DeliHistQ7");
	    AddPropertyMapping("DeliHistQ8", @"DeliHistQ8");
	    AddPropertyMapping("ChrgOffDate", @"ChrgOffDate");
	    AddPropertyMapping("ChrgOffMonth", @"ChrgOffMonth");
	    AddPropertyMapping("ChrgOffYear", @"ChrgOffYear");
	    AddPropertyMapping("ChrgOffMnthsBF", @"ChrgOffMnthsBF");
	    AddPropertyMapping("PropType", @"PropType");
	    AddPropertyMapping("Occupancy", @"Occupancy");
	    AddPropertyMapping("OrigApprVal", @"OrigApprVal");
	    AddPropertyMapping("OrigApprDate", @"OrigApprDate");
	    AddPropertyMapping("CurrApprVal", @"CurrApprVal");
	    AddPropertyMapping("CurrApprDate", @"CurrApprDate");
	    AddPropertyMapping("FirstMortBal", @"FirstMortBal");
	    AddPropertyMapping("FirstMortMoPyt", @"FirstMortMoPyt");
	    AddPropertyMapping("SecondMortBal", @"SecondMortBal");
	    AddPropertyMapping("SecondMortPymt", @"SecondMortPymt");
	    AddPropertyMapping("TaxInsNotInc", @"TaxInsNotInc");
	    AddPropertyMapping("LoadOff", @"LoadOff");
	    AddPropertyMapping("ApprOff", @"ApprOff");
	    AddPropertyMapping("FundingOpp", @"FundingOpp");
	    AddPropertyMapping("Chanel", @"Chanel");
	    AddPropertyMapping("RiskType", @"RiskType");
	    AddPropertyMapping("Gap", @"Gap");
	    AddPropertyMapping("LoadOperator", @"LoadOperator");
	    AddPropertyMapping("DeliAmt", @"DeliAmt");
	    AddPropertyMapping("ChgOffAmt", @"ChgOffAmt");
	    AddPropertyMapping("UsSecDesc", @"UsSecDesc");
	    AddPropertyMapping("TroubledDebt", @"TroubledDebt");
	    AddPropertyMapping("TdrDate", @"TdrDate");
	    AddPropertyMapping("JntFicoUsed", @"JntFicoUsed");
	    AddPropertyMapping("SingleOrJointLoan", @"SingleOrJointLoan");
	    AddPropertyMapping("ApprovingOfficerUD", @"ApprovingOfficerUD");
	    AddPropertyMapping("InsuranceCodeDesc", @"InsuranceCodeDesc");
	    AddPropertyMapping("GapIns", @"GapIns");
	    AddPropertyMapping("MmpIns", @"MmpIns");
	    AddPropertyMapping("ReportCodeDesc", @"ReportCodeDesc");
	    AddPropertyMapping("BallonPmtLoan", @"BallonPmtLoan");
	    AddPropertyMapping("FixedOrVariableRateLoan", @"FixedOrVariableRateLoan");
	    AddPropertyMapping("AlpsOfficer", @"AlpsOfficer");
	    AddPropertyMapping("CUDLAppNumber", @"CUDLAppNumber");
	    AddPropertyMapping("FasbDefCost", @"FasbDefCost");
	    AddPropertyMapping("FasbLtdAmortFees", @"FasbLtdAmortFees");
	    AddPropertyMapping("InterestPriorYtd", @"InterestPriorYtd");
	    AddPropertyMapping("AccruedInterest", @"AccruedInterest");
	    AddPropertyMapping("InterestLTD", @"InterestLTD");
	    AddPropertyMapping("InterestYtd", @"InterestYtd");
	    AddPropertyMapping("PartialPayAmt", @"PartialPayAmt");
	    AddPropertyMapping("InterestUncollected", @"InterestUncollected");
	    AddPropertyMapping("ClosedAcctDate", @"ClosedAcctDate");
	    AddPropertyMapping("LastTranDate", @"LastTranDate");
	    AddPropertyMapping("NextDueDate", @"NextDueDate");
	    AddPropertyMapping("InsuranceCode", @"InsuranceCode");
	    AddPropertyMapping("MbrAgeAtApproval", @"MbrAgeAtApproval");
	}

	private LoanPortfolioDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all LoanPortfolio rows.
	/// </summary>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanPortfolioList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanPortfolioList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of LoanPortfolio rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanPortfolioList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanPortfolioList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    LoanPortfolioList list = new LoanPortfolioList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all LoanPortfolio rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanPortfolioList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanPortfolioList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of LoanPortfolio rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public LoanPortfolioList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of LoanPortfolio rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of LoanPortfolio objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public LoanPortfolioList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    LoanPortfolioList list = new LoanPortfolioList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a LoanPortfolio entity using it's primary key.
	/// </summary>
	/// <param name="loanPortfolio_Id">A key field.</param>
	/// <returns>A LoanPortfolio object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public LoanPortfolio Load(IdType loanPortfolioId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(LoanPortfolioFields.LOANPORTFOLIOID, EqualityOperatorEnum.Equal, loanPortfolioId.IsValid ? loanPortfolioId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(LoanPortfolio instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(LoanPortfolioFields.LOANPORTFOLIOID, EqualityOperatorEnum.Equal, instance.LoanPortfolioId.IsValid ? instance.LoanPortfolioId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for LoanPortfolio.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private LoanPortfolioList GetList(IDataReader reader) {
	    LoanPortfolioList list = new LoanPortfolioList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private LoanPortfolio GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private LoanPortfolio GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    LoanPortfolio data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public LoanPortfolio GetDataObjectFromReader(LoanPortfolio data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public LoanPortfolio GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    LoanPortfolio data = new LoanPortfolio(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public LoanPortfolio GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    LoanPortfolio data = new LoanPortfolio(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public LoanPortfolio GetDataObjectFromReader(LoanPortfolio data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.LoanPortfolio_Id)) {
		data.LoanPortfolioId = IdType.UNSET;
	    } else {
		data.LoanPortfolioId = new IdType(dataReader.GetInt32(ordinals.LoanPortfolio_Id));
	    }
	    if (dataReader.IsDBNull(ordinals.UpDated)) {
		data.UpDated = DateTimeType.UNSET;
	    } else {
		data.UpDated = new DateTimeType(dataReader.GetDateTime(ordinals.UpDated));
	    }
	    if (dataReader.IsDBNull(ordinals.EOMDate)) {
		data.EOMDate = DateType.UNSET;
	    } else {
		data.EOMDate = new DateType(dataReader.GetDateTime(ordinals.EOMDate));
	    }
	    if (dataReader.IsDBNull(ordinals.AccountID)) {
		data.AccountID = StringType.UNSET;
	    } else {
		data.AccountID = StringType.Parse(dataReader.GetString(ordinals.AccountID));
	    }
	    if (dataReader.IsDBNull(ordinals.Member)) {
		data.Member = StringType.UNSET;
	    } else {
		data.Member = StringType.Parse(dataReader.GetString(ordinals.Member));
	    }
	    if (dataReader.IsDBNull(ordinals.MClass)) {
		data.MClass = IntegerType.UNSET;
	    } else {
		data.MClass = new IntegerType(dataReader.GetInt32(ordinals.MClass));
	    }
	    if (dataReader.IsDBNull(ordinals.Ltype)) {
		data.Ltype = IntegerType.UNSET;
	    } else {
		data.Ltype = new IntegerType(dataReader.GetInt32(ordinals.Ltype));
	    }
	    if (dataReader.IsDBNull(ordinals.LtypeSub)) {
		data.LtypeSub = DecimalType.UNSET;
	    } else {
		data.LtypeSub = new DecimalType(dataReader.GetDecimal(ordinals.LtypeSub));
	    }
	    if (dataReader.IsDBNull(ordinals.Product)) {
		data.Product = IntegerType.UNSET;
	    } else {
		data.Product = new IntegerType(dataReader.GetInt32(ordinals.Product));
	    }
	    if (dataReader.IsDBNull(ordinals.LoanDesc)) {
		data.LoanDesc = StringType.UNSET;
	    } else {
		data.LoanDesc = StringType.Parse(dataReader.GetString(ordinals.LoanDesc));
	    }
	    if (dataReader.IsDBNull(ordinals.LoanDate)) {
		data.LoanDate = DateType.UNSET;
	    } else {
		data.LoanDate = new DateType(dataReader.GetDateTime(ordinals.LoanDate));
	    }
	    if (dataReader.IsDBNull(ordinals.LoanDay)) {
		data.LoanDay = StringType.UNSET;
	    } else {
		data.LoanDay = StringType.Parse(dataReader.GetString(ordinals.LoanDay));
	    }
	    if (dataReader.IsDBNull(ordinals.LoanMonth)) {
		data.LoanMonth = IntegerType.UNSET;
	    } else {
		data.LoanMonth = new IntegerType(dataReader.GetInt32(ordinals.LoanMonth));
	    }
	    if (dataReader.IsDBNull(ordinals.LoanYear)) {
		data.LoanYear = IntegerType.UNSET;
	    } else {
		data.LoanYear = new IntegerType(dataReader.GetInt32(ordinals.LoanYear));
	    }
	    if (dataReader.IsDBNull(ordinals.FirstPmtDate)) {
		data.FirstPmtDate = DateType.UNSET;
	    } else {
		data.FirstPmtDate = new DateType(dataReader.GetDateTime(ordinals.FirstPmtDate));
	    }
	    if (dataReader.IsDBNull(ordinals.Status)) {
		data.Status = StringType.UNSET;
	    } else {
		data.Status = StringType.Parse(dataReader.GetString(ordinals.Status));
	    }
	    if (dataReader.IsDBNull(ordinals.RptCode)) {
		data.RptCode = IntegerType.UNSET;
	    } else {
		data.RptCode = new IntegerType(dataReader.GetInt32(ordinals.RptCode));
	    }
	    if (dataReader.IsDBNull(ordinals.FundedBr)) {
		data.FundedBr = IntegerType.UNSET;
	    } else {
		data.FundedBr = new IntegerType(dataReader.GetInt32(ordinals.FundedBr));
	    }
	    if (dataReader.IsDBNull(ordinals.LoadedBr)) {
		data.LoadedBr = IntegerType.UNSET;
	    } else {
		data.LoadedBr = new IntegerType(dataReader.GetInt32(ordinals.LoadedBr));
	    }
	    if (dataReader.IsDBNull(ordinals.FICO)) {
		data.FICO = IntegerType.UNSET;
	    } else {
		data.FICO = new IntegerType(dataReader.GetInt32(ordinals.FICO));
	    }
	    if (dataReader.IsDBNull(ordinals.FICORange)) {
		data.FICORange = StringType.UNSET;
	    } else {
		data.FICORange = StringType.Parse(dataReader.GetString(ordinals.FICORange));
	    }
	    if (dataReader.IsDBNull(ordinals.FastStart)) {
		data.FastStart = IntegerType.UNSET;
	    } else {
		data.FastStart = new IntegerType(dataReader.GetInt32(ordinals.FastStart));
	    }
	    if (dataReader.IsDBNull(ordinals.FastStartRange)) {
		data.FastStartRange = StringType.UNSET;
	    } else {
		data.FastStartRange = StringType.Parse(dataReader.GetString(ordinals.FastStartRange));
	    }
	    if (dataReader.IsDBNull(ordinals.Bankruptcy)) {
		data.Bankruptcy = IntegerType.UNSET;
	    } else {
		data.Bankruptcy = new IntegerType(dataReader.GetInt32(ordinals.Bankruptcy));
	    }
	    if (dataReader.IsDBNull(ordinals.BankruptcyRange)) {
		data.BankruptcyRange = StringType.UNSET;
	    } else {
		data.BankruptcyRange = StringType.Parse(dataReader.GetString(ordinals.BankruptcyRange));
	    }
	    if (dataReader.IsDBNull(ordinals.MembershipDate)) {
		data.MembershipDate = DateType.UNSET;
	    } else {
		data.MembershipDate = new DateType(dataReader.GetDateTime(ordinals.MembershipDate));
	    }
	    if (dataReader.IsDBNull(ordinals.NewMember)) {
		data.NewMember = StringType.UNSET;
	    } else {
		data.NewMember = StringType.Parse(dataReader.GetString(ordinals.NewMember));
	    }
	    if (dataReader.IsDBNull(ordinals.RiskLev)) {
		data.RiskLev = StringType.UNSET;
	    } else {
		data.RiskLev = StringType.Parse(dataReader.GetString(ordinals.RiskLev));
	    }
	    if (dataReader.IsDBNull(ordinals.Traffic)) {
		data.Traffic = IntegerType.UNSET;
	    } else {
		data.Traffic = new IntegerType(dataReader.GetInt32(ordinals.Traffic));
	    }
	    if (dataReader.IsDBNull(ordinals.TrafficColor)) {
		data.TrafficColor = StringType.UNSET;
	    } else {
		data.TrafficColor = StringType.Parse(dataReader.GetString(ordinals.TrafficColor));
	    }
	    if (dataReader.IsDBNull(ordinals.MemAge)) {
		data.MemAge = IntegerType.UNSET;
	    } else {
		data.MemAge = new IntegerType(dataReader.GetInt32(ordinals.MemAge));
	    }
	    if (dataReader.IsDBNull(ordinals.MemSex)) {
		data.MemSex = StringType.UNSET;
	    } else {
		data.MemSex = StringType.Parse(dataReader.GetString(ordinals.MemSex));
	    }
	    if (dataReader.IsDBNull(ordinals.PmtFreq)) {
		data.PmtFreq = StringType.UNSET;
	    } else {
		data.PmtFreq = StringType.Parse(dataReader.GetString(ordinals.PmtFreq));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigTerm)) {
		data.OrigTerm = IntegerType.UNSET;
	    } else {
		data.OrigTerm = new IntegerType(dataReader.GetInt32(ordinals.OrigTerm));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigRate)) {
		data.OrigRate = DecimalType.UNSET;
	    } else {
		data.OrigRate = new DecimalType(dataReader.GetDecimal(ordinals.OrigRate));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigPmt)) {
		data.OrigPmt = CurrencyType.UNSET;
	    } else {
		data.OrigPmt = new CurrencyType(dataReader.GetDecimal(ordinals.OrigPmt));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigAmount)) {
		data.OrigAmount = CurrencyType.UNSET;
	    } else {
		data.OrigAmount = new CurrencyType(dataReader.GetDecimal(ordinals.OrigAmount));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigCrLim)) {
		data.OrigCrLim = CurrencyType.UNSET;
	    } else {
		data.OrigCrLim = new CurrencyType(dataReader.GetDecimal(ordinals.OrigCrLim));
	    }
	    if (dataReader.IsDBNull(ordinals.CurTerm)) {
		data.CurTerm = IntegerType.UNSET;
	    } else {
		data.CurTerm = new IntegerType(dataReader.GetInt32(ordinals.CurTerm));
	    }
	    if (dataReader.IsDBNull(ordinals.CurRate)) {
		data.CurRate = DecimalType.UNSET;
	    } else {
		data.CurRate = new DecimalType(dataReader.GetDecimal(ordinals.CurRate));
	    }
	    if (dataReader.IsDBNull(ordinals.CurPmt)) {
		data.CurPmt = CurrencyType.UNSET;
	    } else {
		data.CurPmt = new CurrencyType(dataReader.GetDecimal(ordinals.CurPmt));
	    }
	    if (dataReader.IsDBNull(ordinals.CurBal)) {
		data.CurBal = CurrencyType.UNSET;
	    } else {
		data.CurBal = new CurrencyType(dataReader.GetDecimal(ordinals.CurBal));
	    }
	    if (dataReader.IsDBNull(ordinals.CurCrLim)) {
		data.CurCrLim = CurrencyType.UNSET;
	    } else {
		data.CurCrLim = new CurrencyType(dataReader.GetDecimal(ordinals.CurCrLim));
	    }
	    if (dataReader.IsDBNull(ordinals.BalloonDate)) {
		data.BalloonDate = DateType.UNSET;
	    } else {
		data.BalloonDate = new DateType(dataReader.GetDateTime(ordinals.BalloonDate));
	    }
	    if (dataReader.IsDBNull(ordinals.MatDate)) {
		data.MatDate = DateType.UNSET;
	    } else {
		data.MatDate = new DateType(dataReader.GetDateTime(ordinals.MatDate));
	    }
	    if (dataReader.IsDBNull(ordinals.InsCode)) {
		data.InsCode = IntegerType.UNSET;
	    } else {
		data.InsCode = new IntegerType(dataReader.GetInt32(ordinals.InsCode));
	    }
	    if (dataReader.IsDBNull(ordinals.CollateralValue)) {
		data.CollateralValue = CurrencyType.UNSET;
	    } else {
		data.CollateralValue = new CurrencyType(dataReader.GetDecimal(ordinals.CollateralValue));
	    }
	    if (dataReader.IsDBNull(ordinals.LTV)) {
		data.LTV = DecimalType.UNSET;
	    } else {
		data.LTV = new DecimalType(dataReader.GetDecimal(ordinals.LTV));
	    }
	    if (dataReader.IsDBNull(ordinals.SecType)) {
		data.SecType = IntegerType.UNSET;
	    } else {
		data.SecType = new IntegerType(dataReader.GetInt32(ordinals.SecType));
	    }
	    if (dataReader.IsDBNull(ordinals.SecDesc1)) {
		data.SecDesc1 = StringType.UNSET;
	    } else {
		data.SecDesc1 = StringType.Parse(dataReader.GetString(ordinals.SecDesc1));
	    }
	    if (dataReader.IsDBNull(ordinals.SecDesc2)) {
		data.SecDesc2 = StringType.UNSET;
	    } else {
		data.SecDesc2 = StringType.Parse(dataReader.GetString(ordinals.SecDesc2));
	    }
	    if (dataReader.IsDBNull(ordinals.DebtRatio)) {
		data.DebtRatio = DecimalType.UNSET;
	    } else {
		data.DebtRatio = new DecimalType(dataReader.GetDecimal(ordinals.DebtRatio));
	    }
	    if (dataReader.IsDBNull(ordinals.Dealer)) {
		data.Dealer = IntegerType.UNSET;
	    } else {
		data.Dealer = new IntegerType(dataReader.GetInt32(ordinals.Dealer));
	    }
	    if (dataReader.IsDBNull(ordinals.DealerName)) {
		data.DealerName = StringType.UNSET;
	    } else {
		data.DealerName = StringType.Parse(dataReader.GetString(ordinals.DealerName));
	    }
	    if (dataReader.IsDBNull(ordinals.CoborComak)) {
		data.CoborComak = StringType.UNSET;
	    } else {
		data.CoborComak = StringType.Parse(dataReader.GetString(ordinals.CoborComak));
	    }
	    if (dataReader.IsDBNull(ordinals.RelPriceGrp)) {
		data.RelPriceGrp = StringType.UNSET;
	    } else {
		data.RelPriceGrp = StringType.Parse(dataReader.GetString(ordinals.RelPriceGrp));
	    }
	    if (dataReader.IsDBNull(ordinals.RelPriceDisc)) {
		data.RelPriceDisc = IntegerType.UNSET;
	    } else {
		data.RelPriceDisc = new IntegerType(dataReader.GetInt32(ordinals.RelPriceDisc));
	    }
	    if (dataReader.IsDBNull(ordinals.OtherDisc)) {
		data.OtherDisc = DecimalType.UNSET;
	    } else {
		data.OtherDisc = new DecimalType(dataReader.GetDecimal(ordinals.OtherDisc));
	    }
	    if (dataReader.IsDBNull(ordinals.DiscReason)) {
		data.DiscReason = StringType.UNSET;
	    } else {
		data.DiscReason = StringType.Parse(dataReader.GetString(ordinals.DiscReason));
	    }
	    if (dataReader.IsDBNull(ordinals.RiskOffset)) {
		data.RiskOffset = DecimalType.UNSET;
	    } else {
		data.RiskOffset = new DecimalType(dataReader.GetDecimal(ordinals.RiskOffset));
	    }
	    if (dataReader.IsDBNull(ordinals.CreditType)) {
		data.CreditType = StringType.UNSET;
	    } else {
		data.CreditType = StringType.Parse(dataReader.GetString(ordinals.CreditType));
	    }
	    if (dataReader.IsDBNull(ordinals.StatedIncome)) {
		data.StatedIncome = StringType.UNSET;
	    } else {
		data.StatedIncome = StringType.Parse(dataReader.GetString(ordinals.StatedIncome));
	    }
	    if (dataReader.IsDBNull(ordinals.Extensions)) {
		data.Extensions = StringType.UNSET;
	    } else {
		data.Extensions = StringType.Parse(dataReader.GetString(ordinals.Extensions));
	    }
	    if (dataReader.IsDBNull(ordinals.FirstExtDate)) {
		data.FirstExtDate = StringType.UNSET;
	    } else {
		data.FirstExtDate = StringType.Parse(dataReader.GetString(ordinals.FirstExtDate));
	    }
	    if (dataReader.IsDBNull(ordinals.LastExtDate)) {
		data.LastExtDate = StringType.UNSET;
	    } else {
		data.LastExtDate = StringType.Parse(dataReader.GetString(ordinals.LastExtDate));
	    }
	    if (dataReader.IsDBNull(ordinals.RiskLevel)) {
		data.RiskLevel = StringType.UNSET;
	    } else {
		data.RiskLevel = StringType.Parse(dataReader.GetString(ordinals.RiskLevel));
	    }
	    if (dataReader.IsDBNull(ordinals.RiskDate)) {
		data.RiskDate = StringType.UNSET;
	    } else {
		data.RiskDate = StringType.Parse(dataReader.GetString(ordinals.RiskDate));
	    }
	    if (dataReader.IsDBNull(ordinals.Watch)) {
		data.Watch = StringType.UNSET;
	    } else {
		data.Watch = StringType.Parse(dataReader.GetString(ordinals.Watch));
	    }
	    if (dataReader.IsDBNull(ordinals.WatchDate)) {
		data.WatchDate = StringType.UNSET;
	    } else {
		data.WatchDate = StringType.Parse(dataReader.GetString(ordinals.WatchDate));
	    }
	    if (dataReader.IsDBNull(ordinals.WorkOut)) {
		data.WorkOut = StringType.UNSET;
	    } else {
		data.WorkOut = StringType.Parse(dataReader.GetString(ordinals.WorkOut));
	    }
	    if (dataReader.IsDBNull(ordinals.WorkOutDate)) {
		data.WorkOutDate = StringType.UNSET;
	    } else {
		data.WorkOutDate = StringType.Parse(dataReader.GetString(ordinals.WorkOutDate));
	    }
	    if (dataReader.IsDBNull(ordinals.FirstMortType)) {
		data.FirstMortType = StringType.UNSET;
	    } else {
		data.FirstMortType = StringType.Parse(dataReader.GetString(ordinals.FirstMortType));
	    }
	    if (dataReader.IsDBNull(ordinals.BusPerReg)) {
		data.BusPerReg = StringType.UNSET;
	    } else {
		data.BusPerReg = StringType.Parse(dataReader.GetString(ordinals.BusPerReg));
	    }
	    if (dataReader.IsDBNull(ordinals.BusGroup)) {
		data.BusGroup = StringType.UNSET;
	    } else {
		data.BusGroup = StringType.Parse(dataReader.GetString(ordinals.BusGroup));
	    }
	    if (dataReader.IsDBNull(ordinals.BusDesc)) {
		data.BusDesc = StringType.UNSET;
	    } else {
		data.BusDesc = StringType.Parse(dataReader.GetString(ordinals.BusDesc));
	    }
	    if (dataReader.IsDBNull(ordinals.BusPart)) {
		data.BusPart = StringType.UNSET;
	    } else {
		data.BusPart = StringType.Parse(dataReader.GetString(ordinals.BusPart));
	    }
	    if (dataReader.IsDBNull(ordinals.BusPartPerc)) {
		data.BusPartPerc = StringType.UNSET;
	    } else {
		data.BusPartPerc = StringType.Parse(dataReader.GetString(ordinals.BusPartPerc));
	    }
	    if (dataReader.IsDBNull(ordinals.SBAguar)) {
		data.SBAguar = StringType.UNSET;
	    } else {
		data.SBAguar = StringType.Parse(dataReader.GetString(ordinals.SBAguar));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliDays)) {
		data.DeliDays = IntegerType.UNSET;
	    } else {
		data.DeliDays = new IntegerType(dataReader.GetInt32(ordinals.DeliDays));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliSect)) {
		data.DeliSect = IntegerType.UNSET;
	    } else {
		data.DeliSect = new IntegerType(dataReader.GetInt32(ordinals.DeliSect));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHist)) {
		data.DeliHist = StringType.UNSET;
	    } else {
		data.DeliHist = StringType.Parse(dataReader.GetString(ordinals.DeliHist));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ1)) {
		data.DeliHistQ1 = StringType.UNSET;
	    } else {
		data.DeliHistQ1 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ1));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ2)) {
		data.DeliHistQ2 = StringType.UNSET;
	    } else {
		data.DeliHistQ2 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ2));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ3)) {
		data.DeliHistQ3 = StringType.UNSET;
	    } else {
		data.DeliHistQ3 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ3));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ4)) {
		data.DeliHistQ4 = StringType.UNSET;
	    } else {
		data.DeliHistQ4 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ4));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ5)) {
		data.DeliHistQ5 = StringType.UNSET;
	    } else {
		data.DeliHistQ5 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ5));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ6)) {
		data.DeliHistQ6 = StringType.UNSET;
	    } else {
		data.DeliHistQ6 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ6));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ7)) {
		data.DeliHistQ7 = StringType.UNSET;
	    } else {
		data.DeliHistQ7 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ7));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliHistQ8)) {
		data.DeliHistQ8 = StringType.UNSET;
	    } else {
		data.DeliHistQ8 = StringType.Parse(dataReader.GetString(ordinals.DeliHistQ8));
	    }
	    if (dataReader.IsDBNull(ordinals.ChrgOffDate)) {
		data.ChrgOffDate = DateType.UNSET;
	    } else {
		data.ChrgOffDate = new DateType(dataReader.GetDateTime(ordinals.ChrgOffDate));
	    }
	    if (dataReader.IsDBNull(ordinals.ChrgOffMonth)) {
		data.ChrgOffMonth = IntegerType.UNSET;
	    } else {
		data.ChrgOffMonth = new IntegerType(dataReader.GetInt32(ordinals.ChrgOffMonth));
	    }
	    if (dataReader.IsDBNull(ordinals.ChrgOffYear)) {
		data.ChrgOffYear = IntegerType.UNSET;
	    } else {
		data.ChrgOffYear = new IntegerType(dataReader.GetInt32(ordinals.ChrgOffYear));
	    }
	    if (dataReader.IsDBNull(ordinals.ChrgOffMnthsBF)) {
		data.ChrgOffMnthsBF = IntegerType.UNSET;
	    } else {
		data.ChrgOffMnthsBF = new IntegerType(dataReader.GetInt32(ordinals.ChrgOffMnthsBF));
	    }
	    if (dataReader.IsDBNull(ordinals.PropType)) {
		data.PropType = StringType.UNSET;
	    } else {
		data.PropType = StringType.Parse(dataReader.GetString(ordinals.PropType));
	    }
	    if (dataReader.IsDBNull(ordinals.Occupancy)) {
		data.Occupancy = StringType.UNSET;
	    } else {
		data.Occupancy = StringType.Parse(dataReader.GetString(ordinals.Occupancy));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigApprVal)) {
		data.OrigApprVal = CurrencyType.UNSET;
	    } else {
		data.OrigApprVal = new CurrencyType(dataReader.GetDecimal(ordinals.OrigApprVal));
	    }
	    if (dataReader.IsDBNull(ordinals.OrigApprDate)) {
		data.OrigApprDate = DateType.UNSET;
	    } else {
		data.OrigApprDate = new DateType(dataReader.GetDateTime(ordinals.OrigApprDate));
	    }
	    if (dataReader.IsDBNull(ordinals.CurrApprVal)) {
		data.CurrApprVal = CurrencyType.UNSET;
	    } else {
		data.CurrApprVal = new CurrencyType(dataReader.GetDecimal(ordinals.CurrApprVal));
	    }
	    if (dataReader.IsDBNull(ordinals.CurrApprDate)) {
		data.CurrApprDate = DateType.UNSET;
	    } else {
		data.CurrApprDate = new DateType(dataReader.GetDateTime(ordinals.CurrApprDate));
	    }
	    if (dataReader.IsDBNull(ordinals.FirstMortBal)) {
		data.FirstMortBal = CurrencyType.UNSET;
	    } else {
		data.FirstMortBal = new CurrencyType(dataReader.GetDecimal(ordinals.FirstMortBal));
	    }
	    if (dataReader.IsDBNull(ordinals.FirstMortMoPyt)) {
		data.FirstMortMoPyt = CurrencyType.UNSET;
	    } else {
		data.FirstMortMoPyt = new CurrencyType(dataReader.GetDecimal(ordinals.FirstMortMoPyt));
	    }
	    if (dataReader.IsDBNull(ordinals.SecondMortBal)) {
		data.SecondMortBal = CurrencyType.UNSET;
	    } else {
		data.SecondMortBal = new CurrencyType(dataReader.GetDecimal(ordinals.SecondMortBal));
	    }
	    if (dataReader.IsDBNull(ordinals.SecondMortPymt)) {
		data.SecondMortPymt = CurrencyType.UNSET;
	    } else {
		data.SecondMortPymt = new CurrencyType(dataReader.GetDecimal(ordinals.SecondMortPymt));
	    }
	    if (dataReader.IsDBNull(ordinals.TaxInsNotInc)) {
		data.TaxInsNotInc = CurrencyType.UNSET;
	    } else {
		data.TaxInsNotInc = new CurrencyType(dataReader.GetDecimal(ordinals.TaxInsNotInc));
	    }
	    if (dataReader.IsDBNull(ordinals.LoadOff)) {
		data.LoadOff = IntegerType.UNSET;
	    } else {
		data.LoadOff = new IntegerType(dataReader.GetInt32(ordinals.LoadOff));
	    }
	    if (dataReader.IsDBNull(ordinals.ApprOff)) {
		data.ApprOff = IntegerType.UNSET;
	    } else {
		data.ApprOff = new IntegerType(dataReader.GetInt32(ordinals.ApprOff));
	    }
	    if (dataReader.IsDBNull(ordinals.FundingOpp)) {
		data.FundingOpp = IntegerType.UNSET;
	    } else {
		data.FundingOpp = new IntegerType(dataReader.GetInt32(ordinals.FundingOpp));
	    }
	    if (dataReader.IsDBNull(ordinals.Chanel)) {
		data.Chanel = StringType.UNSET;
	    } else {
		data.Chanel = StringType.Parse(dataReader.GetString(ordinals.Chanel));
	    }
	    if (dataReader.IsDBNull(ordinals.RiskType)) {
		data.RiskType = StringType.UNSET;
	    } else {
		data.RiskType = StringType.Parse(dataReader.GetString(ordinals.RiskType));
	    }
	    if (dataReader.IsDBNull(ordinals.Gap)) {
		data.Gap = StringType.UNSET;
	    } else {
		data.Gap = StringType.Parse(dataReader.GetString(ordinals.Gap));
	    }
	    if (dataReader.IsDBNull(ordinals.LoadOperator)) {
		data.LoadOperator = IntegerType.UNSET;
	    } else {
		data.LoadOperator = new IntegerType(dataReader.GetInt32(ordinals.LoadOperator));
	    }
	    if (dataReader.IsDBNull(ordinals.DeliAmt)) {
		data.DeliAmt = CurrencyType.UNSET;
	    } else {
		data.DeliAmt = new CurrencyType(dataReader.GetDecimal(ordinals.DeliAmt));
	    }
	    if (dataReader.IsDBNull(ordinals.ChgOffAmt)) {
		data.ChgOffAmt = CurrencyType.UNSET;
	    } else {
		data.ChgOffAmt = new CurrencyType(dataReader.GetDecimal(ordinals.ChgOffAmt));
	    }
	    if (dataReader.IsDBNull(ordinals.UsSecDesc)) {
		data.UsSecDesc = StringType.UNSET;
	    } else {
		data.UsSecDesc = StringType.Parse(dataReader.GetString(ordinals.UsSecDesc));
	    }
	    if (dataReader.IsDBNull(ordinals.TroubledDebt)) {
		data.TroubledDebt = StringType.UNSET;
	    } else {
		data.TroubledDebt = StringType.Parse(dataReader.GetString(ordinals.TroubledDebt));
	    }
	    if (dataReader.IsDBNull(ordinals.TdrDate)) {
		data.TdrDate = StringType.UNSET;
	    } else {
		data.TdrDate = StringType.Parse(dataReader.GetString(ordinals.TdrDate));
	    }
	    if (dataReader.IsDBNull(ordinals.JntFicoUsed)) {
		data.JntFicoUsed = StringType.UNSET;
	    } else {
		data.JntFicoUsed = StringType.Parse(dataReader.GetString(ordinals.JntFicoUsed));
	    }
	    if (dataReader.IsDBNull(ordinals.SingleOrJointLoan)) {
		data.SingleOrJointLoan = StringType.UNSET;
	    } else {
		data.SingleOrJointLoan = StringType.Parse(dataReader.GetString(ordinals.SingleOrJointLoan));
	    }
	    if (dataReader.IsDBNull(ordinals.ApprovingOfficerUD)) {
		data.ApprovingOfficerUD = StringType.UNSET;
	    } else {
		data.ApprovingOfficerUD = StringType.Parse(dataReader.GetString(ordinals.ApprovingOfficerUD));
	    }
	    if (dataReader.IsDBNull(ordinals.InsuranceCodeDesc)) {
		data.InsuranceCodeDesc = StringType.UNSET;
	    } else {
		data.InsuranceCodeDesc = StringType.Parse(dataReader.GetString(ordinals.InsuranceCodeDesc));
	    }
	    if (dataReader.IsDBNull(ordinals.GapIns)) {
		data.GapIns = StringType.UNSET;
	    } else {
		data.GapIns = StringType.Parse(dataReader.GetString(ordinals.GapIns));
	    }
	    if (dataReader.IsDBNull(ordinals.MmpIns)) {
		data.MmpIns = StringType.UNSET;
	    } else {
		data.MmpIns = StringType.Parse(dataReader.GetString(ordinals.MmpIns));
	    }
	    if (dataReader.IsDBNull(ordinals.ReportCodeDesc)) {
		data.ReportCodeDesc = StringType.UNSET;
	    } else {
		data.ReportCodeDesc = StringType.Parse(dataReader.GetString(ordinals.ReportCodeDesc));
	    }
	    if (dataReader.IsDBNull(ordinals.BallonPmtLoan)) {
		data.BallonPmtLoan = StringType.UNSET;
	    } else {
		data.BallonPmtLoan = StringType.Parse(dataReader.GetString(ordinals.BallonPmtLoan));
	    }
	    if (dataReader.IsDBNull(ordinals.FixedOrVariableRateLoan)) {
		data.FixedOrVariableRateLoan = StringType.UNSET;
	    } else {
		data.FixedOrVariableRateLoan = StringType.Parse(dataReader.GetString(ordinals.FixedOrVariableRateLoan));
	    }
	    if (dataReader.IsDBNull(ordinals.AlpsOfficer)) {
		data.AlpsOfficer = StringType.UNSET;
	    } else {
		data.AlpsOfficer = StringType.Parse(dataReader.GetString(ordinals.AlpsOfficer));
	    }
	    if (dataReader.IsDBNull(ordinals.CUDLAppNumber)) {
		data.CUDLAppNumber = StringType.UNSET;
	    } else {
		data.CUDLAppNumber = StringType.Parse(dataReader.GetString(ordinals.CUDLAppNumber));
	    }
	    if (dataReader.IsDBNull(ordinals.FasbDefCost)) {
		data.FasbDefCost = CurrencyType.UNSET;
	    } else {
		data.FasbDefCost = new CurrencyType(dataReader.GetDecimal(ordinals.FasbDefCost));
	    }
	    if (dataReader.IsDBNull(ordinals.FasbLtdAmortFees)) {
		data.FasbLtdAmortFees = CurrencyType.UNSET;
	    } else {
		data.FasbLtdAmortFees = new CurrencyType(dataReader.GetDecimal(ordinals.FasbLtdAmortFees));
	    }
	    if (dataReader.IsDBNull(ordinals.InterestPriorYtd)) {
		data.InterestPriorYtd = CurrencyType.UNSET;
	    } else {
		data.InterestPriorYtd = new CurrencyType(dataReader.GetDecimal(ordinals.InterestPriorYtd));
	    }
	    if (dataReader.IsDBNull(ordinals.AccruedInterest)) {
		data.AccruedInterest = CurrencyType.UNSET;
	    } else {
		data.AccruedInterest = new CurrencyType(dataReader.GetDecimal(ordinals.AccruedInterest));
	    }
	    if (dataReader.IsDBNull(ordinals.InterestLTD)) {
		data.InterestLTD = CurrencyType.UNSET;
	    } else {
		data.InterestLTD = new CurrencyType(dataReader.GetDecimal(ordinals.InterestLTD));
	    }
	    if (dataReader.IsDBNull(ordinals.InterestYtd)) {
		data.InterestYtd = CurrencyType.UNSET;
	    } else {
		data.InterestYtd = new CurrencyType(dataReader.GetDecimal(ordinals.InterestYtd));
	    }
	    if (dataReader.IsDBNull(ordinals.PartialPayAmt)) {
		data.PartialPayAmt = CurrencyType.UNSET;
	    } else {
		data.PartialPayAmt = new CurrencyType(dataReader.GetDecimal(ordinals.PartialPayAmt));
	    }
	    if (dataReader.IsDBNull(ordinals.InterestUncollected)) {
		data.InterestUncollected = CurrencyType.UNSET;
	    } else {
		data.InterestUncollected = new CurrencyType(dataReader.GetDecimal(ordinals.InterestUncollected));
	    }
	    if (dataReader.IsDBNull(ordinals.ClosedAcctDate)) {
		data.ClosedAcctDate = DateType.UNSET;
	    } else {
		data.ClosedAcctDate = new DateType(dataReader.GetDateTime(ordinals.ClosedAcctDate));
	    }
	    if (dataReader.IsDBNull(ordinals.LastTranDate)) {
		data.LastTranDate = DateType.UNSET;
	    } else {
		data.LastTranDate = new DateType(dataReader.GetDateTime(ordinals.LastTranDate));
	    }
	    if (dataReader.IsDBNull(ordinals.NextDueDate)) {
		data.NextDueDate = DateType.UNSET;
	    } else {
		data.NextDueDate = new DateType(dataReader.GetDateTime(ordinals.NextDueDate));
	    }
	    if (dataReader.IsDBNull(ordinals.InsuranceCode)) {
		data.InsuranceCode = IntegerType.UNSET;
	    } else {
		data.InsuranceCode = new IntegerType(dataReader.GetInt32(ordinals.InsuranceCode));
	    }
	    if (dataReader.IsDBNull(ordinals.MbrAgeAtApproval)) {
		data.MbrAgeAtApproval = IntegerType.UNSET;
	    } else {
		data.MbrAgeAtApproval = new IntegerType(dataReader.GetInt32(ordinals.MbrAgeAtApproval));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the LoanPortfolio table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(LoanPortfolio data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the LoanPortfolio table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(LoanPortfolio data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spLoanPortfolio_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.UPDATED, data.UpDated.IsValid ? data.UpDated.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.EOMDATE, data.EOMDate.IsValid ? data.EOMDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ACCOUNTID, data.AccountID.IsValid ? data.AccountID.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMBER, data.Member.IsValid ? data.Member.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MCLASS, data.MClass.IsValid ? data.MClass.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LTYPE, data.Ltype.IsValid ? data.Ltype.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LTYPESUB, data.LtypeSub.IsValid ? data.LtypeSub.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PRODUCT, data.Product.IsValid ? data.Product.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANDESC, data.LoanDesc.IsValid ? data.LoanDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANDATE, data.LoanDate.IsValid ? data.LoanDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANDAY, data.LoanDay.IsValid ? data.LoanDay.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANMONTH, data.LoanMonth.IsValid ? data.LoanMonth.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANYEAR, data.LoanYear.IsValid ? data.LoanYear.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTPMTDATE, data.FirstPmtDate.IsValid ? data.FirstPmtDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.STATUS, data.Status.IsValid ? data.Status.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RPTCODE, data.RptCode.IsValid ? data.RptCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FUNDEDBR, data.FundedBr.IsValid ? data.FundedBr.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOADEDBR, data.LoadedBr.IsValid ? data.LoadedBr.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FICO, data.FICO.IsValid ? data.FICO.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FICORANGE, data.FICORange.IsValid ? data.FICORange.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASTSTART, data.FastStart.IsValid ? data.FastStart.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASTSTARTRANGE, data.FastStartRange.IsValid ? data.FastStartRange.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BANKRUPTCY, data.Bankruptcy.IsValid ? data.Bankruptcy.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BANKRUPTCYRANGE, data.BankruptcyRange.IsValid ? data.BankruptcyRange.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMBERSHIPDATE, data.MembershipDate.IsValid ? data.MembershipDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.NEWMEMBER, data.NewMember.IsValid ? data.NewMember.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKLEV, data.RiskLev.IsValid ? data.RiskLev.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TRAFFIC, data.Traffic.IsValid ? data.Traffic.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TRAFFICCOLOR, data.TrafficColor.IsValid ? data.TrafficColor.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMAGE, data.MemAge.IsValid ? data.MemAge.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMSEX, data.MemSex.IsValid ? data.MemSex.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PMTFREQ, data.PmtFreq.IsValid ? data.PmtFreq.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGTERM, data.OrigTerm.IsValid ? data.OrigTerm.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGRATE, data.OrigRate.IsValid ? data.OrigRate.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGPMT, data.OrigPmt.IsValid ? data.OrigPmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGAMOUNT, data.OrigAmount.IsValid ? data.OrigAmount.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGCRLIM, data.OrigCrLim.IsValid ? data.OrigCrLim.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURTERM, data.CurTerm.IsValid ? data.CurTerm.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURRATE, data.CurRate.IsValid ? data.CurRate.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURPMT, data.CurPmt.IsValid ? data.CurPmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURBAL, data.CurBal.IsValid ? data.CurBal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURCRLIM, data.CurCrLim.IsValid ? data.CurCrLim.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BALLOONDATE, data.BalloonDate.IsValid ? data.BalloonDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MATDATE, data.MatDate.IsValid ? data.MatDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INSCODE, data.InsCode.IsValid ? data.InsCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.COLLATERALVALUE, data.CollateralValue.IsValid ? data.CollateralValue.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LTV, data.LTV.IsValid ? data.LTV.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECTYPE, data.SecType.IsValid ? data.SecType.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECDESC1, data.SecDesc1.IsValid ? data.SecDesc1.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECDESC2, data.SecDesc2.IsValid ? data.SecDesc2.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DEBTRATIO, data.DebtRatio.IsValid ? data.DebtRatio.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DEALER, data.Dealer.IsValid ? data.Dealer.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DEALERNAME, data.DealerName.IsValid ? data.DealerName.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.COBORCOMAK, data.CoborComak.IsValid ? data.CoborComak.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RELPRICEGRP, data.RelPriceGrp.IsValid ? data.RelPriceGrp.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RELPRICEDISC, data.RelPriceDisc.IsValid ? data.RelPriceDisc.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.OTHERDISC, data.OtherDisc.IsValid ? data.OtherDisc.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DISCREASON, data.DiscReason.IsValid ? data.DiscReason.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKOFFSET, data.RiskOffset.IsValid ? data.RiskOffset.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CREDITTYPE, data.CreditType.IsValid ? data.CreditType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.STATEDINCOME, data.StatedIncome.IsValid ? data.StatedIncome.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.EXTENSIONS, data.Extensions.IsValid ? data.Extensions.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTEXTDATE, data.FirstExtDate.IsValid ? data.FirstExtDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LASTEXTDATE, data.LastExtDate.IsValid ? data.LastExtDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKLEVEL, data.RiskLevel.IsValid ? data.RiskLevel.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKDATE, data.RiskDate.IsValid ? data.RiskDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WATCH, data.Watch.IsValid ? data.Watch.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WATCHDATE, data.WatchDate.IsValid ? data.WatchDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WORKOUT, data.WorkOut.IsValid ? data.WorkOut.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WORKOUTDATE, data.WorkOutDate.IsValid ? data.WorkOutDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTMORTTYPE, data.FirstMortType.IsValid ? data.FirstMortType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSPERREG, data.BusPerReg.IsValid ? data.BusPerReg.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSGROUP, data.BusGroup.IsValid ? data.BusGroup.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSDESC, data.BusDesc.IsValid ? data.BusDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSPART, data.BusPart.IsValid ? data.BusPart.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSPARTPERC, data.BusPartPerc.IsValid ? data.BusPartPerc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SBAGUAR, data.SBAguar.IsValid ? data.SBAguar.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIDAYS, data.DeliDays.IsValid ? data.DeliDays.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELISECT, data.DeliSect.IsValid ? data.DeliSect.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHIST, data.DeliHist.IsValid ? data.DeliHist.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ1, data.DeliHistQ1.IsValid ? data.DeliHistQ1.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ2, data.DeliHistQ2.IsValid ? data.DeliHistQ2.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ3, data.DeliHistQ3.IsValid ? data.DeliHistQ3.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ4, data.DeliHistQ4.IsValid ? data.DeliHistQ4.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ5, data.DeliHistQ5.IsValid ? data.DeliHistQ5.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ6, data.DeliHistQ6.IsValid ? data.DeliHistQ6.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ7, data.DeliHistQ7.IsValid ? data.DeliHistQ7.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ8, data.DeliHistQ8.IsValid ? data.DeliHistQ8.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFDATE, data.ChrgOffDate.IsValid ? data.ChrgOffDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFMONTH, data.ChrgOffMonth.IsValid ? data.ChrgOffMonth.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFYEAR, data.ChrgOffYear.IsValid ? data.ChrgOffYear.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFMNTHSBF, data.ChrgOffMnthsBF.IsValid ? data.ChrgOffMnthsBF.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PROPTYPE, data.PropType.IsValid ? data.PropType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.OCCUPANCY, data.Occupancy.IsValid ? data.Occupancy.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGAPPRVAL, data.OrigApprVal.IsValid ? data.OrigApprVal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGAPPRDATE, data.OrigApprDate.IsValid ? data.OrigApprDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURRAPPRVAL, data.CurrApprVal.IsValid ? data.CurrApprVal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURRAPPRDATE, data.CurrApprDate.IsValid ? data.CurrApprDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTMORTBAL, data.FirstMortBal.IsValid ? data.FirstMortBal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTMORTMOPYT, data.FirstMortMoPyt.IsValid ? data.FirstMortMoPyt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECONDMORTBAL, data.SecondMortBal.IsValid ? data.SecondMortBal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECONDMORTPYMT, data.SecondMortPymt.IsValid ? data.SecondMortPymt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TAXINSNOTINC, data.TaxInsNotInc.IsValid ? data.TaxInsNotInc.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOADOFF, data.LoadOff.IsValid ? data.LoadOff.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.APPROFF, data.ApprOff.IsValid ? data.ApprOff.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FUNDINGOPP, data.FundingOpp.IsValid ? data.FundingOpp.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHANEL, data.Chanel.IsValid ? data.Chanel.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKTYPE, data.RiskType.IsValid ? data.RiskType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.GAP, data.Gap.IsValid ? data.Gap.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOADOPERATOR, data.LoadOperator.IsValid ? data.LoadOperator.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIAMT, data.DeliAmt.IsValid ? data.DeliAmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHGOFFAMT, data.ChgOffAmt.IsValid ? data.ChgOffAmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.USSECDESC, data.UsSecDesc.IsValid ? data.UsSecDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TROUBLEDDEBT, data.TroubledDebt.IsValid ? data.TroubledDebt.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TDRDATE, data.TdrDate.IsValid ? data.TdrDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.JNTFICOUSED, data.JntFicoUsed.IsValid ? data.JntFicoUsed.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SINGLEORJOINTLOAN, data.SingleOrJointLoan.IsValid ? data.SingleOrJointLoan.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.APPROVINGOFFICERUD, data.ApprovingOfficerUD.IsValid ? data.ApprovingOfficerUD.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INSURANCECODEDESC, data.InsuranceCodeDesc.IsValid ? data.InsuranceCodeDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.GAPINS, data.GapIns.IsValid ? data.GapIns.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MMPINS, data.MmpIns.IsValid ? data.MmpIns.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.REPORTCODEDESC, data.ReportCodeDesc.IsValid ? data.ReportCodeDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BALLONPMTLOAN, data.BallonPmtLoan.IsValid ? data.BallonPmtLoan.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIXEDORVARIABLERATELOAN, data.FixedOrVariableRateLoan.IsValid ? data.FixedOrVariableRateLoan.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ALPSOFFICER, data.AlpsOfficer.IsValid ? data.AlpsOfficer.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CUDLAPPNUMBER, data.CUDLAppNumber.IsValid ? data.CUDLAppNumber.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASBDEFCOST, data.FasbDefCost.IsValid ? data.FasbDefCost.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASBLTDAMORTFEES, data.FasbLtdAmortFees.IsValid ? data.FasbLtdAmortFees.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTPRIORYTD, data.InterestPriorYtd.IsValid ? data.InterestPriorYtd.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ACCRUEDINTEREST, data.AccruedInterest.IsValid ? data.AccruedInterest.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTLTD, data.InterestLTD.IsValid ? data.InterestLTD.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTYTD, data.InterestYtd.IsValid ? data.InterestYtd.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PARTIALPAYAMT, data.PartialPayAmt.IsValid ? data.PartialPayAmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTUNCOLLECTED, data.InterestUncollected.IsValid ? data.InterestUncollected.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CLOSEDACCTDATE, data.ClosedAcctDate.IsValid ? data.ClosedAcctDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LASTTRANDATE, data.LastTranDate.IsValid ? data.LastTranDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.NEXTDUEDATE, data.NextDueDate.IsValid ? data.NextDueDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INSURANCECODE, data.InsuranceCode.IsValid ? data.InsuranceCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MBRAGEATAPPROVAL, data.MbrAgeAtApproval.IsValid ? data.MbrAgeAtApproval.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the LoanPortfolio table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(LoanPortfolio data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the LoanPortfolio table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(LoanPortfolio data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spLoanPortfolio_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANPORTFOLIOID, data.LoanPortfolioId.IsValid ? data.LoanPortfolioId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.UPDATED, data.UpDated.IsValid ? data.UpDated.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.EOMDATE, data.EOMDate.IsValid ? data.EOMDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ACCOUNTID, data.AccountID.IsValid ? data.AccountID.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMBER, data.Member.IsValid ? data.Member.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MCLASS, data.MClass.IsValid ? data.MClass.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LTYPE, data.Ltype.IsValid ? data.Ltype.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LTYPESUB, data.LtypeSub.IsValid ? data.LtypeSub.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PRODUCT, data.Product.IsValid ? data.Product.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANDESC, data.LoanDesc.IsValid ? data.LoanDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANDATE, data.LoanDate.IsValid ? data.LoanDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANDAY, data.LoanDay.IsValid ? data.LoanDay.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANMONTH, data.LoanMonth.IsValid ? data.LoanMonth.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANYEAR, data.LoanYear.IsValid ? data.LoanYear.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTPMTDATE, data.FirstPmtDate.IsValid ? data.FirstPmtDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.STATUS, data.Status.IsValid ? data.Status.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RPTCODE, data.RptCode.IsValid ? data.RptCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FUNDEDBR, data.FundedBr.IsValid ? data.FundedBr.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOADEDBR, data.LoadedBr.IsValid ? data.LoadedBr.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FICO, data.FICO.IsValid ? data.FICO.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FICORANGE, data.FICORange.IsValid ? data.FICORange.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASTSTART, data.FastStart.IsValid ? data.FastStart.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASTSTARTRANGE, data.FastStartRange.IsValid ? data.FastStartRange.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BANKRUPTCY, data.Bankruptcy.IsValid ? data.Bankruptcy.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BANKRUPTCYRANGE, data.BankruptcyRange.IsValid ? data.BankruptcyRange.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMBERSHIPDATE, data.MembershipDate.IsValid ? data.MembershipDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.NEWMEMBER, data.NewMember.IsValid ? data.NewMember.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKLEV, data.RiskLev.IsValid ? data.RiskLev.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TRAFFIC, data.Traffic.IsValid ? data.Traffic.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TRAFFICCOLOR, data.TrafficColor.IsValid ? data.TrafficColor.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMAGE, data.MemAge.IsValid ? data.MemAge.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MEMSEX, data.MemSex.IsValid ? data.MemSex.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PMTFREQ, data.PmtFreq.IsValid ? data.PmtFreq.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGTERM, data.OrigTerm.IsValid ? data.OrigTerm.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGRATE, data.OrigRate.IsValid ? data.OrigRate.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGPMT, data.OrigPmt.IsValid ? data.OrigPmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGAMOUNT, data.OrigAmount.IsValid ? data.OrigAmount.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGCRLIM, data.OrigCrLim.IsValid ? data.OrigCrLim.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURTERM, data.CurTerm.IsValid ? data.CurTerm.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURRATE, data.CurRate.IsValid ? data.CurRate.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURPMT, data.CurPmt.IsValid ? data.CurPmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURBAL, data.CurBal.IsValid ? data.CurBal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURCRLIM, data.CurCrLim.IsValid ? data.CurCrLim.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BALLOONDATE, data.BalloonDate.IsValid ? data.BalloonDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MATDATE, data.MatDate.IsValid ? data.MatDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INSCODE, data.InsCode.IsValid ? data.InsCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.COLLATERALVALUE, data.CollateralValue.IsValid ? data.CollateralValue.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LTV, data.LTV.IsValid ? data.LTV.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECTYPE, data.SecType.IsValid ? data.SecType.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECDESC1, data.SecDesc1.IsValid ? data.SecDesc1.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECDESC2, data.SecDesc2.IsValid ? data.SecDesc2.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DEBTRATIO, data.DebtRatio.IsValid ? data.DebtRatio.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DEALER, data.Dealer.IsValid ? data.Dealer.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DEALERNAME, data.DealerName.IsValid ? data.DealerName.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.COBORCOMAK, data.CoborComak.IsValid ? data.CoborComak.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RELPRICEGRP, data.RelPriceGrp.IsValid ? data.RelPriceGrp.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RELPRICEDISC, data.RelPriceDisc.IsValid ? data.RelPriceDisc.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.OTHERDISC, data.OtherDisc.IsValid ? data.OtherDisc.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DISCREASON, data.DiscReason.IsValid ? data.DiscReason.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKOFFSET, data.RiskOffset.IsValid ? data.RiskOffset.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CREDITTYPE, data.CreditType.IsValid ? data.CreditType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.STATEDINCOME, data.StatedIncome.IsValid ? data.StatedIncome.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.EXTENSIONS, data.Extensions.IsValid ? data.Extensions.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTEXTDATE, data.FirstExtDate.IsValid ? data.FirstExtDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LASTEXTDATE, data.LastExtDate.IsValid ? data.LastExtDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKLEVEL, data.RiskLevel.IsValid ? data.RiskLevel.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKDATE, data.RiskDate.IsValid ? data.RiskDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WATCH, data.Watch.IsValid ? data.Watch.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WATCHDATE, data.WatchDate.IsValid ? data.WatchDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WORKOUT, data.WorkOut.IsValid ? data.WorkOut.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.WORKOUTDATE, data.WorkOutDate.IsValid ? data.WorkOutDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTMORTTYPE, data.FirstMortType.IsValid ? data.FirstMortType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSPERREG, data.BusPerReg.IsValid ? data.BusPerReg.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSGROUP, data.BusGroup.IsValid ? data.BusGroup.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSDESC, data.BusDesc.IsValid ? data.BusDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSPART, data.BusPart.IsValid ? data.BusPart.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BUSPARTPERC, data.BusPartPerc.IsValid ? data.BusPartPerc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SBAGUAR, data.SBAguar.IsValid ? data.SBAguar.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIDAYS, data.DeliDays.IsValid ? data.DeliDays.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELISECT, data.DeliSect.IsValid ? data.DeliSect.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHIST, data.DeliHist.IsValid ? data.DeliHist.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ1, data.DeliHistQ1.IsValid ? data.DeliHistQ1.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ2, data.DeliHistQ2.IsValid ? data.DeliHistQ2.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ3, data.DeliHistQ3.IsValid ? data.DeliHistQ3.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ4, data.DeliHistQ4.IsValid ? data.DeliHistQ4.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ5, data.DeliHistQ5.IsValid ? data.DeliHistQ5.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ6, data.DeliHistQ6.IsValid ? data.DeliHistQ6.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ7, data.DeliHistQ7.IsValid ? data.DeliHistQ7.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIHISTQ8, data.DeliHistQ8.IsValid ? data.DeliHistQ8.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFDATE, data.ChrgOffDate.IsValid ? data.ChrgOffDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFMONTH, data.ChrgOffMonth.IsValid ? data.ChrgOffMonth.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFYEAR, data.ChrgOffYear.IsValid ? data.ChrgOffYear.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHRGOFFMNTHSBF, data.ChrgOffMnthsBF.IsValid ? data.ChrgOffMnthsBF.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PROPTYPE, data.PropType.IsValid ? data.PropType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.OCCUPANCY, data.Occupancy.IsValid ? data.Occupancy.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGAPPRVAL, data.OrigApprVal.IsValid ? data.OrigApprVal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ORIGAPPRDATE, data.OrigApprDate.IsValid ? data.OrigApprDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURRAPPRVAL, data.CurrApprVal.IsValid ? data.CurrApprVal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CURRAPPRDATE, data.CurrApprDate.IsValid ? data.CurrApprDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTMORTBAL, data.FirstMortBal.IsValid ? data.FirstMortBal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIRSTMORTMOPYT, data.FirstMortMoPyt.IsValid ? data.FirstMortMoPyt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECONDMORTBAL, data.SecondMortBal.IsValid ? data.SecondMortBal.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SECONDMORTPYMT, data.SecondMortPymt.IsValid ? data.SecondMortPymt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TAXINSNOTINC, data.TaxInsNotInc.IsValid ? data.TaxInsNotInc.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOADOFF, data.LoadOff.IsValid ? data.LoadOff.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.APPROFF, data.ApprOff.IsValid ? data.ApprOff.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FUNDINGOPP, data.FundingOpp.IsValid ? data.FundingOpp.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHANEL, data.Chanel.IsValid ? data.Chanel.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.RISKTYPE, data.RiskType.IsValid ? data.RiskType.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.GAP, data.Gap.IsValid ? data.Gap.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOADOPERATOR, data.LoadOperator.IsValid ? data.LoadOperator.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.DELIAMT, data.DeliAmt.IsValid ? data.DeliAmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CHGOFFAMT, data.ChgOffAmt.IsValid ? data.ChgOffAmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.USSECDESC, data.UsSecDesc.IsValid ? data.UsSecDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TROUBLEDDEBT, data.TroubledDebt.IsValid ? data.TroubledDebt.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.TDRDATE, data.TdrDate.IsValid ? data.TdrDate.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.JNTFICOUSED, data.JntFicoUsed.IsValid ? data.JntFicoUsed.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.SINGLEORJOINTLOAN, data.SingleOrJointLoan.IsValid ? data.SingleOrJointLoan.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.APPROVINGOFFICERUD, data.ApprovingOfficerUD.IsValid ? data.ApprovingOfficerUD.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INSURANCECODEDESC, data.InsuranceCodeDesc.IsValid ? data.InsuranceCodeDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.GAPINS, data.GapIns.IsValid ? data.GapIns.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MMPINS, data.MmpIns.IsValid ? data.MmpIns.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.REPORTCODEDESC, data.ReportCodeDesc.IsValid ? data.ReportCodeDesc.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.BALLONPMTLOAN, data.BallonPmtLoan.IsValid ? data.BallonPmtLoan.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FIXEDORVARIABLERATELOAN, data.FixedOrVariableRateLoan.IsValid ? data.FixedOrVariableRateLoan.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ALPSOFFICER, data.AlpsOfficer.IsValid ? data.AlpsOfficer.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CUDLAPPNUMBER, data.CUDLAppNumber.IsValid ? data.CUDLAppNumber.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASBDEFCOST, data.FasbDefCost.IsValid ? data.FasbDefCost.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.FASBLTDAMORTFEES, data.FasbLtdAmortFees.IsValid ? data.FasbLtdAmortFees.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTPRIORYTD, data.InterestPriorYtd.IsValid ? data.InterestPriorYtd.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.ACCRUEDINTEREST, data.AccruedInterest.IsValid ? data.AccruedInterest.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTLTD, data.InterestLTD.IsValid ? data.InterestLTD.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTYTD, data.InterestYtd.IsValid ? data.InterestYtd.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.PARTIALPAYAMT, data.PartialPayAmt.IsValid ? data.PartialPayAmt.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INTERESTUNCOLLECTED, data.InterestUncollected.IsValid ? data.InterestUncollected.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.CLOSEDACCTDATE, data.ClosedAcctDate.IsValid ? data.ClosedAcctDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LASTTRANDATE, data.LastTranDate.IsValid ? data.LastTranDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.NEXTDUEDATE, data.NextDueDate.IsValid ? data.NextDueDate.ToDate() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.INSURANCECODE, data.InsuranceCode.IsValid ? data.InsuranceCode.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.MBRAGEATAPPROVAL, data.MbrAgeAtApproval.IsValid ? data.MbrAgeAtApproval.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the LoanPortfolio table by LoanPortfolioId.
	/// </summary>
	/// <param name="loanPortfolio_Id">A key field.</param>
	public void Delete(IdType loanPortfolioId) {
	    Delete(loanPortfolioId, null);
	}

	/// <summary>
	/// Deletes a record from the LoanPortfolio table by LoanPortfolioId.
	/// </summary>
	/// <param name="loanPortfolio_Id">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType loanPortfolioId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spLoanPortfolio_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(LoanPortfolioFields.LOANPORTFOLIOID, loanPortfolioId.IsValid ? loanPortfolioId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	#region
	#endregion
    }
}
