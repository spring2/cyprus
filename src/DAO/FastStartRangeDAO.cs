using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for FastStartRange business entity.
    /// </summary>
    public class FastStartRangeDAO : Spring2.Core.DAO.SqlEntityDAO, IFastStartRangeDAO {
	private static IFastStartRangeDAO instance = new FastStartRangeDAO();
	public static IFastStartRangeDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IFastStartRangeDAO))) {
		    ClassRegistry.Register<IFastStartRangeDAO>(instance);
		}
		return ClassRegistry.Resolve<IFastStartRangeDAO>();
	    }
	}

	private static readonly String VIEW = "vwFastStartRange";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 FastStartRangeId;
	    public Int32 Value;
	    public Int32 Description;

	    internal ColumnOrdinals(IDataReader reader) {
		FastStartRangeId = reader.GetOrdinal("FastStartRangeId");
		Value = reader.GetOrdinal("Value");
		Description = reader.GetOrdinal("Description");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		FastStartRangeId = reader.GetOrdinal(prefix + "FastStartRangeId");
		Value = reader.GetOrdinal(prefix + "Value");
		Description = reader.GetOrdinal(prefix + "Description");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static FastStartRangeDAO() {
	    AddPropertyMapping("FastStartRangeId", @"FastStartRangeId");
	    AddPropertyMapping("Value", @"Value");
	    AddPropertyMapping("Description", @"Description");
	}

	private FastStartRangeDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all FastStartRange rows.
	/// </summary>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FastStartRangeList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FastStartRangeList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of FastStartRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FastStartRangeList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FastStartRangeList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    FastStartRangeList list = new FastStartRangeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all FastStartRange rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FastStartRangeList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FastStartRangeList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of FastStartRange rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FastStartRangeList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of FastStartRange rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FastStartRange objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FastStartRangeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    FastStartRangeList list = new FastStartRangeList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a FastStartRange entity using it's primary key.
	/// </summary>
	/// <param name="fastStartRangeId">A key field.</param>
	/// <returns>A FastStartRange object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public FastStartRange Load(IdType fastStartRangeId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(FastStartRangeFields.FASTSTARTRANGEID, EqualityOperatorEnum.Equal, fastStartRangeId.IsValid ? fastStartRangeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(FastStartRange instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(FastStartRangeFields.FASTSTARTRANGEID, EqualityOperatorEnum.Equal, instance.FastStartRangeId.IsValid ? instance.FastStartRangeId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for FastStartRange.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private FastStartRangeList GetList(IDataReader reader) {
	    FastStartRangeList list = new FastStartRangeList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private FastStartRange GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private FastStartRange GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    FastStartRange data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public FastStartRange GetDataObjectFromReader(FastStartRange data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public FastStartRange GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    FastStartRange data = new FastStartRange(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public FastStartRange GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    FastStartRange data = new FastStartRange(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public FastStartRange GetDataObjectFromReader(FastStartRange data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.FastStartRangeId)) {
		data.FastStartRangeId = IdType.UNSET;
	    } else {
		data.FastStartRangeId = new IdType(dataReader.GetInt32(ordinals.FastStartRangeId));
	    }
	    if (dataReader.IsDBNull(ordinals.Value)) {
		data.Value = IntegerType.UNSET;
	    } else {
		data.Value = new IntegerType(dataReader.GetInt32(ordinals.Value));
	    }
	    if (dataReader.IsDBNull(ordinals.Description)) {
		data.Description = StringType.UNSET;
	    } else {
		data.Description = StringType.Parse(dataReader.GetString(ordinals.Description));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the FastStartRange table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(FastStartRange data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the FastStartRange table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(FastStartRange data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFastStartRange_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(FastStartRangeFields.VALUE, data.Value.IsValid ? data.Value.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FastStartRangeFields.DESCRIPTION, data.Description.IsValid ? data.Description.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the FastStartRange table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(FastStartRange data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the FastStartRange table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(FastStartRange data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFastStartRange_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(FastStartRangeFields.FASTSTARTRANGEID, data.FastStartRangeId.IsValid ? data.FastStartRangeId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FastStartRangeFields.VALUE, data.Value.IsValid ? data.Value.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FastStartRangeFields.DESCRIPTION, data.Description.IsValid ? data.Description.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the FastStartRange table by FastStartRangeId.
	/// </summary>
	/// <param name="fastStartRangeId">A key field.</param>
	public void Delete(IdType fastStartRangeId) {
	    Delete(fastStartRangeId, null);
	}

	/// <summary>
	/// Deletes a record from the FastStartRange table by FastStartRangeId.
	/// </summary>
	/// <param name="fastStartRangeId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType fastStartRangeId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFastStartRange_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(FastStartRangeFields.FASTSTARTRANGEID, fastStartRangeId.IsValid ? fastStartRangeId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
