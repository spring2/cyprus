using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for ACCOUNT business entity.
    /// </summary>
    public class ACCOUNTDAO : Spring2.Core.DAO.SqlEntityDAO, IACCOUNTDAO {
	private static IACCOUNTDAO instance = new ACCOUNTDAO();
	public static IACCOUNTDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IACCOUNTDAO))) {
		    ClassRegistry.Register<IACCOUNTDAO>(instance);
		}
		return ClassRegistry.Resolve<IACCOUNTDAO>();
	    }
	}

	private static readonly String VIEW = "vwACCOUNT";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 ACCT_NUM;
	    public Int32 MEM_NUM;
	    public Int32 L_ACCT_LEVEL;
	    public Int32 ACCT_SUBLEV;
	    public Int32 ACCOUNT_OPEN_DATE;
	    public Int32 SPEC_REPORT_CODE;
	    public Int32 ORIG_PAY;
	    public Int32 APR;
	    public Int32 CURR_CREDIT_LIM;
	    public Int32 PG_CODE;
	    public Int32 DISC_REASON;
	    public Int32 RISK_OFFSET_VAL;
	    public Int32 FIRST_PAYDATE;
	    public Int32 ORIG_LOAN_TERM;
	    public Int32 L_MATURITY_DATE;
	    public Int32 L_INSCODE;
	    public Int32 L_SECCODE;
	    public Int32 L_SECDESC1;
	    public Int32 L_SECDESC2;
	    public Int32 US_APPLIC_ID;
	    public Int32 L_ACCT_TYPE;

	    internal ColumnOrdinals(IDataReader reader) {
		ACCT_NUM = reader.GetOrdinal("ACCT_NUM");
		MEM_NUM = reader.GetOrdinal("MEM_NUM");
		L_ACCT_LEVEL = reader.GetOrdinal("L_ACCT_LEVEL");
		ACCT_SUBLEV = reader.GetOrdinal("ACCT_SUBLEV");
		ACCOUNT_OPEN_DATE = reader.GetOrdinal("ACCOUNT_OPEN_DATE");
		SPEC_REPORT_CODE = reader.GetOrdinal("SPEC_REPORT_CODE");
		ORIG_PAY = reader.GetOrdinal("ORIG_PAY");
		APR = reader.GetOrdinal("APR");
		CURR_CREDIT_LIM = reader.GetOrdinal("CURR_CREDIT_LIM");
		PG_CODE = reader.GetOrdinal("PG_CODE");
		DISC_REASON = reader.GetOrdinal("DISC_REASON");
		RISK_OFFSET_VAL = reader.GetOrdinal("RISK_OFFSET_VAL");
		FIRST_PAYDATE = reader.GetOrdinal("FIRST_PAYDATE");
		ORIG_LOAN_TERM = reader.GetOrdinal("ORIG_LOAN_TERM");
		L_MATURITY_DATE = reader.GetOrdinal("L_MATURITY_DATE");
		L_INSCODE = reader.GetOrdinal("L_INSCODE");
		L_SECCODE = reader.GetOrdinal("L_SECCODE");
		L_SECDESC1 = reader.GetOrdinal("L_SECDESC1");
		L_SECDESC2 = reader.GetOrdinal("L_SECDESC2");
		US_APPLIC_ID = reader.GetOrdinal("US_APPLIC_ID");
		L_ACCT_TYPE = reader.GetOrdinal("L_ACCT_TYPE");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		ACCT_NUM = reader.GetOrdinal(prefix + "ACCT_NUM");
		MEM_NUM = reader.GetOrdinal(prefix + "MEM_NUM");
		L_ACCT_LEVEL = reader.GetOrdinal(prefix + "L_ACCT_LEVEL");
		ACCT_SUBLEV = reader.GetOrdinal(prefix + "ACCT_SUBLEV");
		ACCOUNT_OPEN_DATE = reader.GetOrdinal(prefix + "ACCOUNT_OPEN_DATE");
		SPEC_REPORT_CODE = reader.GetOrdinal(prefix + "SPEC_REPORT_CODE");
		ORIG_PAY = reader.GetOrdinal(prefix + "ORIG_PAY");
		APR = reader.GetOrdinal(prefix + "APR");
		CURR_CREDIT_LIM = reader.GetOrdinal(prefix + "CURR_CREDIT_LIM");
		PG_CODE = reader.GetOrdinal(prefix + "PG_CODE");
		DISC_REASON = reader.GetOrdinal(prefix + "DISC_REASON");
		RISK_OFFSET_VAL = reader.GetOrdinal(prefix + "RISK_OFFSET_VAL");
		FIRST_PAYDATE = reader.GetOrdinal(prefix + "FIRST_PAYDATE");
		ORIG_LOAN_TERM = reader.GetOrdinal(prefix + "ORIG_LOAN_TERM");
		L_MATURITY_DATE = reader.GetOrdinal(prefix + "L_MATURITY_DATE");
		L_INSCODE = reader.GetOrdinal(prefix + "L_INSCODE");
		L_SECCODE = reader.GetOrdinal(prefix + "L_SECCODE");
		L_SECDESC1 = reader.GetOrdinal(prefix + "L_SECDESC1");
		L_SECDESC2 = reader.GetOrdinal(prefix + "L_SECDESC2");
		US_APPLIC_ID = reader.GetOrdinal(prefix + "US_APPLIC_ID");
		L_ACCT_TYPE = reader.GetOrdinal(prefix + "L_ACCT_TYPE");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static ACCOUNTDAO() {
	    AddPropertyMapping("ACCT_NUM", @"ACCT_NUM");
	    AddPropertyMapping("MEM_NUM", @"MEM_NUM");
	    AddPropertyMapping("L_ACCT_LEVEL", @"L_ACCT_LEVEL");
	    AddPropertyMapping("ACCT_SUBLEV", @"ACCT_SUBLEV");
	    AddPropertyMapping("ACCOUNT_OPEN_DATE", @"ACCOUNT_OPEN_DATE");
	    AddPropertyMapping("SPEC_REPORT_CODE", @"SPEC_REPORT_CODE");
	    AddPropertyMapping("ORIG_PAY", @"ORIG_PAY");
	    AddPropertyMapping("APR", @"APR");
	    AddPropertyMapping("CURR_CREDIT_LIM", @"CURR_CREDIT_LIM");
	    AddPropertyMapping("PG_CODE", @"PG_CODE");
	    AddPropertyMapping("DISC_REASON", @"DISC_REASON");
	    AddPropertyMapping("RISK_OFFSET_VAL", @"RISK_OFFSET_VAL");
	    AddPropertyMapping("FIRST_PAYDATE", @"FIRST_PAYDATE");
	    AddPropertyMapping("ORIG_LOAN_TERM", @"ORIG_LOAN_TERM");
	    AddPropertyMapping("L_MATURITY_DATE", @"L_MATURITY_DATE");
	    AddPropertyMapping("L_INSCODE", @"L_INSCODE");
	    AddPropertyMapping("L_SECCODE", @"L_SECCODE");
	    AddPropertyMapping("L_SECDESC1", @"L_SECDESC1");
	    AddPropertyMapping("L_SECDESC2", @"L_SECDESC2");
	    AddPropertyMapping("US_APPLIC_ID", @"US_APPLIC_ID");
	    AddPropertyMapping("L_ACCT_TYPE", @"L_ACCT_TYPE");
	}

	private ACCOUNTDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all ACCOUNT rows.
	/// </summary>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNTList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of ACCOUNT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNTList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of ACCOUNT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNTList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNTList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    ACCOUNTList list = new ACCOUNTList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all ACCOUNT rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNTList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of ACCOUNT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNTList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of ACCOUNT rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public ACCOUNTList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of ACCOUNT rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of ACCOUNT objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public ACCOUNTList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    ACCOUNTList list = new ACCOUNTList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a ACCOUNT entity using it's primary key.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <returns>A ACCOUNT object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public ACCOUNT Load(IdType aCCT_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ACCOUNTFields.ACCT_NUM, EqualityOperatorEnum.Equal, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(ACCOUNT instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(ACCOUNTFields.ACCT_NUM, EqualityOperatorEnum.Equal, instance.ACCT_NUM.IsValid ? instance.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for ACCOUNT.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private ACCOUNTList GetList(IDataReader reader) {
	    ACCOUNTList list = new ACCOUNTList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private ACCOUNT GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private ACCOUNT GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    ACCOUNT data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT GetDataObjectFromReader(ACCOUNT data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    ACCOUNT data = new ACCOUNT(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    ACCOUNT data = new ACCOUNT(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public ACCOUNT GetDataObjectFromReader(ACCOUNT data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.ACCT_NUM)) {
		data.ACCT_NUM = IdType.UNSET;
	    } else {
		data.ACCT_NUM = new IdType(dataReader.GetInt32(ordinals.ACCT_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.MEM_NUM)) {
		data.MEM_NUM = IdType.UNSET;
	    } else {
		data.MEM_NUM = new IdType(dataReader.GetInt32(ordinals.MEM_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.L_ACCT_LEVEL)) {
		data.L_ACCT_LEVEL = DecimalType.UNSET;
	    } else {
		data.L_ACCT_LEVEL = new DecimalType(dataReader.GetDecimal(ordinals.L_ACCT_LEVEL));
	    }
	    if (dataReader.IsDBNull(ordinals.ACCT_SUBLEV)) {
		data.ACCT_SUBLEV = IntegerType.UNSET;
	    } else {
		data.ACCT_SUBLEV = new IntegerType(dataReader.GetInt32(ordinals.ACCT_SUBLEV));
	    }
	    if (dataReader.IsDBNull(ordinals.ACCOUNT_OPEN_DATE)) {
		data.ACCOUNT_OPEN_DATE = DateTimeType.UNSET;
	    } else {
		data.ACCOUNT_OPEN_DATE = new DateTimeType(dataReader.GetDateTime(ordinals.ACCOUNT_OPEN_DATE));
	    }
	    if (dataReader.IsDBNull(ordinals.SPEC_REPORT_CODE)) {
		data.SPEC_REPORT_CODE = StringType.UNSET;
	    } else {
		data.SPEC_REPORT_CODE = StringType.Parse(dataReader.GetString(ordinals.SPEC_REPORT_CODE));
	    }
	    if (dataReader.IsDBNull(ordinals.ORIG_PAY)) {
		data.ORIG_PAY = CurrencyType.UNSET;
	    } else {
		data.ORIG_PAY = new CurrencyType(dataReader.GetDecimal(ordinals.ORIG_PAY));
	    }
	    if (dataReader.IsDBNull(ordinals.APR)) {
		data.APR = DecimalType.UNSET;
	    } else {
		data.APR = new DecimalType(dataReader.GetDecimal(ordinals.APR));
	    }
	    if (dataReader.IsDBNull(ordinals.CURR_CREDIT_LIM)) {
		data.CURR_CREDIT_LIM = CurrencyType.UNSET;
	    } else {
		data.CURR_CREDIT_LIM = new CurrencyType(dataReader.GetDecimal(ordinals.CURR_CREDIT_LIM));
	    }
	    if (dataReader.IsDBNull(ordinals.PG_CODE)) {
		data.PG_CODE = StringType.UNSET;
	    } else {
		data.PG_CODE = StringType.Parse(dataReader.GetString(ordinals.PG_CODE));
	    }
	    if (dataReader.IsDBNull(ordinals.DISC_REASON)) {
		data.DISC_REASON = StringType.UNSET;
	    } else {
		data.DISC_REASON = StringType.Parse(dataReader.GetString(ordinals.DISC_REASON));
	    }
	    if (dataReader.IsDBNull(ordinals.RISK_OFFSET_VAL)) {
		data.RISK_OFFSET_VAL = StringType.UNSET;
	    } else {
		data.RISK_OFFSET_VAL = StringType.Parse(dataReader.GetString(ordinals.RISK_OFFSET_VAL));
	    }
	    if (dataReader.IsDBNull(ordinals.FIRST_PAYDATE)) {
		data.FIRST_PAYDATE = DateTimeType.UNSET;
	    } else {
		data.FIRST_PAYDATE = new DateTimeType(dataReader.GetDateTime(ordinals.FIRST_PAYDATE));
	    }
	    if (dataReader.IsDBNull(ordinals.ORIG_LOAN_TERM)) {
		data.ORIG_LOAN_TERM = IntegerType.UNSET;
	    } else {
		data.ORIG_LOAN_TERM = new IntegerType(dataReader.GetInt32(ordinals.ORIG_LOAN_TERM));
	    }
	    if (dataReader.IsDBNull(ordinals.L_MATURITY_DATE)) {
		data.L_MATURITY_DATE = DateTimeType.UNSET;
	    } else {
		data.L_MATURITY_DATE = new DateTimeType(dataReader.GetDateTime(ordinals.L_MATURITY_DATE));
	    }
	    if (dataReader.IsDBNull(ordinals.L_INSCODE)) {
		data.L_INSCODE = IntegerType.UNSET;
	    } else {
		data.L_INSCODE = new IntegerType(dataReader.GetInt32(ordinals.L_INSCODE));
	    }
	    if (dataReader.IsDBNull(ordinals.L_SECCODE)) {
		data.L_SECCODE = IntegerType.UNSET;
	    } else {
		data.L_SECCODE = new IntegerType(dataReader.GetInt32(ordinals.L_SECCODE));
	    }
	    if (dataReader.IsDBNull(ordinals.L_SECDESC1)) {
		data.L_SECDESC1 = StringType.UNSET;
	    } else {
		data.L_SECDESC1 = StringType.Parse(dataReader.GetString(ordinals.L_SECDESC1));
	    }
	    if (dataReader.IsDBNull(ordinals.L_SECDESC2)) {
		data.L_SECDESC2 = StringType.UNSET;
	    } else {
		data.L_SECDESC2 = StringType.Parse(dataReader.GetString(ordinals.L_SECDESC2));
	    }
	    if (dataReader.IsDBNull(ordinals.US_APPLIC_ID)) {
		data.US_APPLIC_ID = IdType.UNSET;
	    } else {
		data.US_APPLIC_ID = new IdType(dataReader.GetInt32(ordinals.US_APPLIC_ID));
	    }
	    if (dataReader.IsDBNull(ordinals.L_ACCT_TYPE)) {
		data.L_ACCT_TYPE = StringType.UNSET;
	    } else {
		data.L_ACCT_TYPE = StringType.Parse(dataReader.GetString(ordinals.L_ACCT_TYPE));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the ACCOUNT table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(ACCOUNT data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the ACCOUNT table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(ACCOUNT data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.MEM_NUM, data.MEM_NUM.IsValid ? data.MEM_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_ACCT_LEVEL, data.L_ACCT_LEVEL.IsValid ? data.L_ACCT_LEVEL.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ACCT_SUBLEV, data.ACCT_SUBLEV.IsValid ? data.ACCT_SUBLEV.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ACCOUNT_OPEN_DATE, data.ACCOUNT_OPEN_DATE.IsValid ? data.ACCOUNT_OPEN_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.SPEC_REPORT_CODE, data.SPEC_REPORT_CODE.IsValid ? data.SPEC_REPORT_CODE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ORIG_PAY, data.ORIG_PAY.IsValid ? data.ORIG_PAY.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.APR, data.APR.IsValid ? data.APR.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.CURR_CREDIT_LIM, data.CURR_CREDIT_LIM.IsValid ? data.CURR_CREDIT_LIM.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.PG_CODE, data.PG_CODE.IsValid ? data.PG_CODE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.DISC_REASON, data.DISC_REASON.IsValid ? data.DISC_REASON.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.RISK_OFFSET_VAL, data.RISK_OFFSET_VAL.IsValid ? data.RISK_OFFSET_VAL.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.FIRST_PAYDATE, data.FIRST_PAYDATE.IsValid ? data.FIRST_PAYDATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ORIG_LOAN_TERM, data.ORIG_LOAN_TERM.IsValid ? data.ORIG_LOAN_TERM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_MATURITY_DATE, data.L_MATURITY_DATE.IsValid ? data.L_MATURITY_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_INSCODE, data.L_INSCODE.IsValid ? data.L_INSCODE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_SECCODE, data.L_SECCODE.IsValid ? data.L_SECCODE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_SECDESC1, data.L_SECDESC1.IsValid ? data.L_SECDESC1.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_SECDESC2, data.L_SECDESC2.IsValid ? data.L_SECDESC2.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.US_APPLIC_ID, data.US_APPLIC_ID.IsValid ? data.US_APPLIC_ID.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_ACCT_TYPE, data.L_ACCT_TYPE.IsValid ? data.L_ACCT_TYPE.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the ACCOUNT table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(ACCOUNT data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the ACCOUNT table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(ACCOUNT data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ACCT_NUM, data.ACCT_NUM.IsValid ? data.ACCT_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.MEM_NUM, data.MEM_NUM.IsValid ? data.MEM_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_ACCT_LEVEL, data.L_ACCT_LEVEL.IsValid ? data.L_ACCT_LEVEL.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ACCT_SUBLEV, data.ACCT_SUBLEV.IsValid ? data.ACCT_SUBLEV.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ACCOUNT_OPEN_DATE, data.ACCOUNT_OPEN_DATE.IsValid ? data.ACCOUNT_OPEN_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.SPEC_REPORT_CODE, data.SPEC_REPORT_CODE.IsValid ? data.SPEC_REPORT_CODE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ORIG_PAY, data.ORIG_PAY.IsValid ? data.ORIG_PAY.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.APR, data.APR.IsValid ? data.APR.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.CURR_CREDIT_LIM, data.CURR_CREDIT_LIM.IsValid ? data.CURR_CREDIT_LIM.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.PG_CODE, data.PG_CODE.IsValid ? data.PG_CODE.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.DISC_REASON, data.DISC_REASON.IsValid ? data.DISC_REASON.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.RISK_OFFSET_VAL, data.RISK_OFFSET_VAL.IsValid ? data.RISK_OFFSET_VAL.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.FIRST_PAYDATE, data.FIRST_PAYDATE.IsValid ? data.FIRST_PAYDATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ORIG_LOAN_TERM, data.ORIG_LOAN_TERM.IsValid ? data.ORIG_LOAN_TERM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_MATURITY_DATE, data.L_MATURITY_DATE.IsValid ? data.L_MATURITY_DATE.ToDateTime() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_INSCODE, data.L_INSCODE.IsValid ? data.L_INSCODE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_SECCODE, data.L_SECCODE.IsValid ? data.L_SECCODE.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_SECDESC1, data.L_SECDESC1.IsValid ? data.L_SECDESC1.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_SECDESC2, data.L_SECDESC2.IsValid ? data.L_SECDESC2.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.US_APPLIC_ID, data.US_APPLIC_ID.IsValid ? data.US_APPLIC_ID.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.L_ACCT_TYPE, data.L_ACCT_TYPE.IsValid ? data.L_ACCT_TYPE.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the ACCOUNT table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	public void Delete(IdType aCCT_NUM) {
	    Delete(aCCT_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the ACCOUNT table by ACCT_NUM.
	/// </summary>
	/// <param name="aCCT_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aCCT_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spACCOUNT_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(ACCOUNTFields.ACCT_NUM, aCCT_NUM.IsValid ? aCCT_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
