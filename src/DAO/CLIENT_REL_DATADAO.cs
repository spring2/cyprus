using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for CLIENT_REL_DATA business entity.
    /// </summary>
    public class CLIENT_REL_DATADAO : Spring2.Core.DAO.SqlEntityDAO, ICLIENT_REL_DATADAO {
	private static ICLIENT_REL_DATADAO instance = new CLIENT_REL_DATADAO();
	public static ICLIENT_REL_DATADAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(ICLIENT_REL_DATADAO))) {
		    ClassRegistry.Register<ICLIENT_REL_DATADAO>(instance);
		}
		return ClassRegistry.Resolve<ICLIENT_REL_DATADAO>();
	    }
	}

	private static readonly String VIEW = "vwCLIENT_REL_DATA";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 MEM_NUM;
	    public Int32 AGE;

	    internal ColumnOrdinals(IDataReader reader) {
		MEM_NUM = reader.GetOrdinal("MEM_NUM");
		AGE = reader.GetOrdinal("AGE");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		MEM_NUM = reader.GetOrdinal(prefix + "MEM_NUM");
		AGE = reader.GetOrdinal(prefix + "AGE");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static CLIENT_REL_DATADAO() {
	    AddPropertyMapping("MEM_NUM", @"MEM_NUM");
	    AddPropertyMapping("AGE", @"AGE");
	}

	private CLIENT_REL_DATADAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all CLIENT_REL_DATA rows.
	/// </summary>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENT_REL_DATAList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of CLIENT_REL_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENT_REL_DATAList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of CLIENT_REL_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENT_REL_DATAList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of CLIENT_REL_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENT_REL_DATAList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    CLIENT_REL_DATAList list = new CLIENT_REL_DATAList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all CLIENT_REL_DATA rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENT_REL_DATAList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of CLIENT_REL_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENT_REL_DATAList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of CLIENT_REL_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public CLIENT_REL_DATAList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of CLIENT_REL_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of CLIENT_REL_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public CLIENT_REL_DATAList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    CLIENT_REL_DATAList list = new CLIENT_REL_DATAList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a CLIENT_REL_DATA entity using it's primary key.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	/// <returns>A CLIENT_REL_DATA object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public CLIENT_REL_DATA Load(IdType mEM_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(CLIENT_REL_DATAFields.MEM_NUM, EqualityOperatorEnum.Equal, mEM_NUM.IsValid ? mEM_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(CLIENT_REL_DATA instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(CLIENT_REL_DATAFields.MEM_NUM, EqualityOperatorEnum.Equal, instance.MEM_NUM.IsValid ? instance.MEM_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for CLIENT_REL_DATA.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private CLIENT_REL_DATAList GetList(IDataReader reader) {
	    CLIENT_REL_DATAList list = new CLIENT_REL_DATAList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private CLIENT_REL_DATA GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private CLIENT_REL_DATA GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    CLIENT_REL_DATA data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT_REL_DATA GetDataObjectFromReader(CLIENT_REL_DATA data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT_REL_DATA GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    CLIENT_REL_DATA data = new CLIENT_REL_DATA(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT_REL_DATA GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    CLIENT_REL_DATA data = new CLIENT_REL_DATA(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public CLIENT_REL_DATA GetDataObjectFromReader(CLIENT_REL_DATA data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.MEM_NUM)) {
		data.MEM_NUM = IdType.UNSET;
	    } else {
		data.MEM_NUM = new IdType(dataReader.GetInt32(ordinals.MEM_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.AGE)) {
		data.AGE = IntegerType.UNSET;
	    } else {
		data.AGE = new IntegerType(dataReader.GetInt32(ordinals.AGE));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the CLIENT_REL_DATA table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(CLIENT_REL_DATA data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the CLIENT_REL_DATA table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(CLIENT_REL_DATA data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spCLIENT_REL_DATA_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(CLIENT_REL_DATAFields.AGE, data.AGE.IsValid ? data.AGE.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the CLIENT_REL_DATA table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(CLIENT_REL_DATA data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the CLIENT_REL_DATA table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(CLIENT_REL_DATA data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spCLIENT_REL_DATA_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(CLIENT_REL_DATAFields.MEM_NUM, data.MEM_NUM.IsValid ? data.MEM_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(CLIENT_REL_DATAFields.AGE, data.AGE.IsValid ? data.AGE.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the CLIENT_REL_DATA table by MEM_NUM.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	public void Delete(IdType mEM_NUM) {
	    Delete(mEM_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the CLIENT_REL_DATA table by MEM_NUM.
	/// </summary>
	/// <param name="mEM_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType mEM_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spCLIENT_REL_DATA_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(CLIENT_REL_DATAFields.MEM_NUM, mEM_NUM.IsValid ? mEM_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
