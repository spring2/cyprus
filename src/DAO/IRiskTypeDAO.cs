using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.Types;
using Spring2.DataObject;


using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.Types;

using Spring2.Core.BusinessEntity;

using Spring2.DataTierGenerator.Attribute;


namespace Spring2.Dao {
    /// <summary>
    /// Data access interface for RiskType business entity.
    /// </summary>
    public interface IRiskTypeDAO {
	/// <summary>
	/// Returns a list of all RiskType rows.
	/// </summary>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	RiskTypeList GetList();

	/// <summary>
	/// Returns a filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	RiskTypeList GetList(SqlFilter filter);

	/// <summary>
	/// Returns an ordered list of RiskType rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	RiskTypeList GetList(IOrderBy orderByClause);

	/// <summary>
	/// Returns an ordered and filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	RiskTypeList GetList(SqlFilter filter, IOrderBy orderByClause);

	/// <summary>
	/// Returns a list of all RiskType rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	RiskTypeList GetList(Int32 maxRows);

	/// <summary>
	/// Returns a filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	RiskTypeList GetList(SqlFilter filter, Int32 maxRows);

	/// <summary>
	/// Returns an ordered list of RiskType rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	[Generate]
	RiskTypeList GetList(IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Returns an ordered and filtered list of RiskType rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of RiskType objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	[Generate]
	RiskTypeList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows);

	/// <summary>
	/// Finds a RiskType entity using it's primary key.
	/// </summary>
	/// <param name="riskTypeId">A key field.</param>
	/// <returns>A RiskType object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	[Generate]
	RiskType Load(IdType riskTypeId);

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	[Generate]
	void Reload(RiskType instance);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	RiskType GetDataObjectFromReader(RiskType data, IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	RiskType GetDataObjectFromReader(IDataReader dataReader);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	RiskType GetDataObjectFromReader(IDataReader dataReader, RiskTypeDAO.ColumnOrdinals ordinals);

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	[Generate]
	RiskType GetDataObjectFromReader(RiskType data, IDataReader dataReader, RiskTypeDAO.ColumnOrdinals ordinals);


	/// <summary>
	/// Inserts a record into the RiskType table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	IdType Insert(RiskType data);


	/// <summary>
	/// Updates a record in the RiskType table.
	/// </summary>
	/// <param name="data"></param>
	[Generate]
	void Update(RiskType data);

	/// <summary>
	/// Deletes a record from the RiskType table by RiskTypeId.
	/// </summary>
	/// <param name="riskTypeId">A key field.</param>
	[Generate]
	void Delete(IdType riskTypeId);


	//}
    }
}
