using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for FieldMap business entity.
    /// </summary>
    public class FieldMapDAO : Spring2.Core.DAO.SqlEntityDAO, IFieldMapDAO {
	private static IFieldMapDAO instance = new FieldMapDAO();
	public static IFieldMapDAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IFieldMapDAO))) {
		    ClassRegistry.Register<IFieldMapDAO>(instance);
		}
		return ClassRegistry.Resolve<IFieldMapDAO>();
	    }
	}

	private static readonly String VIEW = "vwFieldMap";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 FieldMapId;
	    public Int32 SrcTable;
	    public Int32 SrcColumn;
	    public Int32 DestTable;
	    public Int32 DestColumn;

	    internal ColumnOrdinals(IDataReader reader) {
		FieldMapId = reader.GetOrdinal("FieldMapId");
		SrcTable = reader.GetOrdinal("SrcTable");
		SrcColumn = reader.GetOrdinal("SrcColumn");
		DestTable = reader.GetOrdinal("DestTable");
		DestColumn = reader.GetOrdinal("DestColumn");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		FieldMapId = reader.GetOrdinal(prefix + "FieldMapId");
		SrcTable = reader.GetOrdinal(prefix + "SrcTable");
		SrcColumn = reader.GetOrdinal(prefix + "SrcColumn");
		DestTable = reader.GetOrdinal(prefix + "DestTable");
		DestColumn = reader.GetOrdinal(prefix + "DestColumn");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static FieldMapDAO() {
	    AddPropertyMapping("FieldMapId", @"FieldMapId");
	    AddPropertyMapping("SrcTable", @"SrcTable");
	    AddPropertyMapping("SrcColumn", @"SrcColumn");
	    AddPropertyMapping("DestTable", @"DestTable");
	    AddPropertyMapping("DestColumn", @"DestColumn");
	}

	private FieldMapDAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all FieldMap rows.
	/// </summary>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FieldMapList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FieldMapList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of FieldMap rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FieldMapList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FieldMapList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    FieldMapList list = new FieldMapList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all FieldMap rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FieldMapList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FieldMapList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of FieldMap rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public FieldMapList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of FieldMap rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of FieldMap objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public FieldMapList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    FieldMapList list = new FieldMapList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a FieldMap entity using it's primary key.
	/// </summary>
	/// <param name="fieldMapId">A key field.</param>
	/// <returns>A FieldMap object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public FieldMap Load(IdType fieldMapId) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(FieldMapFields.FIELDMAPID, EqualityOperatorEnum.Equal, fieldMapId.IsValid ? fieldMapId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(FieldMap instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(FieldMapFields.FIELDMAPID, EqualityOperatorEnum.Equal, instance.FieldMapId.IsValid ? instance.FieldMapId.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for FieldMap.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private FieldMapList GetList(IDataReader reader) {
	    FieldMapList list = new FieldMapList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private FieldMap GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private FieldMap GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    FieldMap data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public FieldMap GetDataObjectFromReader(FieldMap data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public FieldMap GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    FieldMap data = new FieldMap(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public FieldMap GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    FieldMap data = new FieldMap(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public FieldMap GetDataObjectFromReader(FieldMap data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.FieldMapId)) {
		data.FieldMapId = IdType.UNSET;
	    } else {
		data.FieldMapId = new IdType(dataReader.GetInt32(ordinals.FieldMapId));
	    }
	    if (dataReader.IsDBNull(ordinals.SrcTable)) {
		data.SrcTable = StringType.UNSET;
	    } else {
		data.SrcTable = StringType.Parse(dataReader.GetString(ordinals.SrcTable));
	    }
	    if (dataReader.IsDBNull(ordinals.SrcColumn)) {
		data.SrcColumn = StringType.UNSET;
	    } else {
		data.SrcColumn = StringType.Parse(dataReader.GetString(ordinals.SrcColumn));
	    }
	    if (dataReader.IsDBNull(ordinals.DestTable)) {
		data.DestTable = StringType.UNSET;
	    } else {
		data.DestTable = StringType.Parse(dataReader.GetString(ordinals.DestTable));
	    }
	    if (dataReader.IsDBNull(ordinals.DestColumn)) {
		data.DestColumn = StringType.UNSET;
	    } else {
		data.DestColumn = StringType.Parse(dataReader.GetString(ordinals.DestColumn));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the FieldMap table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(FieldMap data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the FieldMap table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(FieldMap data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFieldMap_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.SRCTABLE, data.SrcTable.IsValid ? data.SrcTable.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.SRCCOLUMN, data.SrcColumn.IsValid ? data.SrcColumn.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.DESTTABLE, data.DestTable.IsValid ? data.DestTable.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.DESTCOLUMN, data.DestColumn.IsValid ? data.DestColumn.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the FieldMap table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(FieldMap data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the FieldMap table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(FieldMap data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFieldMap_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.FIELDMAPID, data.FieldMapId.IsValid ? data.FieldMapId.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.SRCTABLE, data.SrcTable.IsValid ? data.SrcTable.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.SRCCOLUMN, data.SrcColumn.IsValid ? data.SrcColumn.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.DESTTABLE, data.DestTable.IsValid ? data.DestTable.ToString() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.DESTCOLUMN, data.DestColumn.IsValid ? data.DestColumn.ToString() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the FieldMap table by FieldMapId.
	/// </summary>
	/// <param name="fieldMapId">A key field.</param>
	public void Delete(IdType fieldMapId) {
	    Delete(fieldMapId, null);
	}

	/// <summary>
	/// Deletes a record from the FieldMap table by FieldMapId.
	/// </summary>
	/// <param name="fieldMapId">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType fieldMapId, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spFieldMap_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(FieldMapFields.FIELDMAPID, fieldMapId.IsValid ? fieldMapId.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
