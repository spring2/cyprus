using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for APPLIC_PROFILE_DATA business entity.
    /// </summary>
    public class APPLIC_PROFILE_DATADAO : Spring2.Core.DAO.SqlEntityDAO, IAPPLIC_PROFILE_DATADAO {
	private static IAPPLIC_PROFILE_DATADAO instance = new APPLIC_PROFILE_DATADAO();
	public static IAPPLIC_PROFILE_DATADAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IAPPLIC_PROFILE_DATADAO))) {
		    ClassRegistry.Register<IAPPLIC_PROFILE_DATADAO>(instance);
		}
		return ClassRegistry.Resolve<IAPPLIC_PROFILE_DATADAO>();
	    }
	}

	private static readonly String VIEW = "vwAPPLIC_PROFILE_DATA";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 APPLIC_NUM;
	    public Int32 NO_YRS_ADDR;
	    public Int32 PREV_YRS_ADDR;

	    internal ColumnOrdinals(IDataReader reader) {
		APPLIC_NUM = reader.GetOrdinal("APPLIC_NUM");
		NO_YRS_ADDR = reader.GetOrdinal("NO_YRS_ADDR");
		PREV_YRS_ADDR = reader.GetOrdinal("PREV_YRS_ADDR");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		APPLIC_NUM = reader.GetOrdinal(prefix + "APPLIC_NUM");
		NO_YRS_ADDR = reader.GetOrdinal(prefix + "NO_YRS_ADDR");
		PREV_YRS_ADDR = reader.GetOrdinal(prefix + "PREV_YRS_ADDR");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static APPLIC_PROFILE_DATADAO() {
	    AddPropertyMapping("APPLIC_NUM", @"APPLIC_NUM");
	    AddPropertyMapping("NO_YRS_ADDR", @"NO_YRS_ADDR");
	    AddPropertyMapping("PREV_YRS_ADDR", @"PREV_YRS_ADDR");
	}

	private APPLIC_PROFILE_DATADAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_PROFILE_DATAList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_PROFILE_DATAList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of APPLIC_PROFILE_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_PROFILE_DATAList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_PROFILE_DATAList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    APPLIC_PROFILE_DATAList list = new APPLIC_PROFILE_DATAList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_PROFILE_DATAList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_PROFILE_DATAList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of APPLIC_PROFILE_DATA rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public APPLIC_PROFILE_DATAList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of APPLIC_PROFILE_DATA rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of APPLIC_PROFILE_DATA objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public APPLIC_PROFILE_DATAList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    APPLIC_PROFILE_DATAList list = new APPLIC_PROFILE_DATAList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a APPLIC_PROFILE_DATA entity using it's primary key.
	/// </summary>
	/// <param name="aPPLIC_NUM">A key field.</param>
	/// <returns>A APPLIC_PROFILE_DATA object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public APPLIC_PROFILE_DATA Load(IdType aPPLIC_NUM) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(APPLIC_PROFILE_DATAFields.APPLIC_NUM, EqualityOperatorEnum.Equal, aPPLIC_NUM.IsValid ? aPPLIC_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(APPLIC_PROFILE_DATA instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(APPLIC_PROFILE_DATAFields.APPLIC_NUM, EqualityOperatorEnum.Equal, instance.APPLIC_NUM.IsValid ? instance.APPLIC_NUM.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for APPLIC_PROFILE_DATA.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private APPLIC_PROFILE_DATAList GetList(IDataReader reader) {
	    APPLIC_PROFILE_DATAList list = new APPLIC_PROFILE_DATAList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private APPLIC_PROFILE_DATA GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private APPLIC_PROFILE_DATA GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    APPLIC_PROFILE_DATA data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_PROFILE_DATA GetDataObjectFromReader(APPLIC_PROFILE_DATA data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_PROFILE_DATA GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    APPLIC_PROFILE_DATA data = new APPLIC_PROFILE_DATA(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_PROFILE_DATA GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    APPLIC_PROFILE_DATA data = new APPLIC_PROFILE_DATA(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public APPLIC_PROFILE_DATA GetDataObjectFromReader(APPLIC_PROFILE_DATA data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.APPLIC_NUM)) {
		data.APPLIC_NUM = IdType.UNSET;
	    } else {
		data.APPLIC_NUM = new IdType(dataReader.GetInt32(ordinals.APPLIC_NUM));
	    }
	    if (dataReader.IsDBNull(ordinals.NO_YRS_ADDR)) {
		data.NO_YRS_ADDR = IntegerType.UNSET;
	    } else {
		data.NO_YRS_ADDR = new IntegerType(dataReader.GetInt32(ordinals.NO_YRS_ADDR));
	    }
	    if (dataReader.IsDBNull(ordinals.PREV_YRS_ADDR)) {
		data.PREV_YRS_ADDR = IntegerType.UNSET;
	    } else {
		data.PREV_YRS_ADDR = new IntegerType(dataReader.GetInt32(ordinals.PREV_YRS_ADDR));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the APPLIC_PROFILE_DATA table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(APPLIC_PROFILE_DATA data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the APPLIC_PROFILE_DATA table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(APPLIC_PROFILE_DATA data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_PROFILE_DATA_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_PROFILE_DATAFields.NO_YRS_ADDR, data.NO_YRS_ADDR.IsValid ? data.NO_YRS_ADDR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_PROFILE_DATAFields.PREV_YRS_ADDR, data.PREV_YRS_ADDR.IsValid ? data.PREV_YRS_ADDR.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the APPLIC_PROFILE_DATA table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(APPLIC_PROFILE_DATA data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the APPLIC_PROFILE_DATA table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(APPLIC_PROFILE_DATA data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_PROFILE_DATA_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_PROFILE_DATAFields.APPLIC_NUM, data.APPLIC_NUM.IsValid ? data.APPLIC_NUM.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_PROFILE_DATAFields.NO_YRS_ADDR, data.NO_YRS_ADDR.IsValid ? data.NO_YRS_ADDR.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_PROFILE_DATAFields.PREV_YRS_ADDR, data.PREV_YRS_ADDR.IsValid ? data.PREV_YRS_ADDR.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the APPLIC_PROFILE_DATA table by APPLIC_NUM.
	/// </summary>
	/// <param name="aPPLIC_NUM">A key field.</param>
	public void Delete(IdType aPPLIC_NUM) {
	    Delete(aPPLIC_NUM, null);
	}

	/// <summary>
	/// Deletes a record from the APPLIC_PROFILE_DATA table by APPLIC_NUM.
	/// </summary>
	/// <param name="aPPLIC_NUM">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType aPPLIC_NUM, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spAPPLIC_PROFILE_DATA_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(APPLIC_PROFILE_DATAFields.APPLIC_NUM, aPPLIC_NUM.IsValid ? aPPLIC_NUM.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
