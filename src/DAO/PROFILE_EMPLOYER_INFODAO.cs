using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System;

using Spring2.Core.DAO;
using Spring2.Core.IoC;
using Spring2.Core.Types;

using Spring2.BusinessLogic;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Dao {
    /// <summary>
    /// Data access class for PROFILE_EMPLOYER_INFO business entity.
    /// </summary>
    public class PROFILE_EMPLOYER_INFODAO : Spring2.Core.DAO.SqlEntityDAO, IPROFILE_EMPLOYER_INFODAO {
	private static IPROFILE_EMPLOYER_INFODAO instance = new PROFILE_EMPLOYER_INFODAO();
	public static IPROFILE_EMPLOYER_INFODAO DAO {
	    get {
		if (!ClassRegistry.CanResolve(typeof(IPROFILE_EMPLOYER_INFODAO))) {
		    ClassRegistry.Register<IPROFILE_EMPLOYER_INFODAO>(instance);
		}
		return ClassRegistry.Resolve<IPROFILE_EMPLOYER_INFODAO>();
	    }
	}

	private static readonly String VIEW = "vwPROFILE_EMPLOYER_INFO";
	private static readonly String CONNECTION_STRING_KEY = "ConnectionString";
	private static readonly Int32 COMMAND_TIMEOUT = 60;
	private static ColumnOrdinals columnOrdinals = null;

	public sealed class ColumnOrdinals {
	    public String Prefix = String.Empty;
	    public Int32 RECORD_KEY;
	    public Int32 EMP_GROSS_INCOME;
	    public Int32 EMP_YRS_PROF;

	    internal ColumnOrdinals(IDataReader reader) {
		RECORD_KEY = reader.GetOrdinal("RECORD_KEY");
		EMP_GROSS_INCOME = reader.GetOrdinal("EMP_GROSS_INCOME");
		EMP_YRS_PROF = reader.GetOrdinal("EMP_YRS_PROF");
	    }

	    internal ColumnOrdinals(IDataReader reader, String prefix) {
		Prefix = prefix;
		RECORD_KEY = reader.GetOrdinal(prefix + "RECORD_KEY");
		EMP_GROSS_INCOME = reader.GetOrdinal(prefix + "EMP_GROSS_INCOME");
		EMP_YRS_PROF = reader.GetOrdinal(prefix + "EMP_YRS_PROF");
	    }
	}

	/// <summary>
	/// Initializes the static map of property names to sql expressions.
	/// </summary>
	static PROFILE_EMPLOYER_INFODAO() {
	    AddPropertyMapping("RECORD_KEY", @"RECORD_KEY");
	    AddPropertyMapping("EMP_GROSS_INCOME", @"EMP_GROSS_INCOME");
	    AddPropertyMapping("EMP_YRS_PROF", @"EMP_YRS_PROF");
	}

	private PROFILE_EMPLOYER_INFODAO() {}

	protected override String ConnectionStringKey {
	    get {
		return CONNECTION_STRING_KEY;
	    }
	}

	/// <summary>
	/// Returns a list of all PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public PROFILE_EMPLOYER_INFOList GetList() {
	    return GetList(null, null);
	}

	/// <summary>
	/// Returns a filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter) {
	    return GetList(filter, null);
	}

	/// <summary>
	/// Returns an ordered list of PROFILE_EMPLOYER_INFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(IOrderBy orderByClause) {
	    return GetList(null, orderByClause);
	}

	/// <summary>
	/// Returns an ordered and filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter, IOrderBy orderByClause) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause);

	    PROFILE_EMPLOYER_INFOList list = new PROFILE_EMPLOYER_INFOList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Returns a list of all PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(Int32 maxRows) {
	    return GetList(null, null, maxRows);
	}

	/// <summary>
	/// Returns a filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter, Int32 maxRows) {
	    return GetList(filter, null, maxRows);
	}

	/// <summary>
	/// Returns an ordered list of PROFILE_EMPLOYER_INFO rows.  All rows in the database are returned
	/// </summary>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(IOrderBy orderByClause, Int32 maxRows) {
	    return GetList(null, orderByClause, maxRows);
	}

	/// <summary>
	/// Returns an ordered and filtered list of PROFILE_EMPLOYER_INFO rows.
	/// </summary>
	/// <param name="filter">Filtering criteria.</param>
	/// <param name="orderByClause">Ordering criteria.</param>
	/// <param name="maxRows">Uses TOP to limit results to specified number of rows</param>
	/// <returns>List of PROFILE_EMPLOYER_INFO objects.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no rows are found matching the where criteria.</exception>
	public PROFILE_EMPLOYER_INFOList GetList(SqlFilter filter, IOrderBy orderByClause, Int32 maxRows) {
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, orderByClause, maxRows);

	    PROFILE_EMPLOYER_INFOList list = new PROFILE_EMPLOYER_INFOList();
	    while (dataReader.Read()) {
		list.Add(GetDataObjectFromReader(dataReader));
	    }
	    dataReader.Close();
	    return list;
	}

	/// <summary>
	/// Finds a PROFILE_EMPLOYER_INFO entity using it's primary key.
	/// </summary>
	/// <param name="rECORD_KEY">A key field.</param>
	/// <returns>A PROFILE_EMPLOYER_INFO object.</returns>
	/// <exception cref="Spring2.Core.DAO.FinderException">Thrown when no entity exists witht he specified primary key..</exception>
	public PROFILE_EMPLOYER_INFO Load(IdType rECORD_KEY) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(PROFILE_EMPLOYER_INFOFields.RECORD_KEY, EqualityOperatorEnum.Equal, rECORD_KEY.IsValid ? rECORD_KEY.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null, 1, COMMAND_TIMEOUT);
	    return GetDataObject(dataReader);
	}

	/// <summary>
	/// Repopulates an existing business entity instance
	/// </summary>
	public void Reload(PROFILE_EMPLOYER_INFO instance) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(CreateEqualityPredicate(PROFILE_EMPLOYER_INFOFields.RECORD_KEY, EqualityOperatorEnum.Equal, instance.RECORD_KEY.IsValid ? instance.RECORD_KEY.ToInt32() as Object : DBNull.Value));
	    IDataReader dataReader = GetListReader(CONNECTION_STRING_KEY, VIEW, filter, null);

	    if (!dataReader.Read()) {
		dataReader.Close();
		throw new FinderException("Reload found no rows for PROFILE_EMPLOYER_INFO.");
	    }
	    GetDataObjectFromReader(instance, dataReader);
	    dataReader.Close();
	}

	/// <summary>
	/// Read through the reader and return a data object list
	/// </summary>
	private PROFILE_EMPLOYER_INFOList GetList(IDataReader reader) {
	    PROFILE_EMPLOYER_INFOList list = new PROFILE_EMPLOYER_INFOList();
	    while (reader.Read()) {
		list.Add(GetDataObjectFromReader(reader));
	    }
	    reader.Close();
	    return list;
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private PROFILE_EMPLOYER_INFO GetDataObject(IDataReader reader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(reader);
	    }
	    return GetDataObject(reader, columnOrdinals);
	}

	/// <summary>
	/// Read from reader and return a single data object
	/// </summary>
	private PROFILE_EMPLOYER_INFO GetDataObject(IDataReader reader, ColumnOrdinals ordinals) {
	    if (!reader.Read()) {
		reader.Close();
		throw new FinderException("Reader contained no rows.");
	    }
	    PROFILE_EMPLOYER_INFO data = GetDataObjectFromReader(reader, ordinals);
	    reader.Close();
	    return data;
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public PROFILE_EMPLOYER_INFO GetDataObjectFromReader(PROFILE_EMPLOYER_INFO data, IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <returns>Data object built from current row.</returns>
	public PROFILE_EMPLOYER_INFO GetDataObjectFromReader(IDataReader dataReader) {
	    if (columnOrdinals == null) {
		columnOrdinals = new ColumnOrdinals(dataReader);
	    }
	    PROFILE_EMPLOYER_INFO data = new PROFILE_EMPLOYER_INFO(false);
	    return GetDataObjectFromReader(data, dataReader, columnOrdinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public PROFILE_EMPLOYER_INFO GetDataObjectFromReader(IDataReader dataReader, ColumnOrdinals ordinals) {
	    PROFILE_EMPLOYER_INFO data = new PROFILE_EMPLOYER_INFO(false);
	    return GetDataObjectFromReader(data, dataReader, ordinals);
	}

	/// <summary>
	/// Builds a data object from the current row in a data reader..
	/// </summary>
	/// <param name="data">Entity to be populated from data reader</param>
	/// <param name="dataReader">Container for database row.</param>
	/// <param name="ordinals">An instance of ColumnOrdinals initialized for this data reader</param>
	/// <returns>Data object built from current row.</returns>
	public PROFILE_EMPLOYER_INFO GetDataObjectFromReader(PROFILE_EMPLOYER_INFO data, IDataReader dataReader, ColumnOrdinals ordinals) {
	    if (dataReader.IsDBNull(ordinals.RECORD_KEY)) {
		data.RECORD_KEY = IdType.UNSET;
	    } else {
		data.RECORD_KEY = new IdType(dataReader.GetInt32(ordinals.RECORD_KEY));
	    }
	    if (dataReader.IsDBNull(ordinals.EMP_GROSS_INCOME)) {
		data.EMP_GROSS_INCOME = CurrencyType.UNSET;
	    } else {
		data.EMP_GROSS_INCOME = new CurrencyType(dataReader.GetDecimal(ordinals.EMP_GROSS_INCOME));
	    }
	    if (dataReader.IsDBNull(ordinals.EMP_YRS_PROF)) {
		data.EMP_YRS_PROF = IntegerType.UNSET;
	    } else {
		data.EMP_YRS_PROF = new IntegerType(dataReader.GetInt32(ordinals.EMP_YRS_PROF));
	    }
	    return data;
	}

	/// <summary>
	/// Inserts a record into the PROFILE_EMPLOYER_INFO table.
	/// </summary>
	/// <param name="data"></param>
	public IdType Insert(PROFILE_EMPLOYER_INFO data) {
	    return Insert(data, null);
	}

	/// <summary>
	/// Inserts a record into the PROFILE_EMPLOYER_INFO table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public IdType Insert(PROFILE_EMPLOYER_INFO data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spPROFILE_EMPLOYER_INFO_Insert", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    IDbDataParameter idParam = CreateDataParameter("RETURN_VALUE", DbType.Int32, ParameterDirection.ReturnValue);
	    cmd.Parameters.Add(idParam);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(PROFILE_EMPLOYER_INFOFields.EMP_GROSS_INCOME, data.EMP_GROSS_INCOME.IsValid ? data.EMP_GROSS_INCOME.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(PROFILE_EMPLOYER_INFOFields.EMP_YRS_PROF, data.EMP_YRS_PROF.IsValid ? data.EMP_YRS_PROF.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }

	    // Set the output paramter value(s)
	    return new IdType((Int32)idParam.Value);
	}

	/// <summary>
	/// Updates a record in the PROFILE_EMPLOYER_INFO table.
	/// </summary>
	/// <param name="data"></param>
	public void Update(PROFILE_EMPLOYER_INFO data) {
	    Update(data, null);
	}

	/// <summary>
	/// Updates a record in the PROFILE_EMPLOYER_INFO table.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="transaction"></param>
	public void Update(PROFILE_EMPLOYER_INFO data, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spPROFILE_EMPLOYER_INFO_Update", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    //Create the parameters and append them to the command object
	    cmd.Parameters.Add(CreateDataParameter(PROFILE_EMPLOYER_INFOFields.RECORD_KEY, data.RECORD_KEY.IsValid ? data.RECORD_KEY.ToInt32() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(PROFILE_EMPLOYER_INFOFields.EMP_GROSS_INCOME, data.EMP_GROSS_INCOME.IsValid ? data.EMP_GROSS_INCOME.ToDecimal() as Object : DBNull.Value));
	    cmd.Parameters.Add(CreateDataParameter(PROFILE_EMPLOYER_INFOFields.EMP_YRS_PROF, data.EMP_YRS_PROF.IsValid ? data.EMP_YRS_PROF.ToInt32() as Object : DBNull.Value));
	    // Execute the query
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}

	/// <summary>
	/// Deletes a record from the PROFILE_EMPLOYER_INFO table by RECORD_KEY.
	/// </summary>
	/// <param name="rECORD_KEY">A key field.</param>
	public void Delete(IdType rECORD_KEY) {
	    Delete(rECORD_KEY, null);
	}

	/// <summary>
	/// Deletes a record from the PROFILE_EMPLOYER_INFO table by RECORD_KEY.
	/// </summary>
	/// <param name="rECORD_KEY">A key field.</param>
	/// <param name="transaction"></param>
	public void Delete(IdType rECORD_KEY, IDbTransaction transaction) {
	    // Create and execute the command
	    IDbCommand cmd = GetDbCommand(CONNECTION_STRING_KEY, "spPROFILE_EMPLOYER_INFO_Delete", CommandType.StoredProcedure, COMMAND_TIMEOUT, transaction);

	    // Create and append the parameters
	    cmd.Parameters.Add(CreateDataParameter(PROFILE_EMPLOYER_INFOFields.RECORD_KEY, rECORD_KEY.IsValid ? rECORD_KEY.ToInt32() as Object : DBNull.Value));
	    // Execute the query and return the result
	    cmd.ExecuteNonQuery();

	    // do not close the connection if it is part of a transaction
	    if (transaction == null && DbConnectionScope.Current == null) {
		cmd.Connection.Close();
	    }
	}
    }
}
