using System.Reflection;
using System.Runtime.CompilerServices;

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:

[assembly: System.Reflection.AssemblyVersionAttribute("0.0.1.0")]

//
// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyCompany("Spring2 Technologies")]
[assembly: AssemblyProduct("Cyprus - Services")]
[assembly: AssemblyCopyright("Copyright � 2010 Spring2 Technologies")]
[assembly: AssemblyTrademark("")]
