using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for APPLIC_PROFILE_DATATest.
    /// </summary>
    [TestFixture()]
    public class APPLIC_PROFILE_DATATest : BaseTest {
	[Generate()]
	private void CheckPersistance(APPLIC_PROFILE_DATA expected) {
	    APPLIC_PROFILE_DATA actual = APPLIC_PROFILE_DATA.GetInstance(expected.APPLIC_NUM);
	    Assert.AreEqual(expected.APPLIC_NUM as Object, actual.APPLIC_NUM as Object, "APPLIC_NUM");
	    Assert.AreEqual(expected.NO_YRS_ADDR as Object, actual.NO_YRS_ADDR as Object, "NO_YRS_ADDR");
	    Assert.AreEqual(expected.PREV_YRS_ADDR as Object, actual.PREV_YRS_ADDR as Object, "PREV_YRS_ADDR");
	}
    }
}
