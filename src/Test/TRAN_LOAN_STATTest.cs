using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for TRAN_LOAN_STATTest.
    /// </summary>
    [TestFixture()]
    public class TRAN_LOAN_STATTest : BaseTest {
	[Generate()]
	private void CheckPersistance(TRAN_LOAN_STAT expected) {
	    TRAN_LOAN_STAT actual = TRAN_LOAN_STAT.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.L_STAT_CHANGE_ACTUAL_DATE.ToString() as Object, actual.L_STAT_CHANGE_ACTUAL_DATE.ToString() as Object, "L_STAT_CHANGE_ACTUAL_DATE");
	}
    }
}
