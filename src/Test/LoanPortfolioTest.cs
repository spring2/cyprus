using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dss.BusinessLogic;
using Spring2.Dss.Dao;
using Spring2.Dss.DataObject;
using Spring2.Dss.Types;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

using Cyprus.BusinessLogic;
using Cyprus.Dao;
using Cyprus.DataObject;
using Cyprus.Types;

namespace Cyprus.Test {
    /// <summary>
    /// Summary description for LoanPortfolioTest.
    /// </summary>
    [TestFixture()]
    public class LoanPortfolioTest : BaseTest {
	[Generate()]
	private void CheckPersistance(LoanPortfolio expected) {
	    LoanPortfolio actual = LoanPortfolio.GetInstance(expected.LoanPortfolioId);
	    Assert.AreEqual(expected.LoanPortfolioId as Object, actual.LoanPortfolioId as Object, "LoanPortfolioId");
	    Assert.AreEqual(expected.Name as Object, actual.Name as Object, "Name");
	    Assert.AreEqual(expected.UpDated.ToString() as Object, actual.UpDated.ToString() as Object, "UpDated");
	    Assert.AreEqual(expected.AccountID as Object, actual.AccountID as Object, "AccountID");
	    Assert.AreEqual(expected.Member as Object, actual.Member as Object, "Member");
	    Assert.AreEqual(expected.MClass as Object, actual.MClass as Object, "MClass");
	    Assert.AreEqual(expected.Ltype as Object, actual.Ltype as Object, "Ltype");
	    Assert.AreEqual(expected.LtypeSub as Object, actual.LtypeSub as Object, "LtypeSub");
	    Assert.AreEqual(expected.Product as Object, actual.Product as Object, "Product");
	    Assert.AreEqual(expected.LoanDesc as Object, actual.LoanDesc as Object, "LoanDesc");
	    Assert.AreEqual(expected.LoanDate.ToString() as Object, actual.LoanDate.ToString() as Object, "LoanDate");
	    Assert.AreEqual(expected.LoanDay as Object, actual.LoanDay as Object, "LoanDay");
	    Assert.AreEqual(expected.LoanMonth as Object, actual.LoanMonth as Object, "LoanMonth");
	    Assert.AreEqual(expected.LoanYear as Object, actual.LoanYear as Object, "LoanYear");
	    Assert.AreEqual(expected.FirstPmtDate.ToString() as Object, actual.FirstPmtDate.ToString() as Object, "FirstPmtDate");
	    Assert.AreEqual(expected.Status as Object, actual.Status as Object, "Status");
	    Assert.AreEqual(expected.RptCode as Object, actual.RptCode as Object, "RptCode");
	    Assert.AreEqual(expected.FundedBr as Object, actual.FundedBr as Object, "FundedBr");
	    Assert.AreEqual(expected.LoadedBr as Object, actual.LoadedBr as Object, "LoadedBr");
	    Assert.AreEqual(expected.FICO as Object, actual.FICO as Object, "FICO");
	    Assert.AreEqual(expected.FICORange as Object, actual.FICORange as Object, "FICORange");
	    Assert.AreEqual(expected.FastStart as Object, actual.FastStart as Object, "FastStart");
	    Assert.AreEqual(expected.FastStartRange as Object, actual.FastStartRange as Object, "FastStartRange");
	    Assert.AreEqual(expected.Bankruptcy as Object, actual.Bankruptcy as Object, "Bankruptcy");
	    Assert.AreEqual(expected.BankruptcyRange as Object, actual.BankruptcyRange as Object, "BankruptcyRange");
	    Assert.AreEqual(expected.MembershipDate.ToString() as Object, actual.MembershipDate.ToString() as Object, "MembershipDate");
	    Assert.AreEqual(expected.NewMember as Object, actual.NewMember as Object, "NewMember");
	    Assert.AreEqual(expected.RiskLev as Object, actual.RiskLev as Object, "RiskLev");
	    Assert.AreEqual(expected.Traffic as Object, actual.Traffic as Object, "Traffic");
	    Assert.AreEqual(expected.TrafficColor as Object, actual.TrafficColor as Object, "TrafficColor");
	    Assert.AreEqual(expected.MemAge as Object, actual.MemAge as Object, "MemAge");
	    Assert.AreEqual(expected.MemSex as Object, actual.MemSex as Object, "MemSex");
	    Assert.AreEqual(expected.PmtFreq as Object, actual.PmtFreq as Object, "PmtFreq");
	    Assert.AreEqual(expected.OrigTerm as Object, actual.OrigTerm as Object, "OrigTerm");
	    Assert.AreEqual(expected.OrigRate as Object, actual.OrigRate as Object, "OrigRate");
	    Assert.AreEqual(expected.OrigPmt as Object, actual.OrigPmt as Object, "OrigPmt");
	    Assert.AreEqual(expected.OrigAmount as Object, actual.OrigAmount as Object, "OrigAmount");
	    Assert.AreEqual(expected.OrigCrLim as Object, actual.OrigCrLim as Object, "OrigCrLim");
	    Assert.AreEqual(expected.CurTerm as Object, actual.CurTerm as Object, "CurTerm");
	    Assert.AreEqual(expected.CurRate as Object, actual.CurRate as Object, "CurRate");
	    Assert.AreEqual(expected.CurPmt as Object, actual.CurPmt as Object, "CurPmt");
	    Assert.AreEqual(expected.CurBal as Object, actual.CurBal as Object, "CurBal");
	    Assert.AreEqual(expected.CurCrLim as Object, actual.CurCrLim as Object, "CurCrLim");
	    Assert.AreEqual(expected.BalloonDate.ToString() as Object, actual.BalloonDate.ToString() as Object, "BalloonDate");
	    Assert.AreEqual(expected.MatDate.ToString() as Object, actual.MatDate.ToString() as Object, "MatDate");
	    Assert.AreEqual(expected.InsCode as Object, actual.InsCode as Object, "InsCode");
	    Assert.AreEqual(expected.CollateralValue as Object, actual.CollateralValue as Object, "CollateralValue");
	    Assert.AreEqual(expected.LTV as Object, actual.LTV as Object, "LTV");
	    Assert.AreEqual(expected.SecType as Object, actual.SecType as Object, "SecType");
	    Assert.AreEqual(expected.SecDesc1 as Object, actual.SecDesc1 as Object, "SecDesc1");
	    Assert.AreEqual(expected.SecDesc2 as Object, actual.SecDesc2 as Object, "SecDesc2");
	    Assert.AreEqual(expected.DebtRatio as Object, actual.DebtRatio as Object, "DebtRatio");
	    Assert.AreEqual(expected.GrossInc as Object, actual.GrossInc as Object, "GrossInc");
	    Assert.AreEqual(expected.Dealer as Object, actual.Dealer as Object, "Dealer");
	    Assert.AreEqual(expected.DealerName as Object, actual.DealerName as Object, "DealerName");
	    Assert.AreEqual(expected.CoborComak as Object, actual.CoborComak as Object, "CoborComak");
	    Assert.AreEqual(expected.RelPriceGrp as Object, actual.RelPriceGrp as Object, "RelPriceGrp");
	    Assert.AreEqual(expected.RelPriceDisc as Object, actual.RelPriceDisc as Object, "RelPriceDisc");
	    Assert.AreEqual(expected.OtherDisc as Object, actual.OtherDisc as Object, "OtherDisc");
	    Assert.AreEqual(expected.DiscReason as Object, actual.DiscReason as Object, "DiscReason");
	    Assert.AreEqual(expected.RiskOffset as Object, actual.RiskOffset as Object, "RiskOffset");
	    Assert.AreEqual(expected.CreditType as Object, actual.CreditType as Object, "CreditType");
	    Assert.AreEqual(expected.ApprOff as Object, actual.ApprOff as Object, "ApprOff");
	    Assert.AreEqual(expected.StatedIncome as Object, actual.StatedIncome as Object, "StatedIncome");
	    Assert.AreEqual(expected.Extensions as Object, actual.Extensions as Object, "Extensions");
	    Assert.AreEqual(expected.FirstExtDate.ToString() as Object, actual.FirstExtDate.ToString() as Object, "FirstExtDate");
	    Assert.AreEqual(expected.LastExtDate.ToString() as Object, actual.LastExtDate.ToString() as Object, "LastExtDate");
	    Assert.AreEqual(expected.RiskLevel as Object, actual.RiskLevel as Object, "RiskLevel");
	    Assert.AreEqual(expected.RiskDate.ToString() as Object, actual.RiskDate.ToString() as Object, "RiskDate");
	    Assert.AreEqual(expected.Watch as Object, actual.Watch as Object, "Watch");
	    Assert.AreEqual(expected.WatchDate.ToString() as Object, actual.WatchDate.ToString() as Object, "WatchDate");
	    Assert.AreEqual(expected.WorkOut as Object, actual.WorkOut as Object, "WorkOut");
	    Assert.AreEqual(expected.WorkOutDate.ToString() as Object, actual.WorkOutDate.ToString() as Object, "WorkOutDate");
	    Assert.AreEqual(expected.FirstMortType as Object, actual.FirstMortType as Object, "FirstMortType");
	    Assert.AreEqual(expected.BusPerReg as Object, actual.BusPerReg as Object, "BusPerReg");
	    Assert.AreEqual(expected.BusGroup as Object, actual.BusGroup as Object, "BusGroup");
	    Assert.AreEqual(expected.BusDesc as Object, actual.BusDesc as Object, "BusDesc");
	    Assert.AreEqual(expected.BusPart as Object, actual.BusPart as Object, "BusPart");
	    Assert.AreEqual(expected.BusPartPerc as Object, actual.BusPartPerc as Object, "BusPartPerc");
	    Assert.AreEqual(expected.SBAguar as Object, actual.SBAguar as Object, "SBAguar");
	    Assert.AreEqual(expected.DeliDays as Object, actual.DeliDays as Object, "DeliDays");
	    Assert.AreEqual(expected.DeliSect as Object, actual.DeliSect as Object, "DeliSect");
	    Assert.AreEqual(expected.DeliHist as Object, actual.DeliHist as Object, "DeliHist");
	    Assert.AreEqual(expected.DeliHistQ1 as Object, actual.DeliHistQ1 as Object, "DeliHistQ1");
	    Assert.AreEqual(expected.DeliHistQ2 as Object, actual.DeliHistQ2 as Object, "DeliHistQ2");
	    Assert.AreEqual(expected.DeliHistQ3 as Object, actual.DeliHistQ3 as Object, "DeliHistQ3");
	    Assert.AreEqual(expected.DeliHistQ4 as Object, actual.DeliHistQ4 as Object, "DeliHistQ4");
	    Assert.AreEqual(expected.DeliHistQ5 as Object, actual.DeliHistQ5 as Object, "DeliHistQ5");
	    Assert.AreEqual(expected.DeliHistQ6 as Object, actual.DeliHistQ6 as Object, "DeliHistQ6");
	    Assert.AreEqual(expected.DeliHistQ7 as Object, actual.DeliHistQ7 as Object, "DeliHistQ7");
	    Assert.AreEqual(expected.DeliHistQ8 as Object, actual.DeliHistQ8 as Object, "DeliHistQ8");
	    Assert.AreEqual(expected.ChrgOffDate.ToString() as Object, actual.ChrgOffDate.ToString() as Object, "ChrgOffDate");
	    Assert.AreEqual(expected.ChrgOffMonth as Object, actual.ChrgOffMonth as Object, "ChrgOffMonth");
	    Assert.AreEqual(expected.ChrgOffYear as Object, actual.ChrgOffYear as Object, "ChrgOffYear");
	    Assert.AreEqual(expected.ChrgOffMnthsBF as Object, actual.ChrgOffMnthsBF as Object, "ChrgOffMnthsBF");
	    Assert.AreEqual(expected.PropType as Object, actual.PropType as Object, "PropType");
	    Assert.AreEqual(expected.Occupancy as Object, actual.Occupancy as Object, "Occupancy");
	    Assert.AreEqual(expected.OrigAppVal as Object, actual.OrigAppVal as Object, "OrigAppVal");
	    Assert.AreEqual(expected.OrigAppDate.ToString() as Object, actual.OrigAppDate.ToString() as Object, "OrigAppDate");
	    Assert.AreEqual(expected.CurrApprVal as Object, actual.CurrApprVal as Object, "CurrApprVal");
	    Assert.AreEqual(expected.CurrApprDate.ToString() as Object, actual.CurrApprDate.ToString() as Object, "CurrApprDate");
	    Assert.AreEqual(expected.FirstMortBal as Object, actual.FirstMortBal as Object, "FirstMortBal");
	    Assert.AreEqual(expected.FirstMortMoPyt as Object, actual.FirstMortMoPyt as Object, "FirstMortMoPyt");
	    Assert.AreEqual(expected.SecondMortBal as Object, actual.SecondMortBal as Object, "SecondMortBal");
	    Assert.AreEqual(expected.SecondMortPymt as Object, actual.SecondMortPymt as Object, "SecondMortPymt");
	    Assert.AreEqual(expected.TaxInsNotInc as Object, actual.TaxInsNotInc as Object, "TaxInsNotInc");
	}
    }
}

namespace Spring2.Test {
    /// <summary>
    /// Summary description for LoanPortfolioTest.
    /// </summary>
    [TestFixture()]
    public class LoanPortfolioTest : BaseTest {
	[Generate()]
	private void CheckPersistance(LoanPortfolio expected) {
	    LoanPortfolio actual = LoanPortfolio.GetInstance(expected.LoanPortfolioId);
	    Assert.AreEqual(expected.LoanPortfolioId as Object, actual.LoanPortfolioId as Object, "LoanPortfolioId");
	    Assert.AreEqual(expected.UpDated.ToString() as Object, actual.UpDated.ToString() as Object, "UpDated");
	    Assert.AreEqual(expected.EOMDate as Object, actual.EOMDate as Object, "EOMDate");
	    Assert.AreEqual(expected.AccountID as Object, actual.AccountID as Object, "AccountID");
	    Assert.AreEqual(expected.Member as Object, actual.Member as Object, "Member");
	    Assert.AreEqual(expected.MClass as Object, actual.MClass as Object, "MClass");
	    Assert.AreEqual(expected.Ltype as Object, actual.Ltype as Object, "Ltype");
	    Assert.AreEqual(expected.LtypeSub as Object, actual.LtypeSub as Object, "LtypeSub");
	    Assert.AreEqual(expected.Product as Object, actual.Product as Object, "Product");
	    Assert.AreEqual(expected.LoanDesc as Object, actual.LoanDesc as Object, "LoanDesc");
	    Assert.AreEqual(expected.LoanDate as Object, actual.LoanDate as Object, "LoanDate");
	    Assert.AreEqual(expected.LoanDay as Object, actual.LoanDay as Object, "LoanDay");
	    Assert.AreEqual(expected.LoanMonth as Object, actual.LoanMonth as Object, "LoanMonth");
	    Assert.AreEqual(expected.LoanYear as Object, actual.LoanYear as Object, "LoanYear");
	    Assert.AreEqual(expected.FirstPmtDate as Object, actual.FirstPmtDate as Object, "FirstPmtDate");
	    Assert.AreEqual(expected.Status as Object, actual.Status as Object, "Status");
	    Assert.AreEqual(expected.RptCode as Object, actual.RptCode as Object, "RptCode");
	    Assert.AreEqual(expected.FundedBr as Object, actual.FundedBr as Object, "FundedBr");
	    Assert.AreEqual(expected.LoadedBr as Object, actual.LoadedBr as Object, "LoadedBr");
	    Assert.AreEqual(expected.FICO as Object, actual.FICO as Object, "FICO");
	    Assert.AreEqual(expected.FICORange as Object, actual.FICORange as Object, "FICORange");
	    Assert.AreEqual(expected.FastStart as Object, actual.FastStart as Object, "FastStart");
	    Assert.AreEqual(expected.FastStartRange as Object, actual.FastStartRange as Object, "FastStartRange");
	    Assert.AreEqual(expected.Bankruptcy as Object, actual.Bankruptcy as Object, "Bankruptcy");
	    Assert.AreEqual(expected.BankruptcyRange as Object, actual.BankruptcyRange as Object, "BankruptcyRange");
	    Assert.AreEqual(expected.MembershipDate as Object, actual.MembershipDate as Object, "MembershipDate");
	    Assert.AreEqual(expected.NewMember as Object, actual.NewMember as Object, "NewMember");
	    Assert.AreEqual(expected.RiskLev as Object, actual.RiskLev as Object, "RiskLev");
	    Assert.AreEqual(expected.Traffic as Object, actual.Traffic as Object, "Traffic");
	    Assert.AreEqual(expected.TrafficColor as Object, actual.TrafficColor as Object, "TrafficColor");
	    Assert.AreEqual(expected.MemAge as Object, actual.MemAge as Object, "MemAge");
	    Assert.AreEqual(expected.MemSex as Object, actual.MemSex as Object, "MemSex");
	    Assert.AreEqual(expected.PmtFreq as Object, actual.PmtFreq as Object, "PmtFreq");
	    Assert.AreEqual(expected.OrigTerm as Object, actual.OrigTerm as Object, "OrigTerm");
	    Assert.AreEqual(expected.OrigRate as Object, actual.OrigRate as Object, "OrigRate");
	    Assert.AreEqual(expected.OrigPmt as Object, actual.OrigPmt as Object, "OrigPmt");
	    Assert.AreEqual(expected.OrigAmount as Object, actual.OrigAmount as Object, "OrigAmount");
	    Assert.AreEqual(expected.OrigCrLim as Object, actual.OrigCrLim as Object, "OrigCrLim");
	    Assert.AreEqual(expected.CurTerm as Object, actual.CurTerm as Object, "CurTerm");
	    Assert.AreEqual(expected.CurRate as Object, actual.CurRate as Object, "CurRate");
	    Assert.AreEqual(expected.CurPmt as Object, actual.CurPmt as Object, "CurPmt");
	    Assert.AreEqual(expected.CurBal as Object, actual.CurBal as Object, "CurBal");
	    Assert.AreEqual(expected.CurCrLim as Object, actual.CurCrLim as Object, "CurCrLim");
	    Assert.AreEqual(expected.BalloonDate as Object, actual.BalloonDate as Object, "BalloonDate");
	    Assert.AreEqual(expected.MatDate as Object, actual.MatDate as Object, "MatDate");
	    Assert.AreEqual(expected.InsCode as Object, actual.InsCode as Object, "InsCode");
	    Assert.AreEqual(expected.CollateralValue as Object, actual.CollateralValue as Object, "CollateralValue");
	    Assert.AreEqual(expected.LTV as Object, actual.LTV as Object, "LTV");
	    Assert.AreEqual(expected.SecType as Object, actual.SecType as Object, "SecType");
	    Assert.AreEqual(expected.SecDesc1 as Object, actual.SecDesc1 as Object, "SecDesc1");
	    Assert.AreEqual(expected.SecDesc2 as Object, actual.SecDesc2 as Object, "SecDesc2");
	    Assert.AreEqual(expected.DebtRatio as Object, actual.DebtRatio as Object, "DebtRatio");
	    Assert.AreEqual(expected.Dealer as Object, actual.Dealer as Object, "Dealer");
	    Assert.AreEqual(expected.DealerName as Object, actual.DealerName as Object, "DealerName");
	    Assert.AreEqual(expected.CoborComak as Object, actual.CoborComak as Object, "CoborComak");
	    Assert.AreEqual(expected.RelPriceGrp as Object, actual.RelPriceGrp as Object, "RelPriceGrp");
	    Assert.AreEqual(expected.RelPriceDisc as Object, actual.RelPriceDisc as Object, "RelPriceDisc");
	    Assert.AreEqual(expected.OtherDisc as Object, actual.OtherDisc as Object, "OtherDisc");
	    Assert.AreEqual(expected.DiscReason as Object, actual.DiscReason as Object, "DiscReason");
	    Assert.AreEqual(expected.RiskOffset as Object, actual.RiskOffset as Object, "RiskOffset");
	    Assert.AreEqual(expected.CreditType as Object, actual.CreditType as Object, "CreditType");
	    Assert.AreEqual(expected.StatedIncome as Object, actual.StatedIncome as Object, "StatedIncome");
	    Assert.AreEqual(expected.Extensions as Object, actual.Extensions as Object, "Extensions");
	    Assert.AreEqual(expected.FirstExtDate as Object, actual.FirstExtDate as Object, "FirstExtDate");
	    Assert.AreEqual(expected.LastExtDate as Object, actual.LastExtDate as Object, "LastExtDate");
	    Assert.AreEqual(expected.RiskLevel as Object, actual.RiskLevel as Object, "RiskLevel");
	    Assert.AreEqual(expected.RiskDate as Object, actual.RiskDate as Object, "RiskDate");
	    Assert.AreEqual(expected.Watch as Object, actual.Watch as Object, "Watch");
	    Assert.AreEqual(expected.WatchDate as Object, actual.WatchDate as Object, "WatchDate");
	    Assert.AreEqual(expected.WorkOut as Object, actual.WorkOut as Object, "WorkOut");
	    Assert.AreEqual(expected.WorkOutDate as Object, actual.WorkOutDate as Object, "WorkOutDate");
	    Assert.AreEqual(expected.FirstMortType as Object, actual.FirstMortType as Object, "FirstMortType");
	    Assert.AreEqual(expected.BusPerReg as Object, actual.BusPerReg as Object, "BusPerReg");
	    Assert.AreEqual(expected.BusGroup as Object, actual.BusGroup as Object, "BusGroup");
	    Assert.AreEqual(expected.BusDesc as Object, actual.BusDesc as Object, "BusDesc");
	    Assert.AreEqual(expected.BusPart as Object, actual.BusPart as Object, "BusPart");
	    Assert.AreEqual(expected.BusPartPerc as Object, actual.BusPartPerc as Object, "BusPartPerc");
	    Assert.AreEqual(expected.SBAguar as Object, actual.SBAguar as Object, "SBAguar");
	    Assert.AreEqual(expected.DeliDays as Object, actual.DeliDays as Object, "DeliDays");
	    Assert.AreEqual(expected.DeliSect as Object, actual.DeliSect as Object, "DeliSect");
	    Assert.AreEqual(expected.DeliHist as Object, actual.DeliHist as Object, "DeliHist");
	    Assert.AreEqual(expected.DeliHistQ1 as Object, actual.DeliHistQ1 as Object, "DeliHistQ1");
	    Assert.AreEqual(expected.DeliHistQ2 as Object, actual.DeliHistQ2 as Object, "DeliHistQ2");
	    Assert.AreEqual(expected.DeliHistQ3 as Object, actual.DeliHistQ3 as Object, "DeliHistQ3");
	    Assert.AreEqual(expected.DeliHistQ4 as Object, actual.DeliHistQ4 as Object, "DeliHistQ4");
	    Assert.AreEqual(expected.DeliHistQ5 as Object, actual.DeliHistQ5 as Object, "DeliHistQ5");
	    Assert.AreEqual(expected.DeliHistQ6 as Object, actual.DeliHistQ6 as Object, "DeliHistQ6");
	    Assert.AreEqual(expected.DeliHistQ7 as Object, actual.DeliHistQ7 as Object, "DeliHistQ7");
	    Assert.AreEqual(expected.DeliHistQ8 as Object, actual.DeliHistQ8 as Object, "DeliHistQ8");
	    Assert.AreEqual(expected.ChrgOffDate as Object, actual.ChrgOffDate as Object, "ChrgOffDate");
	    Assert.AreEqual(expected.ChrgOffMonth as Object, actual.ChrgOffMonth as Object, "ChrgOffMonth");
	    Assert.AreEqual(expected.ChrgOffYear as Object, actual.ChrgOffYear as Object, "ChrgOffYear");
	    Assert.AreEqual(expected.ChrgOffMnthsBF as Object, actual.ChrgOffMnthsBF as Object, "ChrgOffMnthsBF");
	    Assert.AreEqual(expected.PropType as Object, actual.PropType as Object, "PropType");
	    Assert.AreEqual(expected.Occupancy as Object, actual.Occupancy as Object, "Occupancy");
	    Assert.AreEqual(expected.OrigApprVal as Object, actual.OrigApprVal as Object, "OrigApprVal");
	    Assert.AreEqual(expected.OrigApprDate as Object, actual.OrigApprDate as Object, "OrigApprDate");
	    Assert.AreEqual(expected.CurrApprVal as Object, actual.CurrApprVal as Object, "CurrApprVal");
	    Assert.AreEqual(expected.CurrApprDate as Object, actual.CurrApprDate as Object, "CurrApprDate");
	    Assert.AreEqual(expected.FirstMortBal as Object, actual.FirstMortBal as Object, "FirstMortBal");
	    Assert.AreEqual(expected.FirstMortMoPyt as Object, actual.FirstMortMoPyt as Object, "FirstMortMoPyt");
	    Assert.AreEqual(expected.SecondMortBal as Object, actual.SecondMortBal as Object, "SecondMortBal");
	    Assert.AreEqual(expected.SecondMortPymt as Object, actual.SecondMortPymt as Object, "SecondMortPymt");
	    Assert.AreEqual(expected.TaxInsNotInc as Object, actual.TaxInsNotInc as Object, "TaxInsNotInc");
	    Assert.AreEqual(expected.LoadOff as Object, actual.LoadOff as Object, "LoadOff");
	    Assert.AreEqual(expected.ApprOff as Object, actual.ApprOff as Object, "ApprOff");
	    Assert.AreEqual(expected.FundingOpp as Object, actual.FundingOpp as Object, "FundingOpp");
	    Assert.AreEqual(expected.Chanel as Object, actual.Chanel as Object, "Chanel");
	    Assert.AreEqual(expected.RiskType as Object, actual.RiskType as Object, "RiskType");
	    Assert.AreEqual(expected.Gap as Object, actual.Gap as Object, "Gap");
	    Assert.AreEqual(expected.LoadOperator as Object, actual.LoadOperator as Object, "LoadOperator");
	    Assert.AreEqual(expected.DeliAmt as Object, actual.DeliAmt as Object, "DeliAmt");
	    Assert.AreEqual(expected.ChgOffAmt as Object, actual.ChgOffAmt as Object, "ChgOffAmt");
	    Assert.AreEqual(expected.UsSecDesc as Object, actual.UsSecDesc as Object, "UsSecDesc");
	    Assert.AreEqual(expected.TroubledDebt as Object, actual.TroubledDebt as Object, "TroubledDebt");
	    Assert.AreEqual(expected.TdrDate as Object, actual.TdrDate as Object, "TdrDate");
	    Assert.AreEqual(expected.JntFicoUsed as Object, actual.JntFicoUsed as Object, "JntFicoUsed");
	    Assert.AreEqual(expected.SingleOrJointLoan as Object, actual.SingleOrJointLoan as Object, "SingleOrJointLoan");
	    Assert.AreEqual(expected.ApprovingOfficerUD as Object, actual.ApprovingOfficerUD as Object, "ApprovingOfficerUD");
	    Assert.AreEqual(expected.InsuranceCodeDesc as Object, actual.InsuranceCodeDesc as Object, "InsuranceCodeDesc");
	    Assert.AreEqual(expected.GapIns as Object, actual.GapIns as Object, "GapIns");
	    Assert.AreEqual(expected.MmpIns as Object, actual.MmpIns as Object, "MmpIns");
	    Assert.AreEqual(expected.ReportCodeDesc as Object, actual.ReportCodeDesc as Object, "ReportCodeDesc");
	    Assert.AreEqual(expected.BallonPmtLoan as Object, actual.BallonPmtLoan as Object, "BallonPmtLoan");
	    Assert.AreEqual(expected.FixedOrVariableRateLoan as Object, actual.FixedOrVariableRateLoan as Object, "FixedOrVariableRateLoan");
	    Assert.AreEqual(expected.AlpsOfficer as Object, actual.AlpsOfficer as Object, "AlpsOfficer");
	    Assert.AreEqual(expected.CUDLAppNumber as Object, actual.CUDLAppNumber as Object, "CUDLAppNumber");
	    Assert.AreEqual(expected.FasbDefCost as Object, actual.FasbDefCost as Object, "FasbDefCost");
	    Assert.AreEqual(expected.FasbLtdAmortFees as Object, actual.FasbLtdAmortFees as Object, "FasbLtdAmortFees");
	    Assert.AreEqual(expected.InterestPriorYtd as Object, actual.InterestPriorYtd as Object, "InterestPriorYtd");
	    Assert.AreEqual(expected.AccruedInterest as Object, actual.AccruedInterest as Object, "AccruedInterest");
	    Assert.AreEqual(expected.InterestLTD as Object, actual.InterestLTD as Object, "InterestLTD");
	    Assert.AreEqual(expected.InterestYtd as Object, actual.InterestYtd as Object, "InterestYtd");
	    Assert.AreEqual(expected.PartialPayAmt as Object, actual.PartialPayAmt as Object, "PartialPayAmt");
	    Assert.AreEqual(expected.InterestUncollected as Object, actual.InterestUncollected as Object, "InterestUncollected");
	    Assert.AreEqual(expected.ClosedAcctDate as Object, actual.ClosedAcctDate as Object, "ClosedAcctDate");
	    Assert.AreEqual(expected.LastTranDate as Object, actual.LastTranDate as Object, "LastTranDate");
	    Assert.AreEqual(expected.NextDueDate as Object, actual.NextDueDate as Object, "NextDueDate");
	    Assert.AreEqual(expected.InsuranceCode as Object, actual.InsuranceCode as Object, "InsuranceCode");
	    Assert.AreEqual(expected.MbrAgeAtApproval as Object, actual.MbrAgeAtApproval as Object, "MbrAgeAtApproval");
	}
    }
}

namespace Spring2.Dss.Test {
    /// <summary>
    /// Summary description for LoanPortfolioTest.
    /// </summary>
    [TestFixture()]
    public class LoanPortfolioTest : BaseTest {
	[Generate()]
	private void CheckPersistance(LoanPortfolio expected) {
	    LoanPortfolio actual = LoanPortfolio.GetInstance(expected.LoanPortfolioId);
	    Assert.AreEqual(expected.LoanPortfolioId as Object, actual.LoanPortfolioId as Object, "LoanPortfolioId");
	    Assert.AreEqual(expected.Name as Object, actual.Name as Object, "Name");
	    Assert.AreEqual(expected.UpDated.ToString() as Object, actual.UpDated.ToString() as Object, "UpDated");
	    Assert.AreEqual(expected.AccountID as Object, actual.AccountID as Object, "AccountID");
	    Assert.AreEqual(expected.Member as Object, actual.Member as Object, "Member");
	    Assert.AreEqual(expected.MClass as Object, actual.MClass as Object, "MClass");
	    Assert.AreEqual(expected.Ltype as Object, actual.Ltype as Object, "Ltype");
	    Assert.AreEqual(expected.LtypeSub as Object, actual.LtypeSub as Object, "LtypeSub");
	    Assert.AreEqual(expected.Product as Object, actual.Product as Object, "Product");
	    Assert.AreEqual(expected.LoanDesc as Object, actual.LoanDesc as Object, "LoanDesc");
	    Assert.AreEqual(expected.LoanDate.ToString() as Object, actual.LoanDate.ToString() as Object, "LoanDate");
	    Assert.AreEqual(expected.LoanDay as Object, actual.LoanDay as Object, "LoanDay");
	    Assert.AreEqual(expected.LoanMonth as Object, actual.LoanMonth as Object, "LoanMonth");
	    Assert.AreEqual(expected.LoanYear as Object, actual.LoanYear as Object, "LoanYear");
	    Assert.AreEqual(expected.FirstPmtDate.ToString() as Object, actual.FirstPmtDate.ToString() as Object, "FirstPmtDate");
	    Assert.AreEqual(expected.Status as Object, actual.Status as Object, "Status");
	    Assert.AreEqual(expected.RptCode as Object, actual.RptCode as Object, "RptCode");
	    Assert.AreEqual(expected.FundedBr as Object, actual.FundedBr as Object, "FundedBr");
	    Assert.AreEqual(expected.LoadedBr as Object, actual.LoadedBr as Object, "LoadedBr");
	    Assert.AreEqual(expected.FICO as Object, actual.FICO as Object, "FICO");
	    Assert.AreEqual(expected.FICORange as Object, actual.FICORange as Object, "FICORange");
	    Assert.AreEqual(expected.FastStart as Object, actual.FastStart as Object, "FastStart");
	    Assert.AreEqual(expected.FastStartRange as Object, actual.FastStartRange as Object, "FastStartRange");
	    Assert.AreEqual(expected.Bankruptcy as Object, actual.Bankruptcy as Object, "Bankruptcy");
	    Assert.AreEqual(expected.BankruptcyRange as Object, actual.BankruptcyRange as Object, "BankruptcyRange");
	    Assert.AreEqual(expected.MembershipDate.ToString() as Object, actual.MembershipDate.ToString() as Object, "MembershipDate");
	    Assert.AreEqual(expected.NewMember as Object, actual.NewMember as Object, "NewMember");
	    Assert.AreEqual(expected.RiskLev as Object, actual.RiskLev as Object, "RiskLev");
	    Assert.AreEqual(expected.Traffic as Object, actual.Traffic as Object, "Traffic");
	    Assert.AreEqual(expected.TrafficColor as Object, actual.TrafficColor as Object, "TrafficColor");
	    Assert.AreEqual(expected.MemAge as Object, actual.MemAge as Object, "MemAge");
	    Assert.AreEqual(expected.MemSex as Object, actual.MemSex as Object, "MemSex");
	    Assert.AreEqual(expected.PmtFreq as Object, actual.PmtFreq as Object, "PmtFreq");
	    Assert.AreEqual(expected.OrigTerm as Object, actual.OrigTerm as Object, "OrigTerm");
	    Assert.AreEqual(expected.OrigRate as Object, actual.OrigRate as Object, "OrigRate");
	    Assert.AreEqual(expected.OrigPmt as Object, actual.OrigPmt as Object, "OrigPmt");
	    Assert.AreEqual(expected.OrigAmount as Object, actual.OrigAmount as Object, "OrigAmount");
	    Assert.AreEqual(expected.OrigCrLim as Object, actual.OrigCrLim as Object, "OrigCrLim");
	    Assert.AreEqual(expected.CurTerm as Object, actual.CurTerm as Object, "CurTerm");
	    Assert.AreEqual(expected.CurRate as Object, actual.CurRate as Object, "CurRate");
	    Assert.AreEqual(expected.CurPmt as Object, actual.CurPmt as Object, "CurPmt");
	    Assert.AreEqual(expected.CurBal as Object, actual.CurBal as Object, "CurBal");
	    Assert.AreEqual(expected.CurCrLim as Object, actual.CurCrLim as Object, "CurCrLim");
	    Assert.AreEqual(expected.BalloonDate.ToString() as Object, actual.BalloonDate.ToString() as Object, "BalloonDate");
	    Assert.AreEqual(expected.MatDate.ToString() as Object, actual.MatDate.ToString() as Object, "MatDate");
	    Assert.AreEqual(expected.InsCode as Object, actual.InsCode as Object, "InsCode");
	    Assert.AreEqual(expected.CollateralValue as Object, actual.CollateralValue as Object, "CollateralValue");
	    Assert.AreEqual(expected.LTV as Object, actual.LTV as Object, "LTV");
	    Assert.AreEqual(expected.SecType as Object, actual.SecType as Object, "SecType");
	    Assert.AreEqual(expected.SecDesc1 as Object, actual.SecDesc1 as Object, "SecDesc1");
	    Assert.AreEqual(expected.SecDesc2 as Object, actual.SecDesc2 as Object, "SecDesc2");
	    Assert.AreEqual(expected.DebtRatio as Object, actual.DebtRatio as Object, "DebtRatio");
	    Assert.AreEqual(expected.GrossInc as Object, actual.GrossInc as Object, "GrossInc");
	    Assert.AreEqual(expected.Dealer as Object, actual.Dealer as Object, "Dealer");
	    Assert.AreEqual(expected.DealerName as Object, actual.DealerName as Object, "DealerName");
	    Assert.AreEqual(expected.CoborComak as Object, actual.CoborComak as Object, "CoborComak");
	    Assert.AreEqual(expected.RelPriceGrp as Object, actual.RelPriceGrp as Object, "RelPriceGrp");
	    Assert.AreEqual(expected.RelPriceDisc as Object, actual.RelPriceDisc as Object, "RelPriceDisc");
	    Assert.AreEqual(expected.OtherDisc as Object, actual.OtherDisc as Object, "OtherDisc");
	    Assert.AreEqual(expected.DiscReason as Object, actual.DiscReason as Object, "DiscReason");
	    Assert.AreEqual(expected.RiskOffset as Object, actual.RiskOffset as Object, "RiskOffset");
	    Assert.AreEqual(expected.CreditType as Object, actual.CreditType as Object, "CreditType");
	    Assert.AreEqual(expected.ApprOff as Object, actual.ApprOff as Object, "ApprOff");
	    Assert.AreEqual(expected.StatedIncome as Object, actual.StatedIncome as Object, "StatedIncome");
	    Assert.AreEqual(expected.Extensions as Object, actual.Extensions as Object, "Extensions");
	    Assert.AreEqual(expected.FirstExtDate.ToString() as Object, actual.FirstExtDate.ToString() as Object, "FirstExtDate");
	    Assert.AreEqual(expected.LastExtDate.ToString() as Object, actual.LastExtDate.ToString() as Object, "LastExtDate");
	    Assert.AreEqual(expected.RiskLevel as Object, actual.RiskLevel as Object, "RiskLevel");
	    Assert.AreEqual(expected.RiskDate.ToString() as Object, actual.RiskDate.ToString() as Object, "RiskDate");
	    Assert.AreEqual(expected.Watch as Object, actual.Watch as Object, "Watch");
	    Assert.AreEqual(expected.WatchDate.ToString() as Object, actual.WatchDate.ToString() as Object, "WatchDate");
	    Assert.AreEqual(expected.WorkOut as Object, actual.WorkOut as Object, "WorkOut");
	    Assert.AreEqual(expected.WorkOutDate.ToString() as Object, actual.WorkOutDate.ToString() as Object, "WorkOutDate");
	    Assert.AreEqual(expected.FirstMortType as Object, actual.FirstMortType as Object, "FirstMortType");
	    Assert.AreEqual(expected.BusPerReg as Object, actual.BusPerReg as Object, "BusPerReg");
	    Assert.AreEqual(expected.BusGroup as Object, actual.BusGroup as Object, "BusGroup");
	    Assert.AreEqual(expected.BusDesc as Object, actual.BusDesc as Object, "BusDesc");
	    Assert.AreEqual(expected.BusPart as Object, actual.BusPart as Object, "BusPart");
	    Assert.AreEqual(expected.BusPartPerc as Object, actual.BusPartPerc as Object, "BusPartPerc");
	    Assert.AreEqual(expected.SBAguar as Object, actual.SBAguar as Object, "SBAguar");
	    Assert.AreEqual(expected.DeliDays as Object, actual.DeliDays as Object, "DeliDays");
	    Assert.AreEqual(expected.DeliSect as Object, actual.DeliSect as Object, "DeliSect");
	    Assert.AreEqual(expected.DeliHist as Object, actual.DeliHist as Object, "DeliHist");
	    Assert.AreEqual(expected.DeliHistQ1 as Object, actual.DeliHistQ1 as Object, "DeliHistQ1");
	    Assert.AreEqual(expected.DeliHistQ2 as Object, actual.DeliHistQ2 as Object, "DeliHistQ2");
	    Assert.AreEqual(expected.DeliHistQ3 as Object, actual.DeliHistQ3 as Object, "DeliHistQ3");
	    Assert.AreEqual(expected.DeliHistQ4 as Object, actual.DeliHistQ4 as Object, "DeliHistQ4");
	    Assert.AreEqual(expected.DeliHistQ5 as Object, actual.DeliHistQ5 as Object, "DeliHistQ5");
	    Assert.AreEqual(expected.DeliHistQ6 as Object, actual.DeliHistQ6 as Object, "DeliHistQ6");
	    Assert.AreEqual(expected.DeliHistQ7 as Object, actual.DeliHistQ7 as Object, "DeliHistQ7");
	    Assert.AreEqual(expected.DeliHistQ8 as Object, actual.DeliHistQ8 as Object, "DeliHistQ8");
	    Assert.AreEqual(expected.ChrgOffDate.ToString() as Object, actual.ChrgOffDate.ToString() as Object, "ChrgOffDate");
	    Assert.AreEqual(expected.ChrgOffMonth as Object, actual.ChrgOffMonth as Object, "ChrgOffMonth");
	    Assert.AreEqual(expected.ChrgOffYear as Object, actual.ChrgOffYear as Object, "ChrgOffYear");
	    Assert.AreEqual(expected.ChrgOffMnthsBF as Object, actual.ChrgOffMnthsBF as Object, "ChrgOffMnthsBF");
	    Assert.AreEqual(expected.PropType as Object, actual.PropType as Object, "PropType");
	    Assert.AreEqual(expected.Occupancy as Object, actual.Occupancy as Object, "Occupancy");
	    Assert.AreEqual(expected.OrigAppVal as Object, actual.OrigAppVal as Object, "OrigAppVal");
	    Assert.AreEqual(expected.OrigAppDate.ToString() as Object, actual.OrigAppDate.ToString() as Object, "OrigAppDate");
	    Assert.AreEqual(expected.CurrApprVal as Object, actual.CurrApprVal as Object, "CurrApprVal");
	    Assert.AreEqual(expected.CurrApprDate.ToString() as Object, actual.CurrApprDate.ToString() as Object, "CurrApprDate");
	    Assert.AreEqual(expected.FirstMortBal as Object, actual.FirstMortBal as Object, "FirstMortBal");
	    Assert.AreEqual(expected.FirstMortMoPyt as Object, actual.FirstMortMoPyt as Object, "FirstMortMoPyt");
	    Assert.AreEqual(expected.SecondMortBal as Object, actual.SecondMortBal as Object, "SecondMortBal");
	    Assert.AreEqual(expected.SecondMortPymt as Object, actual.SecondMortPymt as Object, "SecondMortPymt");
	    Assert.AreEqual(expected.TaxInsNotInc as Object, actual.TaxInsNotInc as Object, "TaxInsNotInc");
	}
    }
}
