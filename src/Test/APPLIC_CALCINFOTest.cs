using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for APPLIC_CALCINFOTest.
    /// </summary>
    [TestFixture()]
    public class APPLIC_CALCINFOTest : BaseTest {
	[Generate()]
	private void CheckPersistance(APPLIC_CALCINFO expected) {
	    APPLIC_CALCINFO actual = APPLIC_CALCINFO.GetInstance(expected.APPLIC_ID);
	    Assert.AreEqual(expected.APPLIC_ID as Object, actual.APPLIC_ID as Object, "APPLIC_ID");
	    Assert.AreEqual(expected.DEBT_AFTER as Object, actual.DEBT_AFTER as Object, "DEBT_AFTER");
	}
    }
}
