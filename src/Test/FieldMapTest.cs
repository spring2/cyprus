using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dss.BusinessLogic;
using Spring2.Dss.Dao;
using Spring2.Dss.DataObject;
using Spring2.Dss.Types;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

using Cyprus.BusinessLogic;
using Cyprus.Dao;
using Cyprus.DataObject;
using Cyprus.Types;

namespace Cyprus.Test {
    /// <summary>
    /// Summary description for FieldMapTest.
    /// </summary>
    [TestFixture()]
    public class FieldMapTest : BaseTest {
	[Generate()]
	private void CheckPersistance(FieldMap expected) {
	    FieldMap actual = FieldMap.GetInstance();
	    Assert.AreEqual(expected.FieldMapId as Object, actual.FieldMapId as Object, "FieldMapId");
	    Assert.AreEqual(expected.SrcTable as Object, actual.SrcTable as Object, "SrcTable");
	    Assert.AreEqual(expected.SrcColumn as Object, actual.SrcColumn as Object, "SrcColumn");
	    Assert.AreEqual(expected.DestTable as Object, actual.DestTable as Object, "DestTable");
	    Assert.AreEqual(expected.DestColumn as Object, actual.DestColumn as Object, "DestColumn");
	}
    }
}

namespace Spring2.Test {
    /// <summary>
    /// Summary description for FieldMapTest.
    /// </summary>
    [TestFixture()]
    public class FieldMapTest : BaseTest {
	[Generate()]
	private void CheckPersistance(FieldMap expected) {
	    FieldMap actual = FieldMap.GetInstance(expected.FieldMapId);
	    Assert.AreEqual(expected.FieldMapId as Object, actual.FieldMapId as Object, "FieldMapId");
	    Assert.AreEqual(expected.SrcTable as Object, actual.SrcTable as Object, "SrcTable");
	    Assert.AreEqual(expected.SrcColumn as Object, actual.SrcColumn as Object, "SrcColumn");
	    Assert.AreEqual(expected.DestTable as Object, actual.DestTable as Object, "DestTable");
	    Assert.AreEqual(expected.DestColumn as Object, actual.DestColumn as Object, "DestColumn");
	}
    }
}

namespace Spring2.Dss.Test {
    /// <summary>
    /// Summary description for FieldMapTest.
    /// </summary>
    [TestFixture()]
    public class FieldMapTest : BaseTest {
	[Generate()]
	private void CheckPersistance(FieldMap expected) {
	    FieldMap actual = FieldMap.GetInstance();
	    Assert.AreEqual(expected.FieldMapId as Object, actual.FieldMapId as Object, "FieldMapId");
	    Assert.AreEqual(expected.SrcTable as Object, actual.SrcTable as Object, "SrcTable");
	    Assert.AreEqual(expected.SrcColumn as Object, actual.SrcColumn as Object, "SrcColumn");
	    Assert.AreEqual(expected.DestTable as Object, actual.DestTable as Object, "DestTable");
	    Assert.AreEqual(expected.DestColumn as Object, actual.DestColumn as Object, "DestColumn");
	}
    }
}
