using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for ChanelTest.
    /// </summary>
    [TestFixture()]
    public class ChanelTest : BaseTest {
	[Generate()]
	private void CheckPersistance(Chanel expected) {
	    Chanel actual = Chanel.GetInstance(expected.ChanelId);
	    Assert.AreEqual(expected.ChanelId as Object, actual.ChanelId as Object, "ChanelId");
	    Assert.AreEqual(expected.ReportCode as Object, actual.ReportCode as Object, "ReportCode");
	    Assert.AreEqual(expected.ChanelDesc as Object, actual.ChanelDesc as Object, "ChanelDesc");
	}
    }
}
