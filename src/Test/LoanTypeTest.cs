using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for LoanTypeTest.
    /// </summary>
    [TestFixture()]
    public class LoanTypeTest : BaseTest {
	[Generate()]
	private void CheckPersistance(LoanType expected) {
	    LoanType actual = LoanType.GetInstance(expected.LoanTypeId);
	    Assert.AreEqual(expected.LoanTypeId as Object, actual.LoanTypeId as Object, "LoanTypeId");
	    Assert.AreEqual(expected.LType as Object, actual.LType as Object, "LType");
	    Assert.AreEqual(expected.LoanDesc as Object, actual.LoanDesc as Object, "LoanDesc");
	}
    }
}
