using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for ACCOUNT_UD_USTest.
    /// </summary>
    [TestFixture()]
    public class ACCOUNT_UD_USTest : BaseTest {
	[Generate()]
	private void CheckPersistance(ACCOUNT_UD_US expected) {
	    ACCOUNT_UD_US actual = ACCOUNT_UD_US.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.US_INCOME_STATED as Object, actual.US_INCOME_STATED as Object, "US_INCOME_STATED");
	    Assert.AreEqual(expected.US_NUM_OF_EXTENSIONS as Object, actual.US_NUM_OF_EXTENSIONS as Object, "US_NUM_OF_EXTENSIONS");
	    Assert.AreEqual(expected.US_1ST_EXT_DTE as Object, actual.US_1ST_EXT_DTE as Object, "US_1ST_EXT_DTE");
	    Assert.AreEqual(expected.US_MOSTRECENT_EXT_DTE as Object, actual.US_MOSTRECENT_EXT_DTE as Object, "US_MOSTRECENT_EXT_DTE");
	    Assert.AreEqual(expected.US_L_RISKLEV as Object, actual.US_L_RISKLEV as Object, "US_L_RISKLEV");
	    Assert.AreEqual(expected.US_RISKLEVEL_DTE as Object, actual.US_RISKLEVEL_DTE as Object, "US_RISKLEVEL_DTE");
	    Assert.AreEqual(expected.US_WATCHLIST as Object, actual.US_WATCHLIST as Object, "US_WATCHLIST");
	    Assert.AreEqual(expected.US_DATE_WATCHLIST as Object, actual.US_DATE_WATCHLIST as Object, "US_DATE_WATCHLIST");
	    Assert.AreEqual(expected.US_WORKOUT_LOANS as Object, actual.US_WORKOUT_LOANS as Object, "US_WORKOUT_LOANS");
	    Assert.AreEqual(expected.US_WORKOUT_DTE as Object, actual.US_WORKOUT_DTE as Object, "US_WORKOUT_DTE");
	    Assert.AreEqual(expected.US_UD_BUS_LOAN_PER_REG as Object, actual.US_UD_BUS_LOAN_PER_REG as Object, "US_UD_BUS_LOAN_PER_REG");
	    Assert.AreEqual(expected.US_UD_BUS_LOAN_COL_GRP as Object, actual.US_UD_BUS_LOAN_COL_GRP as Object, "US_UD_BUS_LOAN_COL_GRP");
	    Assert.AreEqual(expected.US_UD_BUS_LOAN_COL_DESC as Object, actual.US_UD_BUS_LOAN_COL_DESC as Object, "US_UD_BUS_LOAN_COL_DESC");
	    Assert.AreEqual(expected.US_UD_BUS_LOAN_PART as Object, actual.US_UD_BUS_LOAN_PART as Object, "US_UD_BUS_LOAN_PART");
	    Assert.AreEqual(expected.US_UD_SBA_GUARANTY as Object, actual.US_UD_SBA_GUARANTY as Object, "US_UD_SBA_GUARANTY");
	    Assert.AreEqual(expected.US_UD_1ST_MORT_TYPE as Object, actual.US_UD_1ST_MORT_TYPE as Object, "US_UD_1ST_MORT_TYPE");
	    Assert.AreEqual(expected.US_UD_BUS_LOAN_PRCNT_PART as Object, actual.US_UD_BUS_LOAN_PRCNT_PART as Object, "US_UD_BUS_LOAN_PRCNT_PART");
	}
    }
}
