using System.Collections;
using System;

using Spring2.Core.DAO;
using Spring2.DataTierGenerator.Attribute;

using Spring2.Dss.Dao;
using Spring2.Dss.DataObject;

using NUnit.Framework;

using Spring2.Dao;
using Spring2.DataObject;

using Cyprus.Dao;
using Cyprus.DataObject;

namespace Cyprus.Test {
    [TestFixture()]
    public class DaoTest {
	[Generate()]
	[Test()]
	public void TestLoanPortfolioDAO() {
	    // get 100 random rows
	    IList list = LoanPortfolioDAO.DAO.GetList(new OrderByClause("LoanPortfolioId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (ILoanPortfolio entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.LoanPortfolioId.IsDefault);
		Assert.IsFalse(entity.Name.IsDefault);
		Assert.IsFalse(entity.UpDated.IsDefault);
		Assert.IsFalse(entity.AccountID.IsDefault);
		Assert.IsFalse(entity.Member.IsDefault);
		Assert.IsFalse(entity.MClass.IsDefault);
		Assert.IsFalse(entity.Ltype.IsDefault);
		Assert.IsFalse(entity.LtypeSub.IsDefault);
		Assert.IsFalse(entity.Product.IsDefault);
		Assert.IsFalse(entity.LoanDesc.IsDefault);
		Assert.IsFalse(entity.LoanDate.IsDefault);
		Assert.IsFalse(entity.LoanDay.IsDefault);
		Assert.IsFalse(entity.LoanMonth.IsDefault);
		Assert.IsFalse(entity.LoanYear.IsDefault);
		Assert.IsFalse(entity.FirstPmtDate.IsDefault);
		Assert.IsFalse(entity.Status.IsDefault);
		Assert.IsFalse(entity.RptCode.IsDefault);
		Assert.IsFalse(entity.FundedBr.IsDefault);
		Assert.IsFalse(entity.LoadedBr.IsDefault);
		Assert.IsFalse(entity.FICO.IsDefault);
		Assert.IsFalse(entity.FICORange.IsDefault);
		Assert.IsFalse(entity.FastStart.IsDefault);
		Assert.IsFalse(entity.FastStartRange.IsDefault);
		Assert.IsFalse(entity.Bankruptcy.IsDefault);
		Assert.IsFalse(entity.BankruptcyRange.IsDefault);
		Assert.IsFalse(entity.MembershipDate.IsDefault);
		Assert.IsFalse(entity.NewMember.IsDefault);
		Assert.IsFalse(entity.RiskLev.IsDefault);
		Assert.IsFalse(entity.Traffic.IsDefault);
		Assert.IsFalse(entity.TrafficColor.IsDefault);
		Assert.IsFalse(entity.MemAge.IsDefault);
		Assert.IsFalse(entity.MemSex.IsDefault);
		Assert.IsFalse(entity.PmtFreq.IsDefault);
		Assert.IsFalse(entity.OrigTerm.IsDefault);
		Assert.IsFalse(entity.OrigRate.IsDefault);
		Assert.IsFalse(entity.OrigPmt.IsDefault);
		Assert.IsFalse(entity.OrigAmount.IsDefault);
		Assert.IsFalse(entity.OrigCrLim.IsDefault);
		Assert.IsFalse(entity.CurTerm.IsDefault);
		Assert.IsFalse(entity.CurRate.IsDefault);
		Assert.IsFalse(entity.CurPmt.IsDefault);
		Assert.IsFalse(entity.CurBal.IsDefault);
		Assert.IsFalse(entity.CurCrLim.IsDefault);
		Assert.IsFalse(entity.BalloonDate.IsDefault);
		Assert.IsFalse(entity.MatDate.IsDefault);
		Assert.IsFalse(entity.InsCode.IsDefault);
		Assert.IsFalse(entity.CollateralValue.IsDefault);
		Assert.IsFalse(entity.LTV.IsDefault);
		Assert.IsFalse(entity.SecType.IsDefault);
		Assert.IsFalse(entity.SecDesc1.IsDefault);
		Assert.IsFalse(entity.SecDesc2.IsDefault);
		Assert.IsFalse(entity.DebtRatio.IsDefault);
		Assert.IsFalse(entity.GrossInc.IsDefault);
		Assert.IsFalse(entity.Dealer.IsDefault);
		Assert.IsFalse(entity.DealerName.IsDefault);
		Assert.IsFalse(entity.CoborComak.IsDefault);
		Assert.IsFalse(entity.RelPriceGrp.IsDefault);
		Assert.IsFalse(entity.RelPriceDisc.IsDefault);
		Assert.IsFalse(entity.OtherDisc.IsDefault);
		Assert.IsFalse(entity.DiscReason.IsDefault);
		Assert.IsFalse(entity.RiskOffset.IsDefault);
		Assert.IsFalse(entity.CreditType.IsDefault);
		Assert.IsFalse(entity.ApprOff.IsDefault);
		Assert.IsFalse(entity.StatedIncome.IsDefault);
		Assert.IsFalse(entity.Extensions.IsDefault);
		Assert.IsFalse(entity.FirstExtDate.IsDefault);
		Assert.IsFalse(entity.LastExtDate.IsDefault);
		Assert.IsFalse(entity.RiskLevel.IsDefault);
		Assert.IsFalse(entity.RiskDate.IsDefault);
		Assert.IsFalse(entity.Watch.IsDefault);
		Assert.IsFalse(entity.WatchDate.IsDefault);
		Assert.IsFalse(entity.WorkOut.IsDefault);
		Assert.IsFalse(entity.WorkOutDate.IsDefault);
		Assert.IsFalse(entity.FirstMortType.IsDefault);
		Assert.IsFalse(entity.BusPerReg.IsDefault);
		Assert.IsFalse(entity.BusGroup.IsDefault);
		Assert.IsFalse(entity.BusDesc.IsDefault);
		Assert.IsFalse(entity.BusPart.IsDefault);
		Assert.IsFalse(entity.BusPartPerc.IsDefault);
		Assert.IsFalse(entity.SBAguar.IsDefault);
		Assert.IsFalse(entity.DeliDays.IsDefault);
		Assert.IsFalse(entity.DeliSect.IsDefault);
		Assert.IsFalse(entity.DeliHist.IsDefault);
		Assert.IsFalse(entity.DeliHistQ1.IsDefault);
		Assert.IsFalse(entity.DeliHistQ2.IsDefault);
		Assert.IsFalse(entity.DeliHistQ3.IsDefault);
		Assert.IsFalse(entity.DeliHistQ4.IsDefault);
		Assert.IsFalse(entity.DeliHistQ5.IsDefault);
		Assert.IsFalse(entity.DeliHistQ6.IsDefault);
		Assert.IsFalse(entity.DeliHistQ7.IsDefault);
		Assert.IsFalse(entity.DeliHistQ8.IsDefault);
		Assert.IsFalse(entity.ChrgOffDate.IsDefault);
		Assert.IsFalse(entity.ChrgOffMonth.IsDefault);
		Assert.IsFalse(entity.ChrgOffYear.IsDefault);
		Assert.IsFalse(entity.ChrgOffMnthsBF.IsDefault);
		Assert.IsFalse(entity.PropType.IsDefault);
		Assert.IsFalse(entity.Occupancy.IsDefault);
		Assert.IsFalse(entity.OrigAppVal.IsDefault);
		Assert.IsFalse(entity.OrigAppDate.IsDefault);
		Assert.IsFalse(entity.CurrApprVal.IsDefault);
		Assert.IsFalse(entity.CurrApprDate.IsDefault);
		Assert.IsFalse(entity.FirstMortBal.IsDefault);
		Assert.IsFalse(entity.FirstMortMoPyt.IsDefault);
		Assert.IsFalse(entity.SecondMortBal.IsDefault);
		Assert.IsFalse(entity.SecondMortPymt.IsDefault);
		Assert.IsFalse(entity.TaxInsNotInc.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestFieldMapDAO() {
	    // get 100 random rows
	    IList list = FieldMapDAO.DAO.GetList(100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IFieldMap entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.FieldMapId.IsDefault);
		Assert.IsFalse(entity.SrcTable.IsDefault);
		Assert.IsFalse(entity.SrcColumn.IsDefault);
		Assert.IsFalse(entity.DestTable.IsDefault);
		Assert.IsFalse(entity.DestColumn.IsDefault);
	    }
	}
    }
}

namespace Spring2.Test {
    [TestFixture()]
    public class DaoTest {
	[Generate()]
	[Test()]
	public void TestLoanPortfolioDAO() {
	    // get 100 random rows
	    IList list = LoanPortfolioDAO.DAO.GetList(new OrderByClause("LoanPortfolio_Id desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (ILoanPortfolio entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.LoanPortfolioId.IsDefault);
		Assert.IsFalse(entity.UpDated.IsDefault);
		Assert.IsFalse(entity.EOMDate.IsDefault);
		Assert.IsFalse(entity.AccountID.IsDefault);
		Assert.IsFalse(entity.Member.IsDefault);
		Assert.IsFalse(entity.MClass.IsDefault);
		Assert.IsFalse(entity.Ltype.IsDefault);
		Assert.IsFalse(entity.LtypeSub.IsDefault);
		Assert.IsFalse(entity.Product.IsDefault);
		Assert.IsFalse(entity.LoanDesc.IsDefault);
		Assert.IsFalse(entity.LoanDate.IsDefault);
		Assert.IsFalse(entity.LoanDay.IsDefault);
		Assert.IsFalse(entity.LoanMonth.IsDefault);
		Assert.IsFalse(entity.LoanYear.IsDefault);
		Assert.IsFalse(entity.FirstPmtDate.IsDefault);
		Assert.IsFalse(entity.Status.IsDefault);
		Assert.IsFalse(entity.RptCode.IsDefault);
		Assert.IsFalse(entity.FundedBr.IsDefault);
		Assert.IsFalse(entity.LoadedBr.IsDefault);
		Assert.IsFalse(entity.FICO.IsDefault);
		Assert.IsFalse(entity.FICORange.IsDefault);
		Assert.IsFalse(entity.FastStart.IsDefault);
		Assert.IsFalse(entity.FastStartRange.IsDefault);
		Assert.IsFalse(entity.Bankruptcy.IsDefault);
		Assert.IsFalse(entity.BankruptcyRange.IsDefault);
		Assert.IsFalse(entity.MembershipDate.IsDefault);
		Assert.IsFalse(entity.NewMember.IsDefault);
		Assert.IsFalse(entity.RiskLev.IsDefault);
		Assert.IsFalse(entity.Traffic.IsDefault);
		Assert.IsFalse(entity.TrafficColor.IsDefault);
		Assert.IsFalse(entity.MemAge.IsDefault);
		Assert.IsFalse(entity.MemSex.IsDefault);
		Assert.IsFalse(entity.PmtFreq.IsDefault);
		Assert.IsFalse(entity.OrigTerm.IsDefault);
		Assert.IsFalse(entity.OrigRate.IsDefault);
		Assert.IsFalse(entity.OrigPmt.IsDefault);
		Assert.IsFalse(entity.OrigAmount.IsDefault);
		Assert.IsFalse(entity.OrigCrLim.IsDefault);
		Assert.IsFalse(entity.CurTerm.IsDefault);
		Assert.IsFalse(entity.CurRate.IsDefault);
		Assert.IsFalse(entity.CurPmt.IsDefault);
		Assert.IsFalse(entity.CurBal.IsDefault);
		Assert.IsFalse(entity.CurCrLim.IsDefault);
		Assert.IsFalse(entity.BalloonDate.IsDefault);
		Assert.IsFalse(entity.MatDate.IsDefault);
		Assert.IsFalse(entity.InsCode.IsDefault);
		Assert.IsFalse(entity.CollateralValue.IsDefault);
		Assert.IsFalse(entity.LTV.IsDefault);
		Assert.IsFalse(entity.SecType.IsDefault);
		Assert.IsFalse(entity.SecDesc1.IsDefault);
		Assert.IsFalse(entity.SecDesc2.IsDefault);
		Assert.IsFalse(entity.DebtRatio.IsDefault);
		Assert.IsFalse(entity.Dealer.IsDefault);
		Assert.IsFalse(entity.DealerName.IsDefault);
		Assert.IsFalse(entity.CoborComak.IsDefault);
		Assert.IsFalse(entity.RelPriceGrp.IsDefault);
		Assert.IsFalse(entity.RelPriceDisc.IsDefault);
		Assert.IsFalse(entity.OtherDisc.IsDefault);
		Assert.IsFalse(entity.DiscReason.IsDefault);
		Assert.IsFalse(entity.RiskOffset.IsDefault);
		Assert.IsFalse(entity.CreditType.IsDefault);
		Assert.IsFalse(entity.StatedIncome.IsDefault);
		Assert.IsFalse(entity.Extensions.IsDefault);
		Assert.IsFalse(entity.FirstExtDate.IsDefault);
		Assert.IsFalse(entity.LastExtDate.IsDefault);
		Assert.IsFalse(entity.RiskLevel.IsDefault);
		Assert.IsFalse(entity.RiskDate.IsDefault);
		Assert.IsFalse(entity.Watch.IsDefault);
		Assert.IsFalse(entity.WatchDate.IsDefault);
		Assert.IsFalse(entity.WorkOut.IsDefault);
		Assert.IsFalse(entity.WorkOutDate.IsDefault);
		Assert.IsFalse(entity.FirstMortType.IsDefault);
		Assert.IsFalse(entity.BusPerReg.IsDefault);
		Assert.IsFalse(entity.BusGroup.IsDefault);
		Assert.IsFalse(entity.BusDesc.IsDefault);
		Assert.IsFalse(entity.BusPart.IsDefault);
		Assert.IsFalse(entity.BusPartPerc.IsDefault);
		Assert.IsFalse(entity.SBAguar.IsDefault);
		Assert.IsFalse(entity.DeliDays.IsDefault);
		Assert.IsFalse(entity.DeliSect.IsDefault);
		Assert.IsFalse(entity.DeliHist.IsDefault);
		Assert.IsFalse(entity.DeliHistQ1.IsDefault);
		Assert.IsFalse(entity.DeliHistQ2.IsDefault);
		Assert.IsFalse(entity.DeliHistQ3.IsDefault);
		Assert.IsFalse(entity.DeliHistQ4.IsDefault);
		Assert.IsFalse(entity.DeliHistQ5.IsDefault);
		Assert.IsFalse(entity.DeliHistQ6.IsDefault);
		Assert.IsFalse(entity.DeliHistQ7.IsDefault);
		Assert.IsFalse(entity.DeliHistQ8.IsDefault);
		Assert.IsFalse(entity.ChrgOffDate.IsDefault);
		Assert.IsFalse(entity.ChrgOffMonth.IsDefault);
		Assert.IsFalse(entity.ChrgOffYear.IsDefault);
		Assert.IsFalse(entity.ChrgOffMnthsBF.IsDefault);
		Assert.IsFalse(entity.PropType.IsDefault);
		Assert.IsFalse(entity.Occupancy.IsDefault);
		Assert.IsFalse(entity.OrigApprVal.IsDefault);
		Assert.IsFalse(entity.OrigApprDate.IsDefault);
		Assert.IsFalse(entity.CurrApprVal.IsDefault);
		Assert.IsFalse(entity.CurrApprDate.IsDefault);
		Assert.IsFalse(entity.FirstMortBal.IsDefault);
		Assert.IsFalse(entity.FirstMortMoPyt.IsDefault);
		Assert.IsFalse(entity.SecondMortBal.IsDefault);
		Assert.IsFalse(entity.SecondMortPymt.IsDefault);
		Assert.IsFalse(entity.TaxInsNotInc.IsDefault);
		Assert.IsFalse(entity.LoadOff.IsDefault);
		Assert.IsFalse(entity.ApprOff.IsDefault);
		Assert.IsFalse(entity.FundingOpp.IsDefault);
		Assert.IsFalse(entity.Chanel.IsDefault);
		Assert.IsFalse(entity.RiskType.IsDefault);
		Assert.IsFalse(entity.Gap.IsDefault);
		Assert.IsFalse(entity.LoadOperator.IsDefault);
		Assert.IsFalse(entity.DeliAmt.IsDefault);
		Assert.IsFalse(entity.ChgOffAmt.IsDefault);
		Assert.IsFalse(entity.UsSecDesc.IsDefault);
		Assert.IsFalse(entity.TroubledDebt.IsDefault);
		Assert.IsFalse(entity.TdrDate.IsDefault);
		Assert.IsFalse(entity.JntFicoUsed.IsDefault);
		Assert.IsFalse(entity.SingleOrJointLoan.IsDefault);
		Assert.IsFalse(entity.ApprovingOfficerUD.IsDefault);
		Assert.IsFalse(entity.InsuranceCodeDesc.IsDefault);
		Assert.IsFalse(entity.GapIns.IsDefault);
		Assert.IsFalse(entity.MmpIns.IsDefault);
		Assert.IsFalse(entity.ReportCodeDesc.IsDefault);
		Assert.IsFalse(entity.BallonPmtLoan.IsDefault);
		Assert.IsFalse(entity.FixedOrVariableRateLoan.IsDefault);
		Assert.IsFalse(entity.AlpsOfficer.IsDefault);
		Assert.IsFalse(entity.CUDLAppNumber.IsDefault);
		Assert.IsFalse(entity.FasbDefCost.IsDefault);
		Assert.IsFalse(entity.FasbLtdAmortFees.IsDefault);
		Assert.IsFalse(entity.InterestPriorYtd.IsDefault);
		Assert.IsFalse(entity.AccruedInterest.IsDefault);
		Assert.IsFalse(entity.InterestLTD.IsDefault);
		Assert.IsFalse(entity.InterestYtd.IsDefault);
		Assert.IsFalse(entity.PartialPayAmt.IsDefault);
		Assert.IsFalse(entity.InterestUncollected.IsDefault);
		Assert.IsFalse(entity.ClosedAcctDate.IsDefault);
		Assert.IsFalse(entity.LastTranDate.IsDefault);
		Assert.IsFalse(entity.NextDueDate.IsDefault);
		Assert.IsFalse(entity.InsuranceCode.IsDefault);
		Assert.IsFalse(entity.MbrAgeAtApproval.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestLoanTypeDAO() {
	    // get 100 random rows
	    IList list = LoanTypeDAO.DAO.GetList(new OrderByClause("LoanTypeId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (ILoanType entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.LoanTypeId.IsDefault);
		Assert.IsFalse(entity.LType.IsDefault);
		Assert.IsFalse(entity.LoanDesc.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestFICORangeDAO() {
	    // get 100 random rows
	    IList list = FICORangeDAO.DAO.GetList(new OrderByClause("FICORangeId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IFICORange entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.FICORangeId.IsDefault);
		Assert.IsFalse(entity.Value.IsDefault);
		Assert.IsFalse(entity.Description.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestFastStartRangeDAO() {
	    // get 100 random rows
	    IList list = FastStartRangeDAO.DAO.GetList(new OrderByClause("FastStartRangeId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IFastStartRange entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.FastStartRangeId.IsDefault);
		Assert.IsFalse(entity.Value.IsDefault);
		Assert.IsFalse(entity.Description.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestBankruptcyRangeDAO() {
	    // get 100 random rows
	    IList list = BankruptcyRangeDAO.DAO.GetList(new OrderByClause("BankruptcyRangeId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IBankruptcyRange entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.BankruptcyRangeId.IsDefault);
		Assert.IsFalse(entity.Value.IsDefault);
		Assert.IsFalse(entity.Description.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestRiskTypeDAO() {
	    // get 100 random rows
	    IList list = RiskTypeDAO.DAO.GetList(new OrderByClause("RiskTypeId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IRiskType entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.RiskTypeId.IsDefault);
		Assert.IsFalse(entity.LType.IsDefault);
		Assert.IsFalse(entity.RiskTypeDesc.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestChanelDAO() {
	    // get 100 random rows
	    IList list = ChanelDAO.DAO.GetList(new OrderByClause("ChanelId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IChanel entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.ChanelId.IsDefault);
		Assert.IsFalse(entity.ReportCode.IsDefault);
		Assert.IsFalse(entity.ChanelDesc.IsDefault);
	    }
	}
    }
}

namespace Spring2.Dss.Test {
    [TestFixture()]
    public class DaoTest {
	[Generate()]
	[Test()]
	public void TestLoanPortfolioDAO() {
	    // get 100 random rows
	    IList list = LoanPortfolioDAO.DAO.GetList(new OrderByClause("LoanPortfolioId desc"), 100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (ILoanPortfolio entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.LoanPortfolioId.IsDefault);
		Assert.IsFalse(entity.Name.IsDefault);
		Assert.IsFalse(entity.UpDated.IsDefault);
		Assert.IsFalse(entity.AccountID.IsDefault);
		Assert.IsFalse(entity.Member.IsDefault);
		Assert.IsFalse(entity.MClass.IsDefault);
		Assert.IsFalse(entity.Ltype.IsDefault);
		Assert.IsFalse(entity.LtypeSub.IsDefault);
		Assert.IsFalse(entity.Product.IsDefault);
		Assert.IsFalse(entity.LoanDesc.IsDefault);
		Assert.IsFalse(entity.LoanDate.IsDefault);
		Assert.IsFalse(entity.LoanDay.IsDefault);
		Assert.IsFalse(entity.LoanMonth.IsDefault);
		Assert.IsFalse(entity.LoanYear.IsDefault);
		Assert.IsFalse(entity.FirstPmtDate.IsDefault);
		Assert.IsFalse(entity.Status.IsDefault);
		Assert.IsFalse(entity.RptCode.IsDefault);
		Assert.IsFalse(entity.FundedBr.IsDefault);
		Assert.IsFalse(entity.LoadedBr.IsDefault);
		Assert.IsFalse(entity.FICO.IsDefault);
		Assert.IsFalse(entity.FICORange.IsDefault);
		Assert.IsFalse(entity.FastStart.IsDefault);
		Assert.IsFalse(entity.FastStartRange.IsDefault);
		Assert.IsFalse(entity.Bankruptcy.IsDefault);
		Assert.IsFalse(entity.BankruptcyRange.IsDefault);
		Assert.IsFalse(entity.MembershipDate.IsDefault);
		Assert.IsFalse(entity.NewMember.IsDefault);
		Assert.IsFalse(entity.RiskLev.IsDefault);
		Assert.IsFalse(entity.Traffic.IsDefault);
		Assert.IsFalse(entity.TrafficColor.IsDefault);
		Assert.IsFalse(entity.MemAge.IsDefault);
		Assert.IsFalse(entity.MemSex.IsDefault);
		Assert.IsFalse(entity.PmtFreq.IsDefault);
		Assert.IsFalse(entity.OrigTerm.IsDefault);
		Assert.IsFalse(entity.OrigRate.IsDefault);
		Assert.IsFalse(entity.OrigPmt.IsDefault);
		Assert.IsFalse(entity.OrigAmount.IsDefault);
		Assert.IsFalse(entity.OrigCrLim.IsDefault);
		Assert.IsFalse(entity.CurTerm.IsDefault);
		Assert.IsFalse(entity.CurRate.IsDefault);
		Assert.IsFalse(entity.CurPmt.IsDefault);
		Assert.IsFalse(entity.CurBal.IsDefault);
		Assert.IsFalse(entity.CurCrLim.IsDefault);
		Assert.IsFalse(entity.BalloonDate.IsDefault);
		Assert.IsFalse(entity.MatDate.IsDefault);
		Assert.IsFalse(entity.InsCode.IsDefault);
		Assert.IsFalse(entity.CollateralValue.IsDefault);
		Assert.IsFalse(entity.LTV.IsDefault);
		Assert.IsFalse(entity.SecType.IsDefault);
		Assert.IsFalse(entity.SecDesc1.IsDefault);
		Assert.IsFalse(entity.SecDesc2.IsDefault);
		Assert.IsFalse(entity.DebtRatio.IsDefault);
		Assert.IsFalse(entity.GrossInc.IsDefault);
		Assert.IsFalse(entity.Dealer.IsDefault);
		Assert.IsFalse(entity.DealerName.IsDefault);
		Assert.IsFalse(entity.CoborComak.IsDefault);
		Assert.IsFalse(entity.RelPriceGrp.IsDefault);
		Assert.IsFalse(entity.RelPriceDisc.IsDefault);
		Assert.IsFalse(entity.OtherDisc.IsDefault);
		Assert.IsFalse(entity.DiscReason.IsDefault);
		Assert.IsFalse(entity.RiskOffset.IsDefault);
		Assert.IsFalse(entity.CreditType.IsDefault);
		Assert.IsFalse(entity.ApprOff.IsDefault);
		Assert.IsFalse(entity.StatedIncome.IsDefault);
		Assert.IsFalse(entity.Extensions.IsDefault);
		Assert.IsFalse(entity.FirstExtDate.IsDefault);
		Assert.IsFalse(entity.LastExtDate.IsDefault);
		Assert.IsFalse(entity.RiskLevel.IsDefault);
		Assert.IsFalse(entity.RiskDate.IsDefault);
		Assert.IsFalse(entity.Watch.IsDefault);
		Assert.IsFalse(entity.WatchDate.IsDefault);
		Assert.IsFalse(entity.WorkOut.IsDefault);
		Assert.IsFalse(entity.WorkOutDate.IsDefault);
		Assert.IsFalse(entity.FirstMortType.IsDefault);
		Assert.IsFalse(entity.BusPerReg.IsDefault);
		Assert.IsFalse(entity.BusGroup.IsDefault);
		Assert.IsFalse(entity.BusDesc.IsDefault);
		Assert.IsFalse(entity.BusPart.IsDefault);
		Assert.IsFalse(entity.BusPartPerc.IsDefault);
		Assert.IsFalse(entity.SBAguar.IsDefault);
		Assert.IsFalse(entity.DeliDays.IsDefault);
		Assert.IsFalse(entity.DeliSect.IsDefault);
		Assert.IsFalse(entity.DeliHist.IsDefault);
		Assert.IsFalse(entity.DeliHistQ1.IsDefault);
		Assert.IsFalse(entity.DeliHistQ2.IsDefault);
		Assert.IsFalse(entity.DeliHistQ3.IsDefault);
		Assert.IsFalse(entity.DeliHistQ4.IsDefault);
		Assert.IsFalse(entity.DeliHistQ5.IsDefault);
		Assert.IsFalse(entity.DeliHistQ6.IsDefault);
		Assert.IsFalse(entity.DeliHistQ7.IsDefault);
		Assert.IsFalse(entity.DeliHistQ8.IsDefault);
		Assert.IsFalse(entity.ChrgOffDate.IsDefault);
		Assert.IsFalse(entity.ChrgOffMonth.IsDefault);
		Assert.IsFalse(entity.ChrgOffYear.IsDefault);
		Assert.IsFalse(entity.ChrgOffMnthsBF.IsDefault);
		Assert.IsFalse(entity.PropType.IsDefault);
		Assert.IsFalse(entity.Occupancy.IsDefault);
		Assert.IsFalse(entity.OrigAppVal.IsDefault);
		Assert.IsFalse(entity.OrigAppDate.IsDefault);
		Assert.IsFalse(entity.CurrApprVal.IsDefault);
		Assert.IsFalse(entity.CurrApprDate.IsDefault);
		Assert.IsFalse(entity.FirstMortBal.IsDefault);
		Assert.IsFalse(entity.FirstMortMoPyt.IsDefault);
		Assert.IsFalse(entity.SecondMortBal.IsDefault);
		Assert.IsFalse(entity.SecondMortPymt.IsDefault);
		Assert.IsFalse(entity.TaxInsNotInc.IsDefault);
	    }
	}

	[Generate()]
	[Test()]
	public void TestFieldMapDAO() {
	    // get 100 random rows
	    IList list = FieldMapDAO.DAO.GetList(100);

	    // make sure that none of the rows have DEFAULT values
	    foreach (IFieldMap entity in list) {
		Assert.IsFalse(entity.IsNew);
		Assert.IsFalse(entity.FieldMapId.IsDefault);
		Assert.IsFalse(entity.SrcTable.IsDefault);
		Assert.IsFalse(entity.SrcColumn.IsDefault);
		Assert.IsFalse(entity.DestTable.IsDefault);
		Assert.IsFalse(entity.DestColumn.IsDefault);
	    }
	}
    }
}
