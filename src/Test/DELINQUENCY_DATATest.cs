using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for DELINQUENCY_DATATest.
    /// </summary>
    [TestFixture()]
    public class DELINQUENCY_DATATest : BaseTest {
	[Generate()]
	private void CheckPersistance(DELINQUENCY_DATA expected) {
	    DELINQUENCY_DATA actual = DELINQUENCY_DATA.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.DLQ_HIST as Object, actual.DLQ_HIST as Object, "DLQ_HIST");
	}
    }
}
