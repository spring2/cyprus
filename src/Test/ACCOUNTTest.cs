using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for ACCOUNTTest.
    /// </summary>
    [TestFixture()]
    public class ACCOUNTTest : BaseTest {
	[Generate()]
	private void CheckPersistance(ACCOUNT expected) {
	    ACCOUNT actual = ACCOUNT.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.MEM_NUM as Object, actual.MEM_NUM as Object, "MEM_NUM");
	    Assert.AreEqual(expected.L_ACCT_LEVEL as Object, actual.L_ACCT_LEVEL as Object, "L_ACCT_LEVEL");
	    Assert.AreEqual(expected.ACCT_SUBLEV as Object, actual.ACCT_SUBLEV as Object, "ACCT_SUBLEV");
	    Assert.AreEqual(expected.ACCOUNT_OPEN_DATE.ToString() as Object, actual.ACCOUNT_OPEN_DATE.ToString() as Object, "ACCOUNT_OPEN_DATE");
	    Assert.AreEqual(expected.SPEC_REPORT_CODE as Object, actual.SPEC_REPORT_CODE as Object, "SPEC_REPORT_CODE");
	    Assert.AreEqual(expected.ORIG_PAY as Object, actual.ORIG_PAY as Object, "ORIG_PAY");
	    Assert.AreEqual(expected.APR as Object, actual.APR as Object, "APR");
	    Assert.AreEqual(expected.CURR_CREDIT_LIM as Object, actual.CURR_CREDIT_LIM as Object, "CURR_CREDIT_LIM");
	    Assert.AreEqual(expected.PG_CODE as Object, actual.PG_CODE as Object, "PG_CODE");
	    Assert.AreEqual(expected.DISC_REASON as Object, actual.DISC_REASON as Object, "DISC_REASON");
	    Assert.AreEqual(expected.RISK_OFFSET_VAL as Object, actual.RISK_OFFSET_VAL as Object, "RISK_OFFSET_VAL");
	    Assert.AreEqual(expected.FIRST_PAYDATE.ToString() as Object, actual.FIRST_PAYDATE.ToString() as Object, "FIRST_PAYDATE");
	    Assert.AreEqual(expected.ORIG_LOAN_TERM as Object, actual.ORIG_LOAN_TERM as Object, "ORIG_LOAN_TERM");
	    Assert.AreEqual(expected.L_MATURITY_DATE.ToString() as Object, actual.L_MATURITY_DATE.ToString() as Object, "L_MATURITY_DATE");
	    Assert.AreEqual(expected.L_INSCODE as Object, actual.L_INSCODE as Object, "L_INSCODE");
	    Assert.AreEqual(expected.L_SECCODE as Object, actual.L_SECCODE as Object, "L_SECCODE");
	    Assert.AreEqual(expected.L_SECDESC1 as Object, actual.L_SECDESC1 as Object, "L_SECDESC1");
	    Assert.AreEqual(expected.L_SECDESC2 as Object, actual.L_SECDESC2 as Object, "L_SECDESC2");
	    Assert.AreEqual(expected.US_APPLIC_ID as Object, actual.US_APPLIC_ID as Object, "US_APPLIC_ID");
	    Assert.AreEqual(expected.L_ACCT_TYPE as Object, actual.L_ACCT_TYPE as Object, "L_ACCT_TYPE");
	}
    }
}
