using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for CLIENT_REL_DATATest.
    /// </summary>
    [TestFixture()]
    public class CLIENT_REL_DATATest : BaseTest {
	[Generate()]
	private void CheckPersistance(CLIENT_REL_DATA expected) {
	    CLIENT_REL_DATA actual = CLIENT_REL_DATA.GetInstance(expected.MEM_NUM);
	    Assert.AreEqual(expected.MEM_NUM as Object, actual.MEM_NUM as Object, "MEM_NUM");
	    Assert.AreEqual(expected.AGE as Object, actual.AGE as Object, "AGE");
	}
    }
}
