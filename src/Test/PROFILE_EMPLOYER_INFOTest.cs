using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for PROFILE_EMPLOYER_INFOTest.
    /// </summary>
    [TestFixture()]
    public class PROFILE_EMPLOYER_INFOTest : BaseTest {
	[Generate()]
	private void CheckPersistance(PROFILE_EMPLOYER_INFO expected) {
	    PROFILE_EMPLOYER_INFO actual = PROFILE_EMPLOYER_INFO.GetInstance(expected.RECORD_KEY);
	    Assert.AreEqual(expected.RECORD_KEY as Object, actual.RECORD_KEY as Object, "RECORD_KEY");
	    Assert.AreEqual(expected.EMP_GROSS_INCOME as Object, actual.EMP_GROSS_INCOME as Object, "EMP_GROSS_INCOME");
	    Assert.AreEqual(expected.EMP_YRS_PROF as Object, actual.EMP_YRS_PROF as Object, "EMP_YRS_PROF");
	}
    }
}
