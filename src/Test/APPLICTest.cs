using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for APPLICTest.
    /// </summary>
    [TestFixture()]
    public class APPLICTest : BaseTest {
	[Generate()]
	private void CheckPersistance(APPLIC expected) {
	    APPLIC actual = APPLIC.GetInstance(expected.APPLIC_ID);
	    Assert.AreEqual(expected.APPLIC_ID as Object, actual.APPLIC_ID as Object, "APPLIC_ID");
	    Assert.AreEqual(expected.APPLIC_NUM as Object, actual.APPLIC_NUM as Object, "APPLIC_NUM");
	    Assert.AreEqual(expected.FINAL_STAMP_BR as Object, actual.FINAL_STAMP_BR as Object, "FINAL_STAMP_BR");
	    Assert.AreEqual(expected.LOAD_STAMP_BR as Object, actual.LOAD_STAMP_BR as Object, "LOAD_STAMP_BR");
	    Assert.AreEqual(expected.STAGE2_CRSCORE as Object, actual.STAGE2_CRSCORE as Object, "STAGE2_CRSCORE");
	    Assert.AreEqual(expected.AP_RISK_SCORE as Object, actual.AP_RISK_SCORE as Object, "AP_RISK_SCORE");
	    Assert.AreEqual(expected.BANKRUPT_SCORE as Object, actual.BANKRUPT_SCORE as Object, "BANKRUPT_SCORE");
	    Assert.AreEqual(expected.AP_RISK_LEVEL as Object, actual.AP_RISK_LEVEL as Object, "AP_RISK_LEVEL");
	    Assert.AreEqual(expected.ALPS_COLOR as Object, actual.ALPS_COLOR as Object, "ALPS_COLOR");
	    Assert.AreEqual(expected.PAYMENT_FREQ as Object, actual.PAYMENT_FREQ as Object, "PAYMENT_FREQ");
	    Assert.AreEqual(expected.TOT_FIN as Object, actual.TOT_FIN as Object, "TOT_FIN");
	    Assert.AreEqual(expected.BALLOON_DATE.ToString() as Object, actual.BALLOON_DATE.ToString() as Object, "BALLOON_DATE");
	    Assert.AreEqual(expected.DEALER_NUMBER as Object, actual.DEALER_NUMBER as Object, "DEALER_NUMBER");
	    Assert.AreEqual(expected.CUDL_DEALER_NAME as Object, actual.CUDL_DEALER_NAME as Object, "CUDL_DEALER_NAME");
	    Assert.AreEqual(expected.AP_RP_DISCOUNT as Object, actual.AP_RP_DISCOUNT as Object, "AP_RP_DISCOUNT");
	    Assert.AreEqual(expected.CRSCORE_TYPE as Object, actual.CRSCORE_TYPE as Object, "CRSCORE_TYPE");
	    Assert.AreEqual(expected.HELOC_TYPE as Object, actual.HELOC_TYPE as Object, "HELOC_TYPE");
	    Assert.AreEqual(expected.HELOC_OCCUPANCY as Object, actual.HELOC_OCCUPANCY as Object, "HELOC_OCCUPANCY");
	    Assert.AreEqual(expected.HELOC_VALUE as Object, actual.HELOC_VALUE as Object, "HELOC_VALUE");
	    Assert.AreEqual(expected.HELOC_1STMORT_BAL as Object, actual.HELOC_1STMORT_BAL as Object, "HELOC_1STMORT_BAL");
	    Assert.AreEqual(expected.HELOC_1STMORT_PAY as Object, actual.HELOC_1STMORT_PAY as Object, "HELOC_1STMORT_PAY");
	    Assert.AreEqual(expected.HELOC_2NDMORT_BAL as Object, actual.HELOC_2NDMORT_BAL as Object, "HELOC_2NDMORT_BAL");
	    Assert.AreEqual(expected.HELOC_2NDMORT_PAY as Object, actual.HELOC_2NDMORT_PAY as Object, "HELOC_2NDMORT_PAY");
	    Assert.AreEqual(expected.HELOC_ORIG_DATE.ToString() as Object, actual.HELOC_ORIG_DATE.ToString() as Object, "HELOC_ORIG_DATE");
	    Assert.AreEqual(expected.INSURE_CODE as Object, actual.INSURE_CODE as Object, "INSURE_CODE");
	    Assert.AreEqual(expected.AP_INTEREST_RATE as Object, actual.AP_INTEREST_RATE as Object, "AP_INTEREST_RATE");
	}
    }
}
