using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for TRAN_DATATest.
    /// </summary>
    [TestFixture()]
    public class TRAN_DATATest : BaseTest {
	[Generate()]
	private void CheckPersistance(TRAN_DATA expected) {
	    TRAN_DATA actual = TRAN_DATA.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.CURRENT_BALANCE as Object, actual.CURRENT_BALANCE as Object, "CURRENT_BALANCE");
	    Assert.AreEqual(expected.INT_RATE as Object, actual.INT_RATE as Object, "INT_RATE");
	}
    }
}
