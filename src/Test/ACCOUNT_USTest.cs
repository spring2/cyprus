using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for ACCOUNT_USTest.
    /// </summary>
    [TestFixture()]
    public class ACCOUNT_USTest : BaseTest {
	[Generate()]
	private void CheckPersistance(ACCOUNT_US expected) {
	    ACCOUNT_US actual = ACCOUNT_US.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.US_LOANSTATUS as Object, actual.US_LOANSTATUS as Object, "US_LOANSTATUS");
	    Assert.AreEqual(expected.US_DELI_HIST as Object, actual.US_DELI_HIST as Object, "US_DELI_HIST");
	}
    }
}
