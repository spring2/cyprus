using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for CLIENTTest.
    /// </summary>
    [TestFixture()]
    public class CLIENTTest : BaseTest {
	[Generate()]
	private void CheckPersistance(CLIENT expected) {
	    CLIENT actual = CLIENT.GetInstance(expected.MEM_NUM);
	    Assert.AreEqual(expected.MEM_NUM as Object, actual.MEM_NUM as Object, "MEM_NUM");
	    Assert.AreEqual(expected.CLIENT_CLASS as Object, actual.CLIENT_CLASS as Object, "CLIENT_CLASS");
	    Assert.AreEqual(expected.JOIN_DATE.ToString() as Object, actual.JOIN_DATE.ToString() as Object, "JOIN_DATE");
	    Assert.AreEqual(expected.SEX as Object, actual.SEX as Object, "SEX");
	}
    }
}
