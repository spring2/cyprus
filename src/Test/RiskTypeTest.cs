using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for RiskTypeTest.
    /// </summary>
    [TestFixture()]
    public class RiskTypeTest : BaseTest {
	[Generate()]
	private void CheckPersistance(RiskType expected) {
	    RiskType actual = RiskType.GetInstance(expected.RiskTypeId);
	    Assert.AreEqual(expected.RiskTypeId as Object, actual.RiskTypeId as Object, "RiskTypeId");
	    Assert.AreEqual(expected.LType as Object, actual.LType as Object, "LType");
	    Assert.AreEqual(expected.RiskTypeDesc as Object, actual.RiskTypeDesc as Object, "RiskTypeDesc");
	}
    }
}
