using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for FICORangeTest.
    /// </summary>
    [TestFixture()]
    public class FICORangeTest : BaseTest {
	[Generate()]
	private void CheckPersistance(FICORange expected) {
	    FICORange actual = FICORange.GetInstance(expected.FICORangeId);
	    Assert.AreEqual(expected.FICORangeId as Object, actual.FICORangeId as Object, "FICORangeId");
	    Assert.AreEqual(expected.Value as Object, actual.Value as Object, "Value");
	    Assert.AreEqual(expected.Description as Object, actual.Description as Object, "Description");
	}
    }
}
