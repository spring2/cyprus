using System.Collections;
using System;

using NUnit.Framework;

using Spring2.Core.DAO;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataTierGenerator.Attribute;

using Spring2.BusinessLogic;
using Spring2.Dao;
using Spring2.DataObject;
using Spring2.Types;

namespace Spring2.Test {
    /// <summary>
    /// Summary description for TRAN_LOANTest.
    /// </summary>
    [TestFixture()]
    public class TRAN_LOANTest : BaseTest {
	[Generate()]
	private void CheckPersistance(TRAN_LOAN expected) {
	    TRAN_LOAN actual = TRAN_LOAN.GetInstance(expected.ACCT_NUM);
	    Assert.AreEqual(expected.ACCT_NUM as Object, actual.ACCT_NUM as Object, "ACCT_NUM");
	    Assert.AreEqual(expected.L_PREVIOUS_CREDIT_LIMIT as Object, actual.L_PREVIOUS_CREDIT_LIMIT as Object, "L_PREVIOUS_CREDIT_LIMIT");
	    Assert.AreEqual(expected.L_REPAY_AMT as Object, actual.L_REPAY_AMT as Object, "L_REPAY_AMT");
	}
    }
}
