IF OBJECT_ID('[dbo].[trLoanPortfolio_Update]','TR') IS NOT NULL
BEGIN
	DROP TRIGGER [dbo].[trLoanPortfolio_Update]
	PRINT 'Trigger Dropped: trLoanPortfolio_Update '
END
GO

-- Create Update Trigger [dbo].[trLoanPortfolio_Update] for Table [dbo].[LoanPortfolio]
Print 'Create Update Trigger [dbo].[trLoanPortfolio_Update] for Table [dbo].[LoanPortfolio]'
go
CREATE TRIGGER [dbo].[trLoanPortfolio_Update]
ON [dbo].[LoanPortfolio]
FOR UPDATE
NOT FOR REPLICATION
As

BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AuditLogTransactionId	Int,
		@PRIM_KEY				nvarchar(4000),
		@Inserted	    		bit,
		--@TableName			nvarchar(4000),
 		@ROWS_COUNT				int
 
	SET NOCOUNT ON

	--Set @TableName = '[dbo].[LoanPortfolio]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

-- CAS: get the userId from the inserted data
declare @userId int
select @userId=LastModifiedUserId from inserted

	INSERT
	INTO dbo.AuditLogTransaction 
	(
		TableName,
		TableSchema,
		AuditActionId,
		HostName,
		ApplicationName,
		AuditLogin,
		AuditDate,
		AffectedRows,
		DatabaseName,
		UserId
	)
	values(
		'LoanPortfolio',
		'dbo',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		db_name(),
		@userId
	)
	
	
	Set @AuditLogTransactionId = SCOPE_IDENTITY()
	
	
	SET @Inserted = 0


	If UPDATE(LoanPortfolio_Id)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoanPortfolio_Id',
			CONVERT(nvarchar(4000), OLD.LoanPortfolio_Id, 0),
			CONVERT(nvarchar(4000), NEW.LoanPortfolio_Id, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoanPortfolio_Id <>
					OLD.LoanPortfolio_Id
				) Or
			
				(
					NEW.LoanPortfolio_Id Is Null And
					OLD.LoanPortfolio_Id Is Not Null
				) Or
				(
					NEW.LoanPortfolio_Id Is Not Null And
					OLD.LoanPortfolio_Id Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(UpDated)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'UpDated',
			CONVERT(nvarchar(4000), OLD.UpDated, 121),
			CONVERT(nvarchar(4000), NEW.UpDated, 121),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.UpDated <>
					OLD.UpDated
				) Or
			
				(
					NEW.UpDated Is Null And
					OLD.UpDated Is Not Null
				) Or
				(
					NEW.UpDated Is Not Null And
					OLD.UpDated Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(EOMDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'EOMDate',
			CONVERT(nvarchar(4000), OLD.EOMDate, 0),
			CONVERT(nvarchar(4000), NEW.EOMDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.EOMDate <>
					OLD.EOMDate
				) Or
			
				(
					NEW.EOMDate Is Null And
					OLD.EOMDate Is Not Null
				) Or
				(
					NEW.EOMDate Is Not Null And
					OLD.EOMDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(AccountID)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'AccountID',
			CONVERT(nvarchar(4000), OLD.AccountID, 0),
			CONVERT(nvarchar(4000), NEW.AccountID, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.AccountID <>
					OLD.AccountID
				) Or
			
				(
					NEW.AccountID Is Null And
					OLD.AccountID Is Not Null
				) Or
				(
					NEW.AccountID Is Not Null And
					OLD.AccountID Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Member)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Member',
			CONVERT(nvarchar(4000), OLD.Member, 0),
			CONVERT(nvarchar(4000), NEW.Member, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Member <>
					OLD.Member
				) Or
			
				(
					NEW.Member Is Null And
					OLD.Member Is Not Null
				) Or
				(
					NEW.Member Is Not Null And
					OLD.Member Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MClass)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MClass',
			CONVERT(nvarchar(4000), OLD.MClass, 0),
			CONVERT(nvarchar(4000), NEW.MClass, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MClass <>
					OLD.MClass
				) Or
			
				(
					NEW.MClass Is Null And
					OLD.MClass Is Not Null
				) Or
				(
					NEW.MClass Is Not Null And
					OLD.MClass Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Ltype)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Ltype',
			CONVERT(nvarchar(4000), OLD.Ltype, 0),
			CONVERT(nvarchar(4000), NEW.Ltype, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Ltype <>
					OLD.Ltype
				) Or
			
				(
					NEW.Ltype Is Null And
					OLD.Ltype Is Not Null
				) Or
				(
					NEW.Ltype Is Not Null And
					OLD.Ltype Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LtypeSub)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LtypeSub',
			CONVERT(nvarchar(4000), OLD.LtypeSub, 0),
			CONVERT(nvarchar(4000), NEW.LtypeSub, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LtypeSub <>
					OLD.LtypeSub
				) Or
			
				(
					NEW.LtypeSub Is Null And
					OLD.LtypeSub Is Not Null
				) Or
				(
					NEW.LtypeSub Is Not Null And
					OLD.LtypeSub Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Product)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Product',
			CONVERT(nvarchar(4000), OLD.Product, 0),
			CONVERT(nvarchar(4000), NEW.Product, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Product <>
					OLD.Product
				) Or
			
				(
					NEW.Product Is Null And
					OLD.Product Is Not Null
				) Or
				(
					NEW.Product Is Not Null And
					OLD.Product Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoanDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoanDesc',
			CONVERT(nvarchar(4000), OLD.LoanDesc, 0),
			CONVERT(nvarchar(4000), NEW.LoanDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoanDesc <>
					OLD.LoanDesc
				) Or
			
				(
					NEW.LoanDesc Is Null And
					OLD.LoanDesc Is Not Null
				) Or
				(
					NEW.LoanDesc Is Not Null And
					OLD.LoanDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoanDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoanDate',
			CONVERT(nvarchar(4000), OLD.LoanDate, 0),
			CONVERT(nvarchar(4000), NEW.LoanDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoanDate <>
					OLD.LoanDate
				) Or
			
				(
					NEW.LoanDate Is Null And
					OLD.LoanDate Is Not Null
				) Or
				(
					NEW.LoanDate Is Not Null And
					OLD.LoanDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoanDay)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoanDay',
			CONVERT(nvarchar(4000), OLD.LoanDay, 0),
			CONVERT(nvarchar(4000), NEW.LoanDay, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoanDay <>
					OLD.LoanDay
				) Or
			
				(
					NEW.LoanDay Is Null And
					OLD.LoanDay Is Not Null
				) Or
				(
					NEW.LoanDay Is Not Null And
					OLD.LoanDay Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoanMonth)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoanMonth',
			CONVERT(nvarchar(4000), OLD.LoanMonth, 0),
			CONVERT(nvarchar(4000), NEW.LoanMonth, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoanMonth <>
					OLD.LoanMonth
				) Or
			
				(
					NEW.LoanMonth Is Null And
					OLD.LoanMonth Is Not Null
				) Or
				(
					NEW.LoanMonth Is Not Null And
					OLD.LoanMonth Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoanYear)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoanYear',
			CONVERT(nvarchar(4000), OLD.LoanYear, 0),
			CONVERT(nvarchar(4000), NEW.LoanYear, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoanYear <>
					OLD.LoanYear
				) Or
			
				(
					NEW.LoanYear Is Null And
					OLD.LoanYear Is Not Null
				) Or
				(
					NEW.LoanYear Is Not Null And
					OLD.LoanYear Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FirstPmtDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FirstPmtDate',
			CONVERT(nvarchar(4000), OLD.FirstPmtDate, 0),
			CONVERT(nvarchar(4000), NEW.FirstPmtDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FirstPmtDate <>
					OLD.FirstPmtDate
				) Or
			
				(
					NEW.FirstPmtDate Is Null And
					OLD.FirstPmtDate Is Not Null
				) Or
				(
					NEW.FirstPmtDate Is Not Null And
					OLD.FirstPmtDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Status)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Status',
			CONVERT(nvarchar(4000), OLD.Status, 0),
			CONVERT(nvarchar(4000), NEW.Status, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Status <>
					OLD.Status
				) Or
			
				(
					NEW.Status Is Null And
					OLD.Status Is Not Null
				) Or
				(
					NEW.Status Is Not Null And
					OLD.Status Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RptCode)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RptCode',
			CONVERT(nvarchar(4000), OLD.RptCode, 0),
			CONVERT(nvarchar(4000), NEW.RptCode, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RptCode <>
					OLD.RptCode
				) Or
			
				(
					NEW.RptCode Is Null And
					OLD.RptCode Is Not Null
				) Or
				(
					NEW.RptCode Is Not Null And
					OLD.RptCode Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FundedBr)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FundedBr',
			CONVERT(nvarchar(4000), OLD.FundedBr, 0),
			CONVERT(nvarchar(4000), NEW.FundedBr, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FundedBr <>
					OLD.FundedBr
				) Or
			
				(
					NEW.FundedBr Is Null And
					OLD.FundedBr Is Not Null
				) Or
				(
					NEW.FundedBr Is Not Null And
					OLD.FundedBr Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoadedBr)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoadedBr',
			CONVERT(nvarchar(4000), OLD.LoadedBr, 0),
			CONVERT(nvarchar(4000), NEW.LoadedBr, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoadedBr <>
					OLD.LoadedBr
				) Or
			
				(
					NEW.LoadedBr Is Null And
					OLD.LoadedBr Is Not Null
				) Or
				(
					NEW.LoadedBr Is Not Null And
					OLD.LoadedBr Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FICO)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FICO',
			CONVERT(nvarchar(4000), OLD.FICO, 0),
			CONVERT(nvarchar(4000), NEW.FICO, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FICO <>
					OLD.FICO
				) Or
			
				(
					NEW.FICO Is Null And
					OLD.FICO Is Not Null
				) Or
				(
					NEW.FICO Is Not Null And
					OLD.FICO Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FICORange)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FICORange',
			CONVERT(nvarchar(4000), OLD.FICORange, 0),
			CONVERT(nvarchar(4000), NEW.FICORange, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FICORange <>
					OLD.FICORange
				) Or
			
				(
					NEW.FICORange Is Null And
					OLD.FICORange Is Not Null
				) Or
				(
					NEW.FICORange Is Not Null And
					OLD.FICORange Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FastStart)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FastStart',
			CONVERT(nvarchar(4000), OLD.FastStart, 0),
			CONVERT(nvarchar(4000), NEW.FastStart, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FastStart <>
					OLD.FastStart
				) Or
			
				(
					NEW.FastStart Is Null And
					OLD.FastStart Is Not Null
				) Or
				(
					NEW.FastStart Is Not Null And
					OLD.FastStart Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FastStartRange)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FastStartRange',
			CONVERT(nvarchar(4000), OLD.FastStartRange, 0),
			CONVERT(nvarchar(4000), NEW.FastStartRange, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FastStartRange <>
					OLD.FastStartRange
				) Or
			
				(
					NEW.FastStartRange Is Null And
					OLD.FastStartRange Is Not Null
				) Or
				(
					NEW.FastStartRange Is Not Null And
					OLD.FastStartRange Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Bankruptcy)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Bankruptcy',
			CONVERT(nvarchar(4000), OLD.Bankruptcy, 0),
			CONVERT(nvarchar(4000), NEW.Bankruptcy, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Bankruptcy <>
					OLD.Bankruptcy
				) Or
			
				(
					NEW.Bankruptcy Is Null And
					OLD.Bankruptcy Is Not Null
				) Or
				(
					NEW.Bankruptcy Is Not Null And
					OLD.Bankruptcy Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BankruptcyRange)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BankruptcyRange',
			CONVERT(nvarchar(4000), OLD.BankruptcyRange, 0),
			CONVERT(nvarchar(4000), NEW.BankruptcyRange, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BankruptcyRange <>
					OLD.BankruptcyRange
				) Or
			
				(
					NEW.BankruptcyRange Is Null And
					OLD.BankruptcyRange Is Not Null
				) Or
				(
					NEW.BankruptcyRange Is Not Null And
					OLD.BankruptcyRange Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MembershipDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MembershipDate',
			CONVERT(nvarchar(4000), OLD.MembershipDate, 0),
			CONVERT(nvarchar(4000), NEW.MembershipDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MembershipDate <>
					OLD.MembershipDate
				) Or
			
				(
					NEW.MembershipDate Is Null And
					OLD.MembershipDate Is Not Null
				) Or
				(
					NEW.MembershipDate Is Not Null And
					OLD.MembershipDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(NewMember)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'NewMember',
			CONVERT(nvarchar(4000), OLD.NewMember, 0),
			CONVERT(nvarchar(4000), NEW.NewMember, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.NewMember <>
					OLD.NewMember
				) Or
			
				(
					NEW.NewMember Is Null And
					OLD.NewMember Is Not Null
				) Or
				(
					NEW.NewMember Is Not Null And
					OLD.NewMember Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RiskLev)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RiskLev',
			CONVERT(nvarchar(4000), OLD.RiskLev, 0),
			CONVERT(nvarchar(4000), NEW.RiskLev, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RiskLev <>
					OLD.RiskLev
				) Or
			
				(
					NEW.RiskLev Is Null And
					OLD.RiskLev Is Not Null
				) Or
				(
					NEW.RiskLev Is Not Null And
					OLD.RiskLev Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Traffic)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Traffic',
			CONVERT(nvarchar(4000), OLD.Traffic, 0),
			CONVERT(nvarchar(4000), NEW.Traffic, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Traffic <>
					OLD.Traffic
				) Or
			
				(
					NEW.Traffic Is Null And
					OLD.Traffic Is Not Null
				) Or
				(
					NEW.Traffic Is Not Null And
					OLD.Traffic Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(TrafficColor)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'TrafficColor',
			CONVERT(nvarchar(4000), OLD.TrafficColor, 0),
			CONVERT(nvarchar(4000), NEW.TrafficColor, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.TrafficColor <>
					OLD.TrafficColor
				) Or
			
				(
					NEW.TrafficColor Is Null And
					OLD.TrafficColor Is Not Null
				) Or
				(
					NEW.TrafficColor Is Not Null And
					OLD.TrafficColor Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MemAge)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MemAge',
			CONVERT(nvarchar(4000), OLD.MemAge, 0),
			CONVERT(nvarchar(4000), NEW.MemAge, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MemAge <>
					OLD.MemAge
				) Or
			
				(
					NEW.MemAge Is Null And
					OLD.MemAge Is Not Null
				) Or
				(
					NEW.MemAge Is Not Null And
					OLD.MemAge Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MemSex)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MemSex',
			CONVERT(nvarchar(4000), OLD.MemSex, 0),
			CONVERT(nvarchar(4000), NEW.MemSex, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MemSex <>
					OLD.MemSex
				) Or
			
				(
					NEW.MemSex Is Null And
					OLD.MemSex Is Not Null
				) Or
				(
					NEW.MemSex Is Not Null And
					OLD.MemSex Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(PmtFreq)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'PmtFreq',
			CONVERT(nvarchar(4000), OLD.PmtFreq, 0),
			CONVERT(nvarchar(4000), NEW.PmtFreq, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.PmtFreq <>
					OLD.PmtFreq
				) Or
			
				(
					NEW.PmtFreq Is Null And
					OLD.PmtFreq Is Not Null
				) Or
				(
					NEW.PmtFreq Is Not Null And
					OLD.PmtFreq Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigTerm)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigTerm',
			CONVERT(nvarchar(4000), OLD.OrigTerm, 0),
			CONVERT(nvarchar(4000), NEW.OrigTerm, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigTerm <>
					OLD.OrigTerm
				) Or
			
				(
					NEW.OrigTerm Is Null And
					OLD.OrigTerm Is Not Null
				) Or
				(
					NEW.OrigTerm Is Not Null And
					OLD.OrigTerm Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigRate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigRate',
			CONVERT(nvarchar(4000), OLD.OrigRate, 0),
			CONVERT(nvarchar(4000), NEW.OrigRate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigRate <>
					OLD.OrigRate
				) Or
			
				(
					NEW.OrigRate Is Null And
					OLD.OrigRate Is Not Null
				) Or
				(
					NEW.OrigRate Is Not Null And
					OLD.OrigRate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigPmt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigPmt',
			CONVERT(nvarchar(4000), OLD.OrigPmt, 0),
			CONVERT(nvarchar(4000), NEW.OrigPmt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigPmt <>
					OLD.OrigPmt
				) Or
			
				(
					NEW.OrigPmt Is Null And
					OLD.OrigPmt Is Not Null
				) Or
				(
					NEW.OrigPmt Is Not Null And
					OLD.OrigPmt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigAmount)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigAmount',
			CONVERT(nvarchar(4000), OLD.OrigAmount, 0),
			CONVERT(nvarchar(4000), NEW.OrigAmount, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigAmount <>
					OLD.OrigAmount
				) Or
			
				(
					NEW.OrigAmount Is Null And
					OLD.OrigAmount Is Not Null
				) Or
				(
					NEW.OrigAmount Is Not Null And
					OLD.OrigAmount Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigCrLim)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigCrLim',
			CONVERT(nvarchar(4000), OLD.OrigCrLim, 0),
			CONVERT(nvarchar(4000), NEW.OrigCrLim, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigCrLim <>
					OLD.OrigCrLim
				) Or
			
				(
					NEW.OrigCrLim Is Null And
					OLD.OrigCrLim Is Not Null
				) Or
				(
					NEW.OrigCrLim Is Not Null And
					OLD.OrigCrLim Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurTerm)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurTerm',
			CONVERT(nvarchar(4000), OLD.CurTerm, 0),
			CONVERT(nvarchar(4000), NEW.CurTerm, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurTerm <>
					OLD.CurTerm
				) Or
			
				(
					NEW.CurTerm Is Null And
					OLD.CurTerm Is Not Null
				) Or
				(
					NEW.CurTerm Is Not Null And
					OLD.CurTerm Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurRate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurRate',
			CONVERT(nvarchar(4000), OLD.CurRate, 0),
			CONVERT(nvarchar(4000), NEW.CurRate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurRate <>
					OLD.CurRate
				) Or
			
				(
					NEW.CurRate Is Null And
					OLD.CurRate Is Not Null
				) Or
				(
					NEW.CurRate Is Not Null And
					OLD.CurRate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurPmt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurPmt',
			CONVERT(nvarchar(4000), OLD.CurPmt, 0),
			CONVERT(nvarchar(4000), NEW.CurPmt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurPmt <>
					OLD.CurPmt
				) Or
			
				(
					NEW.CurPmt Is Null And
					OLD.CurPmt Is Not Null
				) Or
				(
					NEW.CurPmt Is Not Null And
					OLD.CurPmt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurBal)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurBal',
			CONVERT(nvarchar(4000), OLD.CurBal, 0),
			CONVERT(nvarchar(4000), NEW.CurBal, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurBal <>
					OLD.CurBal
				) Or
			
				(
					NEW.CurBal Is Null And
					OLD.CurBal Is Not Null
				) Or
				(
					NEW.CurBal Is Not Null And
					OLD.CurBal Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurCrLim)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurCrLim',
			CONVERT(nvarchar(4000), OLD.CurCrLim, 0),
			CONVERT(nvarchar(4000), NEW.CurCrLim, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurCrLim <>
					OLD.CurCrLim
				) Or
			
				(
					NEW.CurCrLim Is Null And
					OLD.CurCrLim Is Not Null
				) Or
				(
					NEW.CurCrLim Is Not Null And
					OLD.CurCrLim Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BalloonDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BalloonDate',
			CONVERT(nvarchar(4000), OLD.BalloonDate, 0),
			CONVERT(nvarchar(4000), NEW.BalloonDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BalloonDate <>
					OLD.BalloonDate
				) Or
			
				(
					NEW.BalloonDate Is Null And
					OLD.BalloonDate Is Not Null
				) Or
				(
					NEW.BalloonDate Is Not Null And
					OLD.BalloonDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MatDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MatDate',
			CONVERT(nvarchar(4000), OLD.MatDate, 0),
			CONVERT(nvarchar(4000), NEW.MatDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MatDate <>
					OLD.MatDate
				) Or
			
				(
					NEW.MatDate Is Null And
					OLD.MatDate Is Not Null
				) Or
				(
					NEW.MatDate Is Not Null And
					OLD.MatDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InsCode)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InsCode',
			CONVERT(nvarchar(4000), OLD.InsCode, 0),
			CONVERT(nvarchar(4000), NEW.InsCode, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InsCode <>
					OLD.InsCode
				) Or
			
				(
					NEW.InsCode Is Null And
					OLD.InsCode Is Not Null
				) Or
				(
					NEW.InsCode Is Not Null And
					OLD.InsCode Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CollateralValue)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CollateralValue',
			CONVERT(nvarchar(4000), OLD.CollateralValue, 0),
			CONVERT(nvarchar(4000), NEW.CollateralValue, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CollateralValue <>
					OLD.CollateralValue
				) Or
			
				(
					NEW.CollateralValue Is Null And
					OLD.CollateralValue Is Not Null
				) Or
				(
					NEW.CollateralValue Is Not Null And
					OLD.CollateralValue Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LTV)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LTV',
			CONVERT(nvarchar(4000), OLD.LTV, 0),
			CONVERT(nvarchar(4000), NEW.LTV, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LTV <>
					OLD.LTV
				) Or
			
				(
					NEW.LTV Is Null And
					OLD.LTV Is Not Null
				) Or
				(
					NEW.LTV Is Not Null And
					OLD.LTV Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SecType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SecType',
			CONVERT(nvarchar(4000), OLD.SecType, 0),
			CONVERT(nvarchar(4000), NEW.SecType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SecType <>
					OLD.SecType
				) Or
			
				(
					NEW.SecType Is Null And
					OLD.SecType Is Not Null
				) Or
				(
					NEW.SecType Is Not Null And
					OLD.SecType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SecDesc1)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SecDesc1',
			CONVERT(nvarchar(4000), OLD.SecDesc1, 0),
			CONVERT(nvarchar(4000), NEW.SecDesc1, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SecDesc1 <>
					OLD.SecDesc1
				) Or
			
				(
					NEW.SecDesc1 Is Null And
					OLD.SecDesc1 Is Not Null
				) Or
				(
					NEW.SecDesc1 Is Not Null And
					OLD.SecDesc1 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SecDesc2)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SecDesc2',
			CONVERT(nvarchar(4000), OLD.SecDesc2, 0),
			CONVERT(nvarchar(4000), NEW.SecDesc2, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SecDesc2 <>
					OLD.SecDesc2
				) Or
			
				(
					NEW.SecDesc2 Is Null And
					OLD.SecDesc2 Is Not Null
				) Or
				(
					NEW.SecDesc2 Is Not Null And
					OLD.SecDesc2 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DebtRatio)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DebtRatio',
			CONVERT(nvarchar(4000), OLD.DebtRatio, 0),
			CONVERT(nvarchar(4000), NEW.DebtRatio, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DebtRatio <>
					OLD.DebtRatio
				) Or
			
				(
					NEW.DebtRatio Is Null And
					OLD.DebtRatio Is Not Null
				) Or
				(
					NEW.DebtRatio Is Not Null And
					OLD.DebtRatio Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Dealer)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Dealer',
			CONVERT(nvarchar(4000), OLD.Dealer, 0),
			CONVERT(nvarchar(4000), NEW.Dealer, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Dealer <>
					OLD.Dealer
				) Or
			
				(
					NEW.Dealer Is Null And
					OLD.Dealer Is Not Null
				) Or
				(
					NEW.Dealer Is Not Null And
					OLD.Dealer Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DealerName)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DealerName',
			CONVERT(nvarchar(4000), OLD.DealerName, 0),
			CONVERT(nvarchar(4000), NEW.DealerName, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DealerName <>
					OLD.DealerName
				) Or
			
				(
					NEW.DealerName Is Null And
					OLD.DealerName Is Not Null
				) Or
				(
					NEW.DealerName Is Not Null And
					OLD.DealerName Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CoborComak)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CoborComak',
			CONVERT(nvarchar(4000), OLD.CoborComak, 0),
			CONVERT(nvarchar(4000), NEW.CoborComak, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CoborComak <>
					OLD.CoborComak
				) Or
			
				(
					NEW.CoborComak Is Null And
					OLD.CoborComak Is Not Null
				) Or
				(
					NEW.CoborComak Is Not Null And
					OLD.CoborComak Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RelPriceGrp)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RelPriceGrp',
			CONVERT(nvarchar(4000), OLD.RelPriceGrp, 0),
			CONVERT(nvarchar(4000), NEW.RelPriceGrp, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RelPriceGrp <>
					OLD.RelPriceGrp
				) Or
			
				(
					NEW.RelPriceGrp Is Null And
					OLD.RelPriceGrp Is Not Null
				) Or
				(
					NEW.RelPriceGrp Is Not Null And
					OLD.RelPriceGrp Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RelPriceDisc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RelPriceDisc',
			CONVERT(nvarchar(4000), OLD.RelPriceDisc, 0),
			CONVERT(nvarchar(4000), NEW.RelPriceDisc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RelPriceDisc <>
					OLD.RelPriceDisc
				) Or
			
				(
					NEW.RelPriceDisc Is Null And
					OLD.RelPriceDisc Is Not Null
				) Or
				(
					NEW.RelPriceDisc Is Not Null And
					OLD.RelPriceDisc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OtherDisc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OtherDisc',
			CONVERT(nvarchar(4000), OLD.OtherDisc, 0),
			CONVERT(nvarchar(4000), NEW.OtherDisc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OtherDisc <>
					OLD.OtherDisc
				) Or
			
				(
					NEW.OtherDisc Is Null And
					OLD.OtherDisc Is Not Null
				) Or
				(
					NEW.OtherDisc Is Not Null And
					OLD.OtherDisc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DiscReason)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DiscReason',
			CONVERT(nvarchar(4000), OLD.DiscReason, 0),
			CONVERT(nvarchar(4000), NEW.DiscReason, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DiscReason <>
					OLD.DiscReason
				) Or
			
				(
					NEW.DiscReason Is Null And
					OLD.DiscReason Is Not Null
				) Or
				(
					NEW.DiscReason Is Not Null And
					OLD.DiscReason Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RiskOffset)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RiskOffset',
			CONVERT(nvarchar(4000), OLD.RiskOffset, 0),
			CONVERT(nvarchar(4000), NEW.RiskOffset, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RiskOffset <>
					OLD.RiskOffset
				) Or
			
				(
					NEW.RiskOffset Is Null And
					OLD.RiskOffset Is Not Null
				) Or
				(
					NEW.RiskOffset Is Not Null And
					OLD.RiskOffset Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CreditType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CreditType',
			CONVERT(nvarchar(4000), OLD.CreditType, 0),
			CONVERT(nvarchar(4000), NEW.CreditType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CreditType <>
					OLD.CreditType
				) Or
			
				(
					NEW.CreditType Is Null And
					OLD.CreditType Is Not Null
				) Or
				(
					NEW.CreditType Is Not Null And
					OLD.CreditType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(StatedIncome)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'StatedIncome',
			CONVERT(nvarchar(4000), OLD.StatedIncome, 0),
			CONVERT(nvarchar(4000), NEW.StatedIncome, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.StatedIncome <>
					OLD.StatedIncome
				) Or
			
				(
					NEW.StatedIncome Is Null And
					OLD.StatedIncome Is Not Null
				) Or
				(
					NEW.StatedIncome Is Not Null And
					OLD.StatedIncome Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Extensions)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Extensions',
			CONVERT(nvarchar(4000), OLD.Extensions, 0),
			CONVERT(nvarchar(4000), NEW.Extensions, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Extensions <>
					OLD.Extensions
				) Or
			
				(
					NEW.Extensions Is Null And
					OLD.Extensions Is Not Null
				) Or
				(
					NEW.Extensions Is Not Null And
					OLD.Extensions Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FirstExtDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FirstExtDate',
			CONVERT(nvarchar(4000), OLD.FirstExtDate, 0),
			CONVERT(nvarchar(4000), NEW.FirstExtDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FirstExtDate <>
					OLD.FirstExtDate
				) Or
			
				(
					NEW.FirstExtDate Is Null And
					OLD.FirstExtDate Is Not Null
				) Or
				(
					NEW.FirstExtDate Is Not Null And
					OLD.FirstExtDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LastExtDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LastExtDate',
			CONVERT(nvarchar(4000), OLD.LastExtDate, 0),
			CONVERT(nvarchar(4000), NEW.LastExtDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LastExtDate <>
					OLD.LastExtDate
				) Or
			
				(
					NEW.LastExtDate Is Null And
					OLD.LastExtDate Is Not Null
				) Or
				(
					NEW.LastExtDate Is Not Null And
					OLD.LastExtDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RiskLevel)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RiskLevel',
			CONVERT(nvarchar(4000), OLD.RiskLevel, 0),
			CONVERT(nvarchar(4000), NEW.RiskLevel, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RiskLevel <>
					OLD.RiskLevel
				) Or
			
				(
					NEW.RiskLevel Is Null And
					OLD.RiskLevel Is Not Null
				) Or
				(
					NEW.RiskLevel Is Not Null And
					OLD.RiskLevel Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RiskDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RiskDate',
			CONVERT(nvarchar(4000), OLD.RiskDate, 0),
			CONVERT(nvarchar(4000), NEW.RiskDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RiskDate <>
					OLD.RiskDate
				) Or
			
				(
					NEW.RiskDate Is Null And
					OLD.RiskDate Is Not Null
				) Or
				(
					NEW.RiskDate Is Not Null And
					OLD.RiskDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Watch)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Watch',
			CONVERT(nvarchar(4000), OLD.Watch, 0),
			CONVERT(nvarchar(4000), NEW.Watch, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Watch <>
					OLD.Watch
				) Or
			
				(
					NEW.Watch Is Null And
					OLD.Watch Is Not Null
				) Or
				(
					NEW.Watch Is Not Null And
					OLD.Watch Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(WatchDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'WatchDate',
			CONVERT(nvarchar(4000), OLD.WatchDate, 0),
			CONVERT(nvarchar(4000), NEW.WatchDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.WatchDate <>
					OLD.WatchDate
				) Or
			
				(
					NEW.WatchDate Is Null And
					OLD.WatchDate Is Not Null
				) Or
				(
					NEW.WatchDate Is Not Null And
					OLD.WatchDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(WorkOut)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'WorkOut',
			CONVERT(nvarchar(4000), OLD.WorkOut, 0),
			CONVERT(nvarchar(4000), NEW.WorkOut, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.WorkOut <>
					OLD.WorkOut
				) Or
			
				(
					NEW.WorkOut Is Null And
					OLD.WorkOut Is Not Null
				) Or
				(
					NEW.WorkOut Is Not Null And
					OLD.WorkOut Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(WorkOutDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'WorkOutDate',
			CONVERT(nvarchar(4000), OLD.WorkOutDate, 0),
			CONVERT(nvarchar(4000), NEW.WorkOutDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.WorkOutDate <>
					OLD.WorkOutDate
				) Or
			
				(
					NEW.WorkOutDate Is Null And
					OLD.WorkOutDate Is Not Null
				) Or
				(
					NEW.WorkOutDate Is Not Null And
					OLD.WorkOutDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FirstMortType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FirstMortType',
			CONVERT(nvarchar(4000), OLD.FirstMortType, 0),
			CONVERT(nvarchar(4000), NEW.FirstMortType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FirstMortType <>
					OLD.FirstMortType
				) Or
			
				(
					NEW.FirstMortType Is Null And
					OLD.FirstMortType Is Not Null
				) Or
				(
					NEW.FirstMortType Is Not Null And
					OLD.FirstMortType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BusPerReg)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BusPerReg',
			CONVERT(nvarchar(4000), OLD.BusPerReg, 0),
			CONVERT(nvarchar(4000), NEW.BusPerReg, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BusPerReg <>
					OLD.BusPerReg
				) Or
			
				(
					NEW.BusPerReg Is Null And
					OLD.BusPerReg Is Not Null
				) Or
				(
					NEW.BusPerReg Is Not Null And
					OLD.BusPerReg Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BusGroup)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BusGroup',
			CONVERT(nvarchar(4000), OLD.BusGroup, 0),
			CONVERT(nvarchar(4000), NEW.BusGroup, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BusGroup <>
					OLD.BusGroup
				) Or
			
				(
					NEW.BusGroup Is Null And
					OLD.BusGroup Is Not Null
				) Or
				(
					NEW.BusGroup Is Not Null And
					OLD.BusGroup Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BusDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BusDesc',
			CONVERT(nvarchar(4000), OLD.BusDesc, 0),
			CONVERT(nvarchar(4000), NEW.BusDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BusDesc <>
					OLD.BusDesc
				) Or
			
				(
					NEW.BusDesc Is Null And
					OLD.BusDesc Is Not Null
				) Or
				(
					NEW.BusDesc Is Not Null And
					OLD.BusDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BusPart)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BusPart',
			CONVERT(nvarchar(4000), OLD.BusPart, 0),
			CONVERT(nvarchar(4000), NEW.BusPart, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BusPart <>
					OLD.BusPart
				) Or
			
				(
					NEW.BusPart Is Null And
					OLD.BusPart Is Not Null
				) Or
				(
					NEW.BusPart Is Not Null And
					OLD.BusPart Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BusPartPerc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BusPartPerc',
			CONVERT(nvarchar(4000), OLD.BusPartPerc, 0),
			CONVERT(nvarchar(4000), NEW.BusPartPerc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BusPartPerc <>
					OLD.BusPartPerc
				) Or
			
				(
					NEW.BusPartPerc Is Null And
					OLD.BusPartPerc Is Not Null
				) Or
				(
					NEW.BusPartPerc Is Not Null And
					OLD.BusPartPerc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SBAguar)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SBAguar',
			CONVERT(nvarchar(4000), OLD.SBAguar, 0),
			CONVERT(nvarchar(4000), NEW.SBAguar, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SBAguar <>
					OLD.SBAguar
				) Or
			
				(
					NEW.SBAguar Is Null And
					OLD.SBAguar Is Not Null
				) Or
				(
					NEW.SBAguar Is Not Null And
					OLD.SBAguar Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliDays)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliDays',
			CONVERT(nvarchar(4000), OLD.DeliDays, 0),
			CONVERT(nvarchar(4000), NEW.DeliDays, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliDays <>
					OLD.DeliDays
				) Or
			
				(
					NEW.DeliDays Is Null And
					OLD.DeliDays Is Not Null
				) Or
				(
					NEW.DeliDays Is Not Null And
					OLD.DeliDays Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliSect)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliSect',
			CONVERT(nvarchar(4000), OLD.DeliSect, 0),
			CONVERT(nvarchar(4000), NEW.DeliSect, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliSect <>
					OLD.DeliSect
				) Or
			
				(
					NEW.DeliSect Is Null And
					OLD.DeliSect Is Not Null
				) Or
				(
					NEW.DeliSect Is Not Null And
					OLD.DeliSect Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHist)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHist',
			CONVERT(nvarchar(4000), OLD.DeliHist, 0),
			CONVERT(nvarchar(4000), NEW.DeliHist, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHist <>
					OLD.DeliHist
				) Or
			
				(
					NEW.DeliHist Is Null And
					OLD.DeliHist Is Not Null
				) Or
				(
					NEW.DeliHist Is Not Null And
					OLD.DeliHist Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ1)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ1',
			CONVERT(nvarchar(4000), OLD.DeliHistQ1, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ1, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ1 <>
					OLD.DeliHistQ1
				) Or
			
				(
					NEW.DeliHistQ1 Is Null And
					OLD.DeliHistQ1 Is Not Null
				) Or
				(
					NEW.DeliHistQ1 Is Not Null And
					OLD.DeliHistQ1 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ2)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ2',
			CONVERT(nvarchar(4000), OLD.DeliHistQ2, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ2, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ2 <>
					OLD.DeliHistQ2
				) Or
			
				(
					NEW.DeliHistQ2 Is Null And
					OLD.DeliHistQ2 Is Not Null
				) Or
				(
					NEW.DeliHistQ2 Is Not Null And
					OLD.DeliHistQ2 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ3)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ3',
			CONVERT(nvarchar(4000), OLD.DeliHistQ3, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ3, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ3 <>
					OLD.DeliHistQ3
				) Or
			
				(
					NEW.DeliHistQ3 Is Null And
					OLD.DeliHistQ3 Is Not Null
				) Or
				(
					NEW.DeliHistQ3 Is Not Null And
					OLD.DeliHistQ3 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ4)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ4',
			CONVERT(nvarchar(4000), OLD.DeliHistQ4, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ4, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ4 <>
					OLD.DeliHistQ4
				) Or
			
				(
					NEW.DeliHistQ4 Is Null And
					OLD.DeliHistQ4 Is Not Null
				) Or
				(
					NEW.DeliHistQ4 Is Not Null And
					OLD.DeliHistQ4 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ5)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ5',
			CONVERT(nvarchar(4000), OLD.DeliHistQ5, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ5, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ5 <>
					OLD.DeliHistQ5
				) Or
			
				(
					NEW.DeliHistQ5 Is Null And
					OLD.DeliHistQ5 Is Not Null
				) Or
				(
					NEW.DeliHistQ5 Is Not Null And
					OLD.DeliHistQ5 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ6)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ6',
			CONVERT(nvarchar(4000), OLD.DeliHistQ6, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ6, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ6 <>
					OLD.DeliHistQ6
				) Or
			
				(
					NEW.DeliHistQ6 Is Null And
					OLD.DeliHistQ6 Is Not Null
				) Or
				(
					NEW.DeliHistQ6 Is Not Null And
					OLD.DeliHistQ6 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ7)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ7',
			CONVERT(nvarchar(4000), OLD.DeliHistQ7, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ7, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ7 <>
					OLD.DeliHistQ7
				) Or
			
				(
					NEW.DeliHistQ7 Is Null And
					OLD.DeliHistQ7 Is Not Null
				) Or
				(
					NEW.DeliHistQ7 Is Not Null And
					OLD.DeliHistQ7 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliHistQ8)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliHistQ8',
			CONVERT(nvarchar(4000), OLD.DeliHistQ8, 0),
			CONVERT(nvarchar(4000), NEW.DeliHistQ8, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliHistQ8 <>
					OLD.DeliHistQ8
				) Or
			
				(
					NEW.DeliHistQ8 Is Null And
					OLD.DeliHistQ8 Is Not Null
				) Or
				(
					NEW.DeliHistQ8 Is Not Null And
					OLD.DeliHistQ8 Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ChrgOffDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ChrgOffDate',
			CONVERT(nvarchar(4000), OLD.ChrgOffDate, 0),
			CONVERT(nvarchar(4000), NEW.ChrgOffDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ChrgOffDate <>
					OLD.ChrgOffDate
				) Or
			
				(
					NEW.ChrgOffDate Is Null And
					OLD.ChrgOffDate Is Not Null
				) Or
				(
					NEW.ChrgOffDate Is Not Null And
					OLD.ChrgOffDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ChrgOffMonth)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ChrgOffMonth',
			CONVERT(nvarchar(4000), OLD.ChrgOffMonth, 0),
			CONVERT(nvarchar(4000), NEW.ChrgOffMonth, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ChrgOffMonth <>
					OLD.ChrgOffMonth
				) Or
			
				(
					NEW.ChrgOffMonth Is Null And
					OLD.ChrgOffMonth Is Not Null
				) Or
				(
					NEW.ChrgOffMonth Is Not Null And
					OLD.ChrgOffMonth Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ChrgOffYear)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ChrgOffYear',
			CONVERT(nvarchar(4000), OLD.ChrgOffYear, 0),
			CONVERT(nvarchar(4000), NEW.ChrgOffYear, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ChrgOffYear <>
					OLD.ChrgOffYear
				) Or
			
				(
					NEW.ChrgOffYear Is Null And
					OLD.ChrgOffYear Is Not Null
				) Or
				(
					NEW.ChrgOffYear Is Not Null And
					OLD.ChrgOffYear Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ChrgOffMnthsBF)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ChrgOffMnthsBF',
			CONVERT(nvarchar(4000), OLD.ChrgOffMnthsBF, 0),
			CONVERT(nvarchar(4000), NEW.ChrgOffMnthsBF, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ChrgOffMnthsBF <>
					OLD.ChrgOffMnthsBF
				) Or
			
				(
					NEW.ChrgOffMnthsBF Is Null And
					OLD.ChrgOffMnthsBF Is Not Null
				) Or
				(
					NEW.ChrgOffMnthsBF Is Not Null And
					OLD.ChrgOffMnthsBF Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(PropType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'PropType',
			CONVERT(nvarchar(4000), OLD.PropType, 0),
			CONVERT(nvarchar(4000), NEW.PropType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.PropType <>
					OLD.PropType
				) Or
			
				(
					NEW.PropType Is Null And
					OLD.PropType Is Not Null
				) Or
				(
					NEW.PropType Is Not Null And
					OLD.PropType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Occupancy)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Occupancy',
			CONVERT(nvarchar(4000), OLD.Occupancy, 0),
			CONVERT(nvarchar(4000), NEW.Occupancy, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Occupancy <>
					OLD.Occupancy
				) Or
			
				(
					NEW.Occupancy Is Null And
					OLD.Occupancy Is Not Null
				) Or
				(
					NEW.Occupancy Is Not Null And
					OLD.Occupancy Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigApprVal)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigApprVal',
			CONVERT(nvarchar(4000), OLD.OrigApprVal, 0),
			CONVERT(nvarchar(4000), NEW.OrigApprVal, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigApprVal <>
					OLD.OrigApprVal
				) Or
			
				(
					NEW.OrigApprVal Is Null And
					OLD.OrigApprVal Is Not Null
				) Or
				(
					NEW.OrigApprVal Is Not Null And
					OLD.OrigApprVal Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(OrigApprDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'OrigApprDate',
			CONVERT(nvarchar(4000), OLD.OrigApprDate, 0),
			CONVERT(nvarchar(4000), NEW.OrigApprDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.OrigApprDate <>
					OLD.OrigApprDate
				) Or
			
				(
					NEW.OrigApprDate Is Null And
					OLD.OrigApprDate Is Not Null
				) Or
				(
					NEW.OrigApprDate Is Not Null And
					OLD.OrigApprDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurrApprVal)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurrApprVal',
			CONVERT(nvarchar(4000), OLD.CurrApprVal, 0),
			CONVERT(nvarchar(4000), NEW.CurrApprVal, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurrApprVal <>
					OLD.CurrApprVal
				) Or
			
				(
					NEW.CurrApprVal Is Null And
					OLD.CurrApprVal Is Not Null
				) Or
				(
					NEW.CurrApprVal Is Not Null And
					OLD.CurrApprVal Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CurrApprDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CurrApprDate',
			CONVERT(nvarchar(4000), OLD.CurrApprDate, 0),
			CONVERT(nvarchar(4000), NEW.CurrApprDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CurrApprDate <>
					OLD.CurrApprDate
				) Or
			
				(
					NEW.CurrApprDate Is Null And
					OLD.CurrApprDate Is Not Null
				) Or
				(
					NEW.CurrApprDate Is Not Null And
					OLD.CurrApprDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FirstMortBal)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FirstMortBal',
			CONVERT(nvarchar(4000), OLD.FirstMortBal, 0),
			CONVERT(nvarchar(4000), NEW.FirstMortBal, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FirstMortBal <>
					OLD.FirstMortBal
				) Or
			
				(
					NEW.FirstMortBal Is Null And
					OLD.FirstMortBal Is Not Null
				) Or
				(
					NEW.FirstMortBal Is Not Null And
					OLD.FirstMortBal Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FirstMortMoPyt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FirstMortMoPyt',
			CONVERT(nvarchar(4000), OLD.FirstMortMoPyt, 0),
			CONVERT(nvarchar(4000), NEW.FirstMortMoPyt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FirstMortMoPyt <>
					OLD.FirstMortMoPyt
				) Or
			
				(
					NEW.FirstMortMoPyt Is Null And
					OLD.FirstMortMoPyt Is Not Null
				) Or
				(
					NEW.FirstMortMoPyt Is Not Null And
					OLD.FirstMortMoPyt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SecondMortBal)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SecondMortBal',
			CONVERT(nvarchar(4000), OLD.SecondMortBal, 0),
			CONVERT(nvarchar(4000), NEW.SecondMortBal, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SecondMortBal <>
					OLD.SecondMortBal
				) Or
			
				(
					NEW.SecondMortBal Is Null And
					OLD.SecondMortBal Is Not Null
				) Or
				(
					NEW.SecondMortBal Is Not Null And
					OLD.SecondMortBal Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SecondMortPymt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SecondMortPymt',
			CONVERT(nvarchar(4000), OLD.SecondMortPymt, 0),
			CONVERT(nvarchar(4000), NEW.SecondMortPymt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SecondMortPymt <>
					OLD.SecondMortPymt
				) Or
			
				(
					NEW.SecondMortPymt Is Null And
					OLD.SecondMortPymt Is Not Null
				) Or
				(
					NEW.SecondMortPymt Is Not Null And
					OLD.SecondMortPymt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(TaxInsNotInc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'TaxInsNotInc',
			CONVERT(nvarchar(4000), OLD.TaxInsNotInc, 0),
			CONVERT(nvarchar(4000), NEW.TaxInsNotInc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.TaxInsNotInc <>
					OLD.TaxInsNotInc
				) Or
			
				(
					NEW.TaxInsNotInc Is Null And
					OLD.TaxInsNotInc Is Not Null
				) Or
				(
					NEW.TaxInsNotInc Is Not Null And
					OLD.TaxInsNotInc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoadOff)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoadOff',
			CONVERT(nvarchar(4000), OLD.LoadOff, 0),
			CONVERT(nvarchar(4000), NEW.LoadOff, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoadOff <>
					OLD.LoadOff
				) Or
			
				(
					NEW.LoadOff Is Null And
					OLD.LoadOff Is Not Null
				) Or
				(
					NEW.LoadOff Is Not Null And
					OLD.LoadOff Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ApprOff)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ApprOff',
			CONVERT(nvarchar(4000), OLD.ApprOff, 0),
			CONVERT(nvarchar(4000), NEW.ApprOff, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ApprOff <>
					OLD.ApprOff
				) Or
			
				(
					NEW.ApprOff Is Null And
					OLD.ApprOff Is Not Null
				) Or
				(
					NEW.ApprOff Is Not Null And
					OLD.ApprOff Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FundingOpp)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FundingOpp',
			CONVERT(nvarchar(4000), OLD.FundingOpp, 0),
			CONVERT(nvarchar(4000), NEW.FundingOpp, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FundingOpp <>
					OLD.FundingOpp
				) Or
			
				(
					NEW.FundingOpp Is Null And
					OLD.FundingOpp Is Not Null
				) Or
				(
					NEW.FundingOpp Is Not Null And
					OLD.FundingOpp Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Chanel)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Chanel',
			CONVERT(nvarchar(4000), OLD.Chanel, 0),
			CONVERT(nvarchar(4000), NEW.Chanel, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Chanel <>
					OLD.Chanel
				) Or
			
				(
					NEW.Chanel Is Null And
					OLD.Chanel Is Not Null
				) Or
				(
					NEW.Chanel Is Not Null And
					OLD.Chanel Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RiskType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'RiskType',
			CONVERT(nvarchar(4000), OLD.RiskType, 0),
			CONVERT(nvarchar(4000), NEW.RiskType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.RiskType <>
					OLD.RiskType
				) Or
			
				(
					NEW.RiskType Is Null And
					OLD.RiskType Is Not Null
				) Or
				(
					NEW.RiskType Is Not Null And
					OLD.RiskType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Gap)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'Gap',
			CONVERT(nvarchar(4000), OLD.Gap, 0),
			CONVERT(nvarchar(4000), NEW.Gap, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.Gap <>
					OLD.Gap
				) Or
			
				(
					NEW.Gap Is Null And
					OLD.Gap Is Not Null
				) Or
				(
					NEW.Gap Is Not Null And
					OLD.Gap Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoadOperator)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LoadOperator',
			CONVERT(nvarchar(4000), OLD.LoadOperator, 0),
			CONVERT(nvarchar(4000), NEW.LoadOperator, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LoadOperator <>
					OLD.LoadOperator
				) Or
			
				(
					NEW.LoadOperator Is Null And
					OLD.LoadOperator Is Not Null
				) Or
				(
					NEW.LoadOperator Is Not Null And
					OLD.LoadOperator Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(DeliAmt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'DeliAmt',
			CONVERT(nvarchar(4000), OLD.DeliAmt, 0),
			CONVERT(nvarchar(4000), NEW.DeliAmt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.DeliAmt <>
					OLD.DeliAmt
				) Or
			
				(
					NEW.DeliAmt Is Null And
					OLD.DeliAmt Is Not Null
				) Or
				(
					NEW.DeliAmt Is Not Null And
					OLD.DeliAmt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ChgOffAmt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ChgOffAmt',
			CONVERT(nvarchar(4000), OLD.ChgOffAmt, 0),
			CONVERT(nvarchar(4000), NEW.ChgOffAmt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ChgOffAmt <>
					OLD.ChgOffAmt
				) Or
			
				(
					NEW.ChgOffAmt Is Null And
					OLD.ChgOffAmt Is Not Null
				) Or
				(
					NEW.ChgOffAmt Is Not Null And
					OLD.ChgOffAmt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(UsSecDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'UsSecDesc',
			CONVERT(nvarchar(4000), OLD.UsSecDesc, 0),
			CONVERT(nvarchar(4000), NEW.UsSecDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.UsSecDesc <>
					OLD.UsSecDesc
				) Or
			
				(
					NEW.UsSecDesc Is Null And
					OLD.UsSecDesc Is Not Null
				) Or
				(
					NEW.UsSecDesc Is Not Null And
					OLD.UsSecDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(TroubledDebt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'TroubledDebt',
			CONVERT(nvarchar(4000), OLD.TroubledDebt, 0),
			CONVERT(nvarchar(4000), NEW.TroubledDebt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.TroubledDebt <>
					OLD.TroubledDebt
				) Or
			
				(
					NEW.TroubledDebt Is Null And
					OLD.TroubledDebt Is Not Null
				) Or
				(
					NEW.TroubledDebt Is Not Null And
					OLD.TroubledDebt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(TdrDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'TdrDate',
			CONVERT(nvarchar(4000), OLD.TdrDate, 0),
			CONVERT(nvarchar(4000), NEW.TdrDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.TdrDate <>
					OLD.TdrDate
				) Or
			
				(
					NEW.TdrDate Is Null And
					OLD.TdrDate Is Not Null
				) Or
				(
					NEW.TdrDate Is Not Null And
					OLD.TdrDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(JntFicoUsed)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'JntFicoUsed',
			CONVERT(nvarchar(4000), OLD.JntFicoUsed, 0),
			CONVERT(nvarchar(4000), NEW.JntFicoUsed, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.JntFicoUsed <>
					OLD.JntFicoUsed
				) Or
			
				(
					NEW.JntFicoUsed Is Null And
					OLD.JntFicoUsed Is Not Null
				) Or
				(
					NEW.JntFicoUsed Is Not Null And
					OLD.JntFicoUsed Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(SingleOrJointLoan)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'SingleOrJointLoan',
			CONVERT(nvarchar(4000), OLD.SingleOrJointLoan, 0),
			CONVERT(nvarchar(4000), NEW.SingleOrJointLoan, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.SingleOrJointLoan <>
					OLD.SingleOrJointLoan
				) Or
			
				(
					NEW.SingleOrJointLoan Is Null And
					OLD.SingleOrJointLoan Is Not Null
				) Or
				(
					NEW.SingleOrJointLoan Is Not Null And
					OLD.SingleOrJointLoan Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ApprovingOfficerUD)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ApprovingOfficerUD',
			CONVERT(nvarchar(4000), OLD.ApprovingOfficerUD, 0),
			CONVERT(nvarchar(4000), NEW.ApprovingOfficerUD, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ApprovingOfficerUD <>
					OLD.ApprovingOfficerUD
				) Or
			
				(
					NEW.ApprovingOfficerUD Is Null And
					OLD.ApprovingOfficerUD Is Not Null
				) Or
				(
					NEW.ApprovingOfficerUD Is Not Null And
					OLD.ApprovingOfficerUD Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InsuranceCodeDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InsuranceCodeDesc',
			CONVERT(nvarchar(4000), OLD.InsuranceCodeDesc, 0),
			CONVERT(nvarchar(4000), NEW.InsuranceCodeDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InsuranceCodeDesc <>
					OLD.InsuranceCodeDesc
				) Or
			
				(
					NEW.InsuranceCodeDesc Is Null And
					OLD.InsuranceCodeDesc Is Not Null
				) Or
				(
					NEW.InsuranceCodeDesc Is Not Null And
					OLD.InsuranceCodeDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(GapIns)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'GapIns',
			CONVERT(nvarchar(4000), OLD.GapIns, 0),
			CONVERT(nvarchar(4000), NEW.GapIns, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.GapIns <>
					OLD.GapIns
				) Or
			
				(
					NEW.GapIns Is Null And
					OLD.GapIns Is Not Null
				) Or
				(
					NEW.GapIns Is Not Null And
					OLD.GapIns Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MmpIns)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MmpIns',
			CONVERT(nvarchar(4000), OLD.MmpIns, 0),
			CONVERT(nvarchar(4000), NEW.MmpIns, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MmpIns <>
					OLD.MmpIns
				) Or
			
				(
					NEW.MmpIns Is Null And
					OLD.MmpIns Is Not Null
				) Or
				(
					NEW.MmpIns Is Not Null And
					OLD.MmpIns Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ReportCodeDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ReportCodeDesc',
			CONVERT(nvarchar(4000), OLD.ReportCodeDesc, 0),
			CONVERT(nvarchar(4000), NEW.ReportCodeDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ReportCodeDesc <>
					OLD.ReportCodeDesc
				) Or
			
				(
					NEW.ReportCodeDesc Is Null And
					OLD.ReportCodeDesc Is Not Null
				) Or
				(
					NEW.ReportCodeDesc Is Not Null And
					OLD.ReportCodeDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(BallonPmtLoan)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'BallonPmtLoan',
			CONVERT(nvarchar(4000), OLD.BallonPmtLoan, 0),
			CONVERT(nvarchar(4000), NEW.BallonPmtLoan, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.BallonPmtLoan <>
					OLD.BallonPmtLoan
				) Or
			
				(
					NEW.BallonPmtLoan Is Null And
					OLD.BallonPmtLoan Is Not Null
				) Or
				(
					NEW.BallonPmtLoan Is Not Null And
					OLD.BallonPmtLoan Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FixedOrVariableRateLoan)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FixedOrVariableRateLoan',
			CONVERT(nvarchar(4000), OLD.FixedOrVariableRateLoan, 0),
			CONVERT(nvarchar(4000), NEW.FixedOrVariableRateLoan, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FixedOrVariableRateLoan <>
					OLD.FixedOrVariableRateLoan
				) Or
			
				(
					NEW.FixedOrVariableRateLoan Is Null And
					OLD.FixedOrVariableRateLoan Is Not Null
				) Or
				(
					NEW.FixedOrVariableRateLoan Is Not Null And
					OLD.FixedOrVariableRateLoan Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(AlpsOfficer)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'AlpsOfficer',
			CONVERT(nvarchar(4000), OLD.AlpsOfficer, 0),
			CONVERT(nvarchar(4000), NEW.AlpsOfficer, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.AlpsOfficer <>
					OLD.AlpsOfficer
				) Or
			
				(
					NEW.AlpsOfficer Is Null And
					OLD.AlpsOfficer Is Not Null
				) Or
				(
					NEW.AlpsOfficer Is Not Null And
					OLD.AlpsOfficer Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(CUDLAppNumber)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'CUDLAppNumber',
			CONVERT(nvarchar(4000), OLD.CUDLAppNumber, 0),
			CONVERT(nvarchar(4000), NEW.CUDLAppNumber, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.CUDLAppNumber <>
					OLD.CUDLAppNumber
				) Or
			
				(
					NEW.CUDLAppNumber Is Null And
					OLD.CUDLAppNumber Is Not Null
				) Or
				(
					NEW.CUDLAppNumber Is Not Null And
					OLD.CUDLAppNumber Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FasbDefCost)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FasbDefCost',
			CONVERT(nvarchar(4000), OLD.FasbDefCost, 0),
			CONVERT(nvarchar(4000), NEW.FasbDefCost, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FasbDefCost <>
					OLD.FasbDefCost
				) Or
			
				(
					NEW.FasbDefCost Is Null And
					OLD.FasbDefCost Is Not Null
				) Or
				(
					NEW.FasbDefCost Is Not Null And
					OLD.FasbDefCost Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(FasbLtdAmortFees)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'FasbLtdAmortFees',
			CONVERT(nvarchar(4000), OLD.FasbLtdAmortFees, 0),
			CONVERT(nvarchar(4000), NEW.FasbLtdAmortFees, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.FasbLtdAmortFees <>
					OLD.FasbLtdAmortFees
				) Or
			
				(
					NEW.FasbLtdAmortFees Is Null And
					OLD.FasbLtdAmortFees Is Not Null
				) Or
				(
					NEW.FasbLtdAmortFees Is Not Null And
					OLD.FasbLtdAmortFees Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InterestPriorYtd)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InterestPriorYtd',
			CONVERT(nvarchar(4000), OLD.InterestPriorYtd, 0),
			CONVERT(nvarchar(4000), NEW.InterestPriorYtd, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InterestPriorYtd <>
					OLD.InterestPriorYtd
				) Or
			
				(
					NEW.InterestPriorYtd Is Null And
					OLD.InterestPriorYtd Is Not Null
				) Or
				(
					NEW.InterestPriorYtd Is Not Null And
					OLD.InterestPriorYtd Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(AccruedInterest)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'AccruedInterest',
			CONVERT(nvarchar(4000), OLD.AccruedInterest, 0),
			CONVERT(nvarchar(4000), NEW.AccruedInterest, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.AccruedInterest <>
					OLD.AccruedInterest
				) Or
			
				(
					NEW.AccruedInterest Is Null And
					OLD.AccruedInterest Is Not Null
				) Or
				(
					NEW.AccruedInterest Is Not Null And
					OLD.AccruedInterest Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InterestLTD)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InterestLTD',
			CONVERT(nvarchar(4000), OLD.InterestLTD, 0),
			CONVERT(nvarchar(4000), NEW.InterestLTD, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InterestLTD <>
					OLD.InterestLTD
				) Or
			
				(
					NEW.InterestLTD Is Null And
					OLD.InterestLTD Is Not Null
				) Or
				(
					NEW.InterestLTD Is Not Null And
					OLD.InterestLTD Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InterestYtd)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InterestYtd',
			CONVERT(nvarchar(4000), OLD.InterestYtd, 0),
			CONVERT(nvarchar(4000), NEW.InterestYtd, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InterestYtd <>
					OLD.InterestYtd
				) Or
			
				(
					NEW.InterestYtd Is Null And
					OLD.InterestYtd Is Not Null
				) Or
				(
					NEW.InterestYtd Is Not Null And
					OLD.InterestYtd Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(PartialPayAmt)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'PartialPayAmt',
			CONVERT(nvarchar(4000), OLD.PartialPayAmt, 0),
			CONVERT(nvarchar(4000), NEW.PartialPayAmt, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.PartialPayAmt <>
					OLD.PartialPayAmt
				) Or
			
				(
					NEW.PartialPayAmt Is Null And
					OLD.PartialPayAmt Is Not Null
				) Or
				(
					NEW.PartialPayAmt Is Not Null And
					OLD.PartialPayAmt Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InterestUncollected)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InterestUncollected',
			CONVERT(nvarchar(4000), OLD.InterestUncollected, 0),
			CONVERT(nvarchar(4000), NEW.InterestUncollected, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InterestUncollected <>
					OLD.InterestUncollected
				) Or
			
				(
					NEW.InterestUncollected Is Null And
					OLD.InterestUncollected Is Not Null
				) Or
				(
					NEW.InterestUncollected Is Not Null And
					OLD.InterestUncollected Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ClosedAcctDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'ClosedAcctDate',
			CONVERT(nvarchar(4000), OLD.ClosedAcctDate, 0),
			CONVERT(nvarchar(4000), NEW.ClosedAcctDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.ClosedAcctDate <>
					OLD.ClosedAcctDate
				) Or
			
				(
					NEW.ClosedAcctDate Is Null And
					OLD.ClosedAcctDate Is Not Null
				) Or
				(
					NEW.ClosedAcctDate Is Not Null And
					OLD.ClosedAcctDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LastTranDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'LastTranDate',
			CONVERT(nvarchar(4000), OLD.LastTranDate, 0),
			CONVERT(nvarchar(4000), NEW.LastTranDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.LastTranDate <>
					OLD.LastTranDate
				) Or
			
				(
					NEW.LastTranDate Is Null And
					OLD.LastTranDate Is Not Null
				) Or
				(
					NEW.LastTranDate Is Not Null And
					OLD.LastTranDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(NextDueDate)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'NextDueDate',
			CONVERT(nvarchar(4000), OLD.NextDueDate, 0),
			CONVERT(nvarchar(4000), NEW.NextDueDate, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.NextDueDate <>
					OLD.NextDueDate
				) Or
			
				(
					NEW.NextDueDate Is Null And
					OLD.NextDueDate Is Not Null
				) Or
				(
					NEW.NextDueDate Is Not Null And
					OLD.NextDueDate Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(InsuranceCode)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'InsuranceCode',
			CONVERT(nvarchar(4000), OLD.InsuranceCode, 0),
			CONVERT(nvarchar(4000), NEW.InsuranceCode, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.InsuranceCode <>
					OLD.InsuranceCode
				) Or
			
				(
					NEW.InsuranceCode Is Null And
					OLD.InsuranceCode Is Not Null
				) Or
				(
					NEW.InsuranceCode Is Not Null And
					OLD.InsuranceCode Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(MbrAgeAtApproval)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanPortfolioId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanPortfolioId], NEW.[LoanPortfolioId]), 0), '[LoanPortfolioId] Is Null')),
		    'MbrAgeAtApproval',
			CONVERT(nvarchar(4000), OLD.MbrAgeAtApproval, 0),
			CONVERT(nvarchar(4000), NEW.MbrAgeAtApproval, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanPortfolioId], 0)=CONVERT(nvarchar(4000), OLD.[LoanPortfolioId], 0) or (NEW.[LoanPortfolioId] Is Null and OLD.[LoanPortfolioId] Is Null))
			where (
				(
					NEW.MbrAgeAtApproval <>
					OLD.MbrAgeAtApproval
				) Or
			
				(
					NEW.MbrAgeAtApproval Is Null And
					OLD.MbrAgeAtApproval Is Not Null
				) Or
				(
					NEW.MbrAgeAtApproval Is Not Null And
					OLD.MbrAgeAtApproval Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	

	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM dbo.AuditLogTransaction WHERE AuditLogTransactionId = @AuditLogTransactionId
	END
	-- Restore @@IDENTITY Value  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
End
GO

-- Display the status of Trigger Created
IF @@Error = 0 PRINT 'Trigger Created: trLoanPortfolio_Update '
ELSE PRINT 'Trigger Failed: trLoanPortfolio_Update Error on Creation'
GO

-- mark the trigger as last 

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE parent_obj=OBJECT_ID('[dbo].[LoanPortfolio]') AND OBJECTPROPERTY(id,'ExecIsLastUpdateTrigger')=1 AND xtype='TR')
BEGIN
  EXEC sp_settriggerorder '[dbo].[trLoanPortfolio_Update]', 'Last', 'Update'
  If @@Error = 0 PRINT 'Trigger trLoanPortfolio_Update has been marked as Last' 
END

GO
