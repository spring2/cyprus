IF OBJECT_ID('[dbo].[trLoanType_Update]','TR') IS NOT NULL
BEGIN
	DROP TRIGGER [dbo].[trLoanType_Update]
	PRINT 'Trigger Dropped: trLoanType_Update '
END
GO

-- Create Update Trigger [dbo].[trLoanType_Update] for Table [dbo].[LoanType]
Print 'Create Update Trigger [dbo].[trLoanType_Update] for Table [dbo].[LoanType]'
go
CREATE TRIGGER [dbo].[trLoanType_Update]
ON [dbo].[LoanType]
FOR UPDATE
NOT FOR REPLICATION
As

BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AuditLogTransactionId	Int,
		@PRIM_KEY				nvarchar(4000),
		@Inserted	    		bit,
		--@TableName			nvarchar(4000),
 		@ROWS_COUNT				int
 
	SET NOCOUNT ON

	--Set @TableName = '[dbo].[LoanType]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

-- CAS: get the userId from the inserted data
declare @userId int
select @userId=LastModifiedUserId from inserted

	INSERT
	INTO dbo.AuditLogTransaction 
	(
		TableName,
		TableSchema,
		AuditActionId,
		HostName,
		ApplicationName,
		AuditLogin,
		AuditDate,
		AffectedRows,
		DatabaseName,
		UserId
	)
	values(
		'LoanType',
		'dbo',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		db_name(),
		@userId
	)
	
	
	Set @AuditLogTransactionId = SCOPE_IDENTITY()
	
	
	SET @Inserted = 0


	If UPDATE(LoanTypeId)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanTypeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanTypeId], NEW.[LoanTypeId]), 0), '[LoanTypeId] Is Null')),
		    'LoanTypeId',
			CONVERT(nvarchar(4000), OLD.LoanTypeId, 0),
			CONVERT(nvarchar(4000), NEW.LoanTypeId, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanTypeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanTypeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanTypeId], 0)=CONVERT(nvarchar(4000), OLD.[LoanTypeId], 0) or (NEW.[LoanTypeId] Is Null and OLD.[LoanTypeId] Is Null))
			where (
				(
					NEW.LoanTypeId <>
					OLD.LoanTypeId
				) Or
			
				(
					NEW.LoanTypeId Is Null And
					OLD.LoanTypeId Is Not Null
				) Or
				(
					NEW.LoanTypeId Is Not Null And
					OLD.LoanTypeId Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanTypeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanTypeId], NEW.[LoanTypeId]), 0), '[LoanTypeId] Is Null')),
		    'LType',
			CONVERT(nvarchar(4000), OLD.LType, 0),
			CONVERT(nvarchar(4000), NEW.LType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanTypeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanTypeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanTypeId], 0)=CONVERT(nvarchar(4000), OLD.[LoanTypeId], 0) or (NEW.[LoanTypeId] Is Null and OLD.[LoanTypeId] Is Null))
			where (
				(
					NEW.LType <>
					OLD.LType
				) Or
			
				(
					NEW.LType Is Null And
					OLD.LType Is Not Null
				) Or
				(
					NEW.LType Is Not Null And
					OLD.LType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LoanDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[LoanTypeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[LoanTypeId], NEW.[LoanTypeId]), 0), '[LoanTypeId] Is Null')),
		    'LoanDesc',
			CONVERT(nvarchar(4000), OLD.LoanDesc, 0),
			CONVERT(nvarchar(4000), NEW.LoanDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[LoanTypeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[LoanTypeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[LoanTypeId], 0)=CONVERT(nvarchar(4000), OLD.[LoanTypeId], 0) or (NEW.[LoanTypeId] Is Null and OLD.[LoanTypeId] Is Null))
			where (
				(
					NEW.LoanDesc <>
					OLD.LoanDesc
				) Or
			
				(
					NEW.LoanDesc Is Null And
					OLD.LoanDesc Is Not Null
				) Or
				(
					NEW.LoanDesc Is Not Null And
					OLD.LoanDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	

	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM dbo.AuditLogTransaction WHERE AuditLogTransactionId = @AuditLogTransactionId
	END
	-- Restore @@IDENTITY Value  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
End
GO

-- Display the status of Trigger Created
IF @@Error = 0 PRINT 'Trigger Created: trLoanType_Update '
ELSE PRINT 'Trigger Failed: trLoanType_Update Error on Creation'
GO

-- mark the trigger as last 

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE parent_obj=OBJECT_ID('[dbo].[LoanType]') AND OBJECTPROPERTY(id,'ExecIsLastUpdateTrigger')=1 AND xtype='TR')
BEGIN
  EXEC sp_settriggerorder '[dbo].[trLoanType_Update]', 'Last', 'Update'
  If @@Error = 0 PRINT 'Trigger trLoanType_Update has been marked as Last' 
END

GO
