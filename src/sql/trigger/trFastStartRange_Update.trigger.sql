IF OBJECT_ID('[dbo].[trFastStartRange_Update]','TR') IS NOT NULL
BEGIN
	DROP TRIGGER [dbo].[trFastStartRange_Update]
	PRINT 'Trigger Dropped: trFastStartRange_Update '
END
GO

-- Create Update Trigger [dbo].[trFastStartRange_Update] for Table [dbo].[FastStartRange]
Print 'Create Update Trigger [dbo].[trFastStartRange_Update] for Table [dbo].[FastStartRange]'
go
CREATE TRIGGER [dbo].[trFastStartRange_Update]
ON [dbo].[FastStartRange]
FOR UPDATE
NOT FOR REPLICATION
As

BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AuditLogTransactionId	Int,
		@PRIM_KEY				nvarchar(4000),
		@Inserted	    		bit,
		--@TableName			nvarchar(4000),
 		@ROWS_COUNT				int
 
	SET NOCOUNT ON

	--Set @TableName = '[dbo].[FastStartRange]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

-- CAS: get the userId from the inserted data
declare @userId int
select @userId=LastModifiedUserId from inserted

	INSERT
	INTO dbo.AuditLogTransaction 
	(
		TableName,
		TableSchema,
		AuditActionId,
		HostName,
		ApplicationName,
		AuditLogin,
		AuditDate,
		AffectedRows,
		DatabaseName,
		UserId
	)
	values(
		'FastStartRange',
		'dbo',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		db_name(),
		@userId
	)
	
	
	Set @AuditLogTransactionId = SCOPE_IDENTITY()
	
	
	SET @Inserted = 0


	If UPDATE(FastStartRangeId)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[FastStartRangeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[FastStartRangeId], NEW.[FastStartRangeId]), 0), '[FastStartRangeId] Is Null')),
		    'FastStartRangeId',
			CONVERT(nvarchar(4000), OLD.FastStartRangeId, 0),
			CONVERT(nvarchar(4000), NEW.FastStartRangeId, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[FastStartRangeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[FastStartRangeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[FastStartRangeId], 0)=CONVERT(nvarchar(4000), OLD.[FastStartRangeId], 0) or (NEW.[FastStartRangeId] Is Null and OLD.[FastStartRangeId] Is Null))
			where (
				(
					NEW.FastStartRangeId <>
					OLD.FastStartRangeId
				) Or
			
				(
					NEW.FastStartRangeId Is Null And
					OLD.FastStartRangeId Is Not Null
				) Or
				(
					NEW.FastStartRangeId Is Not Null And
					OLD.FastStartRangeId Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Value)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[FastStartRangeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[FastStartRangeId], NEW.[FastStartRangeId]), 0), '[FastStartRangeId] Is Null')),
		    'Value',
			CONVERT(nvarchar(4000), OLD.Value, 0),
			CONVERT(nvarchar(4000), NEW.Value, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[FastStartRangeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[FastStartRangeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[FastStartRangeId], 0)=CONVERT(nvarchar(4000), OLD.[FastStartRangeId], 0) or (NEW.[FastStartRangeId] Is Null and OLD.[FastStartRangeId] Is Null))
			where (
				(
					NEW.Value <>
					OLD.Value
				) Or
			
				(
					NEW.Value Is Null And
					OLD.Value Is Not Null
				) Or
				(
					NEW.Value Is Not Null And
					OLD.Value Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(Description)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[FastStartRangeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[FastStartRangeId], NEW.[FastStartRangeId]), 0), '[FastStartRangeId] Is Null')),
		    'Description',
			CONVERT(nvarchar(4000), OLD.Description, 0),
			CONVERT(nvarchar(4000), NEW.Description, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[FastStartRangeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[FastStartRangeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[FastStartRangeId], 0)=CONVERT(nvarchar(4000), OLD.[FastStartRangeId], 0) or (NEW.[FastStartRangeId] Is Null and OLD.[FastStartRangeId] Is Null))
			where (
				(
					NEW.Description <>
					OLD.Description
				) Or
			
				(
					NEW.Description Is Null And
					OLD.Description Is Not Null
				) Or
				(
					NEW.Description Is Not Null And
					OLD.Description Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	

	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM dbo.AuditLogTransaction WHERE AuditLogTransactionId = @AuditLogTransactionId
	END
	-- Restore @@IDENTITY Value  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
End
GO

-- Display the status of Trigger Created
IF @@Error = 0 PRINT 'Trigger Created: trFastStartRange_Update '
ELSE PRINT 'Trigger Failed: trFastStartRange_Update Error on Creation'
GO

-- mark the trigger as last 

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE parent_obj=OBJECT_ID('[dbo].[FastStartRange]') AND OBJECTPROPERTY(id,'ExecIsLastUpdateTrigger')=1 AND xtype='TR')
BEGIN
  EXEC sp_settriggerorder '[dbo].[trFastStartRange_Update]', 'Last', 'Update'
  If @@Error = 0 PRINT 'Trigger trFastStartRange_Update has been marked as Last' 
END

GO
