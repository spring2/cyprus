IF OBJECT_ID('[dbo].[trChanel_Update]','TR') IS NOT NULL
BEGIN
	DROP TRIGGER [dbo].[trChanel_Update]
	PRINT 'Trigger Dropped: trChanel_Update '
END
GO

-- Create Update Trigger [dbo].[trChanel_Update] for Table [dbo].[Chanel]
Print 'Create Update Trigger [dbo].[trChanel_Update] for Table [dbo].[Chanel]'
go
CREATE TRIGGER [dbo].[trChanel_Update]
ON [dbo].[Chanel]
FOR UPDATE
NOT FOR REPLICATION
As

BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AuditLogTransactionId	Int,
		@PRIM_KEY				nvarchar(4000),
		@Inserted	    		bit,
		--@TableName			nvarchar(4000),
 		@ROWS_COUNT				int
 
	SET NOCOUNT ON

	--Set @TableName = '[dbo].[Chanel]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

-- CAS: get the userId from the inserted data
declare @userId int
select @userId=LastModifiedUserId from inserted

	INSERT
	INTO dbo.AuditLogTransaction 
	(
		TableName,
		TableSchema,
		AuditActionId,
		HostName,
		ApplicationName,
		AuditLogin,
		AuditDate,
		AffectedRows,
		DatabaseName,
		UserId
	)
	values(
		'Chanel',
		'dbo',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		db_name(),
		@userId
	)
	
	
	Set @AuditLogTransactionId = SCOPE_IDENTITY()
	
	
	SET @Inserted = 0


	If UPDATE(ChanelId)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[ChanelId]='+CONVERT(nvarchar(4000), IsNull(OLD.[ChanelId], NEW.[ChanelId]), 0), '[ChanelId] Is Null')),
		    'ChanelId',
			CONVERT(nvarchar(4000), OLD.ChanelId, 0),
			CONVERT(nvarchar(4000), NEW.ChanelId, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[ChanelId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ChanelId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[ChanelId], 0)=CONVERT(nvarchar(4000), OLD.[ChanelId], 0) or (NEW.[ChanelId] Is Null and OLD.[ChanelId] Is Null))
			where (
				(
					NEW.ChanelId <>
					OLD.ChanelId
				) Or
			
				(
					NEW.ChanelId Is Null And
					OLD.ChanelId Is Not Null
				) Or
				(
					NEW.ChanelId Is Not Null And
					OLD.ChanelId Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ReportCode)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[ChanelId]='+CONVERT(nvarchar(4000), IsNull(OLD.[ChanelId], NEW.[ChanelId]), 0), '[ChanelId] Is Null')),
		    'ReportCode',
			CONVERT(nvarchar(4000), OLD.ReportCode, 0),
			CONVERT(nvarchar(4000), NEW.ReportCode, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[ChanelId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ChanelId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[ChanelId], 0)=CONVERT(nvarchar(4000), OLD.[ChanelId], 0) or (NEW.[ChanelId] Is Null and OLD.[ChanelId] Is Null))
			where (
				(
					NEW.ReportCode <>
					OLD.ReportCode
				) Or
			
				(
					NEW.ReportCode Is Null And
					OLD.ReportCode Is Not Null
				) Or
				(
					NEW.ReportCode Is Not Null And
					OLD.ReportCode Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(ChanelDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[ChanelId]='+CONVERT(nvarchar(4000), IsNull(OLD.[ChanelId], NEW.[ChanelId]), 0), '[ChanelId] Is Null')),
		    'ChanelDesc',
			CONVERT(nvarchar(4000), OLD.ChanelDesc, 0),
			CONVERT(nvarchar(4000), NEW.ChanelDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[ChanelId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[ChanelId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[ChanelId], 0)=CONVERT(nvarchar(4000), OLD.[ChanelId], 0) or (NEW.[ChanelId] Is Null and OLD.[ChanelId] Is Null))
			where (
				(
					NEW.ChanelDesc <>
					OLD.ChanelDesc
				) Or
			
				(
					NEW.ChanelDesc Is Null And
					OLD.ChanelDesc Is Not Null
				) Or
				(
					NEW.ChanelDesc Is Not Null And
					OLD.ChanelDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	

	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM dbo.AuditLogTransaction WHERE AuditLogTransactionId = @AuditLogTransactionId
	END
	-- Restore @@IDENTITY Value  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
End
GO

-- Display the status of Trigger Created
IF @@Error = 0 PRINT 'Trigger Created: trChanel_Update '
ELSE PRINT 'Trigger Failed: trChanel_Update Error on Creation'
GO

-- mark the trigger as last 

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE parent_obj=OBJECT_ID('[dbo].[Chanel]') AND OBJECTPROPERTY(id,'ExecIsLastUpdateTrigger')=1 AND xtype='TR')
BEGIN
  EXEC sp_settriggerorder '[dbo].[trChanel_Update]', 'Last', 'Update'
  If @@Error = 0 PRINT 'Trigger trChanel_Update has been marked as Last' 
END

GO
