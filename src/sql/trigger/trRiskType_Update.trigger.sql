IF OBJECT_ID('[dbo].[trRiskType_Update]','TR') IS NOT NULL
BEGIN
	DROP TRIGGER [dbo].[trRiskType_Update]
	PRINT 'Trigger Dropped: trRiskType_Update '
END
GO

-- Create Update Trigger [dbo].[trRiskType_Update] for Table [dbo].[RiskType]
Print 'Create Update Trigger [dbo].[trRiskType_Update] for Table [dbo].[RiskType]'
go
CREATE TRIGGER [dbo].[trRiskType_Update]
ON [dbo].[RiskType]
FOR UPDATE
NOT FOR REPLICATION
As

BEGIN
	DECLARE 
		@IDENTITY_SAVE			varchar(50),
		@AuditLogTransactionId	Int,
		@PRIM_KEY				nvarchar(4000),
		@Inserted	    		bit,
		--@TableName			nvarchar(4000),
 		@ROWS_COUNT				int
 
	SET NOCOUNT ON

	--Set @TableName = '[dbo].[RiskType]'
	Select @ROWS_COUNT=count(*) from inserted
	SET @IDENTITY_SAVE = CAST(IsNull(@@IDENTITY,1) AS varchar(50))

-- CAS: get the userId from the inserted data
declare @userId int
select @userId=LastModifiedUserId from inserted

	INSERT
	INTO dbo.AuditLogTransaction 
	(
		TableName,
		TableSchema,
		AuditActionId,
		HostName,
		ApplicationName,
		AuditLogin,
		AuditDate,
		AffectedRows,
		DatabaseName,
		UserId
	)
	values(
		'RiskType',
		'dbo',
		1,	--	ACTION ID For UPDATE
		CASE 
		  WHEN LEN(HOST_NAME()) < 1 THEN ' '
		  ELSE HOST_NAME()
		END,
		CASE 
		  WHEN LEN(APP_NAME()) < 1 THEN ' '
		  ELSE APP_NAME()
		END,
		SUSER_SNAME(),
		GETDATE(),
		@ROWS_COUNT,
		db_name(),
		@userId
	)
	
	
	Set @AuditLogTransactionId = SCOPE_IDENTITY()
	
	
	SET @Inserted = 0


	If UPDATE(RiskTypeId)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[RiskTypeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[RiskTypeId], NEW.[RiskTypeId]), 0), '[RiskTypeId] Is Null')),
		    'RiskTypeId',
			CONVERT(nvarchar(4000), OLD.RiskTypeId, 0),
			CONVERT(nvarchar(4000), NEW.RiskTypeId, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RiskTypeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RiskTypeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[RiskTypeId], 0)=CONVERT(nvarchar(4000), OLD.[RiskTypeId], 0) or (NEW.[RiskTypeId] Is Null and OLD.[RiskTypeId] Is Null))
			where (
				(
					NEW.RiskTypeId <>
					OLD.RiskTypeId
				) Or
			
				(
					NEW.RiskTypeId Is Null And
					OLD.RiskTypeId Is Not Null
				) Or
				(
					NEW.RiskTypeId Is Not Null And
					OLD.RiskTypeId Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(LType)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[RiskTypeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[RiskTypeId], NEW.[RiskTypeId]), 0), '[RiskTypeId] Is Null')),
		    'LType',
			CONVERT(nvarchar(4000), OLD.LType, 0),
			CONVERT(nvarchar(4000), NEW.LType, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RiskTypeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RiskTypeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[RiskTypeId], 0)=CONVERT(nvarchar(4000), OLD.[RiskTypeId], 0) or (NEW.[RiskTypeId] Is Null and OLD.[RiskTypeId] Is Null))
			where (
				(
					NEW.LType <>
					OLD.LType
				) Or
			
				(
					NEW.LType Is Null And
					OLD.LType Is Not Null
				) Or
				(
					NEW.LType Is Not Null And
					OLD.LType Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	
	If UPDATE(RiskTypeDesc)
	BEGIN
    
		INSERT
		INTO AuditLog 
		(
			AuditLogTransactionId,
			PrimaryKey,
			ColumnName,
			OldValue,
			NewValue,
			DataType
			, Key1
		)
		SELECT
			@AuditLogTransactionId,
		    convert(nvarchar(1500), IsNull('[RiskTypeId]='+CONVERT(nvarchar(4000), IsNull(OLD.[RiskTypeId], NEW.[RiskTypeId]), 0), '[RiskTypeId] Is Null')),
		    'RiskTypeDesc',
			CONVERT(nvarchar(4000), OLD.RiskTypeDesc, 0),
			CONVERT(nvarchar(4000), NEW.RiskTypeDesc, 0),
			'A'
			, IsNULL( CONVERT(nvarchar(500), CONVERT(nvarchar(4000), OLD.[RiskTypeId], 0)), CONVERT(nvarchar(500), CONVERT(nvarchar(4000), NEW.[RiskTypeId], 0)))
			
		FROM deleted OLD Inner Join inserted NEW On 
			(CONVERT(nvarchar(4000), NEW.[RiskTypeId], 0)=CONVERT(nvarchar(4000), OLD.[RiskTypeId], 0) or (NEW.[RiskTypeId] Is Null and OLD.[RiskTypeId] Is Null))
			where (
				(
					NEW.RiskTypeDesc <>
					OLD.RiskTypeDesc
				) Or
			
				(
					NEW.RiskTypeDesc Is Null And
					OLD.RiskTypeDesc Is Not Null
				) Or
				(
					NEW.RiskTypeDesc Is Not Null And
					OLD.RiskTypeDesc Is Null
				)
				)
        
		SET @Inserted = CASE WHEN @@ROWCOUNT > 0 Then 1 Else @Inserted End
	END
	

	-- Watch
	
	-- Lookup
	
	IF @Inserted = 0
	BEGIN
		DELETE FROM dbo.AuditLogTransaction WHERE AuditLogTransactionId = @AuditLogTransactionId
	END
	-- Restore @@IDENTITY Value  
    DECLARE @maxprec AS varchar(2)
    SET @maxprec=CAST(@@MAX_PRECISION as varchar(2))
    EXEC('SELECT IDENTITY(decimal('+@maxprec+',0),'+@IDENTITY_SAVE+',1) id INTO #tmp')
End
GO

-- Display the status of Trigger Created
IF @@Error = 0 PRINT 'Trigger Created: trRiskType_Update '
ELSE PRINT 'Trigger Failed: trRiskType_Update Error on Creation'
GO

-- mark the trigger as last 

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE parent_obj=OBJECT_ID('[dbo].[RiskType]') AND OBJECTPROPERTY(id,'ExecIsLastUpdateTrigger')=1 AND xtype='TR')
BEGIN
  EXEC sp_settriggerorder '[dbo].[trRiskType_Update]', 'Last', 'Update'
  If @@Error = 0 PRINT 'Trigger trRiskType_Update has been marked as Last' 
END

GO
