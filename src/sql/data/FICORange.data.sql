-- create "root" demonstrator
if not exists (select * from FICORange)
    BEGIN
		insert into FICORange values(499, '0-499')
		insert into FICORange values(599, '500-599')
		insert into FICORange values(659, '600-659')
		insert into FICORange values(679, '660-679')
		insert into FICORange values(719, '680-719')
		insert into FICORange values(9999, '720 =>')
    END
GO
