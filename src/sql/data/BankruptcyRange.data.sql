-- create "root" demonstrator
if not exists (select * from BankruptcyRange)
    BEGIN
		insert into BankruptcyRange values(99, '0-99')
		insert into BankruptcyRange values(149, '100-149')
		insert into BankruptcyRange values(299, '150-299')
		insert into BankruptcyRange values(399, '300-399')
		insert into BankruptcyRange values(499, '400-499')
		insert into BankruptcyRange values(599, '500-599')
		insert into BankruptcyRange values(799, '600-799')
		insert into BankruptcyRange values(9999, '800 =>')
    END
GO
