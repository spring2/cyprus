if not exists(select * from ConfigurationSetting where [Key]='LoanPortfolio.DerivedValues.NewMemberDays')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('LoanPortfolio.DerivedValues.NewMemberDays', '30', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='Notification.ToAddress')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('Notification.ToAddress', 'support@cypruscu.com', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='SMTP.Host')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('SMTP.Host', '172.16.160.120', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='SMTP.UserName')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('SMTP.UserName', '', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='SMTP.Password')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('SMTP.Password', '', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='SMTP.Port')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('SMTP.Port', '25', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='UniData.ODBC.ConnectionString')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('UniData.ODBC.ConnectionString', 'DSN=cyprus_eom;UID=sqluser;PWD=1234r;', getdate(), 1)
  end
go

if not exists(select * from ConfigurationSetting where [Key]='Test.ODBC.ConnectionString')
  begin
    insert into ConfigurationSetting ([Key], Value, LastModifiedDate, LastModifiedUserId)
    values('Test.ODBC.ConnectionString', '', getdate(), 1)
  end
go