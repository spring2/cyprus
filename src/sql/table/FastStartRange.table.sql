SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_FastStartRange%' and xtype='P')
drop procedure #spAlterColumn_FastStartRange
GO

CREATE PROCEDURE #spAlterColumn_FastStartRange
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[FastStartRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.FastStartRange (
	FastStartRangeId Int IDENTITY(1,1) NOT NULL,
	Value Int NOT NULL,
	Description VarChar(250) NOT NULL
)
GO

if not exists(select * from syscolumns where id=object_id('FastStartRange') and name = 'FastStartRangeId')
  BEGIN
	ALTER TABLE FastStartRange ADD
	    FastStartRangeId Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='FastStartRange' and column_name='FastStartRangeId') and not exists(select * from information_schema.columns where table_name='FastStartRange' and column_name='FastStartRangeId' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_FastStartRange 'FastStartRange', 'FastStartRangeId', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('FastStartRange') and name = 'Value')
  BEGIN
	ALTER TABLE FastStartRange ADD
	    Value Int NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='FastStartRange' and column_name='Value') and not exists(select * from information_schema.columns where table_name='FastStartRange' and column_name='Value' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_FastStartRange 'FastStartRange', 'Value', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('FastStartRange') and name = 'Description')
  BEGIN
	ALTER TABLE FastStartRange ADD
	    Description VarChar(250) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='FastStartRange' and column_name='Description') and not exists(select * from information_schema.columns where table_name='FastStartRange' and column_name='Description' and character_maximum_length=250 and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_FastStartRange 'FastStartRange', 'Description', 'VarChar(250)', 1
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_FastStartRange') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE FastStartRange WITH NOCHECK ADD
	CONSTRAINT PK_FastStartRange PRIMARY KEY NONCLUSTERED
	(
		FastStartRangeId
	)
GO


drop procedure #spAlterColumn_FastStartRange
GO
