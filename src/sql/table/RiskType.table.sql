SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_RiskType%' and xtype='P')
drop procedure #spAlterColumn_RiskType
GO

CREATE PROCEDURE #spAlterColumn_RiskType
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[RiskType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.RiskType (
	RiskTypeId Int IDENTITY(1,1) NOT NULL,
	LType Int NOT NULL,
	RiskTypeDesc VarChar(250) NOT NULL
)
GO

if not exists(select * from syscolumns where id=object_id('RiskType') and name = 'RiskTypeId')
  BEGIN
	ALTER TABLE RiskType ADD
	    RiskTypeId Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='RiskType' and column_name='RiskTypeId') and not exists(select * from information_schema.columns where table_name='RiskType' and column_name='RiskTypeId' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_RiskType 'RiskType', 'RiskTypeId', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('RiskType') and name = 'LType')
  BEGIN
	ALTER TABLE RiskType ADD
	    LType Int NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='RiskType' and column_name='LType') and not exists(select * from information_schema.columns where table_name='RiskType' and column_name='LType' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_RiskType 'RiskType', 'LType', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('RiskType') and name = 'RiskTypeDesc')
  BEGIN
	ALTER TABLE RiskType ADD
	    RiskTypeDesc VarChar(250) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='RiskType' and column_name='RiskTypeDesc') and not exists(select * from information_schema.columns where table_name='RiskType' and column_name='RiskTypeDesc' and character_maximum_length=250 and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_RiskType 'RiskType', 'RiskTypeDesc', 'VarChar(250)', 1
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_RiskType') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE RiskType WITH NOCHECK ADD
	CONSTRAINT PK_RiskType PRIMARY KEY NONCLUSTERED
	(
		RiskTypeId
	)
GO


drop procedure #spAlterColumn_RiskType
GO
