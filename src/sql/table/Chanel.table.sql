SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_Chanel%' and xtype='P')
drop procedure #spAlterColumn_Chanel
GO

CREATE PROCEDURE #spAlterColumn_Chanel
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[Chanel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.Chanel (
	ChanelId Int IDENTITY(1,1) NOT NULL,
	ReportCode Int NOT NULL,
	ChanelDesc VarChar(250) NOT NULL
)
GO

if not exists(select * from syscolumns where id=object_id('Chanel') and name = 'ChanelId')
  BEGIN
	ALTER TABLE Chanel ADD
	    ChanelId Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='Chanel' and column_name='ChanelId') and not exists(select * from information_schema.columns where table_name='Chanel' and column_name='ChanelId' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_Chanel 'Chanel', 'ChanelId', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('Chanel') and name = 'ReportCode')
  BEGIN
	ALTER TABLE Chanel ADD
	    ReportCode Int NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='Chanel' and column_name='ReportCode') and not exists(select * from information_schema.columns where table_name='Chanel' and column_name='ReportCode' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_Chanel 'Chanel', 'ReportCode', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('Chanel') and name = 'ChanelDesc')
  BEGIN
	ALTER TABLE Chanel ADD
	    ChanelDesc VarChar(250) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='Chanel' and column_name='ChanelDesc') and not exists(select * from information_schema.columns where table_name='Chanel' and column_name='ChanelDesc' and character_maximum_length=250 and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_Chanel 'Chanel', 'ChanelDesc', 'VarChar(250)', 1
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_ChanelId') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE Chanel WITH NOCHECK ADD
	CONSTRAINT PK_ChanelId PRIMARY KEY NONCLUSTERED
	(
		ChanelId
	)
GO


drop procedure #spAlterColumn_Chanel
GO
