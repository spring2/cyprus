SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_FICORange%' and xtype='P')
drop procedure #spAlterColumn_FICORange
GO

CREATE PROCEDURE #spAlterColumn_FICORange
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[FICORange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.FICORange (
	FICORangeId Int IDENTITY(1,1) NOT NULL,
	Value Int NOT NULL,
	Description VarChar(250) NOT NULL
)
GO

if not exists(select * from syscolumns where id=object_id('FICORange') and name = 'FICORangeId')
  BEGIN
	ALTER TABLE FICORange ADD
	    FICORangeId Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='FICORange' and column_name='FICORangeId') and not exists(select * from information_schema.columns where table_name='FICORange' and column_name='FICORangeId' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_FICORange 'FICORange', 'FICORangeId', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('FICORange') and name = 'Value')
  BEGIN
	ALTER TABLE FICORange ADD
	    Value Int NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='FICORange' and column_name='Value') and not exists(select * from information_schema.columns where table_name='FICORange' and column_name='Value' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_FICORange 'FICORange', 'Value', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('FICORange') and name = 'Description')
  BEGIN
	ALTER TABLE FICORange ADD
	    Description VarChar(250) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='FICORange' and column_name='Description') and not exists(select * from information_schema.columns where table_name='FICORange' and column_name='Description' and character_maximum_length=250 and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_FICORange 'FICORange', 'Description', 'VarChar(250)', 1
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_FICORange') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE FICORange WITH NOCHECK ADD
	CONSTRAINT PK_FICORange PRIMARY KEY NONCLUSTERED
	(
		FICORangeId
	)
GO


drop procedure #spAlterColumn_FICORange
GO
