SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_LoanType%' and xtype='P')
drop procedure #spAlterColumn_LoanType
GO

CREATE PROCEDURE #spAlterColumn_LoanType
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[LoanType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.LoanType (
	LoanTypeId Int IDENTITY(1,1) NOT NULL,
	LType Int NOT NULL,
	LoanDesc VarChar(250) NOT NULL
)
GO

if not exists(select * from syscolumns where id=object_id('LoanType') and name = 'LoanTypeId')
  BEGIN
	ALTER TABLE LoanType ADD
	    LoanTypeId Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanType' and column_name='LoanTypeId') and not exists(select * from information_schema.columns where table_name='LoanType' and column_name='LoanTypeId' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_LoanType 'LoanType', 'LoanTypeId', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanType') and name = 'LType')
  BEGIN
	ALTER TABLE LoanType ADD
	    LType Int NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanType' and column_name='LType') and not exists(select * from information_schema.columns where table_name='LoanType' and column_name='LType' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_LoanType 'LoanType', 'LType', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanType') and name = 'LoanDesc')
  BEGIN
	ALTER TABLE LoanType ADD
	    LoanDesc VarChar(250) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanType' and column_name='LoanDesc') and not exists(select * from information_schema.columns where table_name='LoanType' and column_name='LoanDesc' and character_maximum_length=250 and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_LoanType 'LoanType', 'LoanDesc', 'VarChar(250)', 1
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_LoanType') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE LoanType WITH NOCHECK ADD
	CONSTRAINT PK_LoanType PRIMARY KEY NONCLUSTERED
	(
		LoanTypeId
	)
GO


drop procedure #spAlterColumn_LoanType
GO
