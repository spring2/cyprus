SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_LoanPortfolio%' and xtype='P')
drop procedure #spAlterColumn_LoanPortfolio
GO

CREATE PROCEDURE #spAlterColumn_LoanPortfolio
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[LoanPortfolio]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.LoanPortfolio (
	LoanPortfolio_Id Int IDENTITY(1,1) NOT NULL,
	UpDated DateTime NULL,
	EOMDate Date NULL,
	AccountID VarChar(50) NULL,
	Member VarChar(50) NULL,
	MClass Int NULL,
	Ltype Int NULL,
	LtypeSub Decimal(18, 8) NULL,
	Product Int NULL,
	LoanDesc VarChar(50) NULL,
	LoanDate Date NULL,
	LoanDay VarChar(3) NULL,
	LoanMonth Int NULL,
	LoanYear Int NULL,
	FirstPmtDate Date NULL,
	Status VarChar(1) NULL,
	RptCode Int NULL,
	FundedBr Int NULL,
	LoadedBr Int NULL,
	FICO Int NULL,
	FICORange VarChar(50) NULL,
	FastStart Int NULL,
	FastStartRange VarChar(50) NULL,
	Bankruptcy Int NULL,
	BankruptcyRange VarChar(50) NULL,
	MembershipDate Date NULL,
	NewMember VarChar(1) NULL,
	RiskLev VarChar(1) NULL,
	Traffic Int NULL,
	TrafficColor VarChar(1) NULL,
	MemAge Int NULL,
	MemSex VarChar(1) NULL,
	PmtFreq VarChar(2) NULL,
	OrigTerm Int NULL,
	OrigRate Decimal(18, 2) NULL,
	OrigPmt Decimal(18, 2) NULL,
	OrigAmount Decimal(18, 2) NULL,
	OrigCrLim Decimal(18, 2) NULL,
	CurTerm Int NULL,
	CurRate Decimal(18, 2) NULL,
	CurPmt Decimal(18, 2) NULL,
	CurBal Decimal(18, 2) NULL,
	CurCrLim Decimal(18, 2) NULL,
	BalloonDate Date NULL,
	MatDate Date NULL,
	InsCode Int NULL,
	CollateralValue Decimal(18, 2) NULL,
	LTV Decimal(18, 2) NULL,
	SecType Int NULL,
	SecDesc1 VarChar(250) NULL,
	SecDesc2 VarChar(250) NULL,
	DebtRatio Decimal(18, 2) NULL,
	Dealer Int NULL,
	DealerName VarChar(100) NULL,
	CoborComak VarChar(1) NULL,
	RelPriceGrp VarChar(50) NULL,
	RelPriceDisc Int NULL,
	OtherDisc Decimal(18, 2) NULL,
	DiscReason VarChar(50) NULL,
	RiskOffset Decimal(18, 2) NULL,
	CreditType VarChar(50) NULL,
	StatedIncome VarChar(100) NULL,
	Extensions VarChar(100) NULL,
	FirstExtDate VarChar(100) NULL,
	LastExtDate VarChar(100) NULL,
	RiskLevel VarChar(100) NULL,
	RiskDate VarChar(100) NULL,
	Watch VarChar(100) NULL,
	WatchDate VarChar(100) NULL,
	WorkOut VarChar(100) NULL,
	WorkOutDate VarChar(100) NULL,
	FirstMortType VarChar(100) NULL,
	BusPerReg VarChar(100) NULL,
	BusGroup VarChar(100) NULL,
	BusDesc VarChar(100) NULL,
	BusPart VarChar(100) NULL,
	BusPartPerc VarChar(100) NULL,
	SBAguar VarChar(100) NULL,
	DeliDays Int NULL,
	DeliSect Int NULL,
	DeliHist VarChar(250) NULL,
	DeliHistQ1 VarChar(1) NULL,
	DeliHistQ2 VarChar(1) NULL,
	DeliHistQ3 VarChar(1) NULL,
	DeliHistQ4 VarChar(1) NULL,
	DeliHistQ5 VarChar(1) NULL,
	DeliHistQ6 VarChar(1) NULL,
	DeliHistQ7 VarChar(1) NULL,
	DeliHistQ8 VarChar(1) NULL,
	ChrgOffDate Date NULL,
	ChrgOffMonth Int NULL,
	ChrgOffYear Int NULL,
	ChrgOffMnthsBF Int NULL,
	PropType VarChar(50) NULL,
	Occupancy VarChar(50) NULL,
	OrigApprVal Decimal(18, 2) NULL,
	OrigApprDate Date NULL,
	CurrApprVal Decimal(18, 2) NULL,
	CurrApprDate Date NULL,
	FirstMortBal Decimal(18, 2) NULL,
	FirstMortMoPyt Decimal(18, 2) NULL,
	SecondMortBal Decimal(18, 2) NULL,
	SecondMortPymt Decimal(18, 2) NULL,
	TaxInsNotInc Decimal(18, 2) NULL,
	LoadOff Int NULL,
	ApprOff Int NULL,
	FundingOpp Int NULL,
	Chanel VarChar(250) NULL,
	RiskType VarChar(250) NULL,
	Gap VarChar(250) NULL,
	LoadOperator Int NULL,
	DeliAmt Decimal(18, 2) NULL,
	ChgOffAmt Decimal(18, 2) NULL,
	UsSecDesc VarChar(250) NULL,
	TroubledDebt VarChar(250) NULL,
	TdrDate VarChar(250) NULL,
	JntFicoUsed VarChar(250) NULL,
	SingleOrJointLoan VarChar(250) NULL,
	ApprovingOfficerUD VarChar(250) NULL,
	InsuranceCodeDesc VarChar(250) NULL,
	GapIns VarChar(250) NULL,
	MmpIns VarChar(250) NULL,
	ReportCodeDesc VarChar(250) NULL,
	BallonPmtLoan VarChar(250) NULL,
	FixedOrVariableRateLoan VarChar(250) NULL,
	AlpsOfficer VarChar(250) NULL,
	CUDLAppNumber VarChar(250) NULL,
	FasbDefCost Decimal(18, 2) NULL,
	FasbLtdAmortFees Decimal(18, 2) NULL,
	InterestPriorYtd Decimal(18, 2) NULL,
	AccruedInterest Decimal(18, 2) NULL,
	InterestLTD Decimal(18, 2) NULL,
	InterestYtd Decimal(18, 2) NULL,
	PartialPayAmt Decimal(18, 2) NULL,
	InterestUncollected Decimal(18, 2) NULL,
	ClosedAcctDate Date NULL,
	LastTranDate Date NULL,
	NextDueDate Date NULL,
	InsuranceCode Int NULL,
	MbrAgeAtApproval Int NULL
)
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoanPortfolio_Id')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoanPortfolio_Id Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanPortfolio_Id') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanPortfolio_Id' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoanPortfolio_Id', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'UpDated')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    UpDated DateTime NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='UpDated') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='UpDated' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'UpDated', 'DateTime', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'EOMDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    EOMDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='EOMDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='EOMDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'EOMDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'AccountID')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    AccountID VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='AccountID') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='AccountID' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'AccountID', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Member')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Member VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Member') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Member' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Member', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MClass')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MClass Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MClass') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MClass' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MClass', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Ltype')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Ltype Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Ltype') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Ltype' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Ltype', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LtypeSub')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LtypeSub Decimal(18, 8) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LtypeSub') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LtypeSub' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LtypeSub', 'Decimal(18, 8)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Product')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Product Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Product') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Product' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Product', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoanDesc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoanDesc VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanDesc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanDesc' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoanDesc', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoanDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoanDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoanDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoanDay')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoanDay VarChar(3) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanDay') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanDay' and character_maximum_length=3 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoanDay', 'VarChar(3)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoanMonth')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoanMonth Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanMonth') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanMonth' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoanMonth', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoanYear')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoanYear Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanYear') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoanYear' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoanYear', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FirstPmtDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FirstPmtDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstPmtDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstPmtDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FirstPmtDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Status')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Status VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Status') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Status' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Status', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RptCode')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RptCode Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RptCode') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RptCode' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RptCode', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FundedBr')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FundedBr Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FundedBr') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FundedBr' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FundedBr', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoadedBr')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoadedBr Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoadedBr') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoadedBr' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoadedBr', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FICO')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FICO Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FICO') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FICO' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FICO', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FICORange')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FICORange VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FICORange') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FICORange' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FICORange', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FastStart')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FastStart Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FastStart') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FastStart' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FastStart', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FastStartRange')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FastStartRange VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FastStartRange') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FastStartRange' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FastStartRange', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Bankruptcy')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Bankruptcy Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Bankruptcy') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Bankruptcy' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Bankruptcy', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BankruptcyRange')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BankruptcyRange VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BankruptcyRange') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BankruptcyRange' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BankruptcyRange', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MembershipDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MembershipDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MembershipDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MembershipDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MembershipDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'NewMember')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    NewMember VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='NewMember') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='NewMember' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'NewMember', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RiskLev')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RiskLev VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskLev') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskLev' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RiskLev', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Traffic')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Traffic Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Traffic') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Traffic' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Traffic', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'TrafficColor')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    TrafficColor VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TrafficColor') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TrafficColor' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'TrafficColor', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MemAge')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MemAge Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MemAge') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MemAge' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MemAge', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MemSex')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MemSex VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MemSex') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MemSex' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MemSex', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'PmtFreq')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    PmtFreq VarChar(2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='PmtFreq') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='PmtFreq' and character_maximum_length=2 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'PmtFreq', 'VarChar(2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigTerm')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigTerm Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigTerm') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigTerm' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigTerm', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigRate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigRate Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigRate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigRate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigRate', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigPmt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigPmt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigPmt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigPmt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigPmt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigAmount')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigAmount Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigAmount') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigAmount' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigAmount', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigCrLim')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigCrLim Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigCrLim') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigCrLim' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigCrLim', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurTerm')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurTerm Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurTerm') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurTerm' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurTerm', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurRate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurRate Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurRate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurRate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurRate', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurPmt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurPmt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurPmt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurPmt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurPmt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurBal')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurBal Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurBal') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurBal' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurBal', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurCrLim')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurCrLim Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurCrLim') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurCrLim' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurCrLim', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BalloonDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BalloonDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BalloonDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BalloonDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BalloonDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MatDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MatDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MatDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MatDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MatDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InsCode')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InsCode Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InsCode') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InsCode' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InsCode', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CollateralValue')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CollateralValue Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CollateralValue') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CollateralValue' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CollateralValue', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LTV')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LTV Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LTV') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LTV' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LTV', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SecType')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SecType Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecType') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecType' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SecType', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SecDesc1')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SecDesc1 VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecDesc1') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecDesc1' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SecDesc1', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SecDesc2')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SecDesc2 VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecDesc2') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecDesc2' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SecDesc2', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DebtRatio')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DebtRatio Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DebtRatio') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DebtRatio' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DebtRatio', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Dealer')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Dealer Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Dealer') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Dealer' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Dealer', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DealerName')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DealerName VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DealerName') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DealerName' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DealerName', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CoborComak')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CoborComak VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CoborComak') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CoborComak' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CoborComak', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RelPriceGrp')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RelPriceGrp VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RelPriceGrp') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RelPriceGrp' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RelPriceGrp', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RelPriceDisc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RelPriceDisc Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RelPriceDisc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RelPriceDisc' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RelPriceDisc', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OtherDisc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OtherDisc Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OtherDisc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OtherDisc' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OtherDisc', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DiscReason')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DiscReason VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DiscReason') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DiscReason' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DiscReason', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RiskOffset')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RiskOffset Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskOffset') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskOffset' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RiskOffset', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CreditType')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CreditType VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CreditType') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CreditType' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CreditType', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'StatedIncome')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    StatedIncome VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='StatedIncome') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='StatedIncome' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'StatedIncome', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Extensions')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Extensions VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Extensions') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Extensions' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Extensions', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FirstExtDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FirstExtDate VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstExtDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstExtDate' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FirstExtDate', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LastExtDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LastExtDate VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LastExtDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LastExtDate' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LastExtDate', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RiskLevel')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RiskLevel VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskLevel') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskLevel' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RiskLevel', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RiskDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RiskDate VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskDate' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RiskDate', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Watch')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Watch VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Watch') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Watch' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Watch', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'WatchDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    WatchDate VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='WatchDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='WatchDate' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'WatchDate', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'WorkOut')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    WorkOut VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='WorkOut') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='WorkOut' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'WorkOut', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'WorkOutDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    WorkOutDate VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='WorkOutDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='WorkOutDate' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'WorkOutDate', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FirstMortType')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FirstMortType VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstMortType') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstMortType' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FirstMortType', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BusPerReg')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BusPerReg VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusPerReg') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusPerReg' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BusPerReg', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BusGroup')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BusGroup VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusGroup') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusGroup' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BusGroup', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BusDesc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BusDesc VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusDesc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusDesc' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BusDesc', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BusPart')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BusPart VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusPart') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusPart' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BusPart', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BusPartPerc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BusPartPerc VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusPartPerc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BusPartPerc' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BusPartPerc', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SBAguar')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SBAguar VarChar(100) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SBAguar') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SBAguar' and character_maximum_length=100 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SBAguar', 'VarChar(100)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliDays')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliDays Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliDays') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliDays' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliDays', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliSect')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliSect Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliSect') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliSect' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliSect', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHist')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHist VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHist') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHist' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHist', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ1')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ1 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ1') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ1' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ1', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ2')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ2 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ2') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ2' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ2', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ3')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ3 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ3') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ3' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ3', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ4')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ4 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ4') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ4' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ4', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ5')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ5 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ5') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ5' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ5', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ6')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ6 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ6') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ6' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ6', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ7')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ7 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ7') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ7' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ7', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliHistQ8')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliHistQ8 VarChar(1) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ8') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliHistQ8' and character_maximum_length=1 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliHistQ8', 'VarChar(1)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ChrgOffDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ChrgOffDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ChrgOffDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ChrgOffMonth')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ChrgOffMonth Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffMonth') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffMonth' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ChrgOffMonth', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ChrgOffYear')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ChrgOffYear Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffYear') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffYear' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ChrgOffYear', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ChrgOffMnthsBF')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ChrgOffMnthsBF Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffMnthsBF') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChrgOffMnthsBF' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ChrgOffMnthsBF', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'PropType')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    PropType VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='PropType') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='PropType' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'PropType', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Occupancy')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Occupancy VarChar(50) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Occupancy') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Occupancy' and character_maximum_length=50 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Occupancy', 'VarChar(50)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigApprVal')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigApprVal Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigApprVal') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigApprVal' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigApprVal', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'OrigApprDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    OrigApprDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigApprDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='OrigApprDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'OrigApprDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurrApprVal')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurrApprVal Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurrApprVal') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurrApprVal' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurrApprVal', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CurrApprDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CurrApprDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurrApprDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CurrApprDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CurrApprDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FirstMortBal')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FirstMortBal Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstMortBal') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstMortBal' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FirstMortBal', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FirstMortMoPyt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FirstMortMoPyt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstMortMoPyt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FirstMortMoPyt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FirstMortMoPyt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SecondMortBal')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SecondMortBal Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecondMortBal') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecondMortBal' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SecondMortBal', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SecondMortPymt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SecondMortPymt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecondMortPymt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SecondMortPymt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SecondMortPymt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'TaxInsNotInc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    TaxInsNotInc Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TaxInsNotInc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TaxInsNotInc' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'TaxInsNotInc', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoadOff')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoadOff Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoadOff') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoadOff' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoadOff', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ApprOff')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ApprOff Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ApprOff') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ApprOff' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ApprOff', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FundingOpp')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FundingOpp Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FundingOpp') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FundingOpp' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FundingOpp', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Chanel')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Chanel VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Chanel') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Chanel' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Chanel', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'RiskType')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    RiskType VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskType') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='RiskType' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'RiskType', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'Gap')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    Gap VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Gap') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='Gap' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'Gap', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LoadOperator')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LoadOperator Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoadOperator') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LoadOperator' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LoadOperator', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'DeliAmt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    DeliAmt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliAmt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='DeliAmt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'DeliAmt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ChgOffAmt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ChgOffAmt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChgOffAmt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ChgOffAmt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ChgOffAmt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'UsSecDesc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    UsSecDesc VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='UsSecDesc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='UsSecDesc' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'UsSecDesc', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'TroubledDebt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    TroubledDebt VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TroubledDebt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TroubledDebt' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'TroubledDebt', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'TdrDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    TdrDate VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TdrDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='TdrDate' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'TdrDate', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'JntFicoUsed')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    JntFicoUsed VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='JntFicoUsed') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='JntFicoUsed' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'JntFicoUsed', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'SingleOrJointLoan')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    SingleOrJointLoan VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SingleOrJointLoan') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='SingleOrJointLoan' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'SingleOrJointLoan', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ApprovingOfficerUD')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ApprovingOfficerUD VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ApprovingOfficerUD') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ApprovingOfficerUD' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ApprovingOfficerUD', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InsuranceCodeDesc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InsuranceCodeDesc VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InsuranceCodeDesc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InsuranceCodeDesc' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InsuranceCodeDesc', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'GapIns')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    GapIns VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='GapIns') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='GapIns' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'GapIns', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MmpIns')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MmpIns VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MmpIns') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MmpIns' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MmpIns', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ReportCodeDesc')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ReportCodeDesc VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ReportCodeDesc') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ReportCodeDesc' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ReportCodeDesc', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'BallonPmtLoan')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    BallonPmtLoan VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BallonPmtLoan') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='BallonPmtLoan' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'BallonPmtLoan', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FixedOrVariableRateLoan')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FixedOrVariableRateLoan VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FixedOrVariableRateLoan') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FixedOrVariableRateLoan' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FixedOrVariableRateLoan', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'AlpsOfficer')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    AlpsOfficer VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='AlpsOfficer') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='AlpsOfficer' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'AlpsOfficer', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'CUDLAppNumber')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    CUDLAppNumber VarChar(250) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CUDLAppNumber') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='CUDLAppNumber' and character_maximum_length=250 and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'CUDLAppNumber', 'VarChar(250)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FasbDefCost')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FasbDefCost Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FasbDefCost') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FasbDefCost' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FasbDefCost', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'FasbLtdAmortFees')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    FasbLtdAmortFees Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FasbLtdAmortFees') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='FasbLtdAmortFees' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'FasbLtdAmortFees', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InterestPriorYtd')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InterestPriorYtd Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestPriorYtd') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestPriorYtd' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InterestPriorYtd', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'AccruedInterest')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    AccruedInterest Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='AccruedInterest') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='AccruedInterest' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'AccruedInterest', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InterestLTD')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InterestLTD Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestLTD') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestLTD' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InterestLTD', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InterestYtd')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InterestYtd Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestYtd') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestYtd' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InterestYtd', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'PartialPayAmt')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    PartialPayAmt Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='PartialPayAmt') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='PartialPayAmt' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'PartialPayAmt', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InterestUncollected')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InterestUncollected Decimal(18, 2) NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestUncollected') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InterestUncollected' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InterestUncollected', 'Decimal(18, 2)', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'ClosedAcctDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    ClosedAcctDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ClosedAcctDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='ClosedAcctDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'ClosedAcctDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'LastTranDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    LastTranDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LastTranDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='LastTranDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'LastTranDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'NextDueDate')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    NextDueDate Date NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='NextDueDate') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='NextDueDate' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'NextDueDate', 'Date', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'InsuranceCode')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    InsuranceCode Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InsuranceCode') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='InsuranceCode' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'InsuranceCode', 'Int', 0
  END
GO

if not exists(select * from syscolumns where id=object_id('LoanPortfolio') and name = 'MbrAgeAtApproval')
  BEGIN
	ALTER TABLE LoanPortfolio ADD
	    MbrAgeAtApproval Int NULL
  END
GO


if exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MbrAgeAtApproval') and not exists(select * from information_schema.columns where table_name='LoanPortfolio' and column_name='MbrAgeAtApproval' and is_nullable='YES')
  BEGIN
	exec #spAlterColumn_LoanPortfolio 'LoanPortfolio', 'MbrAgeAtApproval', 'Int', 0
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_LoanPortfolio') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE LoanPortfolio WITH NOCHECK ADD
	CONSTRAINT PK_LoanPortfolio PRIMARY KEY NONCLUSTERED
	(
		LoanPortfolio_Id
	)
GO

if not exists (select * from sysindexes where name='IxLoanPortfolio_AccountID' and id=object_id(N'[LoanPortfolio]'))
	CREATE INDEX IxLoanPortfolio_AccountID ON LoanPortfolio
	(
        	AccountID
	)
GO
if not exists (select * from sysindexes where name='IxLoanPortfolio_Member' and id=object_id(N'[LoanPortfolio]'))
	CREATE INDEX IxLoanPortfolio_Member ON LoanPortfolio
	(
        	Member
	)
GO
if not exists (select * from sysindexes where name='IxLoanPortfolio_Ltype' and id=object_id(N'[LoanPortfolio]'))
	CREATE INDEX IxLoanPortfolio_Ltype ON LoanPortfolio
	(
        	Ltype
	)
GO

drop procedure #spAlterColumn_LoanPortfolio
GO
