SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from tempdb..sysobjects where name like '#spAlterColumn_BankruptcyRange%' and xtype='P')
drop procedure #spAlterColumn_BankruptcyRange
GO

CREATE PROCEDURE #spAlterColumn_BankruptcyRange
    @table varchar(100),
    @column varchar(100),
    @type varchar(50),
    @required bit
AS
if exists (select * from syscolumns where name=@column and id=object_id(@table))
begin
	declare @nullstring varchar(8)
	set @nullstring = case when @required=0 then 'null' else 'not null' end
	exec('alter table [' + @table + '] alter column [' + @column + '] ' + @type + ' ' + @nullstring)
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[BankruptcyRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.BankruptcyRange (
	BankruptcyRangeId Int IDENTITY(1,1) NOT NULL,
	Value Int NOT NULL,
	Description VarChar(250) NOT NULL
)
GO

if not exists(select * from syscolumns where id=object_id('BankruptcyRange') and name = 'BankruptcyRangeId')
  BEGIN
	ALTER TABLE BankruptcyRange ADD
	    BankruptcyRangeId Int IDENTITY(1,1) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='BankruptcyRange' and column_name='BankruptcyRangeId') and not exists(select * from information_schema.columns where table_name='BankruptcyRange' and column_name='BankruptcyRangeId' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_BankruptcyRange 'BankruptcyRange', 'BankruptcyRangeId', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('BankruptcyRange') and name = 'Value')
  BEGIN
	ALTER TABLE BankruptcyRange ADD
	    Value Int NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='BankruptcyRange' and column_name='Value') and not exists(select * from information_schema.columns where table_name='BankruptcyRange' and column_name='Value' and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_BankruptcyRange 'BankruptcyRange', 'Value', 'Int', 1
  END
GO

if not exists(select * from syscolumns where id=object_id('BankruptcyRange') and name = 'Description')
  BEGIN
	ALTER TABLE BankruptcyRange ADD
	    Description VarChar(250) NOT NULL
  END
GO


if exists(select * from information_schema.columns where table_name='BankruptcyRange' and column_name='Description') and not exists(select * from information_schema.columns where table_name='BankruptcyRange' and column_name='Description' and character_maximum_length=250 and is_nullable='NO')
  BEGIN
	exec #spAlterColumn_BankruptcyRange 'BankruptcyRange', 'Description', 'VarChar(250)', 1
  END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_BankruptcyRange') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE BankruptcyRange WITH NOCHECK ADD
	CONSTRAINT PK_BankruptcyRange PRIMARY KEY NONCLUSTERED
	(
		BankruptcyRangeId
	)
GO


drop procedure #spAlterColumn_BankruptcyRange
GO
