if exists (select * from sysobjects where id = object_id(N'dbo.[vwFICORange]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.[vwFICORange]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.[vwFICORange]

AS

SELECT
    FICORange.FICORangeId,
    FICORange.Value,
    FICORange.Description
FROM
    FICORange
GO
