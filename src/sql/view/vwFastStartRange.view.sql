if exists (select * from sysobjects where id = object_id(N'dbo.[vwFastStartRange]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.[vwFastStartRange]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.[vwFastStartRange]

AS

SELECT
    FastStartRange.FastStartRangeId,
    FastStartRange.Value,
    FastStartRange.Description
FROM
    FastStartRange
GO
