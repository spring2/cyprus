if exists (select * from sysobjects where id = object_id(N'dbo.[vwRiskType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.[vwRiskType]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.[vwRiskType]

AS

SELECT
    RiskType.RiskTypeId,
    RiskType.LType,
    RiskType.RiskTypeDesc
FROM
    RiskType
GO
