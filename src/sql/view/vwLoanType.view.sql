if exists (select * from sysobjects where id = object_id(N'dbo.[vwLoanType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.[vwLoanType]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.[vwLoanType]

AS

SELECT
    LoanType.LoanTypeId,
    LoanType.LType,
    LoanType.LoanDesc
FROM
    LoanType
GO
