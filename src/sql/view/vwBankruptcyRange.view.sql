if exists (select * from sysobjects where id = object_id(N'dbo.[vwBankruptcyRange]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.[vwBankruptcyRange]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.[vwBankruptcyRange]

AS

SELECT
    BankruptcyRange.BankruptcyRangeId,
    BankruptcyRange.Value,
    BankruptcyRange.Description
FROM
    BankruptcyRange
GO
