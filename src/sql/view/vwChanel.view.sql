if exists (select * from sysobjects where id = object_id(N'dbo.[vwChanel]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.[vwChanel]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.[vwChanel]

AS

SELECT
    Chanel.ChanelId,
    Chanel.ReportCode,
    Chanel.ChanelDesc
FROM
    Chanel
GO
