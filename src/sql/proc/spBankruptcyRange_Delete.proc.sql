if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spBankruptcyRange_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spBankruptcyRange_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spBankruptcyRange_Delete

@BankruptcyRangeId	Int

AS

if not exists(SELECT * FROM BankruptcyRange WHERE (	BankruptcyRangeId = @BankruptcyRangeId
))
    BEGIN
        RAISERROR  ('spBankruptcyRange_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	BankruptcyRange
WHERE 
	BankruptcyRangeId = @BankruptcyRangeId
GO

