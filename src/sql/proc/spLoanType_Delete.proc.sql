if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLoanType_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLoanType_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spLoanType_Delete

@LoanTypeId	Int

AS

if not exists(SELECT * FROM LoanType WHERE (	LoanTypeId = @LoanTypeId
))
    BEGIN
        RAISERROR  ('spLoanType_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	LoanType
WHERE 
	LoanTypeId = @LoanTypeId
GO

