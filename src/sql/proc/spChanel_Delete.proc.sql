if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spChanel_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spChanel_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spChanel_Delete

@ChanelId	Int

AS

if not exists(SELECT * FROM Chanel WHERE (	ChanelId = @ChanelId
))
    BEGIN
        RAISERROR  ('spChanel_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	Chanel
WHERE 
	ChanelId = @ChanelId
GO

