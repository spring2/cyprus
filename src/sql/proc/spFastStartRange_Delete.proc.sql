if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spFastStartRange_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spFastStartRange_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spFastStartRange_Delete

@FastStartRangeId	Int

AS

if not exists(SELECT * FROM FastStartRange WHERE (	FastStartRangeId = @FastStartRangeId
))
    BEGIN
        RAISERROR  ('spFastStartRange_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	FastStartRange
WHERE 
	FastStartRangeId = @FastStartRangeId
GO

