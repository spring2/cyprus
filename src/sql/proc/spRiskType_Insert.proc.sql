if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spRiskType_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spRiskType_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spRiskType_Insert
	@LType	Int = null,
	@RiskTypeDesc	VarChar(250) = null
AS


INSERT INTO RiskType
(	LType,
	RiskTypeDesc)
VALUES (
	@LType,
	@RiskTypeDesc)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spRiskType_Insert: Unable to insert new row into RiskType table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

