if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLoanType_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLoanType_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spLoanType_Insert
	@LType	Int = null,
	@LoanDesc	VarChar(250) = null
AS


INSERT INTO LoanType
(	LType,
	LoanDesc)
VALUES (
	@LType,
	@LoanDesc)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spLoanType_Insert: Unable to insert new row into LoanType table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

