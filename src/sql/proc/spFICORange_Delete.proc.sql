if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spFICORange_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spFICORange_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spFICORange_Delete

@FICORangeId	Int

AS

if not exists(SELECT * FROM FICORange WHERE (	FICORangeId = @FICORangeId
))
    BEGIN
        RAISERROR  ('spFICORange_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	FICORange
WHERE 
	FICORangeId = @FICORangeId
GO

