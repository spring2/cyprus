if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spChanel_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spChanel_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spChanel_Insert
	@ReportCode	Int = null,
	@ChanelDesc	VarChar(250) = null
AS


INSERT INTO Chanel
(	ReportCode,
	ChanelDesc)
VALUES (
	@ReportCode,
	@ChanelDesc)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spChanel_Insert: Unable to insert new row into Chanel table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

