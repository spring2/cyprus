if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spDeleteLoanPortfolioByEomDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spDeleteLoanPortfolioByEomDate]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spDeleteLoanPortfolioByEomDate

@EOMDate Date

AS

DELETE
FROM
	LoanPortfolio
WHERE 
	EOMDate = @EOMDate
GO