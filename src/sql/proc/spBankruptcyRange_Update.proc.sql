if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spBankruptcyRange_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spBankruptcyRange_Update]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spBankruptcyRange_Update

	@BankruptcyRangeId	Int = null,
	@Value	Int = null,
	@Description	VarChar(250) = null
AS


UPDATE
	BankruptcyRange
SET
	Value = @Value,
	Description = @Description
WHERE
BankruptcyRangeId = @BankruptcyRangeId


if @@ROWCOUNT <> 1
    BEGIN
        RAISERROR  ('spBankruptcyRange_Update:  update was expected to update a single row and updated %d rows', 16,1, @@ROWCOUNT)
        RETURN(-1)
    END
GO

