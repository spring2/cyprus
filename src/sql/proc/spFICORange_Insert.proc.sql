if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spFICORange_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spFICORange_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spFICORange_Insert
	@Value	Int = null,
	@Description	VarChar(250) = null
AS


INSERT INTO FICORange
(	Value,
	Description)
VALUES (
	@Value,
	@Description)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spFICORange_Insert: Unable to insert new row into FICORange table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

