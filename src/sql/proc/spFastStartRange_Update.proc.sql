if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spFastStartRange_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spFastStartRange_Update]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spFastStartRange_Update

	@FastStartRangeId	Int = null,
	@Value	Int = null,
	@Description	VarChar(250) = null
AS


UPDATE
	FastStartRange
SET
	Value = @Value,
	Description = @Description
WHERE
FastStartRangeId = @FastStartRangeId


if @@ROWCOUNT <> 1
    BEGIN
        RAISERROR  ('spFastStartRange_Update:  update was expected to update a single row and updated %d rows', 16,1, @@ROWCOUNT)
        RETURN(-1)
    END
GO

