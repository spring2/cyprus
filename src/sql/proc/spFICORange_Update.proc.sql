if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spFICORange_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spFICORange_Update]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spFICORange_Update

	@FICORangeId	Int = null,
	@Value	Int = null,
	@Description	VarChar(250) = null
AS


UPDATE
	FICORange
SET
	Value = @Value,
	Description = @Description
WHERE
FICORangeId = @FICORangeId


if @@ROWCOUNT <> 1
    BEGIN
        RAISERROR  ('spFICORange_Update:  update was expected to update a single row and updated %d rows', 16,1, @@ROWCOUNT)
        RETURN(-1)
    END
GO

