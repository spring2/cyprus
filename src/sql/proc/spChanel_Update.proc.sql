if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spChanel_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spChanel_Update]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spChanel_Update

	@ChanelId	Int = null,
	@ReportCode	Int = null,
	@ChanelDesc	VarChar(250) = null
AS


UPDATE
	Chanel
SET
	ReportCode = @ReportCode,
	ChanelDesc = @ChanelDesc
WHERE
ChanelId = @ChanelId


if @@ROWCOUNT <> 1
    BEGIN
        RAISERROR  ('spChanel_Update:  update was expected to update a single row and updated %d rows', 16,1, @@ROWCOUNT)
        RETURN(-1)
    END
GO

