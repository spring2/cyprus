if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLoanPortfolio_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLoanPortfolio_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spLoanPortfolio_Insert
	@UpDated	DateTime = null,
	@EOMDate	Date = null,
	@AccountID	VarChar(50) = null,
	@Member	VarChar(50) = null,
	@MClass	Int = null,
	@Ltype	Int = null,
	@LtypeSub	Decimal(18, 8) = null,
	@Product	Int = null,
	@LoanDesc	VarChar(50) = null,
	@LoanDate	Date = null,
	@LoanDay	VarChar(3) = null,
	@LoanMonth	Int = null,
	@LoanYear	Int = null,
	@FirstPmtDate	Date = null,
	@Status	VarChar(1) = null,
	@RptCode	Int = null,
	@FundedBr	Int = null,
	@LoadedBr	Int = null,
	@FICO	Int = null,
	@FICORange	VarChar(50) = null,
	@FastStart	Int = null,
	@FastStartRange	VarChar(50) = null,
	@Bankruptcy	Int = null,
	@BankruptcyRange	VarChar(50) = null,
	@MembershipDate	Date = null,
	@NewMember	VarChar(1) = null,
	@RiskLev	VarChar(1) = null,
	@Traffic	Int = null,
	@TrafficColor	VarChar(1) = null,
	@MemAge	Int = null,
	@MemSex	VarChar(1) = null,
	@PmtFreq	VarChar(2) = null,
	@OrigTerm	Int = null,
	@OrigRate	Decimal(18, 2) = null,
	@OrigPmt	Decimal(18, 2) = null,
	@OrigAmount	Decimal(18, 2) = null,
	@OrigCrLim	Decimal(18, 2) = null,
	@CurTerm	Int = null,
	@CurRate	Decimal(18, 2) = null,
	@CurPmt	Decimal(18, 2) = null,
	@CurBal	Decimal(18, 2) = null,
	@CurCrLim	Decimal(18, 2) = null,
	@BalloonDate	Date = null,
	@MatDate	Date = null,
	@InsCode	Int = null,
	@CollateralValue	Decimal(18, 2) = null,
	@LTV	Decimal(18, 2) = null,
	@SecType	Int = null,
	@SecDesc1	VarChar(250) = null,
	@SecDesc2	VarChar(250) = null,
	@DebtRatio	Decimal(18, 2) = null,
	@Dealer	Int = null,
	@DealerName	VarChar(100) = null,
	@CoborComak	VarChar(1) = null,
	@RelPriceGrp	VarChar(50) = null,
	@RelPriceDisc	Int = null,
	@OtherDisc	Decimal(18, 2) = null,
	@DiscReason	VarChar(50) = null,
	@RiskOffset	Decimal(18, 2) = null,
	@CreditType	VarChar(50) = null,
	@StatedIncome	VarChar(100) = null,
	@Extensions	VarChar(100) = null,
	@FirstExtDate	VarChar(100) = null,
	@LastExtDate	VarChar(100) = null,
	@RiskLevel	VarChar(100) = null,
	@RiskDate	VarChar(100) = null,
	@Watch	VarChar(100) = null,
	@WatchDate	VarChar(100) = null,
	@WorkOut	VarChar(100) = null,
	@WorkOutDate	VarChar(100) = null,
	@FirstMortType	VarChar(100) = null,
	@BusPerReg	VarChar(100) = null,
	@BusGroup	VarChar(100) = null,
	@BusDesc	VarChar(100) = null,
	@BusPart	VarChar(100) = null,
	@BusPartPerc	VarChar(100) = null,
	@SBAguar	VarChar(100) = null,
	@DeliDays	Int = null,
	@DeliSect	Int = null,
	@DeliHist	VarChar(250) = null,
	@DeliHistQ1	VarChar(1) = null,
	@DeliHistQ2	VarChar(1) = null,
	@DeliHistQ3	VarChar(1) = null,
	@DeliHistQ4	VarChar(1) = null,
	@DeliHistQ5	VarChar(1) = null,
	@DeliHistQ6	VarChar(1) = null,
	@DeliHistQ7	VarChar(1) = null,
	@DeliHistQ8	VarChar(1) = null,
	@ChrgOffDate	Date = null,
	@ChrgOffMonth	Int = null,
	@ChrgOffYear	Int = null,
	@ChrgOffMnthsBF	Int = null,
	@PropType	VarChar(50) = null,
	@Occupancy	VarChar(50) = null,
	@OrigApprVal	Decimal(18, 2) = null,
	@OrigApprDate	Date = null,
	@CurrApprVal	Decimal(18, 2) = null,
	@CurrApprDate	Date = null,
	@FirstMortBal	Decimal(18, 2) = null,
	@FirstMortMoPyt	Decimal(18, 2) = null,
	@SecondMortBal	Decimal(18, 2) = null,
	@SecondMortPymt	Decimal(18, 2) = null,
	@TaxInsNotInc	Decimal(18, 2) = null,
	@LoadOff	Int = null,
	@ApprOff	Int = null,
	@FundingOpp	Int = null,
	@Chanel	VarChar(250) = null,
	@RiskType	VarChar(250) = null,
	@Gap	VarChar(250) = null,
	@LoadOperator	Int = null,
	@DeliAmt	Decimal(18, 2) = null,
	@ChgOffAmt	Decimal(18, 2) = null,
	@UsSecDesc	VarChar(250) = null,
	@TroubledDebt	VarChar(250) = null,
	@TdrDate	VarChar(250) = null,
	@JntFicoUsed	VarChar(250) = null,
	@SingleOrJointLoan	VarChar(250) = null,
	@ApprovingOfficerUD	VarChar(250) = null,
	@InsuranceCodeDesc	VarChar(250) = null,
	@GapIns	VarChar(250) = null,
	@MmpIns	VarChar(250) = null,
	@ReportCodeDesc	VarChar(250) = null,
	@BallonPmtLoan	VarChar(250) = null,
	@FixedOrVariableRateLoan	VarChar(250) = null,
	@AlpsOfficer	VarChar(250) = null,
	@CUDLAppNumber	VarChar(250) = null,
	@FasbDefCost	Decimal(18, 2) = null,
	@FasbLtdAmortFees	Decimal(18, 2) = null,
	@InterestPriorYtd	Decimal(18, 2) = null,
	@AccruedInterest	Decimal(18, 2) = null,
	@InterestLTD	Decimal(18, 2) = null,
	@InterestYtd	Decimal(18, 2) = null,
	@PartialPayAmt	Decimal(18, 2) = null,
	@InterestUncollected	Decimal(18, 2) = null,
	@ClosedAcctDate	Date = null,
	@LastTranDate	Date = null,
	@NextDueDate	Date = null,
	@InsuranceCode	Int = null,
	@MbrAgeAtApproval	Int = null
AS


INSERT INTO LoanPortfolio
(	UpDated,
	EOMDate,
	AccountID,
	Member,
	MClass,
	Ltype,
	LtypeSub,
	Product,
	LoanDesc,
	LoanDate,
	LoanDay,
	LoanMonth,
	LoanYear,
	FirstPmtDate,
	Status,
	RptCode,
	FundedBr,
	LoadedBr,
	FICO,
	FICORange,
	FastStart,
	FastStartRange,
	Bankruptcy,
	BankruptcyRange,
	MembershipDate,
	NewMember,
	RiskLev,
	Traffic,
	TrafficColor,
	MemAge,
	MemSex,
	PmtFreq,
	OrigTerm,
	OrigRate,
	OrigPmt,
	OrigAmount,
	OrigCrLim,
	CurTerm,
	CurRate,
	CurPmt,
	CurBal,
	CurCrLim,
	BalloonDate,
	MatDate,
	InsCode,
	CollateralValue,
	LTV,
	SecType,
	SecDesc1,
	SecDesc2,
	DebtRatio,
	Dealer,
	DealerName,
	CoborComak,
	RelPriceGrp,
	RelPriceDisc,
	OtherDisc,
	DiscReason,
	RiskOffset,
	CreditType,
	StatedIncome,
	Extensions,
	FirstExtDate,
	LastExtDate,
	RiskLevel,
	RiskDate,
	Watch,
	WatchDate,
	WorkOut,
	WorkOutDate,
	FirstMortType,
	BusPerReg,
	BusGroup,
	BusDesc,
	BusPart,
	BusPartPerc,
	SBAguar,
	DeliDays,
	DeliSect,
	DeliHist,
	DeliHistQ1,
	DeliHistQ2,
	DeliHistQ3,
	DeliHistQ4,
	DeliHistQ5,
	DeliHistQ6,
	DeliHistQ7,
	DeliHistQ8,
	ChrgOffDate,
	ChrgOffMonth,
	ChrgOffYear,
	ChrgOffMnthsBF,
	PropType,
	Occupancy,
	OrigApprVal,
	OrigApprDate,
	CurrApprVal,
	CurrApprDate,
	FirstMortBal,
	FirstMortMoPyt,
	SecondMortBal,
	SecondMortPymt,
	TaxInsNotInc,
	LoadOff,
	ApprOff,
	FundingOpp,
	Chanel,
	RiskType,
	Gap,
	LoadOperator,
	DeliAmt,
	ChgOffAmt,
	UsSecDesc,
	TroubledDebt,
	TdrDate,
	JntFicoUsed,
	SingleOrJointLoan,
	ApprovingOfficerUD,
	InsuranceCodeDesc,
	GapIns,
	MmpIns,
	ReportCodeDesc,
	BallonPmtLoan,
	FixedOrVariableRateLoan,
	AlpsOfficer,
	CUDLAppNumber,
	FasbDefCost,
	FasbLtdAmortFees,
	InterestPriorYtd,
	AccruedInterest,
	InterestLTD,
	InterestYtd,
	PartialPayAmt,
	InterestUncollected,
	ClosedAcctDate,
	LastTranDate,
	NextDueDate,
	InsuranceCode,
	MbrAgeAtApproval)
VALUES (
	@UpDated,
	@EOMDate,
	@AccountID,
	@Member,
	@MClass,
	@Ltype,
	@LtypeSub,
	@Product,
	@LoanDesc,
	@LoanDate,
	@LoanDay,
	@LoanMonth,
	@LoanYear,
	@FirstPmtDate,
	@Status,
	@RptCode,
	@FundedBr,
	@LoadedBr,
	@FICO,
	@FICORange,
	@FastStart,
	@FastStartRange,
	@Bankruptcy,
	@BankruptcyRange,
	@MembershipDate,
	@NewMember,
	@RiskLev,
	@Traffic,
	@TrafficColor,
	@MemAge,
	@MemSex,
	@PmtFreq,
	@OrigTerm,
	@OrigRate,
	@OrigPmt,
	@OrigAmount,
	@OrigCrLim,
	@CurTerm,
	@CurRate,
	@CurPmt,
	@CurBal,
	@CurCrLim,
	@BalloonDate,
	@MatDate,
	@InsCode,
	@CollateralValue,
	@LTV,
	@SecType,
	@SecDesc1,
	@SecDesc2,
	@DebtRatio,
	@Dealer,
	@DealerName,
	@CoborComak,
	@RelPriceGrp,
	@RelPriceDisc,
	@OtherDisc,
	@DiscReason,
	@RiskOffset,
	@CreditType,
	@StatedIncome,
	@Extensions,
	@FirstExtDate,
	@LastExtDate,
	@RiskLevel,
	@RiskDate,
	@Watch,
	@WatchDate,
	@WorkOut,
	@WorkOutDate,
	@FirstMortType,
	@BusPerReg,
	@BusGroup,
	@BusDesc,
	@BusPart,
	@BusPartPerc,
	@SBAguar,
	@DeliDays,
	@DeliSect,
	@DeliHist,
	@DeliHistQ1,
	@DeliHistQ2,
	@DeliHistQ3,
	@DeliHistQ4,
	@DeliHistQ5,
	@DeliHistQ6,
	@DeliHistQ7,
	@DeliHistQ8,
	@ChrgOffDate,
	@ChrgOffMonth,
	@ChrgOffYear,
	@ChrgOffMnthsBF,
	@PropType,
	@Occupancy,
	@OrigApprVal,
	@OrigApprDate,
	@CurrApprVal,
	@CurrApprDate,
	@FirstMortBal,
	@FirstMortMoPyt,
	@SecondMortBal,
	@SecondMortPymt,
	@TaxInsNotInc,
	@LoadOff,
	@ApprOff,
	@FundingOpp,
	@Chanel,
	@RiskType,
	@Gap,
	@LoadOperator,
	@DeliAmt,
	@ChgOffAmt,
	@UsSecDesc,
	@TroubledDebt,
	@TdrDate,
	@JntFicoUsed,
	@SingleOrJointLoan,
	@ApprovingOfficerUD,
	@InsuranceCodeDesc,
	@GapIns,
	@MmpIns,
	@ReportCodeDesc,
	@BallonPmtLoan,
	@FixedOrVariableRateLoan,
	@AlpsOfficer,
	@CUDLAppNumber,
	@FasbDefCost,
	@FasbLtdAmortFees,
	@InterestPriorYtd,
	@AccruedInterest,
	@InterestLTD,
	@InterestYtd,
	@PartialPayAmt,
	@InterestUncollected,
	@ClosedAcctDate,
	@LastTranDate,
	@NextDueDate,
	@InsuranceCode,
	@MbrAgeAtApproval)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spLoanPortfolio_Insert: Unable to insert new row into LoanPortfolio table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

