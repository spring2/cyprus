if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spBankruptcyRange_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spBankruptcyRange_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spBankruptcyRange_Insert
	@Value	Int = null,
	@Description	VarChar(250) = null
AS


INSERT INTO BankruptcyRange
(	Value,
	Description)
VALUES (
	@Value,
	@Description)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spBankruptcyRange_Insert: Unable to insert new row into BankruptcyRange table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

