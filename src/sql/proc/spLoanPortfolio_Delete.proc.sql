if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLoanPortfolio_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLoanPortfolio_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spLoanPortfolio_Delete

@LoanPortfolio_Id	Int

AS

if not exists(SELECT * FROM LoanPortfolio WHERE (	LoanPortfolio_Id = @LoanPortfolio_Id
))
    BEGIN
        RAISERROR  ('spLoanPortfolio_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	LoanPortfolio
WHERE 
	LoanPortfolio_Id = @LoanPortfolio_Id
GO

