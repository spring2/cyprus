if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLoanType_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLoanType_Update]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spLoanType_Update

	@LoanTypeId	Int = null,
	@LType	Int = null,
	@LoanDesc	VarChar(250) = null
AS


UPDATE
	LoanType
SET
	LType = @LType,
	LoanDesc = @LoanDesc
WHERE
LoanTypeId = @LoanTypeId


if @@ROWCOUNT <> 1
    BEGIN
        RAISERROR  ('spLoanType_Update:  update was expected to update a single row and updated %d rows', 16,1, @@ROWCOUNT)
        RETURN(-1)
    END
GO

