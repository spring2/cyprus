if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spFastStartRange_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spFastStartRange_Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spFastStartRange_Insert
	@Value	Int = null,
	@Description	VarChar(250) = null
AS


INSERT INTO FastStartRange
(	Value,
	Description)
VALUES (
	@Value,
	@Description)

if @@rowcount <> 1 or @@error!=0
    BEGIN
        RAISERROR  20000 'spFastStartRange_Insert: Unable to insert new row into FastStartRange table '
        RETURN(-1)
    END

return SCOPE_IDENTITY()
GO

