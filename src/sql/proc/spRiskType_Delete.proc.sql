if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spRiskType_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spRiskType_Delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spRiskType_Delete

@RiskTypeId	Int

AS

if not exists(SELECT * FROM RiskType WHERE (	RiskTypeId = @RiskTypeId
))
    BEGIN
        RAISERROR  ('spRiskType_Delete: record not found to delete', 16,1)
        RETURN(-1)
    END

DELETE
FROM
	RiskType
WHERE 
	RiskTypeId = @RiskTypeId
GO

