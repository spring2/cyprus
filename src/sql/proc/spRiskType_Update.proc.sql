if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spRiskType_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spRiskType_Update]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.spRiskType_Update

	@RiskTypeId	Int = null,
	@LType	Int = null,
	@RiskTypeDesc	VarChar(250) = null
AS


UPDATE
	RiskType
SET
	LType = @LType,
	RiskTypeDesc = @RiskTypeDesc
WHERE
RiskTypeId = @RiskTypeId


if @@ROWCOUNT <> 1
    BEGIN
        RAISERROR  ('spRiskType_Update:  update was expected to update a single row and updated %d rows', 16,1, @@ROWCOUNT)
        RETURN(-1)
    END
GO

