﻿using System;
using System.Collections.Generic;
using Rhino.Mocks;
using Spring2.Core.IoC;
using Spring2.Core.Types;
using Spring2.Core.Configuration;
using System.Configuration;

namespace Spring2.UnitTest {
    public class TestMockUtility : MockRepository {
	public IList<Action> CleanUps { protected set; get; }

	/// <summary>
	/// Initializes a new instance of the <see cref="TestMockUtility"/> class.
	/// </summary>
	public TestMockUtility() {
	    CleanUps = new List<Action>();
	}

	/// <summary>
	/// Clears the Rhino Mocks repository and runs tear downs.
	/// </summary>
	public void TearDown() {
	    foreach (Action cleanUp in CleanUps) {
		cleanUp();
	    }
	    CleanUps.Clear();
	    CleanUps = null;
	    ClassRegistry.Flush(true);
	}

	//Strict
	public new T StrictMock<T>(params object[] arguments) {
	    T theType = base.StrictMock<T>(arguments);
	    theType.Replay();
	    return theType;
	}

	public new T StrictMultiMock<T>(Type[] extraTypes, params object[] arguments) {
	    T theType = base.StrictMultiMock<T>(extraTypes, arguments);
	    theType.Replay();
	    return theType;
	}

	public new T StrictMultiMock<T>(params Type[] extraTypes) {
	    T theType = base.StrictMultiMock<T>(extraTypes);
	    theType.Replay();
	    return theType;
	}

	//Partial
	public new T PartialMock<T>(params object[] arguments) where T : class {
	    T theType = base.PartialMock<T>(arguments);
	    theType.Replay();
	    return theType;
	}

	public new T PartialMultiMock<T>(Type[] extraTypes, params object[] arguments) where T : class {
	    T theType = base.PartialMultiMock<T>(extraTypes, arguments);
	    theType.Replay();
	    return theType;
	}

	public new T PartialMultiMock<T>(params Type[] extraTypes) where T : class {
	    T theType = base.PartialMultiMock<T>(extraTypes);
	    theType.Replay();
	    return theType;
	}

	//Dynamic
	public new T DynamicMock<T>(params object[] arguments) where T : class {
	    T theType = base.DynamicMock<T>(arguments);
	    theType.Replay();
	    return theType;
	}

	public new T DynamicMultiMock<T>(params Type[] extraTypes) where T : class {
	    T theType = base.DynamicMultiMock<T>(extraTypes);
	    theType.Replay();
	    return theType;
	}

	public new T DynamicMultiMock<T>(Type[] extraTypes, params object[] arguments) where T : class {
	    T theType = base.DynamicMultiMock<T>(extraTypes, arguments);
	    theType.Replay();
	    return theType;
	}

	//Stub
	public new T Stub<T>(params object[] arguments) {
	    T theType = base.Stub<T>(arguments);
	    theType.Replay();
	    return theType;
	}

	/// <summary>
	/// Creates a strict mock of an interface or a concrete class with public constructor, and registers it in the ClassRegistry.
	/// </summary>
	/// <typeparam name="T">The type to be mocked.</typeparam>
	/// <param name="arguments">The arguments for the constructor.</param>
	public T StrictMockAndRegister<T>(params object[] arguments) {
	    T theType = base.StrictMock<T>(arguments);
	    ClassRegistry.Register<T>(theType);
	    theType.Replay();
	    return theType;
	}

	public T StrictMultiMockAndRegister<T>(Type[] extraTypes, params object[] constructorArgs) {
	    T theType = base.StrictMultiMock<T>(extraTypes, constructorArgs);
	    ClassRegistry.Register<T>(theType);
	    theType.Replay();
	    return theType;
	}

	public T MockAndRegister<T>() where T : class {
	    T theType = base.DynamicMock<T>();
	    ClassRegistry.Register<T>(theType);
	    theType.Replay();
	    return theType;
	}

	public T StubAndRegister<T>() where T : class {
	    T theType = base.Stub<T>();
	    ClassRegistry.Register<T>(theType);
	    theType.Replay();
	    return theType;
	}

	public void ResetExpectations(object o, BackToRecordOptions options) {
	    o.BackToRecord(options);
	    o.Replay();
	}

	public void ResetExpectationsAll(BackToRecordOptions options) {
	    BackToRecordAll(options);
	    ReplayAll();
	}
    }
}
