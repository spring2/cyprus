﻿using Spring2.Core.Types;

namespace Spring2.UnitTest {
    public static class TestValues {
	public static StringType NAME = StringType.Parse("Name");
	public static StringType EMAIL = StringType.Parse("unittest@spring2.com");
	public static StringType ADDRESS1 = StringType.Parse("10150 S. Centennial Parkway");
	public static StringType ADDRESS2 = StringType.Parse("Address2");
	public static StringType ADDRESS3 = StringType.Parse("Address3");
	public static StringType CITY = StringType.Parse("Sandy");
	public static StringType ZIP = StringType.Parse("84070");
	public static StringType CANADAADDRESS1 = StringType.Parse("490 Sussex Drive");
	public static StringType CANADAADDRESS2 = StringType.Parse("Address2");
	public static StringType CANADAADDRESS3 = StringType.Parse("Address3");
	public static StringType CANADACITY = StringType.Parse("Ottawa");
	public static StringType CANADAREGION = StringType.Parse("Ontario");
	public static StringType CANADAZIP = StringType.Parse("K1N 1G8");
	public static DecimalType LATITUDE = new DecimalType(20);
	public static DecimalType LONGITUDE = new DecimalType(20);
	public static StringType STATE = StringType.Parse(USStateCodeEnum.UTAH.ToString());
	public static PhoneNumberType PHONE = new PhoneNumberType("555", "555", "1212", "105");
	public static StringType ACCOUNTNUMBER = StringType.Parse("4111111111111111");
	public static StringType BANKACCOUNTNUMBER = StringType.Parse("987654321");
	public static StringType ROUTINGNUMBER = StringType.Parse("324078909");
	public static StringType EXPIRATION = StringType.Parse("09/59");
	public static StringType UNITTEST = StringType.Parse("UnitTest");
	public static StringType CANADAUNITTEST = StringType.Parse("CanadaUnitTest");
	public static StringType CVV2 = StringType.Parse("123");
	public static StringType CANCELATIONREASON = StringType.Parse("Test Cancelation Reason");
	public static StringType PASSWORD = new StringType("foobar12");
	public static IdType ID = new IdType(1000);
	public static string SUCCESS = "success";
	public static string VALIDATION_ERRORS = "validationErrors";
	public static string LOGIN_REQUIRED = "loginRequired";
	public static string CONTRACT_EXPIRED = "contractExpired";
    }
}
