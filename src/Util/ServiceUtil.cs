﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

using Spring2.Core.Types;

using System.Diagnostics;

namespace Spring2.Util {
    public enum CtrlType {
	CTRL_C_EVENT = 0,
	CTRL_BREAK_EVENT = 1,
	CTRL_CLOSE_EVENT = 2,
	CTRL_LOGOFF_EVENT = 5,
	CTRL_SHUTDOWN_EVENT = 6
    }

    public static class ServiceUtil {
	public static void PrintDataSet(DataSet ds) {
	    Console.WriteLine("Tables in '{0}' DataSet.\n", ds.DataSetName);
	    foreach (DataTable dt in ds.Tables) {
		Console.WriteLine("{0} Table.\n", dt.TableName);
		for (int curCol = 0; curCol < dt.Columns.Count; curCol++) {
		    Console.Write(dt.Columns[curCol].ColumnName.Trim() + "\t");
		}
		for (int curRow = 0; curRow < dt.Rows.Count; curRow++) {
		    for (int curCol = 0; curCol < dt.Columns.Count; curCol++) {
			Console.Write(dt.Rows[curRow][curCol].ToString().Trim() + "\t");
		    }
		    Console.WriteLine();
		    String cmd = Console.ReadLine();
		    if (cmd.ToUpper().Equals("X")) {
			break;
		    }
		}
	    }
	}

	public static void PrintDataTable(DataTable dt) {
	    foreach (DataRow row in dt.Rows) {
		foreach (DataColumn col in dt.Columns) {
		    String value = String.Format("{1}: {0}", row[col].ToString().Trim(), col.ColumnName);
		    Console.WriteLine(value);
		}
		for (int i = 0; i < dt.ChildRelations.Count; i++) {
		    DataTable childTable = dt.ChildRelations[i].ChildTable;
		    PrintChildRows(childTable, dt.ChildRelations[i], row);
		}
		Console.WriteLine("\n=========================================================================\n");
		String cmd = Console.ReadLine();
		if (cmd.ToUpper().Equals("X")) {
		    break;
		}
	    }
	}

	public static void PrintChildRows(DataTable dt, DataRelation dr, DataRow row) {
	    Console.WriteLine("\nChildren: " + dr.RelationName.ToUpper() + "\n");
	    DataRow[] children = row.GetChildRows(dr);
	    for (int curChildRow = 0; curChildRow < children.Length; curChildRow++) {
		DataRow childRow = children[curChildRow];
		for (int curCol = 0; curCol < dt.Columns.Count; curCol++) {
		    String value = String.Format("{1}: {0}", childRow[curCol].ToString().Trim(), dt.Columns[curCol].ColumnName);
		    Console.WriteLine(value);
		}
		for (int i = 0; i < childRow.Table.ChildRelations.Count; i++) {
		    PrintChildRows(childRow.Table.ChildRelations[i].ChildTable, childRow.Table.ChildRelations[i], childRow);
		}
	    }
	}

	public static void PrintColumnNames(DataTable dt) {
	    for (int curCol = 0; curCol < dt.Columns.Count; curCol++) {
		Console.Write(dt.Columns[curCol].ColumnName.Trim() + "\t");
	    }
	}

	public static int MonthDifference(DateType startDate, DateType endDate) {
	    return MonthDifference(startDate.ToDateTime(), endDate.ToDateTime());
	}

	public static int MonthDifference(DateTime startDate, DateTime endDate) {
	    int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
	    return Math.Abs(monthsApart);
	}

	public static string GetElapsedTime(Stopwatch sw) {
	    string elapsedTime = "0";

	    if (sw != null) {
		TimeSpan ts = sw.Elapsed;
		elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
	    }

	    return elapsedTime;
	}
    }
}
