
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataObject;
using Spring2.Types;
using System.Collections.Generic;
using System.Text;
using System;

namespace Spring2.Exceptions {
    public class LoanPortfolioImportError : Message {
	private StringType msg;
	private StringType clientId;
	private StringType accountId;

	public StringType Msg {
	    get { return this.msg; }
	    set { this.msg = value; }
	}
	public StringType ClientId {
	    get { return this.clientId; }
	    set { this.clientId = value; }
	}
	public StringType AccountId {
	    get { return this.accountId; }
	    set { this.accountId = value; }
	}

	public LoanPortfolioImportError (StringType msg, StringType clientId, StringType accountId) : base("There was an error importing a Loan Portfolio. Client={1}, Account={2}, Message={0}", msg, clientId, accountId) {
	    this.msg = msg;
	    this.clientId = clientId;
	    this.accountId = accountId;
	}
    }
}


