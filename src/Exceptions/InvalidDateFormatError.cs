
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.DataObject;
using Spring2.Types;
using System.Collections.Generic;
using System.Text;
using System;

namespace Spring2.Exceptions {
    public class InvalidDateFormatError : Message {
	private StringType value;
	private StringType msg;

	public StringType Value {
	    get { return this.value; }
	    set { this.value = value; }
	}
	public StringType Msg {
	    get { return this.msg; }
	    set { this.msg = value; }
	}

	public InvalidDateFormatError (StringType value, StringType msg) : base("There was an error parsing a Date Time value:{0} {1}", value, msg) {
	    this.value = value;
	    this.msg = msg;
	}
    }
}


