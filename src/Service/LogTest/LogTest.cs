﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Odbc;
using System.Timers;

using System.Runtime.InteropServices;
using System.Reflection;

using System.Diagnostics;

using Spring2.Facade;

using log4net;

namespace Spring2.Service.LogTest {
    class LogTest {
	private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	static ServiceFacade facade = new ServiceFacade();

	static void Main(string[] args) {
	    // use MDC to set the hostname for log messages -- MDC is thread specific
	    MDC.Set("hostname", Environment.MachineName);

	    log.Info("LogTest Starting");
	    Stopwatch sw = new Stopwatch();
	    sw.Start();

	    string arg1 = args.Length > 0 ? args[0] : null;
	    string arg2 = args.Length > 1 ? args[2] : null;
	    Run();

	    sw.Stop();
	    TimeSpan ts = sw.Elapsed;
	    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

	    log.Info("LogTest Finished (" + elapsedTime + ")");
	}

	private static void Run() {
	    facade.LogTest();
	}
    }
}
