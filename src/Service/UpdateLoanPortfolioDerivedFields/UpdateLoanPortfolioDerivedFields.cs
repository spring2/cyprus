﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Odbc;
using System.Timers;

using System.Runtime.InteropServices;
using System.Reflection;

using System.Diagnostics;

using Spring2.Facade;
using Spring2.Util;

using log4net;

namespace Spring2.Service.UpdateLoanPortfolioDerivedFields {
    class UpdateLoanPortfolioDerivedFields {
	private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	static ServiceFacade facade = new ServiceFacade();

	static void Main(string[] args) {
	    // use MDC to set the hostname for log messages -- MDC is thread specific
	    MDC.Set("hostname", Environment.MachineName);

	    log.Info("Updating Loan Portfolio Derived Fields");
	    Stopwatch sw = new Stopwatch();
	    sw.Start();

	    Run();

	    sw.Stop();
	    log.Info("Finished Updating Loan Portfolio Derived Fields (" + ServiceUtil.GetElapsedTime(sw) + ")");
	}

	private static void Run() {
	    facade.ReDeriveAllLoanPortfolios();
	}
    }
}
