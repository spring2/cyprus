﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Odbc;

using System.Runtime.InteropServices;

using System.Transactions;

using Spring2.Facade;

namespace Spring2.Service.LoadTestData {
    class Program {
	const int CLIENT_COUNT = 4;
	const int MAX_TEST_ACCOUNTS = 10;
	enum CtrlType {
	    CTRL_C_EVENT = 0,
	    CTRL_BREAK_EVENT = 1,
	    CTRL_CLOSE_EVENT = 2,
	    CTRL_LOGOFF_EVENT = 5,
	    CTRL_SHUTDOWN_EVENT = 6
	}

	[DllImport("Kernel32")]
	private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);
	private delegate bool EventHandler(CtrlType sig);
	static EventHandler cleanupHander;

	static void Main(string[] args) {
	    cleanupHander += new EventHandler(CleanupHandler);
	    SetConsoleCtrlHandler(cleanupHander, true);

	    String arg1 = args.Length > 0 ? args[0] : null;

	    Console.WriteLine("Starting");
	    Run();
	    Console.WriteLine("Finished");
	    Console.ReadLine();
	}

	private static void Run() {
	    ServiceFacade facade = new ServiceFacade();
	    facade.LoadTestLoanData(CLIENT_COUNT, MAX_TEST_ACCOUNTS);
	}

	//  Cleanup
	private static bool CleanupHandler(CtrlType sig) {
	    switch (sig) {
		case CtrlType.CTRL_C_EVENT:
		case CtrlType.CTRL_LOGOFF_EVENT:
		case CtrlType.CTRL_SHUTDOWN_EVENT:
		case CtrlType.CTRL_CLOSE_EVENT:
		default:
		    break;
	    }
	    return true;
	}
    }
}
