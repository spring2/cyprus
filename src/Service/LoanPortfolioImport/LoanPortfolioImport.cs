﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using System.Data;
using System.Data.Odbc;
using System.Timers;
using System.Reflection;

using System.Runtime.InteropServices;

using Spring2.Facade;
using Spring2.Util;
using Spring2.Exceptions;
using Spring2.Core.Message;
using Spring2.Core.Types;
using Spring2.Core.Util;

using log4net;

namespace Spring2.Service.LoanPortfolioImport {
    class LoanPortfolioImport {
	private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
	static SimpleFormatter formatter = new SimpleFormatter();
	static ServiceFacade facade = new ServiceFacade();

	static String memNum = String.Empty;
	static DateType eomDate = DateType.DEFAULT;
	static Boolean showHelp = false;
	static Boolean verbose = false;
	static Boolean leaveOpen = false;
	static Boolean import = false;
	static Boolean restore = false;

	static void Main(String[] args) {
	    OptionSet os = new OptionSet() {
		{"e|eom=", "Used to specify the end of month date of the import", eom => eomDate = DateType.Parse(eom)},
		{"m|memNum=", "Specifies a single member number to import", m => memNum = m},
		{"h|help", "Show help text and exite", h => showHelp = h != null},
		{"v|verbose", "Increases debug verbosity", v => verbose = v != null},
		{"i|import", "Perform the import", i => import = i != null},
		{"o", "Leave the console open after running", o => leaveOpen = o != null},
		{"r", "Import from the restoration data source", r => restore = r != null}
	    };

	    os.Parse(args);

	    if (showHelp) {
		ShowHelp(os);
		return;
	    }

	    // use MDC to set the hostname for log messages -- MDC is thread specific
	    MDC.Set("hostname", Environment.MachineName);

	    log.Info("Loan Portfolio Import Starting");
	    Stopwatch sw = new Stopwatch();
	    sw.Start();

	    if (import) {
		Run();
	    } else {
		ShowHelp(os);
	    }

	    sw.Stop();
	    log.Info("Loan Portfolio Import Finished (" + ServiceUtil.GetElapsedTime(sw) + ")");

	    if (leaveOpen) {
		Console.ReadLine();
	    }
	}

	private static void ShowHelp(OptionSet os) {
	    Console.WriteLine("Usage: LoanPortfolioImport [OPTIONS]");
	    Console.WriteLine();
	    Console.WriteLine("Options:");
	    os.WriteOptionDescriptions(Console.Out);
	    Console.ReadLine();
	}

	private static void Run() {
	    try {
		ImportLoanPortfolios(memNum, eomDate);
	    } catch (MessageListException mle) {
		StringBuilder messageText = new StringBuilder();
		foreach (Message msg in mle.Messages)
		    messageText.Append(formatter.Format(msg) + Environment.NewLine);
		facade.SendMailMessage("Data Warehouse - Loan Portfolio Import Notification", messageText.ToString());
		log.Error(messageText, mle);
	    } catch (Exception ex) {
		log.Fatal(ex);
	    }
	}

	private static void ImportLoanPortfolios(String memNum, DateType eomDate) {
	    String conStr = String.Empty;
	    if (restore) {
		conStr = facade.GetRestoreODBCConnectionString();
	    } else {
		conStr = facade.GetUniDataODBCConnectionString();
	    }
	    Stopwatch sw = new Stopwatch();
	    sw.Start();
	    log.Info("Loading LoanPortfolio Data");
	    DataSet ds = UniDataProvider.GetLoanPortfolioDataSet(memNum, conStr);
	    sw.Stop();
	    log.Info("Finished Loading LoanPortfolio Data (" + ServiceUtil.GetElapsedTime(sw) + ")");

	    sw.Start();
	    log.Info("Importing Loan Portfolios");
	    MessageList errors = facade.ImportLoanPortfolios(ds, eomDate);
	    sw.Stop();
	    log.Info("Finished Importing Loan Portfolios (" + ServiceUtil.GetElapsedTime(sw) + ")");

	    if (errors.Count > 0)
		throw new MessageListException(errors);
	}

	private static void PrintHelp() {
	    Console.WriteLine("usage: command [args]\n");
	    Console.WriteLine("commands:");
	    Console.WriteLine("import: import loan portfolios");
	    Console.WriteLine("\nargs:");
	    Console.WriteLine("m=Member Number");
	    Console.WriteLine("eom=End of Month Date");
	}
    }
}
