﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Odbc;

using Spring2.Core.DAO;
using Spring2.Util;

using log4net;

using System.Diagnostics;

namespace Spring2.Service.LoanPortfolioImport {
    public class UniDataProvider {
	private static readonly ILog log = LogManager.GetLogger("UniDataProvider");

	enum UniDataTypes {
	    VARCHAR,
	    INT,
	    DATETIME,
	    DATE,
	    FLOAT,
	    MONEY
	}

	static String memNum = null;

	#region Sql Statements
	private static SqlFilter GetAccountTypeFilter(String prefix = "") {
	    SqlFilter filter = new SqlFilter();
	    filter.And(new SqlLiteralPredicate($"{prefix}L_ACCT_TYPE = 'L' and (({prefix}ACCT_NUM in (select ACCT_NUM from TRAN_DATA where CURRENT_BALANCE < 0) or {prefix}ACCT_NUM in (select ACCT_NUM from ACCOUNT where CURR_CREDIT_LIM <> 0)))"));
	    if (!String.IsNullOrEmpty(memNum))
		filter.And(new SqlLiteralPredicate("MEM_NUM = " + memNum));
	    return filter;
	}

	private static SqlFilter GetAccountSubFilter(String field1, String field2) {
	    SqlFilter filter = new SqlFilter();
	    filter.And(new SqlLiteralPredicate(field1 + " in (select " + field2 + " from ACCOUNT" + GetAccountTypeFilter().Statement + ")"));
	    return filter;
	}

	private static SqlFilter GetMemberFilter(String fieldName) {
	    SqlFilter filter = new SqlFilter();
	    if (!String.IsNullOrEmpty(memNum))
		filter.And(new SqlLiteralPredicate(fieldName + " = " + memNum));
	    return filter;
	}

	private static String GetAccountSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCOUNT.ACCT_NUM,
			MEM_NUM, 
			L_ACCT_LEVEL, 
			ACCT_SUBLEV, 
			ACCOUNT_OPEN_DATE, 
			SPEC_REPORT_CODE, 
			ORIG_PAY, 
			PG_CODE, 
			RISK_OFFSET_VAL, 
			FIRST_PAYDATE, 
			ORIG_LOAN_TERM, 
			L_MATURITY_DATE, 
			L_INSCODE, 
			L_SECCODE, 
			L_SECDESC1, 
			L_SECDESC2,
			US_APPLIC_ID,
			ARREAR_DAYS,
			APPR_OFFICER,
			DEALER,
			CONS_LOANAMT,
			CURR_CREDIT_LIM,
			ACCOUNT_DATA.US_ACCT_CLOSED,
			ACCOUNT_FASB.FASB_DEF_COST,
			ACCOUNT_FASB.LTD_AMORT_FEES,
			ACCOUNT_LOAN.L_INSCODE
			from 
			ACCOUNT
			LEFT JOIN ACCOUNT_DATA on ACCOUNT.ACCT_NUM=ACCOUNT_DATA.ACCT_NUM
			LEFT JOIN ACCOUNT_FASB on ACCOUNT.ACCT_NUM=ACCOUNT_FASB.ACCT_NUM
			LEFT JOIN ACCOUNT_LOAN on ACCOUNT.ACCT_NUM=ACCOUNT_LOAN.ACCT_NUM
			";
	    return sql += GetAccountTypeFilter("ACCOUNT.").Statement;
	}

	private static String GetApplicSql() {
	    String sql = String.Empty;
	    sql = @"select 
			APPLIC_ID,
			APPLIC_NUM,
			FINAL_STAMP_BR,
			LOAD_STAMP_BR,
			STAGE2_CRSCORE,
			AP_RISK_SCORE,
			BANKRUPT_SCORE,
			AP_RISK_LEVEL,
			ALPS_COLOR,
			PAYMENT_FREQ,
			BALLOON_DATE,
			DEALER_NUMBER,
			AP_RP_DISCOUNT,
			CRSCORE_TYPE,
			HELOC_TYPE,
			HELOC_OCCUPANCY,
			HELOC_VALUE,
			HELOC_1STMORT_BAL,
			HELOC_1STMORT_PAY,
			HELOC_2NDMORT_BAL,
			HELOC_2NDMORT_PAY,
			INSURE_CODE,
			AP_INTEREST_RATE,
			LOAD_STAMP_OFF,
			FINAL_STAMP_OPER,
			NUM_PAYMENTS,
			AP_PURPOSE_CODE,
			HELOC_ORIG_DATE,
			HELOC_MTH_TAX,
			DISCOUNT_REASON,
			LOAD_STAMP_OPER
			from  
			APPLIC";
	    return sql += GetAccountSubFilter("APPLIC_ID", "US_APPLIC_ID").Statement;
	}

	private static String GetApplicCalcInfoSql() {
	    String sql = String.Empty;
	    sql = @"select 
			APPLIC_ID,
			DEBT_AFTER
			from 
			APPLIC_CALCINFO";
	    return sql += GetAccountSubFilter("APPLIC_ID", "US_APPLIC_ID").Statement;
	}

	private static String GetClientSql() {
	    String sql = String.Empty;
	    sql = @"select 
			MEM_NUM,
			CLIENT_CLASS,
			JOIN_DATE,
			SEX,
			BIRTH_DATE
			from 
			CLIENT";
	    return sql += GetAccountSubFilter("MEM_NUM", "MEM_NUM").Statement;
	}

	private static String GetAccountUsSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			US_LOANSTATUS
			from 
			ACCOUNT_US";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetAccountUdUsSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			US_INCOME_STATED,
			US_NUM_OF_EXTENSIONS,
			US_1ST_EXT_DTE,
			US_MOSTRECENT_EXT_DTE,
			US_L_RISKLEV,
			US_RISKLEVEL_DTE,
			US_WATCHLIST,
			US_DATE_WATCHLIST,
			US_WORKOUT_LOANS,
			US_WORKOUT_DTE,
			US_UD_BUS_LOAN_PER_REG,
			US_UD_BUS_LOAN_COL_GRP,
			US_UD_BUS_LOAN_COL_DESC,
			US_UD_BUS_LOAN_PART,
			US_UD_SBA_GUARANTY,
			US_UD_1ST_MORT_TYPE,
			US_UD_BUS_LOAN_PERCNT_PART,
			US_GAP,
			US_SECDESC,
			US_UD_TROUBLED_DEBT,
			US_UD_TDR_DATE,
			US_COBORROWER_FICO_USED,
			US_APPR_OFFICER,
			US_INS_CODE_DESC,
			US_GAP_INS,
			US_MMP_INS,
			US_MBR_YAGE_LOAN_APPR,
			US_JNT_ACCT,
			US_RPT_CODE_DESC,
			US_RATE_FIX_VAR,
			US_CUDL_APP_NUM,
			US_LTV
			from 
			ACCOUNT_UD_US";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetTranDataSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			CURRENT_BALANCE,
			INT_RATE,
			LAST_YEAR_INT,
			LAST_TRANSACT_DATE
			from 
			TRAN_DATA";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetTranLoanSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			L_PREVIOUS_CREDIT_LIMIT,
			L_REPAY_AMT,
			L_ACCR_INT,
			L_TOT_INT,
			L_INT_YTD,
			L_PARTIAL_PAY_ACCUM,
			L_UNCOLL_INT_AMT,
			L_NEXT_DUE_DATE
			from 
			TRAN_LOAN";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetAccountChgOffSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			CHARGEOFF_APPR,
			CHARGEOFF_AMT
			from 
			ACCOUNT_CHGOFF";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetCollatValueSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			US_COLLATERAL_VALUE
			from 
			ACCOUNT_COLLAT_VALUE";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetCoborrowSql() {
	    String sql = String.Empty;
	    sql = @"select 
			ACCT_NUM,
			COBORROW_MEMBERS
			from 
			ACCOUNT_COBORROW";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetDelinquencyDataSql() {
	    String sql = String.Empty;

	    sql = @"select 
			ACCT_NUM,
			SECTION_NUM,
			DLQ_HIST,
			PAST_DUE_AHEAD_AMT
			from 
			DELINQUENCY_DATA";
	    return sql += GetAccountSubFilter("ACCT_NUM", "ACCT_NUM").Statement;
	}

	private static String GetProfileSql() {
	    String sql = String.Empty;
	    sql = @"select 
			RECORD_KEY,
			NO_YRS_ADDR,
			PREV_YRS_ADDR,
			CURR_LIVING_STATUS
			from 
			PROFILE_DATA";
	    return sql += GetMemberFilter("RECORD_KEY").Statement;
	}

	private static String GetProfileEmployerInfoSql() {
	    String sql = String.Empty;
	    sql = @"select 
			RECORD_KEY,
			EMP_GROSS_INCOME,
			EMP_YRS_PROF
			from 
			PROFILE_EMPLOYER_INFO";
	    return sql += GetMemberFilter("RECORD_KEY").Statement + "and RECORD_KEY in (select MEM_NUM from CLIENT)";
	}

	private static String GetDealerDataSql() {
	    String sql = String.Empty;
	    sql = @"select 
			DEALER_NUM,
			DEALER_NAME
			from 
			DEALER_DATA";
	    return sql;
	}
	#endregion

	public static DataSet GetLoanPortfolioDataSet(String conStr) {
	    return GetLoanPortfolioDataSet(null, conStr);
	}

	public static DataSet GetLoanPortfolioDataSet(String memberNumber, String conStr) {
	    memNum = memberNumber;
	    DataSet ds = new DataSet("LoadPortfolios");
	    using (OdbcConnection con = new OdbcConnection(conStr)) {
		con.Open();
		log.Info("Connected");

		Stopwatch sw = new Stopwatch();
		sw.Start();
		int rowCount = FillDataSetTable(ds, GetAccountSql(), "Account", con);
		log.Info("Retrieved Account Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetDealerDataSql(), "DealerData", con);
		log.Info("Retrieved DealerData Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetApplicSql(), "Applic", con);
		log.Info("Retrieved Applic Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetApplicCalcInfoSql(), "ApplicCalcInfo", con);
		log.Info("Retrieved ApplicCalcInfo Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetClientSql(), "Client", con);
		log.Info("Retrieved Client Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetAccountUsSql(), "AccountUs", con);
		log.Info("Retrieved AccountUs Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetAccountUdUsSql(), "AccountUdUs", con);
		log.Info("Retrieved AccountUdUs Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetTranDataSql(), "TranData", con);
		log.Info("Retrieved TranData Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetTranLoanSql(), "TranLoan", con);
		log.Info("Retrieved TranLoan Data (" + rowCount + ") (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetAccountChgOffSql(), "ChgOff", con);
		log.Info("Retrieved ChgOff Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetCoborrowSql(), "Coborrow", con);
		log.Info("Retrieved Coborrow Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetCollatValueSql(), "CollatValue", con);
		log.Info("Retrieved CollatValue Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		sw.Reset();
		sw.Start();
		rowCount = FillDataSetTable(ds, GetDelinquencyDataSql(), "DelinquencyData", con);
		log.Info("Retrieved DelinquencyData Data (" + rowCount + ") (" + ServiceUtil.GetElapsedTime(sw) + ")");

		//FillDataSetTable(ds, GetProfileSql(), "Profile", con);
		//FillDataSetTable(ds, GetProfileEmployerInfoSql(), "ProfileEmployerInfo", con);

		//  Set Primary Keys for manual Lookup
		ds.Tables["Applic"].PrimaryKey = new DataColumn[] { ds.Tables["Applic"].Columns["APPLIC_ID"] };
		ds.Tables["DealerData"].PrimaryKey = new DataColumn[] { ds.Tables["DealerData"].Columns["DEALER_NUM"] };
	    }

	    CreateDataRelations(ds);

	    return ds;
	}

	private static int FillDataSetTable(DataSet ds, String sql, String tableName, OdbcConnection con) {
	    int rowCount = 0;
	    using (OdbcCommand com = new OdbcCommand(sql, con)) {
		OdbcDataAdapter accountAdapter = new OdbcDataAdapter(com);
		accountAdapter.TableMappings.Add("Table", tableName);
		accountAdapter.Fill(ds);
		rowCount = ds.Tables[tableName].Rows.Count;
	    }
	    return rowCount;
	}

	private static void CreateDataRelations(DataSet ds) {
	    log.Info("Creating Data Relations");
	    CreateDataRelation(ds, ds.Tables["Client"].Columns["MEM_NUM"], ds.Tables["Account"].Columns["MEM_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Applic"].Columns["APPLIC_ID"], ds.Tables["ApplicCalcInfo"].Columns["APPLIC_ID"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["Coborrow"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["CollatValue"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["AccountUS"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["DelinquencyData"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["AccountUdUs"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["TranData"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["TranLoan"].Columns["ACCT_NUM"]);
	    CreateDataRelation(ds, ds.Tables["Account"].Columns["ACCT_NUM"], ds.Tables["ChgOff"].Columns["ACCT_NUM"]);
	    //CreateDataRelation(ds, ds.Tables["Account"].Columns["DEALER"], ds.Tables["DealerData"].Columns["DEALER_NUM"]);
	    //CreateDataRelation(ds, ds.Tables["Account"].Columns["MEM_NUM"], ds.Tables["ProfileEmployerInfo"].Columns["RECORD_KEY"]);
	    //CreateDataRelation(ds, ds.Tables["Account"].Columns["MEM_NUM"], ds.Tables["Profile"].Columns["RECORD_KEY"]);
	}

	private static void CreateDataRelation(DataSet ds, DataColumn parentColumn, DataColumn childColumn) {
	    ds.Relations.Add(new DataRelation(parentColumn.Table.TableName + childColumn.Table.TableName, parentColumn, childColumn));
	}
    }
}