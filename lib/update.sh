cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core*.dll .
cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core*.pdb .
cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.Geocode*.exe .
cp c:/data/work/spring2/Core/build/net-2.0/Maverick.* .
cp c:/data/work/spring2/Core/build/net-2.0/log4net.* .
cp c:/data/work/spring2/Core/build/net-2.0/eSELECTplus_dotNet_API.* .
cp c:/data/work/spring2/Core/build/net-2.0/Interop.PFPro.* .
cp c:/data/work/spring2/Core/build/net-2.0/Payflow_dotNET.dll .
#cp c:/data/work/spring2/Core/build/net-2.0/Newtonsoft.Json.* .

rm -rf Spring2.Core.WebControl*
rm -rf Spring2.Core.Test*
rm -rf Spring2.Core.xml
rm -rf Spring2.Core.Mail.SendMailMessages.*
rm -rf Spring2.Core.*Test.*
rm -rf Spring2.Core.*TestUtility.*

cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.SeleniumLoadTest.* .

cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.Configuration* SendMailMessages/
cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.dll SendMailMessages/
cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.pdb SendMailMessages/
cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.*Mail* SendMailMessages/
cp c:/data/work/spring2/Core/build/net-2.0/Spring2.Core.Types* SendMailMessages/
rm -rf SendMailMessages/Spring2.Core*.xml
rm -rf SendMailMessages/PostBuildEvent.bat

#cp -p c:/data/work/spring2/Core/docs/api/Spring2.Core.chm .
#cp -p c:/data/work/spring2/Core/ChangeLog.txt Spring2.Core-ChangeLog.txt

chmod -R a+rwx *